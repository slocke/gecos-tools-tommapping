package tom.mapping;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import tom.library.sl.Introspector;

public class GenericIntrospector implements Introspector {
	
	public static final GenericIntrospector INSTANCE = new GenericIntrospector();
	
	protected GenericIntrospector() {}
	
	protected Introspector findIntrospector(Object obj) {
		EObject eobj = (EObject)obj;
		Introspector intro =  findIntrospectorRecurse(eobj.eClass());

		if (intro == null) {
			throw new RuntimeException("No introspector found for: " + eobj.eClass().getEPackage());
		}
		return intro;
	}

	protected Introspector findIntrospectorRecurse(EClass eclass) {
		Introspector intro =  IntrospectorManager.INSTANCE.getIntrospector(eclass.getEPackage());

		if (intro == null) {
			for (EClass superType : eclass.getESuperTypes()) {
				intro =  findIntrospectorRecurse(superType);
				if (intro != null) return intro;
			}
		}
		return intro;
	}
	
	@Override
	public Object getChildAt(Object arg0, int arg1) {
		return getChildren(arg0)[arg1];
	}

	@Override
	public int getChildCount(Object arg0) {
		return getChildren(arg0).length;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object[] getChildren(Object arg0) {
		if (arg0 instanceof List) {
			List<Object> l = new ArrayList<Object>();
			// Children of a list are its content
			for(Object object : (List<Object>) arg0) {
				l.add(object);
			}
			return l.toArray();
		}
		return findIntrospector(arg0).getChildren(arg0);
	}

	@Override
	public <T> T setChildAt(T arg0, int arg1, Object arg2) {
		return findIntrospector(arg0).setChildAt(arg0, arg1, arg2);
	}

	@Override
	public <T> T setChildren(T arg0, Object[] arg1) {
		if (arg0 instanceof List) {
			// If object is a list then content of the original list has to be replaced
			@SuppressWarnings("unchecked")
			List<Object> list = (List<Object>) arg0;
			list.clear();
			for (int i = 0; i < arg1.length; i++) {
				list.add(arg1[i]);
			}
			return arg0;
		} else {
			return findIntrospector(arg0).setChildren(arg0, arg1);
		}
	}

}
