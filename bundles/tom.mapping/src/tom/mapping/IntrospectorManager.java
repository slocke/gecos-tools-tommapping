package tom.mapping;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.ecore.EPackage;

import tom.library.sl.Introspector;

public class IntrospectorManager {
	
	public final static IntrospectorManager INSTANCE = new IntrospectorManager();

	protected Map<EPackage, Introspector> introspectorRegistry;
	
	protected IntrospectorManager() {
		introspectorRegistry = new HashMap<EPackage, Introspector>();
	}
	
	public void register(EPackage epack, Introspector introspector) {
		introspectorRegistry.put(epack, introspector);
	}
	
	public Introspector getIntrospector(EPackage epack) {
		if (!introspectorRegistry.containsKey(epack)) {
			while (epack != null) {
				load(epack);
				if (introspectorRegistry.containsKey(epack)) {
					return introspectorRegistry.get(epack);
				}
				epack = epack.getESuperPackage();
			}
		}
		return introspectorRegistry.get(epack);
	}
	
	private void load(EPackage epack) {
		try {
			String introspectorName = "tom.mapping.introspectors."+ePackagePath(epack)+"."+toFirstUpper(epack.getName()+"Introspector");
			Class.forName(introspectorName, true, epack.getClass().getClassLoader());
		} catch (ClassNotFoundException e) {
			
		}
	}
	
	private String ePackagePath(EPackage epackage) {
		if (epackage.getESuperPackage() != null) {
			return ePackagePath(epackage.getESuperPackage())+"."+epackage.getName();
		}
		return epackage.getName();
	}
	
	private String toFirstUpper(String str) {
		return str.substring(0,1).toUpperCase()+str.substring(1);
	}
}
