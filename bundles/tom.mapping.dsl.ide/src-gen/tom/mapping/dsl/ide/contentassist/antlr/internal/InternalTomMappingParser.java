package tom.mapping.dsl.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import tom.mapping.dsl.services.TomMappingGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalTomMappingParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'true'", "'false'", "'TomMapping'", "';'", "'prefix'", "'package'", "'terminals'", "'{'", "'}'", "'define'", "','", "'use'", "'operators'", "'custom'", "'factory'", "'module'", "'import'", "':'", "'alias'", "'::'", "'('", "')'", "'op'", "'make'", "'='", "'is_fsym'", "'slot'", "'ignore'", "'.'", "'fullyQualified'", "'userFactory'", "'[]'"
    };
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__37=37;
    public static final int T__16=16;
    public static final int T__38=38;
    public static final int T__17=17;
    public static final int T__39=39;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__33=33;
    public static final int T__12=12;
    public static final int T__34=34;
    public static final int T__13=13;
    public static final int T__35=35;
    public static final int T__14=14;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__20=20;
    public static final int T__42=42;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalTomMappingParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalTomMappingParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalTomMappingParser.tokenNames; }
    public String getGrammarFileName() { return "InternalTomMapping.g"; }


    	private TomMappingGrammarAccess grammarAccess;

    	public void setGrammarAccess(TomMappingGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleMapping"
    // InternalTomMapping.g:53:1: entryRuleMapping : ruleMapping EOF ;
    public final void entryRuleMapping() throws RecognitionException {
        try {
            // InternalTomMapping.g:54:1: ( ruleMapping EOF )
            // InternalTomMapping.g:55:1: ruleMapping EOF
            {
             before(grammarAccess.getMappingRule()); 
            pushFollow(FOLLOW_1);
            ruleMapping();

            state._fsp--;

             after(grammarAccess.getMappingRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleMapping"


    // $ANTLR start "ruleMapping"
    // InternalTomMapping.g:62:1: ruleMapping : ( ( rule__Mapping__Group__0 ) ) ;
    public final void ruleMapping() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:66:2: ( ( ( rule__Mapping__Group__0 ) ) )
            // InternalTomMapping.g:67:2: ( ( rule__Mapping__Group__0 ) )
            {
            // InternalTomMapping.g:67:2: ( ( rule__Mapping__Group__0 ) )
            // InternalTomMapping.g:68:3: ( rule__Mapping__Group__0 )
            {
             before(grammarAccess.getMappingAccess().getGroup()); 
            // InternalTomMapping.g:69:3: ( rule__Mapping__Group__0 )
            // InternalTomMapping.g:69:4: rule__Mapping__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Mapping__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getMappingAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleMapping"


    // $ANTLR start "entryRuleInstanceMapping"
    // InternalTomMapping.g:78:1: entryRuleInstanceMapping : ruleInstanceMapping EOF ;
    public final void entryRuleInstanceMapping() throws RecognitionException {
        try {
            // InternalTomMapping.g:79:1: ( ruleInstanceMapping EOF )
            // InternalTomMapping.g:80:1: ruleInstanceMapping EOF
            {
             before(grammarAccess.getInstanceMappingRule()); 
            pushFollow(FOLLOW_1);
            ruleInstanceMapping();

            state._fsp--;

             after(grammarAccess.getInstanceMappingRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleInstanceMapping"


    // $ANTLR start "ruleInstanceMapping"
    // InternalTomMapping.g:87:1: ruleInstanceMapping : ( ( rule__InstanceMapping__Group__0 ) ) ;
    public final void ruleInstanceMapping() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:91:2: ( ( ( rule__InstanceMapping__Group__0 ) ) )
            // InternalTomMapping.g:92:2: ( ( rule__InstanceMapping__Group__0 ) )
            {
            // InternalTomMapping.g:92:2: ( ( rule__InstanceMapping__Group__0 ) )
            // InternalTomMapping.g:93:3: ( rule__InstanceMapping__Group__0 )
            {
             before(grammarAccess.getInstanceMappingAccess().getGroup()); 
            // InternalTomMapping.g:94:3: ( rule__InstanceMapping__Group__0 )
            // InternalTomMapping.g:94:4: rule__InstanceMapping__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__InstanceMapping__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getInstanceMappingAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleInstanceMapping"


    // $ANTLR start "entryRuleModule"
    // InternalTomMapping.g:103:1: entryRuleModule : ruleModule EOF ;
    public final void entryRuleModule() throws RecognitionException {
        try {
            // InternalTomMapping.g:104:1: ( ruleModule EOF )
            // InternalTomMapping.g:105:1: ruleModule EOF
            {
             before(grammarAccess.getModuleRule()); 
            pushFollow(FOLLOW_1);
            ruleModule();

            state._fsp--;

             after(grammarAccess.getModuleRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleModule"


    // $ANTLR start "ruleModule"
    // InternalTomMapping.g:112:1: ruleModule : ( ( rule__Module__Group__0 ) ) ;
    public final void ruleModule() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:116:2: ( ( ( rule__Module__Group__0 ) ) )
            // InternalTomMapping.g:117:2: ( ( rule__Module__Group__0 ) )
            {
            // InternalTomMapping.g:117:2: ( ( rule__Module__Group__0 ) )
            // InternalTomMapping.g:118:3: ( rule__Module__Group__0 )
            {
             before(grammarAccess.getModuleAccess().getGroup()); 
            // InternalTomMapping.g:119:3: ( rule__Module__Group__0 )
            // InternalTomMapping.g:119:4: rule__Module__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Module__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getModuleAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleModule"


    // $ANTLR start "entryRuleOperator"
    // InternalTomMapping.g:128:1: entryRuleOperator : ruleOperator EOF ;
    public final void entryRuleOperator() throws RecognitionException {
        try {
            // InternalTomMapping.g:129:1: ( ruleOperator EOF )
            // InternalTomMapping.g:130:1: ruleOperator EOF
            {
             before(grammarAccess.getOperatorRule()); 
            pushFollow(FOLLOW_1);
            ruleOperator();

            state._fsp--;

             after(grammarAccess.getOperatorRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOperator"


    // $ANTLR start "ruleOperator"
    // InternalTomMapping.g:137:1: ruleOperator : ( ( rule__Operator__Alternatives ) ) ;
    public final void ruleOperator() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:141:2: ( ( ( rule__Operator__Alternatives ) ) )
            // InternalTomMapping.g:142:2: ( ( rule__Operator__Alternatives ) )
            {
            // InternalTomMapping.g:142:2: ( ( rule__Operator__Alternatives ) )
            // InternalTomMapping.g:143:3: ( rule__Operator__Alternatives )
            {
             before(grammarAccess.getOperatorAccess().getAlternatives()); 
            // InternalTomMapping.g:144:3: ( rule__Operator__Alternatives )
            // InternalTomMapping.g:144:4: rule__Operator__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Operator__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getOperatorAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOperator"


    // $ANTLR start "entryRuleImport"
    // InternalTomMapping.g:153:1: entryRuleImport : ruleImport EOF ;
    public final void entryRuleImport() throws RecognitionException {
        try {
            // InternalTomMapping.g:154:1: ( ruleImport EOF )
            // InternalTomMapping.g:155:1: ruleImport EOF
            {
             before(grammarAccess.getImportRule()); 
            pushFollow(FOLLOW_1);
            ruleImport();

            state._fsp--;

             after(grammarAccess.getImportRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleImport"


    // $ANTLR start "ruleImport"
    // InternalTomMapping.g:162:1: ruleImport : ( ( rule__Import__Group__0 ) ) ;
    public final void ruleImport() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:166:2: ( ( ( rule__Import__Group__0 ) ) )
            // InternalTomMapping.g:167:2: ( ( rule__Import__Group__0 ) )
            {
            // InternalTomMapping.g:167:2: ( ( rule__Import__Group__0 ) )
            // InternalTomMapping.g:168:3: ( rule__Import__Group__0 )
            {
             before(grammarAccess.getImportAccess().getGroup()); 
            // InternalTomMapping.g:169:3: ( rule__Import__Group__0 )
            // InternalTomMapping.g:169:4: rule__Import__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Import__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getImportAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleImport"


    // $ANTLR start "entryRuleTerminal"
    // InternalTomMapping.g:178:1: entryRuleTerminal : ruleTerminal EOF ;
    public final void entryRuleTerminal() throws RecognitionException {
        try {
            // InternalTomMapping.g:179:1: ( ruleTerminal EOF )
            // InternalTomMapping.g:180:1: ruleTerminal EOF
            {
             before(grammarAccess.getTerminalRule()); 
            pushFollow(FOLLOW_1);
            ruleTerminal();

            state._fsp--;

             after(grammarAccess.getTerminalRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTerminal"


    // $ANTLR start "ruleTerminal"
    // InternalTomMapping.g:187:1: ruleTerminal : ( ( rule__Terminal__Group__0 ) ) ;
    public final void ruleTerminal() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:191:2: ( ( ( rule__Terminal__Group__0 ) ) )
            // InternalTomMapping.g:192:2: ( ( rule__Terminal__Group__0 ) )
            {
            // InternalTomMapping.g:192:2: ( ( rule__Terminal__Group__0 ) )
            // InternalTomMapping.g:193:3: ( rule__Terminal__Group__0 )
            {
             before(grammarAccess.getTerminalAccess().getGroup()); 
            // InternalTomMapping.g:194:3: ( rule__Terminal__Group__0 )
            // InternalTomMapping.g:194:4: rule__Terminal__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Terminal__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getTerminalAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTerminal"


    // $ANTLR start "entryRuleAliasOperator"
    // InternalTomMapping.g:203:1: entryRuleAliasOperator : ruleAliasOperator EOF ;
    public final void entryRuleAliasOperator() throws RecognitionException {
        try {
            // InternalTomMapping.g:204:1: ( ruleAliasOperator EOF )
            // InternalTomMapping.g:205:1: ruleAliasOperator EOF
            {
             before(grammarAccess.getAliasOperatorRule()); 
            pushFollow(FOLLOW_1);
            ruleAliasOperator();

            state._fsp--;

             after(grammarAccess.getAliasOperatorRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAliasOperator"


    // $ANTLR start "ruleAliasOperator"
    // InternalTomMapping.g:212:1: ruleAliasOperator : ( ( rule__AliasOperator__Group__0 ) ) ;
    public final void ruleAliasOperator() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:216:2: ( ( ( rule__AliasOperator__Group__0 ) ) )
            // InternalTomMapping.g:217:2: ( ( rule__AliasOperator__Group__0 ) )
            {
            // InternalTomMapping.g:217:2: ( ( rule__AliasOperator__Group__0 ) )
            // InternalTomMapping.g:218:3: ( rule__AliasOperator__Group__0 )
            {
             before(grammarAccess.getAliasOperatorAccess().getGroup()); 
            // InternalTomMapping.g:219:3: ( rule__AliasOperator__Group__0 )
            // InternalTomMapping.g:219:4: rule__AliasOperator__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__AliasOperator__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAliasOperatorAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAliasOperator"


    // $ANTLR start "entryRuleAliasNode"
    // InternalTomMapping.g:228:1: entryRuleAliasNode : ruleAliasNode EOF ;
    public final void entryRuleAliasNode() throws RecognitionException {
        try {
            // InternalTomMapping.g:229:1: ( ruleAliasNode EOF )
            // InternalTomMapping.g:230:1: ruleAliasNode EOF
            {
             before(grammarAccess.getAliasNodeRule()); 
            pushFollow(FOLLOW_1);
            ruleAliasNode();

            state._fsp--;

             after(grammarAccess.getAliasNodeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAliasNode"


    // $ANTLR start "ruleAliasNode"
    // InternalTomMapping.g:237:1: ruleAliasNode : ( ( rule__AliasNode__Alternatives ) ) ;
    public final void ruleAliasNode() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:241:2: ( ( ( rule__AliasNode__Alternatives ) ) )
            // InternalTomMapping.g:242:2: ( ( rule__AliasNode__Alternatives ) )
            {
            // InternalTomMapping.g:242:2: ( ( rule__AliasNode__Alternatives ) )
            // InternalTomMapping.g:243:3: ( rule__AliasNode__Alternatives )
            {
             before(grammarAccess.getAliasNodeAccess().getAlternatives()); 
            // InternalTomMapping.g:244:3: ( rule__AliasNode__Alternatives )
            // InternalTomMapping.g:244:4: rule__AliasNode__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__AliasNode__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getAliasNodeAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAliasNode"


    // $ANTLR start "entryRuleFeatureNode"
    // InternalTomMapping.g:253:1: entryRuleFeatureNode : ruleFeatureNode EOF ;
    public final void entryRuleFeatureNode() throws RecognitionException {
        try {
            // InternalTomMapping.g:254:1: ( ruleFeatureNode EOF )
            // InternalTomMapping.g:255:1: ruleFeatureNode EOF
            {
             before(grammarAccess.getFeatureNodeRule()); 
            pushFollow(FOLLOW_1);
            ruleFeatureNode();

            state._fsp--;

             after(grammarAccess.getFeatureNodeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFeatureNode"


    // $ANTLR start "ruleFeatureNode"
    // InternalTomMapping.g:262:1: ruleFeatureNode : ( ( rule__FeatureNode__FeatureAssignment ) ) ;
    public final void ruleFeatureNode() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:266:2: ( ( ( rule__FeatureNode__FeatureAssignment ) ) )
            // InternalTomMapping.g:267:2: ( ( rule__FeatureNode__FeatureAssignment ) )
            {
            // InternalTomMapping.g:267:2: ( ( rule__FeatureNode__FeatureAssignment ) )
            // InternalTomMapping.g:268:3: ( rule__FeatureNode__FeatureAssignment )
            {
             before(grammarAccess.getFeatureNodeAccess().getFeatureAssignment()); 
            // InternalTomMapping.g:269:3: ( rule__FeatureNode__FeatureAssignment )
            // InternalTomMapping.g:269:4: rule__FeatureNode__FeatureAssignment
            {
            pushFollow(FOLLOW_2);
            rule__FeatureNode__FeatureAssignment();

            state._fsp--;


            }

             after(grammarAccess.getFeatureNodeAccess().getFeatureAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFeatureNode"


    // $ANTLR start "entryRuleOperatorNode"
    // InternalTomMapping.g:278:1: entryRuleOperatorNode : ruleOperatorNode EOF ;
    public final void entryRuleOperatorNode() throws RecognitionException {
        try {
            // InternalTomMapping.g:279:1: ( ruleOperatorNode EOF )
            // InternalTomMapping.g:280:1: ruleOperatorNode EOF
            {
             before(grammarAccess.getOperatorNodeRule()); 
            pushFollow(FOLLOW_1);
            ruleOperatorNode();

            state._fsp--;

             after(grammarAccess.getOperatorNodeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOperatorNode"


    // $ANTLR start "ruleOperatorNode"
    // InternalTomMapping.g:287:1: ruleOperatorNode : ( ( rule__OperatorNode__Group__0 ) ) ;
    public final void ruleOperatorNode() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:291:2: ( ( ( rule__OperatorNode__Group__0 ) ) )
            // InternalTomMapping.g:292:2: ( ( rule__OperatorNode__Group__0 ) )
            {
            // InternalTomMapping.g:292:2: ( ( rule__OperatorNode__Group__0 ) )
            // InternalTomMapping.g:293:3: ( rule__OperatorNode__Group__0 )
            {
             before(grammarAccess.getOperatorNodeAccess().getGroup()); 
            // InternalTomMapping.g:294:3: ( rule__OperatorNode__Group__0 )
            // InternalTomMapping.g:294:4: rule__OperatorNode__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__OperatorNode__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getOperatorNodeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOperatorNode"


    // $ANTLR start "entryRuleClassOperator"
    // InternalTomMapping.g:303:1: entryRuleClassOperator : ruleClassOperator EOF ;
    public final void entryRuleClassOperator() throws RecognitionException {
        try {
            // InternalTomMapping.g:304:1: ( ruleClassOperator EOF )
            // InternalTomMapping.g:305:1: ruleClassOperator EOF
            {
             before(grammarAccess.getClassOperatorRule()); 
            pushFollow(FOLLOW_1);
            ruleClassOperator();

            state._fsp--;

             after(grammarAccess.getClassOperatorRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleClassOperator"


    // $ANTLR start "ruleClassOperator"
    // InternalTomMapping.g:312:1: ruleClassOperator : ( ( rule__ClassOperator__Group__0 ) ) ;
    public final void ruleClassOperator() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:316:2: ( ( ( rule__ClassOperator__Group__0 ) ) )
            // InternalTomMapping.g:317:2: ( ( rule__ClassOperator__Group__0 ) )
            {
            // InternalTomMapping.g:317:2: ( ( rule__ClassOperator__Group__0 ) )
            // InternalTomMapping.g:318:3: ( rule__ClassOperator__Group__0 )
            {
             before(grammarAccess.getClassOperatorAccess().getGroup()); 
            // InternalTomMapping.g:319:3: ( rule__ClassOperator__Group__0 )
            // InternalTomMapping.g:319:4: rule__ClassOperator__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ClassOperator__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getClassOperatorAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleClassOperator"


    // $ANTLR start "entryRuleClassOperatorWithExceptions"
    // InternalTomMapping.g:328:1: entryRuleClassOperatorWithExceptions : ruleClassOperatorWithExceptions EOF ;
    public final void entryRuleClassOperatorWithExceptions() throws RecognitionException {
        try {
            // InternalTomMapping.g:329:1: ( ruleClassOperatorWithExceptions EOF )
            // InternalTomMapping.g:330:1: ruleClassOperatorWithExceptions EOF
            {
             before(grammarAccess.getClassOperatorWithExceptionsRule()); 
            pushFollow(FOLLOW_1);
            ruleClassOperatorWithExceptions();

            state._fsp--;

             after(grammarAccess.getClassOperatorWithExceptionsRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleClassOperatorWithExceptions"


    // $ANTLR start "ruleClassOperatorWithExceptions"
    // InternalTomMapping.g:337:1: ruleClassOperatorWithExceptions : ( ( rule__ClassOperatorWithExceptions__Group__0 ) ) ;
    public final void ruleClassOperatorWithExceptions() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:341:2: ( ( ( rule__ClassOperatorWithExceptions__Group__0 ) ) )
            // InternalTomMapping.g:342:2: ( ( rule__ClassOperatorWithExceptions__Group__0 ) )
            {
            // InternalTomMapping.g:342:2: ( ( rule__ClassOperatorWithExceptions__Group__0 ) )
            // InternalTomMapping.g:343:3: ( rule__ClassOperatorWithExceptions__Group__0 )
            {
             before(grammarAccess.getClassOperatorWithExceptionsAccess().getGroup()); 
            // InternalTomMapping.g:344:3: ( rule__ClassOperatorWithExceptions__Group__0 )
            // InternalTomMapping.g:344:4: rule__ClassOperatorWithExceptions__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ClassOperatorWithExceptions__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getClassOperatorWithExceptionsAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleClassOperatorWithExceptions"


    // $ANTLR start "entryRuleUserOperator"
    // InternalTomMapping.g:353:1: entryRuleUserOperator : ruleUserOperator EOF ;
    public final void entryRuleUserOperator() throws RecognitionException {
        try {
            // InternalTomMapping.g:354:1: ( ruleUserOperator EOF )
            // InternalTomMapping.g:355:1: ruleUserOperator EOF
            {
             before(grammarAccess.getUserOperatorRule()); 
            pushFollow(FOLLOW_1);
            ruleUserOperator();

            state._fsp--;

             after(grammarAccess.getUserOperatorRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleUserOperator"


    // $ANTLR start "ruleUserOperator"
    // InternalTomMapping.g:362:1: ruleUserOperator : ( ( rule__UserOperator__Group__0 ) ) ;
    public final void ruleUserOperator() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:366:2: ( ( ( rule__UserOperator__Group__0 ) ) )
            // InternalTomMapping.g:367:2: ( ( rule__UserOperator__Group__0 ) )
            {
            // InternalTomMapping.g:367:2: ( ( rule__UserOperator__Group__0 ) )
            // InternalTomMapping.g:368:3: ( rule__UserOperator__Group__0 )
            {
             before(grammarAccess.getUserOperatorAccess().getGroup()); 
            // InternalTomMapping.g:369:3: ( rule__UserOperator__Group__0 )
            // InternalTomMapping.g:369:4: rule__UserOperator__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__UserOperator__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getUserOperatorAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleUserOperator"


    // $ANTLR start "entryRuleParameter"
    // InternalTomMapping.g:378:1: entryRuleParameter : ruleParameter EOF ;
    public final void entryRuleParameter() throws RecognitionException {
        try {
            // InternalTomMapping.g:379:1: ( ruleParameter EOF )
            // InternalTomMapping.g:380:1: ruleParameter EOF
            {
             before(grammarAccess.getParameterRule()); 
            pushFollow(FOLLOW_1);
            ruleParameter();

            state._fsp--;

             after(grammarAccess.getParameterRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleParameter"


    // $ANTLR start "ruleParameter"
    // InternalTomMapping.g:387:1: ruleParameter : ( ( rule__Parameter__Group__0 ) ) ;
    public final void ruleParameter() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:391:2: ( ( ( rule__Parameter__Group__0 ) ) )
            // InternalTomMapping.g:392:2: ( ( rule__Parameter__Group__0 ) )
            {
            // InternalTomMapping.g:392:2: ( ( rule__Parameter__Group__0 ) )
            // InternalTomMapping.g:393:3: ( rule__Parameter__Group__0 )
            {
             before(grammarAccess.getParameterAccess().getGroup()); 
            // InternalTomMapping.g:394:3: ( rule__Parameter__Group__0 )
            // InternalTomMapping.g:394:4: rule__Parameter__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Parameter__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getParameterAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleParameter"


    // $ANTLR start "entryRuleAccessor"
    // InternalTomMapping.g:403:1: entryRuleAccessor : ruleAccessor EOF ;
    public final void entryRuleAccessor() throws RecognitionException {
        try {
            // InternalTomMapping.g:404:1: ( ruleAccessor EOF )
            // InternalTomMapping.g:405:1: ruleAccessor EOF
            {
             before(grammarAccess.getAccessorRule()); 
            pushFollow(FOLLOW_1);
            ruleAccessor();

            state._fsp--;

             after(grammarAccess.getAccessorRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAccessor"


    // $ANTLR start "ruleAccessor"
    // InternalTomMapping.g:412:1: ruleAccessor : ( ( rule__Accessor__Group__0 ) ) ;
    public final void ruleAccessor() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:416:2: ( ( ( rule__Accessor__Group__0 ) ) )
            // InternalTomMapping.g:417:2: ( ( rule__Accessor__Group__0 ) )
            {
            // InternalTomMapping.g:417:2: ( ( rule__Accessor__Group__0 ) )
            // InternalTomMapping.g:418:3: ( rule__Accessor__Group__0 )
            {
             before(grammarAccess.getAccessorAccess().getGroup()); 
            // InternalTomMapping.g:419:3: ( rule__Accessor__Group__0 )
            // InternalTomMapping.g:419:4: rule__Accessor__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Accessor__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAccessorAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAccessor"


    // $ANTLR start "entryRuleFeatureException"
    // InternalTomMapping.g:428:1: entryRuleFeatureException : ruleFeatureException EOF ;
    public final void entryRuleFeatureException() throws RecognitionException {
        try {
            // InternalTomMapping.g:429:1: ( ruleFeatureException EOF )
            // InternalTomMapping.g:430:1: ruleFeatureException EOF
            {
             before(grammarAccess.getFeatureExceptionRule()); 
            pushFollow(FOLLOW_1);
            ruleFeatureException();

            state._fsp--;

             after(grammarAccess.getFeatureExceptionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFeatureException"


    // $ANTLR start "ruleFeatureException"
    // InternalTomMapping.g:437:1: ruleFeatureException : ( ( rule__FeatureException__Group__0 ) ) ;
    public final void ruleFeatureException() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:441:2: ( ( ( rule__FeatureException__Group__0 ) ) )
            // InternalTomMapping.g:442:2: ( ( rule__FeatureException__Group__0 ) )
            {
            // InternalTomMapping.g:442:2: ( ( rule__FeatureException__Group__0 ) )
            // InternalTomMapping.g:443:3: ( rule__FeatureException__Group__0 )
            {
             before(grammarAccess.getFeatureExceptionAccess().getGroup()); 
            // InternalTomMapping.g:444:3: ( rule__FeatureException__Group__0 )
            // InternalTomMapping.g:444:4: rule__FeatureException__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__FeatureException__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getFeatureExceptionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFeatureException"


    // $ANTLR start "entryRuleFeatureParameter"
    // InternalTomMapping.g:453:1: entryRuleFeatureParameter : ruleFeatureParameter EOF ;
    public final void entryRuleFeatureParameter() throws RecognitionException {
        try {
            // InternalTomMapping.g:454:1: ( ruleFeatureParameter EOF )
            // InternalTomMapping.g:455:1: ruleFeatureParameter EOF
            {
             before(grammarAccess.getFeatureParameterRule()); 
            pushFollow(FOLLOW_1);
            ruleFeatureParameter();

            state._fsp--;

             after(grammarAccess.getFeatureParameterRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFeatureParameter"


    // $ANTLR start "ruleFeatureParameter"
    // InternalTomMapping.g:462:1: ruleFeatureParameter : ( ( rule__FeatureParameter__Alternatives ) ) ;
    public final void ruleFeatureParameter() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:466:2: ( ( ( rule__FeatureParameter__Alternatives ) ) )
            // InternalTomMapping.g:467:2: ( ( rule__FeatureParameter__Alternatives ) )
            {
            // InternalTomMapping.g:467:2: ( ( rule__FeatureParameter__Alternatives ) )
            // InternalTomMapping.g:468:3: ( rule__FeatureParameter__Alternatives )
            {
             before(grammarAccess.getFeatureParameterAccess().getAlternatives()); 
            // InternalTomMapping.g:469:3: ( rule__FeatureParameter__Alternatives )
            // InternalTomMapping.g:469:4: rule__FeatureParameter__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__FeatureParameter__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getFeatureParameterAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFeatureParameter"


    // $ANTLR start "entryRuleSettedFeatureParameter"
    // InternalTomMapping.g:478:1: entryRuleSettedFeatureParameter : ruleSettedFeatureParameter EOF ;
    public final void entryRuleSettedFeatureParameter() throws RecognitionException {
        try {
            // InternalTomMapping.g:479:1: ( ruleSettedFeatureParameter EOF )
            // InternalTomMapping.g:480:1: ruleSettedFeatureParameter EOF
            {
             before(grammarAccess.getSettedFeatureParameterRule()); 
            pushFollow(FOLLOW_1);
            ruleSettedFeatureParameter();

            state._fsp--;

             after(grammarAccess.getSettedFeatureParameterRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSettedFeatureParameter"


    // $ANTLR start "ruleSettedFeatureParameter"
    // InternalTomMapping.g:487:1: ruleSettedFeatureParameter : ( ( rule__SettedFeatureParameter__Group__0 ) ) ;
    public final void ruleSettedFeatureParameter() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:491:2: ( ( ( rule__SettedFeatureParameter__Group__0 ) ) )
            // InternalTomMapping.g:492:2: ( ( rule__SettedFeatureParameter__Group__0 ) )
            {
            // InternalTomMapping.g:492:2: ( ( rule__SettedFeatureParameter__Group__0 ) )
            // InternalTomMapping.g:493:3: ( rule__SettedFeatureParameter__Group__0 )
            {
             before(grammarAccess.getSettedFeatureParameterAccess().getGroup()); 
            // InternalTomMapping.g:494:3: ( rule__SettedFeatureParameter__Group__0 )
            // InternalTomMapping.g:494:4: rule__SettedFeatureParameter__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__SettedFeatureParameter__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getSettedFeatureParameterAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSettedFeatureParameter"


    // $ANTLR start "entryRuleSettedValue"
    // InternalTomMapping.g:503:1: entryRuleSettedValue : ruleSettedValue EOF ;
    public final void entryRuleSettedValue() throws RecognitionException {
        try {
            // InternalTomMapping.g:504:1: ( ruleSettedValue EOF )
            // InternalTomMapping.g:505:1: ruleSettedValue EOF
            {
             before(grammarAccess.getSettedValueRule()); 
            pushFollow(FOLLOW_1);
            ruleSettedValue();

            state._fsp--;

             after(grammarAccess.getSettedValueRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSettedValue"


    // $ANTLR start "ruleSettedValue"
    // InternalTomMapping.g:512:1: ruleSettedValue : ( ( rule__SettedValue__Alternatives ) ) ;
    public final void ruleSettedValue() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:516:2: ( ( ( rule__SettedValue__Alternatives ) ) )
            // InternalTomMapping.g:517:2: ( ( rule__SettedValue__Alternatives ) )
            {
            // InternalTomMapping.g:517:2: ( ( rule__SettedValue__Alternatives ) )
            // InternalTomMapping.g:518:3: ( rule__SettedValue__Alternatives )
            {
             before(grammarAccess.getSettedValueAccess().getAlternatives()); 
            // InternalTomMapping.g:519:3: ( rule__SettedValue__Alternatives )
            // InternalTomMapping.g:519:4: rule__SettedValue__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__SettedValue__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getSettedValueAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSettedValue"


    // $ANTLR start "entryRuleIntSettedValue"
    // InternalTomMapping.g:528:1: entryRuleIntSettedValue : ruleIntSettedValue EOF ;
    public final void entryRuleIntSettedValue() throws RecognitionException {
        try {
            // InternalTomMapping.g:529:1: ( ruleIntSettedValue EOF )
            // InternalTomMapping.g:530:1: ruleIntSettedValue EOF
            {
             before(grammarAccess.getIntSettedValueRule()); 
            pushFollow(FOLLOW_1);
            ruleIntSettedValue();

            state._fsp--;

             after(grammarAccess.getIntSettedValueRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleIntSettedValue"


    // $ANTLR start "ruleIntSettedValue"
    // InternalTomMapping.g:537:1: ruleIntSettedValue : ( ( rule__IntSettedValue__JavaAssignment ) ) ;
    public final void ruleIntSettedValue() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:541:2: ( ( ( rule__IntSettedValue__JavaAssignment ) ) )
            // InternalTomMapping.g:542:2: ( ( rule__IntSettedValue__JavaAssignment ) )
            {
            // InternalTomMapping.g:542:2: ( ( rule__IntSettedValue__JavaAssignment ) )
            // InternalTomMapping.g:543:3: ( rule__IntSettedValue__JavaAssignment )
            {
             before(grammarAccess.getIntSettedValueAccess().getJavaAssignment()); 
            // InternalTomMapping.g:544:3: ( rule__IntSettedValue__JavaAssignment )
            // InternalTomMapping.g:544:4: rule__IntSettedValue__JavaAssignment
            {
            pushFollow(FOLLOW_2);
            rule__IntSettedValue__JavaAssignment();

            state._fsp--;


            }

             after(grammarAccess.getIntSettedValueAccess().getJavaAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleIntSettedValue"


    // $ANTLR start "entryRuleBooleanSettedValue"
    // InternalTomMapping.g:553:1: entryRuleBooleanSettedValue : ruleBooleanSettedValue EOF ;
    public final void entryRuleBooleanSettedValue() throws RecognitionException {
        try {
            // InternalTomMapping.g:554:1: ( ruleBooleanSettedValue EOF )
            // InternalTomMapping.g:555:1: ruleBooleanSettedValue EOF
            {
             before(grammarAccess.getBooleanSettedValueRule()); 
            pushFollow(FOLLOW_1);
            ruleBooleanSettedValue();

            state._fsp--;

             after(grammarAccess.getBooleanSettedValueRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBooleanSettedValue"


    // $ANTLR start "ruleBooleanSettedValue"
    // InternalTomMapping.g:562:1: ruleBooleanSettedValue : ( ( rule__BooleanSettedValue__JavaAssignment ) ) ;
    public final void ruleBooleanSettedValue() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:566:2: ( ( ( rule__BooleanSettedValue__JavaAssignment ) ) )
            // InternalTomMapping.g:567:2: ( ( rule__BooleanSettedValue__JavaAssignment ) )
            {
            // InternalTomMapping.g:567:2: ( ( rule__BooleanSettedValue__JavaAssignment ) )
            // InternalTomMapping.g:568:3: ( rule__BooleanSettedValue__JavaAssignment )
            {
             before(grammarAccess.getBooleanSettedValueAccess().getJavaAssignment()); 
            // InternalTomMapping.g:569:3: ( rule__BooleanSettedValue__JavaAssignment )
            // InternalTomMapping.g:569:4: rule__BooleanSettedValue__JavaAssignment
            {
            pushFollow(FOLLOW_2);
            rule__BooleanSettedValue__JavaAssignment();

            state._fsp--;


            }

             after(grammarAccess.getBooleanSettedValueAccess().getJavaAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBooleanSettedValue"


    // $ANTLR start "entryRuleJavaCodeValue"
    // InternalTomMapping.g:578:1: entryRuleJavaCodeValue : ruleJavaCodeValue EOF ;
    public final void entryRuleJavaCodeValue() throws RecognitionException {
        try {
            // InternalTomMapping.g:579:1: ( ruleJavaCodeValue EOF )
            // InternalTomMapping.g:580:1: ruleJavaCodeValue EOF
            {
             before(grammarAccess.getJavaCodeValueRule()); 
            pushFollow(FOLLOW_1);
            ruleJavaCodeValue();

            state._fsp--;

             after(grammarAccess.getJavaCodeValueRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleJavaCodeValue"


    // $ANTLR start "ruleJavaCodeValue"
    // InternalTomMapping.g:587:1: ruleJavaCodeValue : ( ( rule__JavaCodeValue__JavaAssignment ) ) ;
    public final void ruleJavaCodeValue() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:591:2: ( ( ( rule__JavaCodeValue__JavaAssignment ) ) )
            // InternalTomMapping.g:592:2: ( ( rule__JavaCodeValue__JavaAssignment ) )
            {
            // InternalTomMapping.g:592:2: ( ( rule__JavaCodeValue__JavaAssignment ) )
            // InternalTomMapping.g:593:3: ( rule__JavaCodeValue__JavaAssignment )
            {
             before(grammarAccess.getJavaCodeValueAccess().getJavaAssignment()); 
            // InternalTomMapping.g:594:3: ( rule__JavaCodeValue__JavaAssignment )
            // InternalTomMapping.g:594:4: rule__JavaCodeValue__JavaAssignment
            {
            pushFollow(FOLLOW_2);
            rule__JavaCodeValue__JavaAssignment();

            state._fsp--;


            }

             after(grammarAccess.getJavaCodeValueAccess().getJavaAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleJavaCodeValue"


    // $ANTLR start "entryRuleLiteralValue"
    // InternalTomMapping.g:603:1: entryRuleLiteralValue : ruleLiteralValue EOF ;
    public final void entryRuleLiteralValue() throws RecognitionException {
        try {
            // InternalTomMapping.g:604:1: ( ruleLiteralValue EOF )
            // InternalTomMapping.g:605:1: ruleLiteralValue EOF
            {
             before(grammarAccess.getLiteralValueRule()); 
            pushFollow(FOLLOW_1);
            ruleLiteralValue();

            state._fsp--;

             after(grammarAccess.getLiteralValueRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLiteralValue"


    // $ANTLR start "ruleLiteralValue"
    // InternalTomMapping.g:612:1: ruleLiteralValue : ( ( rule__LiteralValue__LiteralAssignment ) ) ;
    public final void ruleLiteralValue() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:616:2: ( ( ( rule__LiteralValue__LiteralAssignment ) ) )
            // InternalTomMapping.g:617:2: ( ( rule__LiteralValue__LiteralAssignment ) )
            {
            // InternalTomMapping.g:617:2: ( ( rule__LiteralValue__LiteralAssignment ) )
            // InternalTomMapping.g:618:3: ( rule__LiteralValue__LiteralAssignment )
            {
             before(grammarAccess.getLiteralValueAccess().getLiteralAssignment()); 
            // InternalTomMapping.g:619:3: ( rule__LiteralValue__LiteralAssignment )
            // InternalTomMapping.g:619:4: rule__LiteralValue__LiteralAssignment
            {
            pushFollow(FOLLOW_2);
            rule__LiteralValue__LiteralAssignment();

            state._fsp--;


            }

             after(grammarAccess.getLiteralValueAccess().getLiteralAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLiteralValue"


    // $ANTLR start "entryRuleEBoolean"
    // InternalTomMapping.g:628:1: entryRuleEBoolean : ruleEBoolean EOF ;
    public final void entryRuleEBoolean() throws RecognitionException {
        try {
            // InternalTomMapping.g:629:1: ( ruleEBoolean EOF )
            // InternalTomMapping.g:630:1: ruleEBoolean EOF
            {
             before(grammarAccess.getEBooleanRule()); 
            pushFollow(FOLLOW_1);
            ruleEBoolean();

            state._fsp--;

             after(grammarAccess.getEBooleanRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEBoolean"


    // $ANTLR start "ruleEBoolean"
    // InternalTomMapping.g:637:1: ruleEBoolean : ( ( rule__EBoolean__Alternatives ) ) ;
    public final void ruleEBoolean() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:641:2: ( ( ( rule__EBoolean__Alternatives ) ) )
            // InternalTomMapping.g:642:2: ( ( rule__EBoolean__Alternatives ) )
            {
            // InternalTomMapping.g:642:2: ( ( rule__EBoolean__Alternatives ) )
            // InternalTomMapping.g:643:3: ( rule__EBoolean__Alternatives )
            {
             before(grammarAccess.getEBooleanAccess().getAlternatives()); 
            // InternalTomMapping.g:644:3: ( rule__EBoolean__Alternatives )
            // InternalTomMapping.g:644:4: rule__EBoolean__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__EBoolean__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getEBooleanAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEBoolean"


    // $ANTLR start "entryRuleSN"
    // InternalTomMapping.g:653:1: entryRuleSN : ruleSN EOF ;
    public final void entryRuleSN() throws RecognitionException {
        try {
            // InternalTomMapping.g:654:1: ( ruleSN EOF )
            // InternalTomMapping.g:655:1: ruleSN EOF
            {
             before(grammarAccess.getSNRule()); 
            pushFollow(FOLLOW_1);
            ruleSN();

            state._fsp--;

             after(grammarAccess.getSNRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSN"


    // $ANTLR start "ruleSN"
    // InternalTomMapping.g:662:1: ruleSN : ( RULE_ID ) ;
    public final void ruleSN() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:666:2: ( ( RULE_ID ) )
            // InternalTomMapping.g:667:2: ( RULE_ID )
            {
            // InternalTomMapping.g:667:2: ( RULE_ID )
            // InternalTomMapping.g:668:3: RULE_ID
            {
             before(grammarAccess.getSNAccess().getIDTerminalRuleCall()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getSNAccess().getIDTerminalRuleCall()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSN"


    // $ANTLR start "entryRuleFQN"
    // InternalTomMapping.g:678:1: entryRuleFQN : ruleFQN EOF ;
    public final void entryRuleFQN() throws RecognitionException {
        try {
            // InternalTomMapping.g:679:1: ( ruleFQN EOF )
            // InternalTomMapping.g:680:1: ruleFQN EOF
            {
             before(grammarAccess.getFQNRule()); 
            pushFollow(FOLLOW_1);
            ruleFQN();

            state._fsp--;

             after(grammarAccess.getFQNRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFQN"


    // $ANTLR start "ruleFQN"
    // InternalTomMapping.g:687:1: ruleFQN : ( ( rule__FQN__Group__0 ) ) ;
    public final void ruleFQN() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:691:2: ( ( ( rule__FQN__Group__0 ) ) )
            // InternalTomMapping.g:692:2: ( ( rule__FQN__Group__0 ) )
            {
            // InternalTomMapping.g:692:2: ( ( rule__FQN__Group__0 ) )
            // InternalTomMapping.g:693:3: ( rule__FQN__Group__0 )
            {
             before(grammarAccess.getFQNAccess().getGroup()); 
            // InternalTomMapping.g:694:3: ( rule__FQN__Group__0 )
            // InternalTomMapping.g:694:4: rule__FQN__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__FQN__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getFQNAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFQN"


    // $ANTLR start "rule__Mapping__Alternatives_9_2"
    // InternalTomMapping.g:702:1: rule__Mapping__Alternatives_9_2 : ( ( ( rule__Mapping__Group_9_2_0__0 ) ) | ( ( rule__Mapping__Group_9_2_1__0 ) ) );
    public final void rule__Mapping__Alternatives_9_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:706:1: ( ( ( rule__Mapping__Group_9_2_0__0 ) ) | ( ( rule__Mapping__Group_9_2_1__0 ) ) )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( ((LA1_0>=19 && LA1_0<=20)||LA1_0==22) ) {
                alt1=1;
            }
            else if ( (LA1_0==RULE_ID) ) {
                alt1=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // InternalTomMapping.g:707:2: ( ( rule__Mapping__Group_9_2_0__0 ) )
                    {
                    // InternalTomMapping.g:707:2: ( ( rule__Mapping__Group_9_2_0__0 ) )
                    // InternalTomMapping.g:708:3: ( rule__Mapping__Group_9_2_0__0 )
                    {
                     before(grammarAccess.getMappingAccess().getGroup_9_2_0()); 
                    // InternalTomMapping.g:709:3: ( rule__Mapping__Group_9_2_0__0 )
                    // InternalTomMapping.g:709:4: rule__Mapping__Group_9_2_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Mapping__Group_9_2_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getMappingAccess().getGroup_9_2_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalTomMapping.g:713:2: ( ( rule__Mapping__Group_9_2_1__0 ) )
                    {
                    // InternalTomMapping.g:713:2: ( ( rule__Mapping__Group_9_2_1__0 ) )
                    // InternalTomMapping.g:714:3: ( rule__Mapping__Group_9_2_1__0 )
                    {
                     before(grammarAccess.getMappingAccess().getGroup_9_2_1()); 
                    // InternalTomMapping.g:715:3: ( rule__Mapping__Group_9_2_1__0 )
                    // InternalTomMapping.g:715:4: rule__Mapping__Group_9_2_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Mapping__Group_9_2_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getMappingAccess().getGroup_9_2_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Alternatives_9_2"


    // $ANTLR start "rule__Mapping__Alternatives_10"
    // InternalTomMapping.g:723:1: rule__Mapping__Alternatives_10 : ( ( ( rule__Mapping__Group_10_0__0 ) ) | ( ( rule__Mapping__ModulesAssignment_10_1 ) ) );
    public final void rule__Mapping__Alternatives_10() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:727:1: ( ( ( rule__Mapping__Group_10_0__0 ) ) | ( ( rule__Mapping__ModulesAssignment_10_1 ) ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==23) ) {
                alt2=1;
            }
            else if ( (LA2_0==26) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // InternalTomMapping.g:728:2: ( ( rule__Mapping__Group_10_0__0 ) )
                    {
                    // InternalTomMapping.g:728:2: ( ( rule__Mapping__Group_10_0__0 ) )
                    // InternalTomMapping.g:729:3: ( rule__Mapping__Group_10_0__0 )
                    {
                     before(grammarAccess.getMappingAccess().getGroup_10_0()); 
                    // InternalTomMapping.g:730:3: ( rule__Mapping__Group_10_0__0 )
                    // InternalTomMapping.g:730:4: rule__Mapping__Group_10_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Mapping__Group_10_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getMappingAccess().getGroup_10_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalTomMapping.g:734:2: ( ( rule__Mapping__ModulesAssignment_10_1 ) )
                    {
                    // InternalTomMapping.g:734:2: ( ( rule__Mapping__ModulesAssignment_10_1 ) )
                    // InternalTomMapping.g:735:3: ( rule__Mapping__ModulesAssignment_10_1 )
                    {
                     before(grammarAccess.getMappingAccess().getModulesAssignment_10_1()); 
                    // InternalTomMapping.g:736:3: ( rule__Mapping__ModulesAssignment_10_1 )
                    // InternalTomMapping.g:736:4: rule__Mapping__ModulesAssignment_10_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Mapping__ModulesAssignment_10_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getMappingAccess().getModulesAssignment_10_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Alternatives_10"


    // $ANTLR start "rule__Operator__Alternatives"
    // InternalTomMapping.g:744:1: rule__Operator__Alternatives : ( ( ruleClassOperator ) | ( ruleClassOperatorWithExceptions ) | ( ruleUserOperator ) | ( ruleAliasOperator ) );
    public final void rule__Operator__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:748:1: ( ( ruleClassOperator ) | ( ruleClassOperatorWithExceptions ) | ( ruleUserOperator ) | ( ruleAliasOperator ) )
            int alt3=4;
            alt3 = dfa3.predict(input);
            switch (alt3) {
                case 1 :
                    // InternalTomMapping.g:749:2: ( ruleClassOperator )
                    {
                    // InternalTomMapping.g:749:2: ( ruleClassOperator )
                    // InternalTomMapping.g:750:3: ruleClassOperator
                    {
                     before(grammarAccess.getOperatorAccess().getClassOperatorParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleClassOperator();

                    state._fsp--;

                     after(grammarAccess.getOperatorAccess().getClassOperatorParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalTomMapping.g:755:2: ( ruleClassOperatorWithExceptions )
                    {
                    // InternalTomMapping.g:755:2: ( ruleClassOperatorWithExceptions )
                    // InternalTomMapping.g:756:3: ruleClassOperatorWithExceptions
                    {
                     before(grammarAccess.getOperatorAccess().getClassOperatorWithExceptionsParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleClassOperatorWithExceptions();

                    state._fsp--;

                     after(grammarAccess.getOperatorAccess().getClassOperatorWithExceptionsParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalTomMapping.g:761:2: ( ruleUserOperator )
                    {
                    // InternalTomMapping.g:761:2: ( ruleUserOperator )
                    // InternalTomMapping.g:762:3: ruleUserOperator
                    {
                     before(grammarAccess.getOperatorAccess().getUserOperatorParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    ruleUserOperator();

                    state._fsp--;

                     after(grammarAccess.getOperatorAccess().getUserOperatorParserRuleCall_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalTomMapping.g:767:2: ( ruleAliasOperator )
                    {
                    // InternalTomMapping.g:767:2: ( ruleAliasOperator )
                    // InternalTomMapping.g:768:3: ruleAliasOperator
                    {
                     before(grammarAccess.getOperatorAccess().getAliasOperatorParserRuleCall_3()); 
                    pushFollow(FOLLOW_2);
                    ruleAliasOperator();

                    state._fsp--;

                     after(grammarAccess.getOperatorAccess().getAliasOperatorParserRuleCall_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Operator__Alternatives"


    // $ANTLR start "rule__AliasNode__Alternatives"
    // InternalTomMapping.g:777:1: rule__AliasNode__Alternatives : ( ( ruleFeatureNode ) | ( ruleOperatorNode ) );
    public final void rule__AliasNode__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:781:1: ( ( ruleFeatureNode ) | ( ruleOperatorNode ) )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==RULE_ID) ) {
                int LA4_1 = input.LA(2);

                if ( (LA4_1==31) ) {
                    alt4=2;
                }
                else if ( (LA4_1==EOF||LA4_1==21||LA4_1==32) ) {
                    alt4=1;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 4, 1, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // InternalTomMapping.g:782:2: ( ruleFeatureNode )
                    {
                    // InternalTomMapping.g:782:2: ( ruleFeatureNode )
                    // InternalTomMapping.g:783:3: ruleFeatureNode
                    {
                     before(grammarAccess.getAliasNodeAccess().getFeatureNodeParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleFeatureNode();

                    state._fsp--;

                     after(grammarAccess.getAliasNodeAccess().getFeatureNodeParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalTomMapping.g:788:2: ( ruleOperatorNode )
                    {
                    // InternalTomMapping.g:788:2: ( ruleOperatorNode )
                    // InternalTomMapping.g:789:3: ruleOperatorNode
                    {
                     before(grammarAccess.getAliasNodeAccess().getOperatorNodeParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleOperatorNode();

                    state._fsp--;

                     after(grammarAccess.getAliasNodeAccess().getOperatorNodeParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasNode__Alternatives"


    // $ANTLR start "rule__FeatureParameter__Alternatives"
    // InternalTomMapping.g:798:1: rule__FeatureParameter__Alternatives : ( ( ( rule__FeatureParameter__FeatureAssignment_0 ) ) | ( ruleFeatureException ) | ( ruleSettedFeatureParameter ) );
    public final void rule__FeatureParameter__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:802:1: ( ( ( rule__FeatureParameter__FeatureAssignment_0 ) ) | ( ruleFeatureException ) | ( ruleSettedFeatureParameter ) )
            int alt5=3;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==RULE_ID) ) {
                int LA5_1 = input.LA(2);

                if ( (LA5_1==35) ) {
                    alt5=3;
                }
                else if ( (LA5_1==EOF||LA5_1==21||LA5_1==32) ) {
                    alt5=1;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 5, 1, input);

                    throw nvae;
                }
            }
            else if ( (LA5_0==38) ) {
                alt5=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                    // InternalTomMapping.g:803:2: ( ( rule__FeatureParameter__FeatureAssignment_0 ) )
                    {
                    // InternalTomMapping.g:803:2: ( ( rule__FeatureParameter__FeatureAssignment_0 ) )
                    // InternalTomMapping.g:804:3: ( rule__FeatureParameter__FeatureAssignment_0 )
                    {
                     before(grammarAccess.getFeatureParameterAccess().getFeatureAssignment_0()); 
                    // InternalTomMapping.g:805:3: ( rule__FeatureParameter__FeatureAssignment_0 )
                    // InternalTomMapping.g:805:4: rule__FeatureParameter__FeatureAssignment_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__FeatureParameter__FeatureAssignment_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getFeatureParameterAccess().getFeatureAssignment_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalTomMapping.g:809:2: ( ruleFeatureException )
                    {
                    // InternalTomMapping.g:809:2: ( ruleFeatureException )
                    // InternalTomMapping.g:810:3: ruleFeatureException
                    {
                     before(grammarAccess.getFeatureParameterAccess().getFeatureExceptionParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleFeatureException();

                    state._fsp--;

                     after(grammarAccess.getFeatureParameterAccess().getFeatureExceptionParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalTomMapping.g:815:2: ( ruleSettedFeatureParameter )
                    {
                    // InternalTomMapping.g:815:2: ( ruleSettedFeatureParameter )
                    // InternalTomMapping.g:816:3: ruleSettedFeatureParameter
                    {
                     before(grammarAccess.getFeatureParameterAccess().getSettedFeatureParameterParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    ruleSettedFeatureParameter();

                    state._fsp--;

                     after(grammarAccess.getFeatureParameterAccess().getSettedFeatureParameterParserRuleCall_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureParameter__Alternatives"


    // $ANTLR start "rule__SettedValue__Alternatives"
    // InternalTomMapping.g:825:1: rule__SettedValue__Alternatives : ( ( ruleJavaCodeValue ) | ( ruleLiteralValue ) | ( ruleIntSettedValue ) | ( ruleBooleanSettedValue ) );
    public final void rule__SettedValue__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:829:1: ( ( ruleJavaCodeValue ) | ( ruleLiteralValue ) | ( ruleIntSettedValue ) | ( ruleBooleanSettedValue ) )
            int alt6=4;
            switch ( input.LA(1) ) {
            case RULE_STRING:
                {
                alt6=1;
                }
                break;
            case RULE_ID:
                {
                alt6=2;
                }
                break;
            case RULE_INT:
                {
                alt6=3;
                }
                break;
            case 11:
            case 12:
                {
                alt6=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }

            switch (alt6) {
                case 1 :
                    // InternalTomMapping.g:830:2: ( ruleJavaCodeValue )
                    {
                    // InternalTomMapping.g:830:2: ( ruleJavaCodeValue )
                    // InternalTomMapping.g:831:3: ruleJavaCodeValue
                    {
                     before(grammarAccess.getSettedValueAccess().getJavaCodeValueParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleJavaCodeValue();

                    state._fsp--;

                     after(grammarAccess.getSettedValueAccess().getJavaCodeValueParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalTomMapping.g:836:2: ( ruleLiteralValue )
                    {
                    // InternalTomMapping.g:836:2: ( ruleLiteralValue )
                    // InternalTomMapping.g:837:3: ruleLiteralValue
                    {
                     before(grammarAccess.getSettedValueAccess().getLiteralValueParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleLiteralValue();

                    state._fsp--;

                     after(grammarAccess.getSettedValueAccess().getLiteralValueParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalTomMapping.g:842:2: ( ruleIntSettedValue )
                    {
                    // InternalTomMapping.g:842:2: ( ruleIntSettedValue )
                    // InternalTomMapping.g:843:3: ruleIntSettedValue
                    {
                     before(grammarAccess.getSettedValueAccess().getIntSettedValueParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    ruleIntSettedValue();

                    state._fsp--;

                     after(grammarAccess.getSettedValueAccess().getIntSettedValueParserRuleCall_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalTomMapping.g:848:2: ( ruleBooleanSettedValue )
                    {
                    // InternalTomMapping.g:848:2: ( ruleBooleanSettedValue )
                    // InternalTomMapping.g:849:3: ruleBooleanSettedValue
                    {
                     before(grammarAccess.getSettedValueAccess().getBooleanSettedValueParserRuleCall_3()); 
                    pushFollow(FOLLOW_2);
                    ruleBooleanSettedValue();

                    state._fsp--;

                     after(grammarAccess.getSettedValueAccess().getBooleanSettedValueParserRuleCall_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SettedValue__Alternatives"


    // $ANTLR start "rule__EBoolean__Alternatives"
    // InternalTomMapping.g:858:1: rule__EBoolean__Alternatives : ( ( 'true' ) | ( 'false' ) );
    public final void rule__EBoolean__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:862:1: ( ( 'true' ) | ( 'false' ) )
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==11) ) {
                alt7=1;
            }
            else if ( (LA7_0==12) ) {
                alt7=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // InternalTomMapping.g:863:2: ( 'true' )
                    {
                    // InternalTomMapping.g:863:2: ( 'true' )
                    // InternalTomMapping.g:864:3: 'true'
                    {
                     before(grammarAccess.getEBooleanAccess().getTrueKeyword_0()); 
                    match(input,11,FOLLOW_2); 
                     after(grammarAccess.getEBooleanAccess().getTrueKeyword_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalTomMapping.g:869:2: ( 'false' )
                    {
                    // InternalTomMapping.g:869:2: ( 'false' )
                    // InternalTomMapping.g:870:3: 'false'
                    {
                     before(grammarAccess.getEBooleanAccess().getFalseKeyword_1()); 
                    match(input,12,FOLLOW_2); 
                     after(grammarAccess.getEBooleanAccess().getFalseKeyword_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EBoolean__Alternatives"


    // $ANTLR start "rule__Mapping__Group__0"
    // InternalTomMapping.g:879:1: rule__Mapping__Group__0 : rule__Mapping__Group__0__Impl rule__Mapping__Group__1 ;
    public final void rule__Mapping__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:883:1: ( rule__Mapping__Group__0__Impl rule__Mapping__Group__1 )
            // InternalTomMapping.g:884:2: rule__Mapping__Group__0__Impl rule__Mapping__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Mapping__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group__0"


    // $ANTLR start "rule__Mapping__Group__0__Impl"
    // InternalTomMapping.g:891:1: rule__Mapping__Group__0__Impl : ( 'TomMapping' ) ;
    public final void rule__Mapping__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:895:1: ( ( 'TomMapping' ) )
            // InternalTomMapping.g:896:1: ( 'TomMapping' )
            {
            // InternalTomMapping.g:896:1: ( 'TomMapping' )
            // InternalTomMapping.g:897:2: 'TomMapping'
            {
             before(grammarAccess.getMappingAccess().getTomMappingKeyword_0()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getMappingAccess().getTomMappingKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group__0__Impl"


    // $ANTLR start "rule__Mapping__Group__1"
    // InternalTomMapping.g:906:1: rule__Mapping__Group__1 : rule__Mapping__Group__1__Impl rule__Mapping__Group__2 ;
    public final void rule__Mapping__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:910:1: ( rule__Mapping__Group__1__Impl rule__Mapping__Group__2 )
            // InternalTomMapping.g:911:2: rule__Mapping__Group__1__Impl rule__Mapping__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__Mapping__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group__1"


    // $ANTLR start "rule__Mapping__Group__1__Impl"
    // InternalTomMapping.g:918:1: rule__Mapping__Group__1__Impl : ( ( rule__Mapping__NameAssignment_1 ) ) ;
    public final void rule__Mapping__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:922:1: ( ( ( rule__Mapping__NameAssignment_1 ) ) )
            // InternalTomMapping.g:923:1: ( ( rule__Mapping__NameAssignment_1 ) )
            {
            // InternalTomMapping.g:923:1: ( ( rule__Mapping__NameAssignment_1 ) )
            // InternalTomMapping.g:924:2: ( rule__Mapping__NameAssignment_1 )
            {
             before(grammarAccess.getMappingAccess().getNameAssignment_1()); 
            // InternalTomMapping.g:925:2: ( rule__Mapping__NameAssignment_1 )
            // InternalTomMapping.g:925:3: rule__Mapping__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Mapping__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getMappingAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group__1__Impl"


    // $ANTLR start "rule__Mapping__Group__2"
    // InternalTomMapping.g:933:1: rule__Mapping__Group__2 : rule__Mapping__Group__2__Impl rule__Mapping__Group__3 ;
    public final void rule__Mapping__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:937:1: ( rule__Mapping__Group__2__Impl rule__Mapping__Group__3 )
            // InternalTomMapping.g:938:2: rule__Mapping__Group__2__Impl rule__Mapping__Group__3
            {
            pushFollow(FOLLOW_5);
            rule__Mapping__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group__2"


    // $ANTLR start "rule__Mapping__Group__2__Impl"
    // InternalTomMapping.g:945:1: rule__Mapping__Group__2__Impl : ( ';' ) ;
    public final void rule__Mapping__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:949:1: ( ( ';' ) )
            // InternalTomMapping.g:950:1: ( ';' )
            {
            // InternalTomMapping.g:950:1: ( ';' )
            // InternalTomMapping.g:951:2: ';'
            {
             before(grammarAccess.getMappingAccess().getSemicolonKeyword_2()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getMappingAccess().getSemicolonKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group__2__Impl"


    // $ANTLR start "rule__Mapping__Group__3"
    // InternalTomMapping.g:960:1: rule__Mapping__Group__3 : rule__Mapping__Group__3__Impl rule__Mapping__Group__4 ;
    public final void rule__Mapping__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:964:1: ( rule__Mapping__Group__3__Impl rule__Mapping__Group__4 )
            // InternalTomMapping.g:965:2: rule__Mapping__Group__3__Impl rule__Mapping__Group__4
            {
            pushFollow(FOLLOW_5);
            rule__Mapping__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group__3"


    // $ANTLR start "rule__Mapping__Group__3__Impl"
    // InternalTomMapping.g:972:1: rule__Mapping__Group__3__Impl : ( ( rule__Mapping__Group_3__0 )? ) ;
    public final void rule__Mapping__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:976:1: ( ( ( rule__Mapping__Group_3__0 )? ) )
            // InternalTomMapping.g:977:1: ( ( rule__Mapping__Group_3__0 )? )
            {
            // InternalTomMapping.g:977:1: ( ( rule__Mapping__Group_3__0 )? )
            // InternalTomMapping.g:978:2: ( rule__Mapping__Group_3__0 )?
            {
             before(grammarAccess.getMappingAccess().getGroup_3()); 
            // InternalTomMapping.g:979:2: ( rule__Mapping__Group_3__0 )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==15) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // InternalTomMapping.g:979:3: rule__Mapping__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Mapping__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getMappingAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group__3__Impl"


    // $ANTLR start "rule__Mapping__Group__4"
    // InternalTomMapping.g:987:1: rule__Mapping__Group__4 : rule__Mapping__Group__4__Impl rule__Mapping__Group__5 ;
    public final void rule__Mapping__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:991:1: ( rule__Mapping__Group__4__Impl rule__Mapping__Group__5 )
            // InternalTomMapping.g:992:2: rule__Mapping__Group__4__Impl rule__Mapping__Group__5
            {
            pushFollow(FOLLOW_5);
            rule__Mapping__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group__4"


    // $ANTLR start "rule__Mapping__Group__4__Impl"
    // InternalTomMapping.g:999:1: rule__Mapping__Group__4__Impl : ( ( rule__Mapping__Group_4__0 )? ) ;
    public final void rule__Mapping__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1003:1: ( ( ( rule__Mapping__Group_4__0 )? ) )
            // InternalTomMapping.g:1004:1: ( ( rule__Mapping__Group_4__0 )? )
            {
            // InternalTomMapping.g:1004:1: ( ( rule__Mapping__Group_4__0 )? )
            // InternalTomMapping.g:1005:2: ( rule__Mapping__Group_4__0 )?
            {
             before(grammarAccess.getMappingAccess().getGroup_4()); 
            // InternalTomMapping.g:1006:2: ( rule__Mapping__Group_4__0 )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==16) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // InternalTomMapping.g:1006:3: rule__Mapping__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Mapping__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getMappingAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group__4__Impl"


    // $ANTLR start "rule__Mapping__Group__5"
    // InternalTomMapping.g:1014:1: rule__Mapping__Group__5 : rule__Mapping__Group__5__Impl rule__Mapping__Group__6 ;
    public final void rule__Mapping__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1018:1: ( rule__Mapping__Group__5__Impl rule__Mapping__Group__6 )
            // InternalTomMapping.g:1019:2: rule__Mapping__Group__5__Impl rule__Mapping__Group__6
            {
            pushFollow(FOLLOW_5);
            rule__Mapping__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group__5"


    // $ANTLR start "rule__Mapping__Group__5__Impl"
    // InternalTomMapping.g:1026:1: rule__Mapping__Group__5__Impl : ( ( rule__Mapping__Group_5__0 )? ) ;
    public final void rule__Mapping__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1030:1: ( ( ( rule__Mapping__Group_5__0 )? ) )
            // InternalTomMapping.g:1031:1: ( ( rule__Mapping__Group_5__0 )? )
            {
            // InternalTomMapping.g:1031:1: ( ( rule__Mapping__Group_5__0 )? )
            // InternalTomMapping.g:1032:2: ( rule__Mapping__Group_5__0 )?
            {
             before(grammarAccess.getMappingAccess().getGroup_5()); 
            // InternalTomMapping.g:1033:2: ( rule__Mapping__Group_5__0 )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==40) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // InternalTomMapping.g:1033:3: rule__Mapping__Group_5__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Mapping__Group_5__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getMappingAccess().getGroup_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group__5__Impl"


    // $ANTLR start "rule__Mapping__Group__6"
    // InternalTomMapping.g:1041:1: rule__Mapping__Group__6 : rule__Mapping__Group__6__Impl rule__Mapping__Group__7 ;
    public final void rule__Mapping__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1045:1: ( rule__Mapping__Group__6__Impl rule__Mapping__Group__7 )
            // InternalTomMapping.g:1046:2: rule__Mapping__Group__6__Impl rule__Mapping__Group__7
            {
            pushFollow(FOLLOW_5);
            rule__Mapping__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group__6"


    // $ANTLR start "rule__Mapping__Group__6__Impl"
    // InternalTomMapping.g:1053:1: rule__Mapping__Group__6__Impl : ( ( rule__Mapping__Group_6__0 )? ) ;
    public final void rule__Mapping__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1057:1: ( ( ( rule__Mapping__Group_6__0 )? ) )
            // InternalTomMapping.g:1058:1: ( ( rule__Mapping__Group_6__0 )? )
            {
            // InternalTomMapping.g:1058:1: ( ( rule__Mapping__Group_6__0 )? )
            // InternalTomMapping.g:1059:2: ( rule__Mapping__Group_6__0 )?
            {
             before(grammarAccess.getMappingAccess().getGroup_6()); 
            // InternalTomMapping.g:1060:2: ( rule__Mapping__Group_6__0 )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==41) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // InternalTomMapping.g:1060:3: rule__Mapping__Group_6__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Mapping__Group_6__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getMappingAccess().getGroup_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group__6__Impl"


    // $ANTLR start "rule__Mapping__Group__7"
    // InternalTomMapping.g:1068:1: rule__Mapping__Group__7 : rule__Mapping__Group__7__Impl rule__Mapping__Group__8 ;
    public final void rule__Mapping__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1072:1: ( rule__Mapping__Group__7__Impl rule__Mapping__Group__8 )
            // InternalTomMapping.g:1073:2: rule__Mapping__Group__7__Impl rule__Mapping__Group__8
            {
            pushFollow(FOLLOW_5);
            rule__Mapping__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group__7"


    // $ANTLR start "rule__Mapping__Group__7__Impl"
    // InternalTomMapping.g:1080:1: rule__Mapping__Group__7__Impl : ( ( rule__Mapping__ImportsAssignment_7 )* ) ;
    public final void rule__Mapping__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1084:1: ( ( ( rule__Mapping__ImportsAssignment_7 )* ) )
            // InternalTomMapping.g:1085:1: ( ( rule__Mapping__ImportsAssignment_7 )* )
            {
            // InternalTomMapping.g:1085:1: ( ( rule__Mapping__ImportsAssignment_7 )* )
            // InternalTomMapping.g:1086:2: ( rule__Mapping__ImportsAssignment_7 )*
            {
             before(grammarAccess.getMappingAccess().getImportsAssignment_7()); 
            // InternalTomMapping.g:1087:2: ( rule__Mapping__ImportsAssignment_7 )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( (LA12_0==27) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // InternalTomMapping.g:1087:3: rule__Mapping__ImportsAssignment_7
            	    {
            	    pushFollow(FOLLOW_6);
            	    rule__Mapping__ImportsAssignment_7();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);

             after(grammarAccess.getMappingAccess().getImportsAssignment_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group__7__Impl"


    // $ANTLR start "rule__Mapping__Group__8"
    // InternalTomMapping.g:1095:1: rule__Mapping__Group__8 : rule__Mapping__Group__8__Impl rule__Mapping__Group__9 ;
    public final void rule__Mapping__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1099:1: ( rule__Mapping__Group__8__Impl rule__Mapping__Group__9 )
            // InternalTomMapping.g:1100:2: rule__Mapping__Group__8__Impl rule__Mapping__Group__9
            {
            pushFollow(FOLLOW_5);
            rule__Mapping__Group__8__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group__8"


    // $ANTLR start "rule__Mapping__Group__8__Impl"
    // InternalTomMapping.g:1107:1: rule__Mapping__Group__8__Impl : ( ( rule__Mapping__InstanceMappingsAssignment_8 )* ) ;
    public final void rule__Mapping__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1111:1: ( ( ( rule__Mapping__InstanceMappingsAssignment_8 )* ) )
            // InternalTomMapping.g:1112:1: ( ( rule__Mapping__InstanceMappingsAssignment_8 )* )
            {
            // InternalTomMapping.g:1112:1: ( ( rule__Mapping__InstanceMappingsAssignment_8 )* )
            // InternalTomMapping.g:1113:2: ( rule__Mapping__InstanceMappingsAssignment_8 )*
            {
             before(grammarAccess.getMappingAccess().getInstanceMappingsAssignment_8()); 
            // InternalTomMapping.g:1114:2: ( rule__Mapping__InstanceMappingsAssignment_8 )*
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( (LA13_0==24) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // InternalTomMapping.g:1114:3: rule__Mapping__InstanceMappingsAssignment_8
            	    {
            	    pushFollow(FOLLOW_7);
            	    rule__Mapping__InstanceMappingsAssignment_8();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);

             after(grammarAccess.getMappingAccess().getInstanceMappingsAssignment_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group__8__Impl"


    // $ANTLR start "rule__Mapping__Group__9"
    // InternalTomMapping.g:1122:1: rule__Mapping__Group__9 : rule__Mapping__Group__9__Impl rule__Mapping__Group__10 ;
    public final void rule__Mapping__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1126:1: ( rule__Mapping__Group__9__Impl rule__Mapping__Group__10 )
            // InternalTomMapping.g:1127:2: rule__Mapping__Group__9__Impl rule__Mapping__Group__10
            {
            pushFollow(FOLLOW_5);
            rule__Mapping__Group__9__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group__10();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group__9"


    // $ANTLR start "rule__Mapping__Group__9__Impl"
    // InternalTomMapping.g:1134:1: rule__Mapping__Group__9__Impl : ( ( rule__Mapping__Group_9__0 )? ) ;
    public final void rule__Mapping__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1138:1: ( ( ( rule__Mapping__Group_9__0 )? ) )
            // InternalTomMapping.g:1139:1: ( ( rule__Mapping__Group_9__0 )? )
            {
            // InternalTomMapping.g:1139:1: ( ( rule__Mapping__Group_9__0 )? )
            // InternalTomMapping.g:1140:2: ( rule__Mapping__Group_9__0 )?
            {
             before(grammarAccess.getMappingAccess().getGroup_9()); 
            // InternalTomMapping.g:1141:2: ( rule__Mapping__Group_9__0 )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==17) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // InternalTomMapping.g:1141:3: rule__Mapping__Group_9__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Mapping__Group_9__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getMappingAccess().getGroup_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group__9__Impl"


    // $ANTLR start "rule__Mapping__Group__10"
    // InternalTomMapping.g:1149:1: rule__Mapping__Group__10 : rule__Mapping__Group__10__Impl ;
    public final void rule__Mapping__Group__10() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1153:1: ( rule__Mapping__Group__10__Impl )
            // InternalTomMapping.g:1154:2: rule__Mapping__Group__10__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Mapping__Group__10__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group__10"


    // $ANTLR start "rule__Mapping__Group__10__Impl"
    // InternalTomMapping.g:1160:1: rule__Mapping__Group__10__Impl : ( ( rule__Mapping__Alternatives_10 )* ) ;
    public final void rule__Mapping__Group__10__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1164:1: ( ( ( rule__Mapping__Alternatives_10 )* ) )
            // InternalTomMapping.g:1165:1: ( ( rule__Mapping__Alternatives_10 )* )
            {
            // InternalTomMapping.g:1165:1: ( ( rule__Mapping__Alternatives_10 )* )
            // InternalTomMapping.g:1166:2: ( rule__Mapping__Alternatives_10 )*
            {
             before(grammarAccess.getMappingAccess().getAlternatives_10()); 
            // InternalTomMapping.g:1167:2: ( rule__Mapping__Alternatives_10 )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==23||LA15_0==26) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // InternalTomMapping.g:1167:3: rule__Mapping__Alternatives_10
            	    {
            	    pushFollow(FOLLOW_8);
            	    rule__Mapping__Alternatives_10();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);

             after(grammarAccess.getMappingAccess().getAlternatives_10()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group__10__Impl"


    // $ANTLR start "rule__Mapping__Group_3__0"
    // InternalTomMapping.g:1176:1: rule__Mapping__Group_3__0 : rule__Mapping__Group_3__0__Impl rule__Mapping__Group_3__1 ;
    public final void rule__Mapping__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1180:1: ( rule__Mapping__Group_3__0__Impl rule__Mapping__Group_3__1 )
            // InternalTomMapping.g:1181:2: rule__Mapping__Group_3__0__Impl rule__Mapping__Group_3__1
            {
            pushFollow(FOLLOW_9);
            rule__Mapping__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_3__0"


    // $ANTLR start "rule__Mapping__Group_3__0__Impl"
    // InternalTomMapping.g:1188:1: rule__Mapping__Group_3__0__Impl : ( 'prefix' ) ;
    public final void rule__Mapping__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1192:1: ( ( 'prefix' ) )
            // InternalTomMapping.g:1193:1: ( 'prefix' )
            {
            // InternalTomMapping.g:1193:1: ( 'prefix' )
            // InternalTomMapping.g:1194:2: 'prefix'
            {
             before(grammarAccess.getMappingAccess().getPrefixKeyword_3_0()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getMappingAccess().getPrefixKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_3__0__Impl"


    // $ANTLR start "rule__Mapping__Group_3__1"
    // InternalTomMapping.g:1203:1: rule__Mapping__Group_3__1 : rule__Mapping__Group_3__1__Impl rule__Mapping__Group_3__2 ;
    public final void rule__Mapping__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1207:1: ( rule__Mapping__Group_3__1__Impl rule__Mapping__Group_3__2 )
            // InternalTomMapping.g:1208:2: rule__Mapping__Group_3__1__Impl rule__Mapping__Group_3__2
            {
            pushFollow(FOLLOW_4);
            rule__Mapping__Group_3__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group_3__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_3__1"


    // $ANTLR start "rule__Mapping__Group_3__1__Impl"
    // InternalTomMapping.g:1215:1: rule__Mapping__Group_3__1__Impl : ( ( rule__Mapping__PrefixAssignment_3_1 ) ) ;
    public final void rule__Mapping__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1219:1: ( ( ( rule__Mapping__PrefixAssignment_3_1 ) ) )
            // InternalTomMapping.g:1220:1: ( ( rule__Mapping__PrefixAssignment_3_1 ) )
            {
            // InternalTomMapping.g:1220:1: ( ( rule__Mapping__PrefixAssignment_3_1 ) )
            // InternalTomMapping.g:1221:2: ( rule__Mapping__PrefixAssignment_3_1 )
            {
             before(grammarAccess.getMappingAccess().getPrefixAssignment_3_1()); 
            // InternalTomMapping.g:1222:2: ( rule__Mapping__PrefixAssignment_3_1 )
            // InternalTomMapping.g:1222:3: rule__Mapping__PrefixAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Mapping__PrefixAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getMappingAccess().getPrefixAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_3__1__Impl"


    // $ANTLR start "rule__Mapping__Group_3__2"
    // InternalTomMapping.g:1230:1: rule__Mapping__Group_3__2 : rule__Mapping__Group_3__2__Impl ;
    public final void rule__Mapping__Group_3__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1234:1: ( rule__Mapping__Group_3__2__Impl )
            // InternalTomMapping.g:1235:2: rule__Mapping__Group_3__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Mapping__Group_3__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_3__2"


    // $ANTLR start "rule__Mapping__Group_3__2__Impl"
    // InternalTomMapping.g:1241:1: rule__Mapping__Group_3__2__Impl : ( ';' ) ;
    public final void rule__Mapping__Group_3__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1245:1: ( ( ';' ) )
            // InternalTomMapping.g:1246:1: ( ';' )
            {
            // InternalTomMapping.g:1246:1: ( ';' )
            // InternalTomMapping.g:1247:2: ';'
            {
             before(grammarAccess.getMappingAccess().getSemicolonKeyword_3_2()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getMappingAccess().getSemicolonKeyword_3_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_3__2__Impl"


    // $ANTLR start "rule__Mapping__Group_4__0"
    // InternalTomMapping.g:1257:1: rule__Mapping__Group_4__0 : rule__Mapping__Group_4__0__Impl rule__Mapping__Group_4__1 ;
    public final void rule__Mapping__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1261:1: ( rule__Mapping__Group_4__0__Impl rule__Mapping__Group_4__1 )
            // InternalTomMapping.g:1262:2: rule__Mapping__Group_4__0__Impl rule__Mapping__Group_4__1
            {
            pushFollow(FOLLOW_9);
            rule__Mapping__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_4__0"


    // $ANTLR start "rule__Mapping__Group_4__0__Impl"
    // InternalTomMapping.g:1269:1: rule__Mapping__Group_4__0__Impl : ( 'package' ) ;
    public final void rule__Mapping__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1273:1: ( ( 'package' ) )
            // InternalTomMapping.g:1274:1: ( 'package' )
            {
            // InternalTomMapping.g:1274:1: ( 'package' )
            // InternalTomMapping.g:1275:2: 'package'
            {
             before(grammarAccess.getMappingAccess().getPackageKeyword_4_0()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getMappingAccess().getPackageKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_4__0__Impl"


    // $ANTLR start "rule__Mapping__Group_4__1"
    // InternalTomMapping.g:1284:1: rule__Mapping__Group_4__1 : rule__Mapping__Group_4__1__Impl rule__Mapping__Group_4__2 ;
    public final void rule__Mapping__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1288:1: ( rule__Mapping__Group_4__1__Impl rule__Mapping__Group_4__2 )
            // InternalTomMapping.g:1289:2: rule__Mapping__Group_4__1__Impl rule__Mapping__Group_4__2
            {
            pushFollow(FOLLOW_4);
            rule__Mapping__Group_4__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group_4__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_4__1"


    // $ANTLR start "rule__Mapping__Group_4__1__Impl"
    // InternalTomMapping.g:1296:1: rule__Mapping__Group_4__1__Impl : ( ( rule__Mapping__PackageNameAssignment_4_1 ) ) ;
    public final void rule__Mapping__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1300:1: ( ( ( rule__Mapping__PackageNameAssignment_4_1 ) ) )
            // InternalTomMapping.g:1301:1: ( ( rule__Mapping__PackageNameAssignment_4_1 ) )
            {
            // InternalTomMapping.g:1301:1: ( ( rule__Mapping__PackageNameAssignment_4_1 ) )
            // InternalTomMapping.g:1302:2: ( rule__Mapping__PackageNameAssignment_4_1 )
            {
             before(grammarAccess.getMappingAccess().getPackageNameAssignment_4_1()); 
            // InternalTomMapping.g:1303:2: ( rule__Mapping__PackageNameAssignment_4_1 )
            // InternalTomMapping.g:1303:3: rule__Mapping__PackageNameAssignment_4_1
            {
            pushFollow(FOLLOW_2);
            rule__Mapping__PackageNameAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getMappingAccess().getPackageNameAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_4__1__Impl"


    // $ANTLR start "rule__Mapping__Group_4__2"
    // InternalTomMapping.g:1311:1: rule__Mapping__Group_4__2 : rule__Mapping__Group_4__2__Impl ;
    public final void rule__Mapping__Group_4__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1315:1: ( rule__Mapping__Group_4__2__Impl )
            // InternalTomMapping.g:1316:2: rule__Mapping__Group_4__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Mapping__Group_4__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_4__2"


    // $ANTLR start "rule__Mapping__Group_4__2__Impl"
    // InternalTomMapping.g:1322:1: rule__Mapping__Group_4__2__Impl : ( ';' ) ;
    public final void rule__Mapping__Group_4__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1326:1: ( ( ';' ) )
            // InternalTomMapping.g:1327:1: ( ';' )
            {
            // InternalTomMapping.g:1327:1: ( ';' )
            // InternalTomMapping.g:1328:2: ';'
            {
             before(grammarAccess.getMappingAccess().getSemicolonKeyword_4_2()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getMappingAccess().getSemicolonKeyword_4_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_4__2__Impl"


    // $ANTLR start "rule__Mapping__Group_5__0"
    // InternalTomMapping.g:1338:1: rule__Mapping__Group_5__0 : rule__Mapping__Group_5__0__Impl rule__Mapping__Group_5__1 ;
    public final void rule__Mapping__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1342:1: ( rule__Mapping__Group_5__0__Impl rule__Mapping__Group_5__1 )
            // InternalTomMapping.g:1343:2: rule__Mapping__Group_5__0__Impl rule__Mapping__Group_5__1
            {
            pushFollow(FOLLOW_4);
            rule__Mapping__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_5__0"


    // $ANTLR start "rule__Mapping__Group_5__0__Impl"
    // InternalTomMapping.g:1350:1: rule__Mapping__Group_5__0__Impl : ( ( rule__Mapping__UseFullyQualifiedTypeNameAssignment_5_0 ) ) ;
    public final void rule__Mapping__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1354:1: ( ( ( rule__Mapping__UseFullyQualifiedTypeNameAssignment_5_0 ) ) )
            // InternalTomMapping.g:1355:1: ( ( rule__Mapping__UseFullyQualifiedTypeNameAssignment_5_0 ) )
            {
            // InternalTomMapping.g:1355:1: ( ( rule__Mapping__UseFullyQualifiedTypeNameAssignment_5_0 ) )
            // InternalTomMapping.g:1356:2: ( rule__Mapping__UseFullyQualifiedTypeNameAssignment_5_0 )
            {
             before(grammarAccess.getMappingAccess().getUseFullyQualifiedTypeNameAssignment_5_0()); 
            // InternalTomMapping.g:1357:2: ( rule__Mapping__UseFullyQualifiedTypeNameAssignment_5_0 )
            // InternalTomMapping.g:1357:3: rule__Mapping__UseFullyQualifiedTypeNameAssignment_5_0
            {
            pushFollow(FOLLOW_2);
            rule__Mapping__UseFullyQualifiedTypeNameAssignment_5_0();

            state._fsp--;


            }

             after(grammarAccess.getMappingAccess().getUseFullyQualifiedTypeNameAssignment_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_5__0__Impl"


    // $ANTLR start "rule__Mapping__Group_5__1"
    // InternalTomMapping.g:1365:1: rule__Mapping__Group_5__1 : rule__Mapping__Group_5__1__Impl ;
    public final void rule__Mapping__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1369:1: ( rule__Mapping__Group_5__1__Impl )
            // InternalTomMapping.g:1370:2: rule__Mapping__Group_5__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Mapping__Group_5__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_5__1"


    // $ANTLR start "rule__Mapping__Group_5__1__Impl"
    // InternalTomMapping.g:1376:1: rule__Mapping__Group_5__1__Impl : ( ';' ) ;
    public final void rule__Mapping__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1380:1: ( ( ';' ) )
            // InternalTomMapping.g:1381:1: ( ';' )
            {
            // InternalTomMapping.g:1381:1: ( ';' )
            // InternalTomMapping.g:1382:2: ';'
            {
             before(grammarAccess.getMappingAccess().getSemicolonKeyword_5_1()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getMappingAccess().getSemicolonKeyword_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_5__1__Impl"


    // $ANTLR start "rule__Mapping__Group_6__0"
    // InternalTomMapping.g:1392:1: rule__Mapping__Group_6__0 : rule__Mapping__Group_6__0__Impl rule__Mapping__Group_6__1 ;
    public final void rule__Mapping__Group_6__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1396:1: ( rule__Mapping__Group_6__0__Impl rule__Mapping__Group_6__1 )
            // InternalTomMapping.g:1397:2: rule__Mapping__Group_6__0__Impl rule__Mapping__Group_6__1
            {
            pushFollow(FOLLOW_4);
            rule__Mapping__Group_6__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group_6__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_6__0"


    // $ANTLR start "rule__Mapping__Group_6__0__Impl"
    // InternalTomMapping.g:1404:1: rule__Mapping__Group_6__0__Impl : ( ( rule__Mapping__GenerateUserFactoryAssignment_6_0 ) ) ;
    public final void rule__Mapping__Group_6__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1408:1: ( ( ( rule__Mapping__GenerateUserFactoryAssignment_6_0 ) ) )
            // InternalTomMapping.g:1409:1: ( ( rule__Mapping__GenerateUserFactoryAssignment_6_0 ) )
            {
            // InternalTomMapping.g:1409:1: ( ( rule__Mapping__GenerateUserFactoryAssignment_6_0 ) )
            // InternalTomMapping.g:1410:2: ( rule__Mapping__GenerateUserFactoryAssignment_6_0 )
            {
             before(grammarAccess.getMappingAccess().getGenerateUserFactoryAssignment_6_0()); 
            // InternalTomMapping.g:1411:2: ( rule__Mapping__GenerateUserFactoryAssignment_6_0 )
            // InternalTomMapping.g:1411:3: rule__Mapping__GenerateUserFactoryAssignment_6_0
            {
            pushFollow(FOLLOW_2);
            rule__Mapping__GenerateUserFactoryAssignment_6_0();

            state._fsp--;


            }

             after(grammarAccess.getMappingAccess().getGenerateUserFactoryAssignment_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_6__0__Impl"


    // $ANTLR start "rule__Mapping__Group_6__1"
    // InternalTomMapping.g:1419:1: rule__Mapping__Group_6__1 : rule__Mapping__Group_6__1__Impl ;
    public final void rule__Mapping__Group_6__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1423:1: ( rule__Mapping__Group_6__1__Impl )
            // InternalTomMapping.g:1424:2: rule__Mapping__Group_6__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Mapping__Group_6__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_6__1"


    // $ANTLR start "rule__Mapping__Group_6__1__Impl"
    // InternalTomMapping.g:1430:1: rule__Mapping__Group_6__1__Impl : ( ';' ) ;
    public final void rule__Mapping__Group_6__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1434:1: ( ( ';' ) )
            // InternalTomMapping.g:1435:1: ( ';' )
            {
            // InternalTomMapping.g:1435:1: ( ';' )
            // InternalTomMapping.g:1436:2: ';'
            {
             before(grammarAccess.getMappingAccess().getSemicolonKeyword_6_1()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getMappingAccess().getSemicolonKeyword_6_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_6__1__Impl"


    // $ANTLR start "rule__Mapping__Group_9__0"
    // InternalTomMapping.g:1446:1: rule__Mapping__Group_9__0 : rule__Mapping__Group_9__0__Impl rule__Mapping__Group_9__1 ;
    public final void rule__Mapping__Group_9__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1450:1: ( rule__Mapping__Group_9__0__Impl rule__Mapping__Group_9__1 )
            // InternalTomMapping.g:1451:2: rule__Mapping__Group_9__0__Impl rule__Mapping__Group_9__1
            {
            pushFollow(FOLLOW_10);
            rule__Mapping__Group_9__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group_9__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_9__0"


    // $ANTLR start "rule__Mapping__Group_9__0__Impl"
    // InternalTomMapping.g:1458:1: rule__Mapping__Group_9__0__Impl : ( 'terminals' ) ;
    public final void rule__Mapping__Group_9__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1462:1: ( ( 'terminals' ) )
            // InternalTomMapping.g:1463:1: ( 'terminals' )
            {
            // InternalTomMapping.g:1463:1: ( 'terminals' )
            // InternalTomMapping.g:1464:2: 'terminals'
            {
             before(grammarAccess.getMappingAccess().getTerminalsKeyword_9_0()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getMappingAccess().getTerminalsKeyword_9_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_9__0__Impl"


    // $ANTLR start "rule__Mapping__Group_9__1"
    // InternalTomMapping.g:1473:1: rule__Mapping__Group_9__1 : rule__Mapping__Group_9__1__Impl rule__Mapping__Group_9__2 ;
    public final void rule__Mapping__Group_9__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1477:1: ( rule__Mapping__Group_9__1__Impl rule__Mapping__Group_9__2 )
            // InternalTomMapping.g:1478:2: rule__Mapping__Group_9__1__Impl rule__Mapping__Group_9__2
            {
            pushFollow(FOLLOW_11);
            rule__Mapping__Group_9__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group_9__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_9__1"


    // $ANTLR start "rule__Mapping__Group_9__1__Impl"
    // InternalTomMapping.g:1485:1: rule__Mapping__Group_9__1__Impl : ( '{' ) ;
    public final void rule__Mapping__Group_9__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1489:1: ( ( '{' ) )
            // InternalTomMapping.g:1490:1: ( '{' )
            {
            // InternalTomMapping.g:1490:1: ( '{' )
            // InternalTomMapping.g:1491:2: '{'
            {
             before(grammarAccess.getMappingAccess().getLeftCurlyBracketKeyword_9_1()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getMappingAccess().getLeftCurlyBracketKeyword_9_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_9__1__Impl"


    // $ANTLR start "rule__Mapping__Group_9__2"
    // InternalTomMapping.g:1500:1: rule__Mapping__Group_9__2 : rule__Mapping__Group_9__2__Impl rule__Mapping__Group_9__3 ;
    public final void rule__Mapping__Group_9__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1504:1: ( rule__Mapping__Group_9__2__Impl rule__Mapping__Group_9__3 )
            // InternalTomMapping.g:1505:2: rule__Mapping__Group_9__2__Impl rule__Mapping__Group_9__3
            {
            pushFollow(FOLLOW_12);
            rule__Mapping__Group_9__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group_9__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_9__2"


    // $ANTLR start "rule__Mapping__Group_9__2__Impl"
    // InternalTomMapping.g:1512:1: rule__Mapping__Group_9__2__Impl : ( ( rule__Mapping__Alternatives_9_2 ) ) ;
    public final void rule__Mapping__Group_9__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1516:1: ( ( ( rule__Mapping__Alternatives_9_2 ) ) )
            // InternalTomMapping.g:1517:1: ( ( rule__Mapping__Alternatives_9_2 ) )
            {
            // InternalTomMapping.g:1517:1: ( ( rule__Mapping__Alternatives_9_2 ) )
            // InternalTomMapping.g:1518:2: ( rule__Mapping__Alternatives_9_2 )
            {
             before(grammarAccess.getMappingAccess().getAlternatives_9_2()); 
            // InternalTomMapping.g:1519:2: ( rule__Mapping__Alternatives_9_2 )
            // InternalTomMapping.g:1519:3: rule__Mapping__Alternatives_9_2
            {
            pushFollow(FOLLOW_2);
            rule__Mapping__Alternatives_9_2();

            state._fsp--;


            }

             after(grammarAccess.getMappingAccess().getAlternatives_9_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_9__2__Impl"


    // $ANTLR start "rule__Mapping__Group_9__3"
    // InternalTomMapping.g:1527:1: rule__Mapping__Group_9__3 : rule__Mapping__Group_9__3__Impl ;
    public final void rule__Mapping__Group_9__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1531:1: ( rule__Mapping__Group_9__3__Impl )
            // InternalTomMapping.g:1532:2: rule__Mapping__Group_9__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Mapping__Group_9__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_9__3"


    // $ANTLR start "rule__Mapping__Group_9__3__Impl"
    // InternalTomMapping.g:1538:1: rule__Mapping__Group_9__3__Impl : ( '}' ) ;
    public final void rule__Mapping__Group_9__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1542:1: ( ( '}' ) )
            // InternalTomMapping.g:1543:1: ( '}' )
            {
            // InternalTomMapping.g:1543:1: ( '}' )
            // InternalTomMapping.g:1544:2: '}'
            {
             before(grammarAccess.getMappingAccess().getRightCurlyBracketKeyword_9_3()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getMappingAccess().getRightCurlyBracketKeyword_9_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_9__3__Impl"


    // $ANTLR start "rule__Mapping__Group_9_2_0__0"
    // InternalTomMapping.g:1554:1: rule__Mapping__Group_9_2_0__0 : rule__Mapping__Group_9_2_0__0__Impl rule__Mapping__Group_9_2_0__1 ;
    public final void rule__Mapping__Group_9_2_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1558:1: ( rule__Mapping__Group_9_2_0__0__Impl rule__Mapping__Group_9_2_0__1 )
            // InternalTomMapping.g:1559:2: rule__Mapping__Group_9_2_0__0__Impl rule__Mapping__Group_9_2_0__1
            {
            pushFollow(FOLLOW_13);
            rule__Mapping__Group_9_2_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group_9_2_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_9_2_0__0"


    // $ANTLR start "rule__Mapping__Group_9_2_0__0__Impl"
    // InternalTomMapping.g:1566:1: rule__Mapping__Group_9_2_0__0__Impl : ( ( rule__Mapping__Group_9_2_0_0__0 )? ) ;
    public final void rule__Mapping__Group_9_2_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1570:1: ( ( ( rule__Mapping__Group_9_2_0_0__0 )? ) )
            // InternalTomMapping.g:1571:1: ( ( rule__Mapping__Group_9_2_0_0__0 )? )
            {
            // InternalTomMapping.g:1571:1: ( ( rule__Mapping__Group_9_2_0_0__0 )? )
            // InternalTomMapping.g:1572:2: ( rule__Mapping__Group_9_2_0_0__0 )?
            {
             before(grammarAccess.getMappingAccess().getGroup_9_2_0_0()); 
            // InternalTomMapping.g:1573:2: ( rule__Mapping__Group_9_2_0_0__0 )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==20) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // InternalTomMapping.g:1573:3: rule__Mapping__Group_9_2_0_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Mapping__Group_9_2_0_0__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getMappingAccess().getGroup_9_2_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_9_2_0__0__Impl"


    // $ANTLR start "rule__Mapping__Group_9_2_0__1"
    // InternalTomMapping.g:1581:1: rule__Mapping__Group_9_2_0__1 : rule__Mapping__Group_9_2_0__1__Impl ;
    public final void rule__Mapping__Group_9_2_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1585:1: ( rule__Mapping__Group_9_2_0__1__Impl )
            // InternalTomMapping.g:1586:2: rule__Mapping__Group_9_2_0__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Mapping__Group_9_2_0__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_9_2_0__1"


    // $ANTLR start "rule__Mapping__Group_9_2_0__1__Impl"
    // InternalTomMapping.g:1592:1: rule__Mapping__Group_9_2_0__1__Impl : ( ( rule__Mapping__Group_9_2_0_1__0 )? ) ;
    public final void rule__Mapping__Group_9_2_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1596:1: ( ( ( rule__Mapping__Group_9_2_0_1__0 )? ) )
            // InternalTomMapping.g:1597:1: ( ( rule__Mapping__Group_9_2_0_1__0 )? )
            {
            // InternalTomMapping.g:1597:1: ( ( rule__Mapping__Group_9_2_0_1__0 )? )
            // InternalTomMapping.g:1598:2: ( rule__Mapping__Group_9_2_0_1__0 )?
            {
             before(grammarAccess.getMappingAccess().getGroup_9_2_0_1()); 
            // InternalTomMapping.g:1599:2: ( rule__Mapping__Group_9_2_0_1__0 )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==22) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // InternalTomMapping.g:1599:3: rule__Mapping__Group_9_2_0_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Mapping__Group_9_2_0_1__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getMappingAccess().getGroup_9_2_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_9_2_0__1__Impl"


    // $ANTLR start "rule__Mapping__Group_9_2_0_0__0"
    // InternalTomMapping.g:1608:1: rule__Mapping__Group_9_2_0_0__0 : rule__Mapping__Group_9_2_0_0__0__Impl rule__Mapping__Group_9_2_0_0__1 ;
    public final void rule__Mapping__Group_9_2_0_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1612:1: ( rule__Mapping__Group_9_2_0_0__0__Impl rule__Mapping__Group_9_2_0_0__1 )
            // InternalTomMapping.g:1613:2: rule__Mapping__Group_9_2_0_0__0__Impl rule__Mapping__Group_9_2_0_0__1
            {
            pushFollow(FOLLOW_10);
            rule__Mapping__Group_9_2_0_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group_9_2_0_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_9_2_0_0__0"


    // $ANTLR start "rule__Mapping__Group_9_2_0_0__0__Impl"
    // InternalTomMapping.g:1620:1: rule__Mapping__Group_9_2_0_0__0__Impl : ( 'define' ) ;
    public final void rule__Mapping__Group_9_2_0_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1624:1: ( ( 'define' ) )
            // InternalTomMapping.g:1625:1: ( 'define' )
            {
            // InternalTomMapping.g:1625:1: ( 'define' )
            // InternalTomMapping.g:1626:2: 'define'
            {
             before(grammarAccess.getMappingAccess().getDefineKeyword_9_2_0_0_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getMappingAccess().getDefineKeyword_9_2_0_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_9_2_0_0__0__Impl"


    // $ANTLR start "rule__Mapping__Group_9_2_0_0__1"
    // InternalTomMapping.g:1635:1: rule__Mapping__Group_9_2_0_0__1 : rule__Mapping__Group_9_2_0_0__1__Impl rule__Mapping__Group_9_2_0_0__2 ;
    public final void rule__Mapping__Group_9_2_0_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1639:1: ( rule__Mapping__Group_9_2_0_0__1__Impl rule__Mapping__Group_9_2_0_0__2 )
            // InternalTomMapping.g:1640:2: rule__Mapping__Group_9_2_0_0__1__Impl rule__Mapping__Group_9_2_0_0__2
            {
            pushFollow(FOLLOW_11);
            rule__Mapping__Group_9_2_0_0__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group_9_2_0_0__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_9_2_0_0__1"


    // $ANTLR start "rule__Mapping__Group_9_2_0_0__1__Impl"
    // InternalTomMapping.g:1647:1: rule__Mapping__Group_9_2_0_0__1__Impl : ( '{' ) ;
    public final void rule__Mapping__Group_9_2_0_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1651:1: ( ( '{' ) )
            // InternalTomMapping.g:1652:1: ( '{' )
            {
            // InternalTomMapping.g:1652:1: ( '{' )
            // InternalTomMapping.g:1653:2: '{'
            {
             before(grammarAccess.getMappingAccess().getLeftCurlyBracketKeyword_9_2_0_0_1()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getMappingAccess().getLeftCurlyBracketKeyword_9_2_0_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_9_2_0_0__1__Impl"


    // $ANTLR start "rule__Mapping__Group_9_2_0_0__2"
    // InternalTomMapping.g:1662:1: rule__Mapping__Group_9_2_0_0__2 : rule__Mapping__Group_9_2_0_0__2__Impl rule__Mapping__Group_9_2_0_0__3 ;
    public final void rule__Mapping__Group_9_2_0_0__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1666:1: ( rule__Mapping__Group_9_2_0_0__2__Impl rule__Mapping__Group_9_2_0_0__3 )
            // InternalTomMapping.g:1667:2: rule__Mapping__Group_9_2_0_0__2__Impl rule__Mapping__Group_9_2_0_0__3
            {
            pushFollow(FOLLOW_14);
            rule__Mapping__Group_9_2_0_0__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group_9_2_0_0__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_9_2_0_0__2"


    // $ANTLR start "rule__Mapping__Group_9_2_0_0__2__Impl"
    // InternalTomMapping.g:1674:1: rule__Mapping__Group_9_2_0_0__2__Impl : ( ( rule__Mapping__TerminalsAssignment_9_2_0_0_2 ) ) ;
    public final void rule__Mapping__Group_9_2_0_0__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1678:1: ( ( ( rule__Mapping__TerminalsAssignment_9_2_0_0_2 ) ) )
            // InternalTomMapping.g:1679:1: ( ( rule__Mapping__TerminalsAssignment_9_2_0_0_2 ) )
            {
            // InternalTomMapping.g:1679:1: ( ( rule__Mapping__TerminalsAssignment_9_2_0_0_2 ) )
            // InternalTomMapping.g:1680:2: ( rule__Mapping__TerminalsAssignment_9_2_0_0_2 )
            {
             before(grammarAccess.getMappingAccess().getTerminalsAssignment_9_2_0_0_2()); 
            // InternalTomMapping.g:1681:2: ( rule__Mapping__TerminalsAssignment_9_2_0_0_2 )
            // InternalTomMapping.g:1681:3: rule__Mapping__TerminalsAssignment_9_2_0_0_2
            {
            pushFollow(FOLLOW_2);
            rule__Mapping__TerminalsAssignment_9_2_0_0_2();

            state._fsp--;


            }

             after(grammarAccess.getMappingAccess().getTerminalsAssignment_9_2_0_0_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_9_2_0_0__2__Impl"


    // $ANTLR start "rule__Mapping__Group_9_2_0_0__3"
    // InternalTomMapping.g:1689:1: rule__Mapping__Group_9_2_0_0__3 : rule__Mapping__Group_9_2_0_0__3__Impl rule__Mapping__Group_9_2_0_0__4 ;
    public final void rule__Mapping__Group_9_2_0_0__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1693:1: ( rule__Mapping__Group_9_2_0_0__3__Impl rule__Mapping__Group_9_2_0_0__4 )
            // InternalTomMapping.g:1694:2: rule__Mapping__Group_9_2_0_0__3__Impl rule__Mapping__Group_9_2_0_0__4
            {
            pushFollow(FOLLOW_14);
            rule__Mapping__Group_9_2_0_0__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group_9_2_0_0__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_9_2_0_0__3"


    // $ANTLR start "rule__Mapping__Group_9_2_0_0__3__Impl"
    // InternalTomMapping.g:1701:1: rule__Mapping__Group_9_2_0_0__3__Impl : ( ( rule__Mapping__Group_9_2_0_0_3__0 )* ) ;
    public final void rule__Mapping__Group_9_2_0_0__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1705:1: ( ( ( rule__Mapping__Group_9_2_0_0_3__0 )* ) )
            // InternalTomMapping.g:1706:1: ( ( rule__Mapping__Group_9_2_0_0_3__0 )* )
            {
            // InternalTomMapping.g:1706:1: ( ( rule__Mapping__Group_9_2_0_0_3__0 )* )
            // InternalTomMapping.g:1707:2: ( rule__Mapping__Group_9_2_0_0_3__0 )*
            {
             before(grammarAccess.getMappingAccess().getGroup_9_2_0_0_3()); 
            // InternalTomMapping.g:1708:2: ( rule__Mapping__Group_9_2_0_0_3__0 )*
            loop18:
            do {
                int alt18=2;
                int LA18_0 = input.LA(1);

                if ( (LA18_0==21) ) {
                    alt18=1;
                }


                switch (alt18) {
            	case 1 :
            	    // InternalTomMapping.g:1708:3: rule__Mapping__Group_9_2_0_0_3__0
            	    {
            	    pushFollow(FOLLOW_15);
            	    rule__Mapping__Group_9_2_0_0_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop18;
                }
            } while (true);

             after(grammarAccess.getMappingAccess().getGroup_9_2_0_0_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_9_2_0_0__3__Impl"


    // $ANTLR start "rule__Mapping__Group_9_2_0_0__4"
    // InternalTomMapping.g:1716:1: rule__Mapping__Group_9_2_0_0__4 : rule__Mapping__Group_9_2_0_0__4__Impl ;
    public final void rule__Mapping__Group_9_2_0_0__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1720:1: ( rule__Mapping__Group_9_2_0_0__4__Impl )
            // InternalTomMapping.g:1721:2: rule__Mapping__Group_9_2_0_0__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Mapping__Group_9_2_0_0__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_9_2_0_0__4"


    // $ANTLR start "rule__Mapping__Group_9_2_0_0__4__Impl"
    // InternalTomMapping.g:1727:1: rule__Mapping__Group_9_2_0_0__4__Impl : ( '}' ) ;
    public final void rule__Mapping__Group_9_2_0_0__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1731:1: ( ( '}' ) )
            // InternalTomMapping.g:1732:1: ( '}' )
            {
            // InternalTomMapping.g:1732:1: ( '}' )
            // InternalTomMapping.g:1733:2: '}'
            {
             before(grammarAccess.getMappingAccess().getRightCurlyBracketKeyword_9_2_0_0_4()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getMappingAccess().getRightCurlyBracketKeyword_9_2_0_0_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_9_2_0_0__4__Impl"


    // $ANTLR start "rule__Mapping__Group_9_2_0_0_3__0"
    // InternalTomMapping.g:1743:1: rule__Mapping__Group_9_2_0_0_3__0 : rule__Mapping__Group_9_2_0_0_3__0__Impl rule__Mapping__Group_9_2_0_0_3__1 ;
    public final void rule__Mapping__Group_9_2_0_0_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1747:1: ( rule__Mapping__Group_9_2_0_0_3__0__Impl rule__Mapping__Group_9_2_0_0_3__1 )
            // InternalTomMapping.g:1748:2: rule__Mapping__Group_9_2_0_0_3__0__Impl rule__Mapping__Group_9_2_0_0_3__1
            {
            pushFollow(FOLLOW_11);
            rule__Mapping__Group_9_2_0_0_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group_9_2_0_0_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_9_2_0_0_3__0"


    // $ANTLR start "rule__Mapping__Group_9_2_0_0_3__0__Impl"
    // InternalTomMapping.g:1755:1: rule__Mapping__Group_9_2_0_0_3__0__Impl : ( ',' ) ;
    public final void rule__Mapping__Group_9_2_0_0_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1759:1: ( ( ',' ) )
            // InternalTomMapping.g:1760:1: ( ',' )
            {
            // InternalTomMapping.g:1760:1: ( ',' )
            // InternalTomMapping.g:1761:2: ','
            {
             before(grammarAccess.getMappingAccess().getCommaKeyword_9_2_0_0_3_0()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getMappingAccess().getCommaKeyword_9_2_0_0_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_9_2_0_0_3__0__Impl"


    // $ANTLR start "rule__Mapping__Group_9_2_0_0_3__1"
    // InternalTomMapping.g:1770:1: rule__Mapping__Group_9_2_0_0_3__1 : rule__Mapping__Group_9_2_0_0_3__1__Impl ;
    public final void rule__Mapping__Group_9_2_0_0_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1774:1: ( rule__Mapping__Group_9_2_0_0_3__1__Impl )
            // InternalTomMapping.g:1775:2: rule__Mapping__Group_9_2_0_0_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Mapping__Group_9_2_0_0_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_9_2_0_0_3__1"


    // $ANTLR start "rule__Mapping__Group_9_2_0_0_3__1__Impl"
    // InternalTomMapping.g:1781:1: rule__Mapping__Group_9_2_0_0_3__1__Impl : ( ( rule__Mapping__TerminalsAssignment_9_2_0_0_3_1 ) ) ;
    public final void rule__Mapping__Group_9_2_0_0_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1785:1: ( ( ( rule__Mapping__TerminalsAssignment_9_2_0_0_3_1 ) ) )
            // InternalTomMapping.g:1786:1: ( ( rule__Mapping__TerminalsAssignment_9_2_0_0_3_1 ) )
            {
            // InternalTomMapping.g:1786:1: ( ( rule__Mapping__TerminalsAssignment_9_2_0_0_3_1 ) )
            // InternalTomMapping.g:1787:2: ( rule__Mapping__TerminalsAssignment_9_2_0_0_3_1 )
            {
             before(grammarAccess.getMappingAccess().getTerminalsAssignment_9_2_0_0_3_1()); 
            // InternalTomMapping.g:1788:2: ( rule__Mapping__TerminalsAssignment_9_2_0_0_3_1 )
            // InternalTomMapping.g:1788:3: rule__Mapping__TerminalsAssignment_9_2_0_0_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Mapping__TerminalsAssignment_9_2_0_0_3_1();

            state._fsp--;


            }

             after(grammarAccess.getMappingAccess().getTerminalsAssignment_9_2_0_0_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_9_2_0_0_3__1__Impl"


    // $ANTLR start "rule__Mapping__Group_9_2_0_1__0"
    // InternalTomMapping.g:1797:1: rule__Mapping__Group_9_2_0_1__0 : rule__Mapping__Group_9_2_0_1__0__Impl rule__Mapping__Group_9_2_0_1__1 ;
    public final void rule__Mapping__Group_9_2_0_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1801:1: ( rule__Mapping__Group_9_2_0_1__0__Impl rule__Mapping__Group_9_2_0_1__1 )
            // InternalTomMapping.g:1802:2: rule__Mapping__Group_9_2_0_1__0__Impl rule__Mapping__Group_9_2_0_1__1
            {
            pushFollow(FOLLOW_10);
            rule__Mapping__Group_9_2_0_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group_9_2_0_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_9_2_0_1__0"


    // $ANTLR start "rule__Mapping__Group_9_2_0_1__0__Impl"
    // InternalTomMapping.g:1809:1: rule__Mapping__Group_9_2_0_1__0__Impl : ( 'use' ) ;
    public final void rule__Mapping__Group_9_2_0_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1813:1: ( ( 'use' ) )
            // InternalTomMapping.g:1814:1: ( 'use' )
            {
            // InternalTomMapping.g:1814:1: ( 'use' )
            // InternalTomMapping.g:1815:2: 'use'
            {
             before(grammarAccess.getMappingAccess().getUseKeyword_9_2_0_1_0()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getMappingAccess().getUseKeyword_9_2_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_9_2_0_1__0__Impl"


    // $ANTLR start "rule__Mapping__Group_9_2_0_1__1"
    // InternalTomMapping.g:1824:1: rule__Mapping__Group_9_2_0_1__1 : rule__Mapping__Group_9_2_0_1__1__Impl rule__Mapping__Group_9_2_0_1__2 ;
    public final void rule__Mapping__Group_9_2_0_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1828:1: ( rule__Mapping__Group_9_2_0_1__1__Impl rule__Mapping__Group_9_2_0_1__2 )
            // InternalTomMapping.g:1829:2: rule__Mapping__Group_9_2_0_1__1__Impl rule__Mapping__Group_9_2_0_1__2
            {
            pushFollow(FOLLOW_3);
            rule__Mapping__Group_9_2_0_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group_9_2_0_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_9_2_0_1__1"


    // $ANTLR start "rule__Mapping__Group_9_2_0_1__1__Impl"
    // InternalTomMapping.g:1836:1: rule__Mapping__Group_9_2_0_1__1__Impl : ( '{' ) ;
    public final void rule__Mapping__Group_9_2_0_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1840:1: ( ( '{' ) )
            // InternalTomMapping.g:1841:1: ( '{' )
            {
            // InternalTomMapping.g:1841:1: ( '{' )
            // InternalTomMapping.g:1842:2: '{'
            {
             before(grammarAccess.getMappingAccess().getLeftCurlyBracketKeyword_9_2_0_1_1()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getMappingAccess().getLeftCurlyBracketKeyword_9_2_0_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_9_2_0_1__1__Impl"


    // $ANTLR start "rule__Mapping__Group_9_2_0_1__2"
    // InternalTomMapping.g:1851:1: rule__Mapping__Group_9_2_0_1__2 : rule__Mapping__Group_9_2_0_1__2__Impl rule__Mapping__Group_9_2_0_1__3 ;
    public final void rule__Mapping__Group_9_2_0_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1855:1: ( rule__Mapping__Group_9_2_0_1__2__Impl rule__Mapping__Group_9_2_0_1__3 )
            // InternalTomMapping.g:1856:2: rule__Mapping__Group_9_2_0_1__2__Impl rule__Mapping__Group_9_2_0_1__3
            {
            pushFollow(FOLLOW_14);
            rule__Mapping__Group_9_2_0_1__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group_9_2_0_1__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_9_2_0_1__2"


    // $ANTLR start "rule__Mapping__Group_9_2_0_1__2__Impl"
    // InternalTomMapping.g:1863:1: rule__Mapping__Group_9_2_0_1__2__Impl : ( ( rule__Mapping__ExternalTerminalsAssignment_9_2_0_1_2 ) ) ;
    public final void rule__Mapping__Group_9_2_0_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1867:1: ( ( ( rule__Mapping__ExternalTerminalsAssignment_9_2_0_1_2 ) ) )
            // InternalTomMapping.g:1868:1: ( ( rule__Mapping__ExternalTerminalsAssignment_9_2_0_1_2 ) )
            {
            // InternalTomMapping.g:1868:1: ( ( rule__Mapping__ExternalTerminalsAssignment_9_2_0_1_2 ) )
            // InternalTomMapping.g:1869:2: ( rule__Mapping__ExternalTerminalsAssignment_9_2_0_1_2 )
            {
             before(grammarAccess.getMappingAccess().getExternalTerminalsAssignment_9_2_0_1_2()); 
            // InternalTomMapping.g:1870:2: ( rule__Mapping__ExternalTerminalsAssignment_9_2_0_1_2 )
            // InternalTomMapping.g:1870:3: rule__Mapping__ExternalTerminalsAssignment_9_2_0_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Mapping__ExternalTerminalsAssignment_9_2_0_1_2();

            state._fsp--;


            }

             after(grammarAccess.getMappingAccess().getExternalTerminalsAssignment_9_2_0_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_9_2_0_1__2__Impl"


    // $ANTLR start "rule__Mapping__Group_9_2_0_1__3"
    // InternalTomMapping.g:1878:1: rule__Mapping__Group_9_2_0_1__3 : rule__Mapping__Group_9_2_0_1__3__Impl rule__Mapping__Group_9_2_0_1__4 ;
    public final void rule__Mapping__Group_9_2_0_1__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1882:1: ( rule__Mapping__Group_9_2_0_1__3__Impl rule__Mapping__Group_9_2_0_1__4 )
            // InternalTomMapping.g:1883:2: rule__Mapping__Group_9_2_0_1__3__Impl rule__Mapping__Group_9_2_0_1__4
            {
            pushFollow(FOLLOW_14);
            rule__Mapping__Group_9_2_0_1__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group_9_2_0_1__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_9_2_0_1__3"


    // $ANTLR start "rule__Mapping__Group_9_2_0_1__3__Impl"
    // InternalTomMapping.g:1890:1: rule__Mapping__Group_9_2_0_1__3__Impl : ( ( rule__Mapping__Group_9_2_0_1_3__0 )* ) ;
    public final void rule__Mapping__Group_9_2_0_1__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1894:1: ( ( ( rule__Mapping__Group_9_2_0_1_3__0 )* ) )
            // InternalTomMapping.g:1895:1: ( ( rule__Mapping__Group_9_2_0_1_3__0 )* )
            {
            // InternalTomMapping.g:1895:1: ( ( rule__Mapping__Group_9_2_0_1_3__0 )* )
            // InternalTomMapping.g:1896:2: ( rule__Mapping__Group_9_2_0_1_3__0 )*
            {
             before(grammarAccess.getMappingAccess().getGroup_9_2_0_1_3()); 
            // InternalTomMapping.g:1897:2: ( rule__Mapping__Group_9_2_0_1_3__0 )*
            loop19:
            do {
                int alt19=2;
                int LA19_0 = input.LA(1);

                if ( (LA19_0==21) ) {
                    alt19=1;
                }


                switch (alt19) {
            	case 1 :
            	    // InternalTomMapping.g:1897:3: rule__Mapping__Group_9_2_0_1_3__0
            	    {
            	    pushFollow(FOLLOW_15);
            	    rule__Mapping__Group_9_2_0_1_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop19;
                }
            } while (true);

             after(grammarAccess.getMappingAccess().getGroup_9_2_0_1_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_9_2_0_1__3__Impl"


    // $ANTLR start "rule__Mapping__Group_9_2_0_1__4"
    // InternalTomMapping.g:1905:1: rule__Mapping__Group_9_2_0_1__4 : rule__Mapping__Group_9_2_0_1__4__Impl ;
    public final void rule__Mapping__Group_9_2_0_1__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1909:1: ( rule__Mapping__Group_9_2_0_1__4__Impl )
            // InternalTomMapping.g:1910:2: rule__Mapping__Group_9_2_0_1__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Mapping__Group_9_2_0_1__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_9_2_0_1__4"


    // $ANTLR start "rule__Mapping__Group_9_2_0_1__4__Impl"
    // InternalTomMapping.g:1916:1: rule__Mapping__Group_9_2_0_1__4__Impl : ( '}' ) ;
    public final void rule__Mapping__Group_9_2_0_1__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1920:1: ( ( '}' ) )
            // InternalTomMapping.g:1921:1: ( '}' )
            {
            // InternalTomMapping.g:1921:1: ( '}' )
            // InternalTomMapping.g:1922:2: '}'
            {
             before(grammarAccess.getMappingAccess().getRightCurlyBracketKeyword_9_2_0_1_4()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getMappingAccess().getRightCurlyBracketKeyword_9_2_0_1_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_9_2_0_1__4__Impl"


    // $ANTLR start "rule__Mapping__Group_9_2_0_1_3__0"
    // InternalTomMapping.g:1932:1: rule__Mapping__Group_9_2_0_1_3__0 : rule__Mapping__Group_9_2_0_1_3__0__Impl rule__Mapping__Group_9_2_0_1_3__1 ;
    public final void rule__Mapping__Group_9_2_0_1_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1936:1: ( rule__Mapping__Group_9_2_0_1_3__0__Impl rule__Mapping__Group_9_2_0_1_3__1 )
            // InternalTomMapping.g:1937:2: rule__Mapping__Group_9_2_0_1_3__0__Impl rule__Mapping__Group_9_2_0_1_3__1
            {
            pushFollow(FOLLOW_3);
            rule__Mapping__Group_9_2_0_1_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group_9_2_0_1_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_9_2_0_1_3__0"


    // $ANTLR start "rule__Mapping__Group_9_2_0_1_3__0__Impl"
    // InternalTomMapping.g:1944:1: rule__Mapping__Group_9_2_0_1_3__0__Impl : ( ',' ) ;
    public final void rule__Mapping__Group_9_2_0_1_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1948:1: ( ( ',' ) )
            // InternalTomMapping.g:1949:1: ( ',' )
            {
            // InternalTomMapping.g:1949:1: ( ',' )
            // InternalTomMapping.g:1950:2: ','
            {
             before(grammarAccess.getMappingAccess().getCommaKeyword_9_2_0_1_3_0()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getMappingAccess().getCommaKeyword_9_2_0_1_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_9_2_0_1_3__0__Impl"


    // $ANTLR start "rule__Mapping__Group_9_2_0_1_3__1"
    // InternalTomMapping.g:1959:1: rule__Mapping__Group_9_2_0_1_3__1 : rule__Mapping__Group_9_2_0_1_3__1__Impl ;
    public final void rule__Mapping__Group_9_2_0_1_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1963:1: ( rule__Mapping__Group_9_2_0_1_3__1__Impl )
            // InternalTomMapping.g:1964:2: rule__Mapping__Group_9_2_0_1_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Mapping__Group_9_2_0_1_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_9_2_0_1_3__1"


    // $ANTLR start "rule__Mapping__Group_9_2_0_1_3__1__Impl"
    // InternalTomMapping.g:1970:1: rule__Mapping__Group_9_2_0_1_3__1__Impl : ( ( rule__Mapping__ExternalTerminalsAssignment_9_2_0_1_3_1 ) ) ;
    public final void rule__Mapping__Group_9_2_0_1_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1974:1: ( ( ( rule__Mapping__ExternalTerminalsAssignment_9_2_0_1_3_1 ) ) )
            // InternalTomMapping.g:1975:1: ( ( rule__Mapping__ExternalTerminalsAssignment_9_2_0_1_3_1 ) )
            {
            // InternalTomMapping.g:1975:1: ( ( rule__Mapping__ExternalTerminalsAssignment_9_2_0_1_3_1 ) )
            // InternalTomMapping.g:1976:2: ( rule__Mapping__ExternalTerminalsAssignment_9_2_0_1_3_1 )
            {
             before(grammarAccess.getMappingAccess().getExternalTerminalsAssignment_9_2_0_1_3_1()); 
            // InternalTomMapping.g:1977:2: ( rule__Mapping__ExternalTerminalsAssignment_9_2_0_1_3_1 )
            // InternalTomMapping.g:1977:3: rule__Mapping__ExternalTerminalsAssignment_9_2_0_1_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Mapping__ExternalTerminalsAssignment_9_2_0_1_3_1();

            state._fsp--;


            }

             after(grammarAccess.getMappingAccess().getExternalTerminalsAssignment_9_2_0_1_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_9_2_0_1_3__1__Impl"


    // $ANTLR start "rule__Mapping__Group_9_2_1__0"
    // InternalTomMapping.g:1986:1: rule__Mapping__Group_9_2_1__0 : rule__Mapping__Group_9_2_1__0__Impl rule__Mapping__Group_9_2_1__1 ;
    public final void rule__Mapping__Group_9_2_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:1990:1: ( rule__Mapping__Group_9_2_1__0__Impl rule__Mapping__Group_9_2_1__1 )
            // InternalTomMapping.g:1991:2: rule__Mapping__Group_9_2_1__0__Impl rule__Mapping__Group_9_2_1__1
            {
            pushFollow(FOLLOW_16);
            rule__Mapping__Group_9_2_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group_9_2_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_9_2_1__0"


    // $ANTLR start "rule__Mapping__Group_9_2_1__0__Impl"
    // InternalTomMapping.g:1998:1: rule__Mapping__Group_9_2_1__0__Impl : ( ( rule__Mapping__TerminalsAssignment_9_2_1_0 ) ) ;
    public final void rule__Mapping__Group_9_2_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2002:1: ( ( ( rule__Mapping__TerminalsAssignment_9_2_1_0 ) ) )
            // InternalTomMapping.g:2003:1: ( ( rule__Mapping__TerminalsAssignment_9_2_1_0 ) )
            {
            // InternalTomMapping.g:2003:1: ( ( rule__Mapping__TerminalsAssignment_9_2_1_0 ) )
            // InternalTomMapping.g:2004:2: ( rule__Mapping__TerminalsAssignment_9_2_1_0 )
            {
             before(grammarAccess.getMappingAccess().getTerminalsAssignment_9_2_1_0()); 
            // InternalTomMapping.g:2005:2: ( rule__Mapping__TerminalsAssignment_9_2_1_0 )
            // InternalTomMapping.g:2005:3: rule__Mapping__TerminalsAssignment_9_2_1_0
            {
            pushFollow(FOLLOW_2);
            rule__Mapping__TerminalsAssignment_9_2_1_0();

            state._fsp--;


            }

             after(grammarAccess.getMappingAccess().getTerminalsAssignment_9_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_9_2_1__0__Impl"


    // $ANTLR start "rule__Mapping__Group_9_2_1__1"
    // InternalTomMapping.g:2013:1: rule__Mapping__Group_9_2_1__1 : rule__Mapping__Group_9_2_1__1__Impl ;
    public final void rule__Mapping__Group_9_2_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2017:1: ( rule__Mapping__Group_9_2_1__1__Impl )
            // InternalTomMapping.g:2018:2: rule__Mapping__Group_9_2_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Mapping__Group_9_2_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_9_2_1__1"


    // $ANTLR start "rule__Mapping__Group_9_2_1__1__Impl"
    // InternalTomMapping.g:2024:1: rule__Mapping__Group_9_2_1__1__Impl : ( ( rule__Mapping__Group_9_2_1_1__0 )* ) ;
    public final void rule__Mapping__Group_9_2_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2028:1: ( ( ( rule__Mapping__Group_9_2_1_1__0 )* ) )
            // InternalTomMapping.g:2029:1: ( ( rule__Mapping__Group_9_2_1_1__0 )* )
            {
            // InternalTomMapping.g:2029:1: ( ( rule__Mapping__Group_9_2_1_1__0 )* )
            // InternalTomMapping.g:2030:2: ( rule__Mapping__Group_9_2_1_1__0 )*
            {
             before(grammarAccess.getMappingAccess().getGroup_9_2_1_1()); 
            // InternalTomMapping.g:2031:2: ( rule__Mapping__Group_9_2_1_1__0 )*
            loop20:
            do {
                int alt20=2;
                int LA20_0 = input.LA(1);

                if ( (LA20_0==21) ) {
                    alt20=1;
                }


                switch (alt20) {
            	case 1 :
            	    // InternalTomMapping.g:2031:3: rule__Mapping__Group_9_2_1_1__0
            	    {
            	    pushFollow(FOLLOW_15);
            	    rule__Mapping__Group_9_2_1_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop20;
                }
            } while (true);

             after(grammarAccess.getMappingAccess().getGroup_9_2_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_9_2_1__1__Impl"


    // $ANTLR start "rule__Mapping__Group_9_2_1_1__0"
    // InternalTomMapping.g:2040:1: rule__Mapping__Group_9_2_1_1__0 : rule__Mapping__Group_9_2_1_1__0__Impl rule__Mapping__Group_9_2_1_1__1 ;
    public final void rule__Mapping__Group_9_2_1_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2044:1: ( rule__Mapping__Group_9_2_1_1__0__Impl rule__Mapping__Group_9_2_1_1__1 )
            // InternalTomMapping.g:2045:2: rule__Mapping__Group_9_2_1_1__0__Impl rule__Mapping__Group_9_2_1_1__1
            {
            pushFollow(FOLLOW_11);
            rule__Mapping__Group_9_2_1_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group_9_2_1_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_9_2_1_1__0"


    // $ANTLR start "rule__Mapping__Group_9_2_1_1__0__Impl"
    // InternalTomMapping.g:2052:1: rule__Mapping__Group_9_2_1_1__0__Impl : ( ',' ) ;
    public final void rule__Mapping__Group_9_2_1_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2056:1: ( ( ',' ) )
            // InternalTomMapping.g:2057:1: ( ',' )
            {
            // InternalTomMapping.g:2057:1: ( ',' )
            // InternalTomMapping.g:2058:2: ','
            {
             before(grammarAccess.getMappingAccess().getCommaKeyword_9_2_1_1_0()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getMappingAccess().getCommaKeyword_9_2_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_9_2_1_1__0__Impl"


    // $ANTLR start "rule__Mapping__Group_9_2_1_1__1"
    // InternalTomMapping.g:2067:1: rule__Mapping__Group_9_2_1_1__1 : rule__Mapping__Group_9_2_1_1__1__Impl ;
    public final void rule__Mapping__Group_9_2_1_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2071:1: ( rule__Mapping__Group_9_2_1_1__1__Impl )
            // InternalTomMapping.g:2072:2: rule__Mapping__Group_9_2_1_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Mapping__Group_9_2_1_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_9_2_1_1__1"


    // $ANTLR start "rule__Mapping__Group_9_2_1_1__1__Impl"
    // InternalTomMapping.g:2078:1: rule__Mapping__Group_9_2_1_1__1__Impl : ( ( rule__Mapping__TerminalsAssignment_9_2_1_1_1 ) ) ;
    public final void rule__Mapping__Group_9_2_1_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2082:1: ( ( ( rule__Mapping__TerminalsAssignment_9_2_1_1_1 ) ) )
            // InternalTomMapping.g:2083:1: ( ( rule__Mapping__TerminalsAssignment_9_2_1_1_1 ) )
            {
            // InternalTomMapping.g:2083:1: ( ( rule__Mapping__TerminalsAssignment_9_2_1_1_1 ) )
            // InternalTomMapping.g:2084:2: ( rule__Mapping__TerminalsAssignment_9_2_1_1_1 )
            {
             before(grammarAccess.getMappingAccess().getTerminalsAssignment_9_2_1_1_1()); 
            // InternalTomMapping.g:2085:2: ( rule__Mapping__TerminalsAssignment_9_2_1_1_1 )
            // InternalTomMapping.g:2085:3: rule__Mapping__TerminalsAssignment_9_2_1_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Mapping__TerminalsAssignment_9_2_1_1_1();

            state._fsp--;


            }

             after(grammarAccess.getMappingAccess().getTerminalsAssignment_9_2_1_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_9_2_1_1__1__Impl"


    // $ANTLR start "rule__Mapping__Group_10_0__0"
    // InternalTomMapping.g:2094:1: rule__Mapping__Group_10_0__0 : rule__Mapping__Group_10_0__0__Impl rule__Mapping__Group_10_0__1 ;
    public final void rule__Mapping__Group_10_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2098:1: ( rule__Mapping__Group_10_0__0__Impl rule__Mapping__Group_10_0__1 )
            // InternalTomMapping.g:2099:2: rule__Mapping__Group_10_0__0__Impl rule__Mapping__Group_10_0__1
            {
            pushFollow(FOLLOW_10);
            rule__Mapping__Group_10_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group_10_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_10_0__0"


    // $ANTLR start "rule__Mapping__Group_10_0__0__Impl"
    // InternalTomMapping.g:2106:1: rule__Mapping__Group_10_0__0__Impl : ( 'operators' ) ;
    public final void rule__Mapping__Group_10_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2110:1: ( ( 'operators' ) )
            // InternalTomMapping.g:2111:1: ( 'operators' )
            {
            // InternalTomMapping.g:2111:1: ( 'operators' )
            // InternalTomMapping.g:2112:2: 'operators'
            {
             before(grammarAccess.getMappingAccess().getOperatorsKeyword_10_0_0()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getMappingAccess().getOperatorsKeyword_10_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_10_0__0__Impl"


    // $ANTLR start "rule__Mapping__Group_10_0__1"
    // InternalTomMapping.g:2121:1: rule__Mapping__Group_10_0__1 : rule__Mapping__Group_10_0__1__Impl rule__Mapping__Group_10_0__2 ;
    public final void rule__Mapping__Group_10_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2125:1: ( rule__Mapping__Group_10_0__1__Impl rule__Mapping__Group_10_0__2 )
            // InternalTomMapping.g:2126:2: rule__Mapping__Group_10_0__1__Impl rule__Mapping__Group_10_0__2
            {
            pushFollow(FOLLOW_17);
            rule__Mapping__Group_10_0__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group_10_0__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_10_0__1"


    // $ANTLR start "rule__Mapping__Group_10_0__1__Impl"
    // InternalTomMapping.g:2133:1: rule__Mapping__Group_10_0__1__Impl : ( '{' ) ;
    public final void rule__Mapping__Group_10_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2137:1: ( ( '{' ) )
            // InternalTomMapping.g:2138:1: ( '{' )
            {
            // InternalTomMapping.g:2138:1: ( '{' )
            // InternalTomMapping.g:2139:2: '{'
            {
             before(grammarAccess.getMappingAccess().getLeftCurlyBracketKeyword_10_0_1()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getMappingAccess().getLeftCurlyBracketKeyword_10_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_10_0__1__Impl"


    // $ANTLR start "rule__Mapping__Group_10_0__2"
    // InternalTomMapping.g:2148:1: rule__Mapping__Group_10_0__2 : rule__Mapping__Group_10_0__2__Impl rule__Mapping__Group_10_0__3 ;
    public final void rule__Mapping__Group_10_0__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2152:1: ( rule__Mapping__Group_10_0__2__Impl rule__Mapping__Group_10_0__3 )
            // InternalTomMapping.g:2153:2: rule__Mapping__Group_10_0__2__Impl rule__Mapping__Group_10_0__3
            {
            pushFollow(FOLLOW_4);
            rule__Mapping__Group_10_0__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group_10_0__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_10_0__2"


    // $ANTLR start "rule__Mapping__Group_10_0__2__Impl"
    // InternalTomMapping.g:2160:1: rule__Mapping__Group_10_0__2__Impl : ( ( rule__Mapping__OperatorsAssignment_10_0_2 ) ) ;
    public final void rule__Mapping__Group_10_0__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2164:1: ( ( ( rule__Mapping__OperatorsAssignment_10_0_2 ) ) )
            // InternalTomMapping.g:2165:1: ( ( rule__Mapping__OperatorsAssignment_10_0_2 ) )
            {
            // InternalTomMapping.g:2165:1: ( ( rule__Mapping__OperatorsAssignment_10_0_2 ) )
            // InternalTomMapping.g:2166:2: ( rule__Mapping__OperatorsAssignment_10_0_2 )
            {
             before(grammarAccess.getMappingAccess().getOperatorsAssignment_10_0_2()); 
            // InternalTomMapping.g:2167:2: ( rule__Mapping__OperatorsAssignment_10_0_2 )
            // InternalTomMapping.g:2167:3: rule__Mapping__OperatorsAssignment_10_0_2
            {
            pushFollow(FOLLOW_2);
            rule__Mapping__OperatorsAssignment_10_0_2();

            state._fsp--;


            }

             after(grammarAccess.getMappingAccess().getOperatorsAssignment_10_0_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_10_0__2__Impl"


    // $ANTLR start "rule__Mapping__Group_10_0__3"
    // InternalTomMapping.g:2175:1: rule__Mapping__Group_10_0__3 : rule__Mapping__Group_10_0__3__Impl rule__Mapping__Group_10_0__4 ;
    public final void rule__Mapping__Group_10_0__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2179:1: ( rule__Mapping__Group_10_0__3__Impl rule__Mapping__Group_10_0__4 )
            // InternalTomMapping.g:2180:2: rule__Mapping__Group_10_0__3__Impl rule__Mapping__Group_10_0__4
            {
            pushFollow(FOLLOW_4);
            rule__Mapping__Group_10_0__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group_10_0__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_10_0__3"


    // $ANTLR start "rule__Mapping__Group_10_0__3__Impl"
    // InternalTomMapping.g:2187:1: rule__Mapping__Group_10_0__3__Impl : ( ( rule__Mapping__Group_10_0_3__0 )* ) ;
    public final void rule__Mapping__Group_10_0__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2191:1: ( ( ( rule__Mapping__Group_10_0_3__0 )* ) )
            // InternalTomMapping.g:2192:1: ( ( rule__Mapping__Group_10_0_3__0 )* )
            {
            // InternalTomMapping.g:2192:1: ( ( rule__Mapping__Group_10_0_3__0 )* )
            // InternalTomMapping.g:2193:2: ( rule__Mapping__Group_10_0_3__0 )*
            {
             before(grammarAccess.getMappingAccess().getGroup_10_0_3()); 
            // InternalTomMapping.g:2194:2: ( rule__Mapping__Group_10_0_3__0 )*
            loop21:
            do {
                int alt21=2;
                int LA21_0 = input.LA(1);

                if ( (LA21_0==14) ) {
                    int LA21_1 = input.LA(2);

                    if ( (LA21_1==29||LA21_1==33) ) {
                        alt21=1;
                    }


                }


                switch (alt21) {
            	case 1 :
            	    // InternalTomMapping.g:2194:3: rule__Mapping__Group_10_0_3__0
            	    {
            	    pushFollow(FOLLOW_18);
            	    rule__Mapping__Group_10_0_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop21;
                }
            } while (true);

             after(grammarAccess.getMappingAccess().getGroup_10_0_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_10_0__3__Impl"


    // $ANTLR start "rule__Mapping__Group_10_0__4"
    // InternalTomMapping.g:2202:1: rule__Mapping__Group_10_0__4 : rule__Mapping__Group_10_0__4__Impl rule__Mapping__Group_10_0__5 ;
    public final void rule__Mapping__Group_10_0__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2206:1: ( rule__Mapping__Group_10_0__4__Impl rule__Mapping__Group_10_0__5 )
            // InternalTomMapping.g:2207:2: rule__Mapping__Group_10_0__4__Impl rule__Mapping__Group_10_0__5
            {
            pushFollow(FOLLOW_12);
            rule__Mapping__Group_10_0__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group_10_0__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_10_0__4"


    // $ANTLR start "rule__Mapping__Group_10_0__4__Impl"
    // InternalTomMapping.g:2214:1: rule__Mapping__Group_10_0__4__Impl : ( ';' ) ;
    public final void rule__Mapping__Group_10_0__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2218:1: ( ( ';' ) )
            // InternalTomMapping.g:2219:1: ( ';' )
            {
            // InternalTomMapping.g:2219:1: ( ';' )
            // InternalTomMapping.g:2220:2: ';'
            {
             before(grammarAccess.getMappingAccess().getSemicolonKeyword_10_0_4()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getMappingAccess().getSemicolonKeyword_10_0_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_10_0__4__Impl"


    // $ANTLR start "rule__Mapping__Group_10_0__5"
    // InternalTomMapping.g:2229:1: rule__Mapping__Group_10_0__5 : rule__Mapping__Group_10_0__5__Impl ;
    public final void rule__Mapping__Group_10_0__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2233:1: ( rule__Mapping__Group_10_0__5__Impl )
            // InternalTomMapping.g:2234:2: rule__Mapping__Group_10_0__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Mapping__Group_10_0__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_10_0__5"


    // $ANTLR start "rule__Mapping__Group_10_0__5__Impl"
    // InternalTomMapping.g:2240:1: rule__Mapping__Group_10_0__5__Impl : ( '}' ) ;
    public final void rule__Mapping__Group_10_0__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2244:1: ( ( '}' ) )
            // InternalTomMapping.g:2245:1: ( '}' )
            {
            // InternalTomMapping.g:2245:1: ( '}' )
            // InternalTomMapping.g:2246:2: '}'
            {
             before(grammarAccess.getMappingAccess().getRightCurlyBracketKeyword_10_0_5()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getMappingAccess().getRightCurlyBracketKeyword_10_0_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_10_0__5__Impl"


    // $ANTLR start "rule__Mapping__Group_10_0_3__0"
    // InternalTomMapping.g:2256:1: rule__Mapping__Group_10_0_3__0 : rule__Mapping__Group_10_0_3__0__Impl rule__Mapping__Group_10_0_3__1 ;
    public final void rule__Mapping__Group_10_0_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2260:1: ( rule__Mapping__Group_10_0_3__0__Impl rule__Mapping__Group_10_0_3__1 )
            // InternalTomMapping.g:2261:2: rule__Mapping__Group_10_0_3__0__Impl rule__Mapping__Group_10_0_3__1
            {
            pushFollow(FOLLOW_17);
            rule__Mapping__Group_10_0_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group_10_0_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_10_0_3__0"


    // $ANTLR start "rule__Mapping__Group_10_0_3__0__Impl"
    // InternalTomMapping.g:2268:1: rule__Mapping__Group_10_0_3__0__Impl : ( ';' ) ;
    public final void rule__Mapping__Group_10_0_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2272:1: ( ( ';' ) )
            // InternalTomMapping.g:2273:1: ( ';' )
            {
            // InternalTomMapping.g:2273:1: ( ';' )
            // InternalTomMapping.g:2274:2: ';'
            {
             before(grammarAccess.getMappingAccess().getSemicolonKeyword_10_0_3_0()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getMappingAccess().getSemicolonKeyword_10_0_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_10_0_3__0__Impl"


    // $ANTLR start "rule__Mapping__Group_10_0_3__1"
    // InternalTomMapping.g:2283:1: rule__Mapping__Group_10_0_3__1 : rule__Mapping__Group_10_0_3__1__Impl ;
    public final void rule__Mapping__Group_10_0_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2287:1: ( rule__Mapping__Group_10_0_3__1__Impl )
            // InternalTomMapping.g:2288:2: rule__Mapping__Group_10_0_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Mapping__Group_10_0_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_10_0_3__1"


    // $ANTLR start "rule__Mapping__Group_10_0_3__1__Impl"
    // InternalTomMapping.g:2294:1: rule__Mapping__Group_10_0_3__1__Impl : ( ( rule__Mapping__OperatorsAssignment_10_0_3_1 ) ) ;
    public final void rule__Mapping__Group_10_0_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2298:1: ( ( ( rule__Mapping__OperatorsAssignment_10_0_3_1 ) ) )
            // InternalTomMapping.g:2299:1: ( ( rule__Mapping__OperatorsAssignment_10_0_3_1 ) )
            {
            // InternalTomMapping.g:2299:1: ( ( rule__Mapping__OperatorsAssignment_10_0_3_1 ) )
            // InternalTomMapping.g:2300:2: ( rule__Mapping__OperatorsAssignment_10_0_3_1 )
            {
             before(grammarAccess.getMappingAccess().getOperatorsAssignment_10_0_3_1()); 
            // InternalTomMapping.g:2301:2: ( rule__Mapping__OperatorsAssignment_10_0_3_1 )
            // InternalTomMapping.g:2301:3: rule__Mapping__OperatorsAssignment_10_0_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Mapping__OperatorsAssignment_10_0_3_1();

            state._fsp--;


            }

             after(grammarAccess.getMappingAccess().getOperatorsAssignment_10_0_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group_10_0_3__1__Impl"


    // $ANTLR start "rule__InstanceMapping__Group__0"
    // InternalTomMapping.g:2310:1: rule__InstanceMapping__Group__0 : rule__InstanceMapping__Group__0__Impl rule__InstanceMapping__Group__1 ;
    public final void rule__InstanceMapping__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2314:1: ( rule__InstanceMapping__Group__0__Impl rule__InstanceMapping__Group__1 )
            // InternalTomMapping.g:2315:2: rule__InstanceMapping__Group__0__Impl rule__InstanceMapping__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__InstanceMapping__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__InstanceMapping__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InstanceMapping__Group__0"


    // $ANTLR start "rule__InstanceMapping__Group__0__Impl"
    // InternalTomMapping.g:2322:1: rule__InstanceMapping__Group__0__Impl : ( 'custom' ) ;
    public final void rule__InstanceMapping__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2326:1: ( ( 'custom' ) )
            // InternalTomMapping.g:2327:1: ( 'custom' )
            {
            // InternalTomMapping.g:2327:1: ( 'custom' )
            // InternalTomMapping.g:2328:2: 'custom'
            {
             before(grammarAccess.getInstanceMappingAccess().getCustomKeyword_0()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getInstanceMappingAccess().getCustomKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InstanceMapping__Group__0__Impl"


    // $ANTLR start "rule__InstanceMapping__Group__1"
    // InternalTomMapping.g:2337:1: rule__InstanceMapping__Group__1 : rule__InstanceMapping__Group__1__Impl rule__InstanceMapping__Group__2 ;
    public final void rule__InstanceMapping__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2341:1: ( rule__InstanceMapping__Group__1__Impl rule__InstanceMapping__Group__2 )
            // InternalTomMapping.g:2342:2: rule__InstanceMapping__Group__1__Impl rule__InstanceMapping__Group__2
            {
            pushFollow(FOLLOW_10);
            rule__InstanceMapping__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__InstanceMapping__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InstanceMapping__Group__1"


    // $ANTLR start "rule__InstanceMapping__Group__1__Impl"
    // InternalTomMapping.g:2349:1: rule__InstanceMapping__Group__1__Impl : ( ( rule__InstanceMapping__EpackageAssignment_1 ) ) ;
    public final void rule__InstanceMapping__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2353:1: ( ( ( rule__InstanceMapping__EpackageAssignment_1 ) ) )
            // InternalTomMapping.g:2354:1: ( ( rule__InstanceMapping__EpackageAssignment_1 ) )
            {
            // InternalTomMapping.g:2354:1: ( ( rule__InstanceMapping__EpackageAssignment_1 ) )
            // InternalTomMapping.g:2355:2: ( rule__InstanceMapping__EpackageAssignment_1 )
            {
             before(grammarAccess.getInstanceMappingAccess().getEpackageAssignment_1()); 
            // InternalTomMapping.g:2356:2: ( rule__InstanceMapping__EpackageAssignment_1 )
            // InternalTomMapping.g:2356:3: rule__InstanceMapping__EpackageAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__InstanceMapping__EpackageAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getInstanceMappingAccess().getEpackageAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InstanceMapping__Group__1__Impl"


    // $ANTLR start "rule__InstanceMapping__Group__2"
    // InternalTomMapping.g:2364:1: rule__InstanceMapping__Group__2 : rule__InstanceMapping__Group__2__Impl rule__InstanceMapping__Group__3 ;
    public final void rule__InstanceMapping__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2368:1: ( rule__InstanceMapping__Group__2__Impl rule__InstanceMapping__Group__3 )
            // InternalTomMapping.g:2369:2: rule__InstanceMapping__Group__2__Impl rule__InstanceMapping__Group__3
            {
            pushFollow(FOLLOW_19);
            rule__InstanceMapping__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__InstanceMapping__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InstanceMapping__Group__2"


    // $ANTLR start "rule__InstanceMapping__Group__2__Impl"
    // InternalTomMapping.g:2376:1: rule__InstanceMapping__Group__2__Impl : ( '{' ) ;
    public final void rule__InstanceMapping__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2380:1: ( ( '{' ) )
            // InternalTomMapping.g:2381:1: ( '{' )
            {
            // InternalTomMapping.g:2381:1: ( '{' )
            // InternalTomMapping.g:2382:2: '{'
            {
             before(grammarAccess.getInstanceMappingAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getInstanceMappingAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InstanceMapping__Group__2__Impl"


    // $ANTLR start "rule__InstanceMapping__Group__3"
    // InternalTomMapping.g:2391:1: rule__InstanceMapping__Group__3 : rule__InstanceMapping__Group__3__Impl rule__InstanceMapping__Group__4 ;
    public final void rule__InstanceMapping__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2395:1: ( rule__InstanceMapping__Group__3__Impl rule__InstanceMapping__Group__4 )
            // InternalTomMapping.g:2396:2: rule__InstanceMapping__Group__3__Impl rule__InstanceMapping__Group__4
            {
            pushFollow(FOLLOW_19);
            rule__InstanceMapping__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__InstanceMapping__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InstanceMapping__Group__3"


    // $ANTLR start "rule__InstanceMapping__Group__3__Impl"
    // InternalTomMapping.g:2403:1: rule__InstanceMapping__Group__3__Impl : ( ( rule__InstanceMapping__Group_3__0 )? ) ;
    public final void rule__InstanceMapping__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2407:1: ( ( ( rule__InstanceMapping__Group_3__0 )? ) )
            // InternalTomMapping.g:2408:1: ( ( rule__InstanceMapping__Group_3__0 )? )
            {
            // InternalTomMapping.g:2408:1: ( ( rule__InstanceMapping__Group_3__0 )? )
            // InternalTomMapping.g:2409:2: ( rule__InstanceMapping__Group_3__0 )?
            {
             before(grammarAccess.getInstanceMappingAccess().getGroup_3()); 
            // InternalTomMapping.g:2410:2: ( rule__InstanceMapping__Group_3__0 )?
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( (LA22_0==16) ) {
                alt22=1;
            }
            switch (alt22) {
                case 1 :
                    // InternalTomMapping.g:2410:3: rule__InstanceMapping__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__InstanceMapping__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getInstanceMappingAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InstanceMapping__Group__3__Impl"


    // $ANTLR start "rule__InstanceMapping__Group__4"
    // InternalTomMapping.g:2418:1: rule__InstanceMapping__Group__4 : rule__InstanceMapping__Group__4__Impl rule__InstanceMapping__Group__5 ;
    public final void rule__InstanceMapping__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2422:1: ( rule__InstanceMapping__Group__4__Impl rule__InstanceMapping__Group__5 )
            // InternalTomMapping.g:2423:2: rule__InstanceMapping__Group__4__Impl rule__InstanceMapping__Group__5
            {
            pushFollow(FOLLOW_19);
            rule__InstanceMapping__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__InstanceMapping__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InstanceMapping__Group__4"


    // $ANTLR start "rule__InstanceMapping__Group__4__Impl"
    // InternalTomMapping.g:2430:1: rule__InstanceMapping__Group__4__Impl : ( ( rule__InstanceMapping__Group_4__0 )? ) ;
    public final void rule__InstanceMapping__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2434:1: ( ( ( rule__InstanceMapping__Group_4__0 )? ) )
            // InternalTomMapping.g:2435:1: ( ( rule__InstanceMapping__Group_4__0 )? )
            {
            // InternalTomMapping.g:2435:1: ( ( rule__InstanceMapping__Group_4__0 )? )
            // InternalTomMapping.g:2436:2: ( rule__InstanceMapping__Group_4__0 )?
            {
             before(grammarAccess.getInstanceMappingAccess().getGroup_4()); 
            // InternalTomMapping.g:2437:2: ( rule__InstanceMapping__Group_4__0 )?
            int alt23=2;
            int LA23_0 = input.LA(1);

            if ( (LA23_0==25) ) {
                alt23=1;
            }
            switch (alt23) {
                case 1 :
                    // InternalTomMapping.g:2437:3: rule__InstanceMapping__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__InstanceMapping__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getInstanceMappingAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InstanceMapping__Group__4__Impl"


    // $ANTLR start "rule__InstanceMapping__Group__5"
    // InternalTomMapping.g:2445:1: rule__InstanceMapping__Group__5 : rule__InstanceMapping__Group__5__Impl ;
    public final void rule__InstanceMapping__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2449:1: ( rule__InstanceMapping__Group__5__Impl )
            // InternalTomMapping.g:2450:2: rule__InstanceMapping__Group__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__InstanceMapping__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InstanceMapping__Group__5"


    // $ANTLR start "rule__InstanceMapping__Group__5__Impl"
    // InternalTomMapping.g:2456:1: rule__InstanceMapping__Group__5__Impl : ( '}' ) ;
    public final void rule__InstanceMapping__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2460:1: ( ( '}' ) )
            // InternalTomMapping.g:2461:1: ( '}' )
            {
            // InternalTomMapping.g:2461:1: ( '}' )
            // InternalTomMapping.g:2462:2: '}'
            {
             before(grammarAccess.getInstanceMappingAccess().getRightCurlyBracketKeyword_5()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getInstanceMappingAccess().getRightCurlyBracketKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InstanceMapping__Group__5__Impl"


    // $ANTLR start "rule__InstanceMapping__Group_3__0"
    // InternalTomMapping.g:2472:1: rule__InstanceMapping__Group_3__0 : rule__InstanceMapping__Group_3__0__Impl rule__InstanceMapping__Group_3__1 ;
    public final void rule__InstanceMapping__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2476:1: ( rule__InstanceMapping__Group_3__0__Impl rule__InstanceMapping__Group_3__1 )
            // InternalTomMapping.g:2477:2: rule__InstanceMapping__Group_3__0__Impl rule__InstanceMapping__Group_3__1
            {
            pushFollow(FOLLOW_9);
            rule__InstanceMapping__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__InstanceMapping__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InstanceMapping__Group_3__0"


    // $ANTLR start "rule__InstanceMapping__Group_3__0__Impl"
    // InternalTomMapping.g:2484:1: rule__InstanceMapping__Group_3__0__Impl : ( 'package' ) ;
    public final void rule__InstanceMapping__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2488:1: ( ( 'package' ) )
            // InternalTomMapping.g:2489:1: ( 'package' )
            {
            // InternalTomMapping.g:2489:1: ( 'package' )
            // InternalTomMapping.g:2490:2: 'package'
            {
             before(grammarAccess.getInstanceMappingAccess().getPackageKeyword_3_0()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getInstanceMappingAccess().getPackageKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InstanceMapping__Group_3__0__Impl"


    // $ANTLR start "rule__InstanceMapping__Group_3__1"
    // InternalTomMapping.g:2499:1: rule__InstanceMapping__Group_3__1 : rule__InstanceMapping__Group_3__1__Impl rule__InstanceMapping__Group_3__2 ;
    public final void rule__InstanceMapping__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2503:1: ( rule__InstanceMapping__Group_3__1__Impl rule__InstanceMapping__Group_3__2 )
            // InternalTomMapping.g:2504:2: rule__InstanceMapping__Group_3__1__Impl rule__InstanceMapping__Group_3__2
            {
            pushFollow(FOLLOW_4);
            rule__InstanceMapping__Group_3__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__InstanceMapping__Group_3__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InstanceMapping__Group_3__1"


    // $ANTLR start "rule__InstanceMapping__Group_3__1__Impl"
    // InternalTomMapping.g:2511:1: rule__InstanceMapping__Group_3__1__Impl : ( ( rule__InstanceMapping__InstancePackageNameAssignment_3_1 ) ) ;
    public final void rule__InstanceMapping__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2515:1: ( ( ( rule__InstanceMapping__InstancePackageNameAssignment_3_1 ) ) )
            // InternalTomMapping.g:2516:1: ( ( rule__InstanceMapping__InstancePackageNameAssignment_3_1 ) )
            {
            // InternalTomMapping.g:2516:1: ( ( rule__InstanceMapping__InstancePackageNameAssignment_3_1 ) )
            // InternalTomMapping.g:2517:2: ( rule__InstanceMapping__InstancePackageNameAssignment_3_1 )
            {
             before(grammarAccess.getInstanceMappingAccess().getInstancePackageNameAssignment_3_1()); 
            // InternalTomMapping.g:2518:2: ( rule__InstanceMapping__InstancePackageNameAssignment_3_1 )
            // InternalTomMapping.g:2518:3: rule__InstanceMapping__InstancePackageNameAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__InstanceMapping__InstancePackageNameAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getInstanceMappingAccess().getInstancePackageNameAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InstanceMapping__Group_3__1__Impl"


    // $ANTLR start "rule__InstanceMapping__Group_3__2"
    // InternalTomMapping.g:2526:1: rule__InstanceMapping__Group_3__2 : rule__InstanceMapping__Group_3__2__Impl ;
    public final void rule__InstanceMapping__Group_3__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2530:1: ( rule__InstanceMapping__Group_3__2__Impl )
            // InternalTomMapping.g:2531:2: rule__InstanceMapping__Group_3__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__InstanceMapping__Group_3__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InstanceMapping__Group_3__2"


    // $ANTLR start "rule__InstanceMapping__Group_3__2__Impl"
    // InternalTomMapping.g:2537:1: rule__InstanceMapping__Group_3__2__Impl : ( ';' ) ;
    public final void rule__InstanceMapping__Group_3__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2541:1: ( ( ';' ) )
            // InternalTomMapping.g:2542:1: ( ';' )
            {
            // InternalTomMapping.g:2542:1: ( ';' )
            // InternalTomMapping.g:2543:2: ';'
            {
             before(grammarAccess.getInstanceMappingAccess().getSemicolonKeyword_3_2()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getInstanceMappingAccess().getSemicolonKeyword_3_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InstanceMapping__Group_3__2__Impl"


    // $ANTLR start "rule__InstanceMapping__Group_4__0"
    // InternalTomMapping.g:2553:1: rule__InstanceMapping__Group_4__0 : rule__InstanceMapping__Group_4__0__Impl rule__InstanceMapping__Group_4__1 ;
    public final void rule__InstanceMapping__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2557:1: ( rule__InstanceMapping__Group_4__0__Impl rule__InstanceMapping__Group_4__1 )
            // InternalTomMapping.g:2558:2: rule__InstanceMapping__Group_4__0__Impl rule__InstanceMapping__Group_4__1
            {
            pushFollow(FOLLOW_9);
            rule__InstanceMapping__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__InstanceMapping__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InstanceMapping__Group_4__0"


    // $ANTLR start "rule__InstanceMapping__Group_4__0__Impl"
    // InternalTomMapping.g:2565:1: rule__InstanceMapping__Group_4__0__Impl : ( 'factory' ) ;
    public final void rule__InstanceMapping__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2569:1: ( ( 'factory' ) )
            // InternalTomMapping.g:2570:1: ( 'factory' )
            {
            // InternalTomMapping.g:2570:1: ( 'factory' )
            // InternalTomMapping.g:2571:2: 'factory'
            {
             before(grammarAccess.getInstanceMappingAccess().getFactoryKeyword_4_0()); 
            match(input,25,FOLLOW_2); 
             after(grammarAccess.getInstanceMappingAccess().getFactoryKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InstanceMapping__Group_4__0__Impl"


    // $ANTLR start "rule__InstanceMapping__Group_4__1"
    // InternalTomMapping.g:2580:1: rule__InstanceMapping__Group_4__1 : rule__InstanceMapping__Group_4__1__Impl rule__InstanceMapping__Group_4__2 ;
    public final void rule__InstanceMapping__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2584:1: ( rule__InstanceMapping__Group_4__1__Impl rule__InstanceMapping__Group_4__2 )
            // InternalTomMapping.g:2585:2: rule__InstanceMapping__Group_4__1__Impl rule__InstanceMapping__Group_4__2
            {
            pushFollow(FOLLOW_4);
            rule__InstanceMapping__Group_4__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__InstanceMapping__Group_4__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InstanceMapping__Group_4__1"


    // $ANTLR start "rule__InstanceMapping__Group_4__1__Impl"
    // InternalTomMapping.g:2592:1: rule__InstanceMapping__Group_4__1__Impl : ( ( rule__InstanceMapping__PackageFactoryNameAssignment_4_1 ) ) ;
    public final void rule__InstanceMapping__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2596:1: ( ( ( rule__InstanceMapping__PackageFactoryNameAssignment_4_1 ) ) )
            // InternalTomMapping.g:2597:1: ( ( rule__InstanceMapping__PackageFactoryNameAssignment_4_1 ) )
            {
            // InternalTomMapping.g:2597:1: ( ( rule__InstanceMapping__PackageFactoryNameAssignment_4_1 ) )
            // InternalTomMapping.g:2598:2: ( rule__InstanceMapping__PackageFactoryNameAssignment_4_1 )
            {
             before(grammarAccess.getInstanceMappingAccess().getPackageFactoryNameAssignment_4_1()); 
            // InternalTomMapping.g:2599:2: ( rule__InstanceMapping__PackageFactoryNameAssignment_4_1 )
            // InternalTomMapping.g:2599:3: rule__InstanceMapping__PackageFactoryNameAssignment_4_1
            {
            pushFollow(FOLLOW_2);
            rule__InstanceMapping__PackageFactoryNameAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getInstanceMappingAccess().getPackageFactoryNameAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InstanceMapping__Group_4__1__Impl"


    // $ANTLR start "rule__InstanceMapping__Group_4__2"
    // InternalTomMapping.g:2607:1: rule__InstanceMapping__Group_4__2 : rule__InstanceMapping__Group_4__2__Impl ;
    public final void rule__InstanceMapping__Group_4__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2611:1: ( rule__InstanceMapping__Group_4__2__Impl )
            // InternalTomMapping.g:2612:2: rule__InstanceMapping__Group_4__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__InstanceMapping__Group_4__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InstanceMapping__Group_4__2"


    // $ANTLR start "rule__InstanceMapping__Group_4__2__Impl"
    // InternalTomMapping.g:2618:1: rule__InstanceMapping__Group_4__2__Impl : ( ';' ) ;
    public final void rule__InstanceMapping__Group_4__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2622:1: ( ( ';' ) )
            // InternalTomMapping.g:2623:1: ( ';' )
            {
            // InternalTomMapping.g:2623:1: ( ';' )
            // InternalTomMapping.g:2624:2: ';'
            {
             before(grammarAccess.getInstanceMappingAccess().getSemicolonKeyword_4_2()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getInstanceMappingAccess().getSemicolonKeyword_4_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InstanceMapping__Group_4__2__Impl"


    // $ANTLR start "rule__Module__Group__0"
    // InternalTomMapping.g:2634:1: rule__Module__Group__0 : rule__Module__Group__0__Impl rule__Module__Group__1 ;
    public final void rule__Module__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2638:1: ( rule__Module__Group__0__Impl rule__Module__Group__1 )
            // InternalTomMapping.g:2639:2: rule__Module__Group__0__Impl rule__Module__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Module__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Module__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__Group__0"


    // $ANTLR start "rule__Module__Group__0__Impl"
    // InternalTomMapping.g:2646:1: rule__Module__Group__0__Impl : ( 'module' ) ;
    public final void rule__Module__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2650:1: ( ( 'module' ) )
            // InternalTomMapping.g:2651:1: ( 'module' )
            {
            // InternalTomMapping.g:2651:1: ( 'module' )
            // InternalTomMapping.g:2652:2: 'module'
            {
             before(grammarAccess.getModuleAccess().getModuleKeyword_0()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getModuleAccess().getModuleKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__Group__0__Impl"


    // $ANTLR start "rule__Module__Group__1"
    // InternalTomMapping.g:2661:1: rule__Module__Group__1 : rule__Module__Group__1__Impl rule__Module__Group__2 ;
    public final void rule__Module__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2665:1: ( rule__Module__Group__1__Impl rule__Module__Group__2 )
            // InternalTomMapping.g:2666:2: rule__Module__Group__1__Impl rule__Module__Group__2
            {
            pushFollow(FOLLOW_10);
            rule__Module__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Module__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__Group__1"


    // $ANTLR start "rule__Module__Group__1__Impl"
    // InternalTomMapping.g:2673:1: rule__Module__Group__1__Impl : ( ( rule__Module__NameAssignment_1 ) ) ;
    public final void rule__Module__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2677:1: ( ( ( rule__Module__NameAssignment_1 ) ) )
            // InternalTomMapping.g:2678:1: ( ( rule__Module__NameAssignment_1 ) )
            {
            // InternalTomMapping.g:2678:1: ( ( rule__Module__NameAssignment_1 ) )
            // InternalTomMapping.g:2679:2: ( rule__Module__NameAssignment_1 )
            {
             before(grammarAccess.getModuleAccess().getNameAssignment_1()); 
            // InternalTomMapping.g:2680:2: ( rule__Module__NameAssignment_1 )
            // InternalTomMapping.g:2680:3: rule__Module__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Module__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getModuleAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__Group__1__Impl"


    // $ANTLR start "rule__Module__Group__2"
    // InternalTomMapping.g:2688:1: rule__Module__Group__2 : rule__Module__Group__2__Impl rule__Module__Group__3 ;
    public final void rule__Module__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2692:1: ( rule__Module__Group__2__Impl rule__Module__Group__3 )
            // InternalTomMapping.g:2693:2: rule__Module__Group__2__Impl rule__Module__Group__3
            {
            pushFollow(FOLLOW_20);
            rule__Module__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Module__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__Group__2"


    // $ANTLR start "rule__Module__Group__2__Impl"
    // InternalTomMapping.g:2700:1: rule__Module__Group__2__Impl : ( '{' ) ;
    public final void rule__Module__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2704:1: ( ( '{' ) )
            // InternalTomMapping.g:2705:1: ( '{' )
            {
            // InternalTomMapping.g:2705:1: ( '{' )
            // InternalTomMapping.g:2706:2: '{'
            {
             before(grammarAccess.getModuleAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getModuleAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__Group__2__Impl"


    // $ANTLR start "rule__Module__Group__3"
    // InternalTomMapping.g:2715:1: rule__Module__Group__3 : rule__Module__Group__3__Impl rule__Module__Group__4 ;
    public final void rule__Module__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2719:1: ( rule__Module__Group__3__Impl rule__Module__Group__4 )
            // InternalTomMapping.g:2720:2: rule__Module__Group__3__Impl rule__Module__Group__4
            {
            pushFollow(FOLLOW_20);
            rule__Module__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Module__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__Group__3"


    // $ANTLR start "rule__Module__Group__3__Impl"
    // InternalTomMapping.g:2727:1: rule__Module__Group__3__Impl : ( ( rule__Module__Group_3__0 )? ) ;
    public final void rule__Module__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2731:1: ( ( ( rule__Module__Group_3__0 )? ) )
            // InternalTomMapping.g:2732:1: ( ( rule__Module__Group_3__0 )? )
            {
            // InternalTomMapping.g:2732:1: ( ( rule__Module__Group_3__0 )? )
            // InternalTomMapping.g:2733:2: ( rule__Module__Group_3__0 )?
            {
             before(grammarAccess.getModuleAccess().getGroup_3()); 
            // InternalTomMapping.g:2734:2: ( rule__Module__Group_3__0 )?
            int alt24=2;
            int LA24_0 = input.LA(1);

            if ( (LA24_0==23) ) {
                alt24=1;
            }
            switch (alt24) {
                case 1 :
                    // InternalTomMapping.g:2734:3: rule__Module__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Module__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getModuleAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__Group__3__Impl"


    // $ANTLR start "rule__Module__Group__4"
    // InternalTomMapping.g:2742:1: rule__Module__Group__4 : rule__Module__Group__4__Impl ;
    public final void rule__Module__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2746:1: ( rule__Module__Group__4__Impl )
            // InternalTomMapping.g:2747:2: rule__Module__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Module__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__Group__4"


    // $ANTLR start "rule__Module__Group__4__Impl"
    // InternalTomMapping.g:2753:1: rule__Module__Group__4__Impl : ( '}' ) ;
    public final void rule__Module__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2757:1: ( ( '}' ) )
            // InternalTomMapping.g:2758:1: ( '}' )
            {
            // InternalTomMapping.g:2758:1: ( '}' )
            // InternalTomMapping.g:2759:2: '}'
            {
             before(grammarAccess.getModuleAccess().getRightCurlyBracketKeyword_4()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getModuleAccess().getRightCurlyBracketKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__Group__4__Impl"


    // $ANTLR start "rule__Module__Group_3__0"
    // InternalTomMapping.g:2769:1: rule__Module__Group_3__0 : rule__Module__Group_3__0__Impl rule__Module__Group_3__1 ;
    public final void rule__Module__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2773:1: ( rule__Module__Group_3__0__Impl rule__Module__Group_3__1 )
            // InternalTomMapping.g:2774:2: rule__Module__Group_3__0__Impl rule__Module__Group_3__1
            {
            pushFollow(FOLLOW_10);
            rule__Module__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Module__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__Group_3__0"


    // $ANTLR start "rule__Module__Group_3__0__Impl"
    // InternalTomMapping.g:2781:1: rule__Module__Group_3__0__Impl : ( 'operators' ) ;
    public final void rule__Module__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2785:1: ( ( 'operators' ) )
            // InternalTomMapping.g:2786:1: ( 'operators' )
            {
            // InternalTomMapping.g:2786:1: ( 'operators' )
            // InternalTomMapping.g:2787:2: 'operators'
            {
             before(grammarAccess.getModuleAccess().getOperatorsKeyword_3_0()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getModuleAccess().getOperatorsKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__Group_3__0__Impl"


    // $ANTLR start "rule__Module__Group_3__1"
    // InternalTomMapping.g:2796:1: rule__Module__Group_3__1 : rule__Module__Group_3__1__Impl rule__Module__Group_3__2 ;
    public final void rule__Module__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2800:1: ( rule__Module__Group_3__1__Impl rule__Module__Group_3__2 )
            // InternalTomMapping.g:2801:2: rule__Module__Group_3__1__Impl rule__Module__Group_3__2
            {
            pushFollow(FOLLOW_17);
            rule__Module__Group_3__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Module__Group_3__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__Group_3__1"


    // $ANTLR start "rule__Module__Group_3__1__Impl"
    // InternalTomMapping.g:2808:1: rule__Module__Group_3__1__Impl : ( '{' ) ;
    public final void rule__Module__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2812:1: ( ( '{' ) )
            // InternalTomMapping.g:2813:1: ( '{' )
            {
            // InternalTomMapping.g:2813:1: ( '{' )
            // InternalTomMapping.g:2814:2: '{'
            {
             before(grammarAccess.getModuleAccess().getLeftCurlyBracketKeyword_3_1()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getModuleAccess().getLeftCurlyBracketKeyword_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__Group_3__1__Impl"


    // $ANTLR start "rule__Module__Group_3__2"
    // InternalTomMapping.g:2823:1: rule__Module__Group_3__2 : rule__Module__Group_3__2__Impl rule__Module__Group_3__3 ;
    public final void rule__Module__Group_3__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2827:1: ( rule__Module__Group_3__2__Impl rule__Module__Group_3__3 )
            // InternalTomMapping.g:2828:2: rule__Module__Group_3__2__Impl rule__Module__Group_3__3
            {
            pushFollow(FOLLOW_4);
            rule__Module__Group_3__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Module__Group_3__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__Group_3__2"


    // $ANTLR start "rule__Module__Group_3__2__Impl"
    // InternalTomMapping.g:2835:1: rule__Module__Group_3__2__Impl : ( ( rule__Module__OperatorsAssignment_3_2 ) ) ;
    public final void rule__Module__Group_3__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2839:1: ( ( ( rule__Module__OperatorsAssignment_3_2 ) ) )
            // InternalTomMapping.g:2840:1: ( ( rule__Module__OperatorsAssignment_3_2 ) )
            {
            // InternalTomMapping.g:2840:1: ( ( rule__Module__OperatorsAssignment_3_2 ) )
            // InternalTomMapping.g:2841:2: ( rule__Module__OperatorsAssignment_3_2 )
            {
             before(grammarAccess.getModuleAccess().getOperatorsAssignment_3_2()); 
            // InternalTomMapping.g:2842:2: ( rule__Module__OperatorsAssignment_3_2 )
            // InternalTomMapping.g:2842:3: rule__Module__OperatorsAssignment_3_2
            {
            pushFollow(FOLLOW_2);
            rule__Module__OperatorsAssignment_3_2();

            state._fsp--;


            }

             after(grammarAccess.getModuleAccess().getOperatorsAssignment_3_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__Group_3__2__Impl"


    // $ANTLR start "rule__Module__Group_3__3"
    // InternalTomMapping.g:2850:1: rule__Module__Group_3__3 : rule__Module__Group_3__3__Impl rule__Module__Group_3__4 ;
    public final void rule__Module__Group_3__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2854:1: ( rule__Module__Group_3__3__Impl rule__Module__Group_3__4 )
            // InternalTomMapping.g:2855:2: rule__Module__Group_3__3__Impl rule__Module__Group_3__4
            {
            pushFollow(FOLLOW_4);
            rule__Module__Group_3__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Module__Group_3__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__Group_3__3"


    // $ANTLR start "rule__Module__Group_3__3__Impl"
    // InternalTomMapping.g:2862:1: rule__Module__Group_3__3__Impl : ( ( rule__Module__Group_3_3__0 )* ) ;
    public final void rule__Module__Group_3__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2866:1: ( ( ( rule__Module__Group_3_3__0 )* ) )
            // InternalTomMapping.g:2867:1: ( ( rule__Module__Group_3_3__0 )* )
            {
            // InternalTomMapping.g:2867:1: ( ( rule__Module__Group_3_3__0 )* )
            // InternalTomMapping.g:2868:2: ( rule__Module__Group_3_3__0 )*
            {
             before(grammarAccess.getModuleAccess().getGroup_3_3()); 
            // InternalTomMapping.g:2869:2: ( rule__Module__Group_3_3__0 )*
            loop25:
            do {
                int alt25=2;
                int LA25_0 = input.LA(1);

                if ( (LA25_0==14) ) {
                    int LA25_1 = input.LA(2);

                    if ( (LA25_1==29||LA25_1==33) ) {
                        alt25=1;
                    }


                }


                switch (alt25) {
            	case 1 :
            	    // InternalTomMapping.g:2869:3: rule__Module__Group_3_3__0
            	    {
            	    pushFollow(FOLLOW_18);
            	    rule__Module__Group_3_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop25;
                }
            } while (true);

             after(grammarAccess.getModuleAccess().getGroup_3_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__Group_3__3__Impl"


    // $ANTLR start "rule__Module__Group_3__4"
    // InternalTomMapping.g:2877:1: rule__Module__Group_3__4 : rule__Module__Group_3__4__Impl rule__Module__Group_3__5 ;
    public final void rule__Module__Group_3__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2881:1: ( rule__Module__Group_3__4__Impl rule__Module__Group_3__5 )
            // InternalTomMapping.g:2882:2: rule__Module__Group_3__4__Impl rule__Module__Group_3__5
            {
            pushFollow(FOLLOW_12);
            rule__Module__Group_3__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Module__Group_3__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__Group_3__4"


    // $ANTLR start "rule__Module__Group_3__4__Impl"
    // InternalTomMapping.g:2889:1: rule__Module__Group_3__4__Impl : ( ';' ) ;
    public final void rule__Module__Group_3__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2893:1: ( ( ';' ) )
            // InternalTomMapping.g:2894:1: ( ';' )
            {
            // InternalTomMapping.g:2894:1: ( ';' )
            // InternalTomMapping.g:2895:2: ';'
            {
             before(grammarAccess.getModuleAccess().getSemicolonKeyword_3_4()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getModuleAccess().getSemicolonKeyword_3_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__Group_3__4__Impl"


    // $ANTLR start "rule__Module__Group_3__5"
    // InternalTomMapping.g:2904:1: rule__Module__Group_3__5 : rule__Module__Group_3__5__Impl ;
    public final void rule__Module__Group_3__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2908:1: ( rule__Module__Group_3__5__Impl )
            // InternalTomMapping.g:2909:2: rule__Module__Group_3__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Module__Group_3__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__Group_3__5"


    // $ANTLR start "rule__Module__Group_3__5__Impl"
    // InternalTomMapping.g:2915:1: rule__Module__Group_3__5__Impl : ( '}' ) ;
    public final void rule__Module__Group_3__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2919:1: ( ( '}' ) )
            // InternalTomMapping.g:2920:1: ( '}' )
            {
            // InternalTomMapping.g:2920:1: ( '}' )
            // InternalTomMapping.g:2921:2: '}'
            {
             before(grammarAccess.getModuleAccess().getRightCurlyBracketKeyword_3_5()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getModuleAccess().getRightCurlyBracketKeyword_3_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__Group_3__5__Impl"


    // $ANTLR start "rule__Module__Group_3_3__0"
    // InternalTomMapping.g:2931:1: rule__Module__Group_3_3__0 : rule__Module__Group_3_3__0__Impl rule__Module__Group_3_3__1 ;
    public final void rule__Module__Group_3_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2935:1: ( rule__Module__Group_3_3__0__Impl rule__Module__Group_3_3__1 )
            // InternalTomMapping.g:2936:2: rule__Module__Group_3_3__0__Impl rule__Module__Group_3_3__1
            {
            pushFollow(FOLLOW_17);
            rule__Module__Group_3_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Module__Group_3_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__Group_3_3__0"


    // $ANTLR start "rule__Module__Group_3_3__0__Impl"
    // InternalTomMapping.g:2943:1: rule__Module__Group_3_3__0__Impl : ( ';' ) ;
    public final void rule__Module__Group_3_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2947:1: ( ( ';' ) )
            // InternalTomMapping.g:2948:1: ( ';' )
            {
            // InternalTomMapping.g:2948:1: ( ';' )
            // InternalTomMapping.g:2949:2: ';'
            {
             before(grammarAccess.getModuleAccess().getSemicolonKeyword_3_3_0()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getModuleAccess().getSemicolonKeyword_3_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__Group_3_3__0__Impl"


    // $ANTLR start "rule__Module__Group_3_3__1"
    // InternalTomMapping.g:2958:1: rule__Module__Group_3_3__1 : rule__Module__Group_3_3__1__Impl ;
    public final void rule__Module__Group_3_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2962:1: ( rule__Module__Group_3_3__1__Impl )
            // InternalTomMapping.g:2963:2: rule__Module__Group_3_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Module__Group_3_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__Group_3_3__1"


    // $ANTLR start "rule__Module__Group_3_3__1__Impl"
    // InternalTomMapping.g:2969:1: rule__Module__Group_3_3__1__Impl : ( ( rule__Module__OperatorsAssignment_3_3_1 ) ) ;
    public final void rule__Module__Group_3_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2973:1: ( ( ( rule__Module__OperatorsAssignment_3_3_1 ) ) )
            // InternalTomMapping.g:2974:1: ( ( rule__Module__OperatorsAssignment_3_3_1 ) )
            {
            // InternalTomMapping.g:2974:1: ( ( rule__Module__OperatorsAssignment_3_3_1 ) )
            // InternalTomMapping.g:2975:2: ( rule__Module__OperatorsAssignment_3_3_1 )
            {
             before(grammarAccess.getModuleAccess().getOperatorsAssignment_3_3_1()); 
            // InternalTomMapping.g:2976:2: ( rule__Module__OperatorsAssignment_3_3_1 )
            // InternalTomMapping.g:2976:3: rule__Module__OperatorsAssignment_3_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Module__OperatorsAssignment_3_3_1();

            state._fsp--;


            }

             after(grammarAccess.getModuleAccess().getOperatorsAssignment_3_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__Group_3_3__1__Impl"


    // $ANTLR start "rule__Import__Group__0"
    // InternalTomMapping.g:2985:1: rule__Import__Group__0 : rule__Import__Group__0__Impl rule__Import__Group__1 ;
    public final void rule__Import__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:2989:1: ( rule__Import__Group__0__Impl rule__Import__Group__1 )
            // InternalTomMapping.g:2990:2: rule__Import__Group__0__Impl rule__Import__Group__1
            {
            pushFollow(FOLLOW_9);
            rule__Import__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Import__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__0"


    // $ANTLR start "rule__Import__Group__0__Impl"
    // InternalTomMapping.g:2997:1: rule__Import__Group__0__Impl : ( 'import' ) ;
    public final void rule__Import__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3001:1: ( ( 'import' ) )
            // InternalTomMapping.g:3002:1: ( 'import' )
            {
            // InternalTomMapping.g:3002:1: ( 'import' )
            // InternalTomMapping.g:3003:2: 'import'
            {
             before(grammarAccess.getImportAccess().getImportKeyword_0()); 
            match(input,27,FOLLOW_2); 
             after(grammarAccess.getImportAccess().getImportKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__0__Impl"


    // $ANTLR start "rule__Import__Group__1"
    // InternalTomMapping.g:3012:1: rule__Import__Group__1 : rule__Import__Group__1__Impl rule__Import__Group__2 ;
    public final void rule__Import__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3016:1: ( rule__Import__Group__1__Impl rule__Import__Group__2 )
            // InternalTomMapping.g:3017:2: rule__Import__Group__1__Impl rule__Import__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__Import__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Import__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__1"


    // $ANTLR start "rule__Import__Group__1__Impl"
    // InternalTomMapping.g:3024:1: rule__Import__Group__1__Impl : ( ( rule__Import__ImportURIAssignment_1 ) ) ;
    public final void rule__Import__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3028:1: ( ( ( rule__Import__ImportURIAssignment_1 ) ) )
            // InternalTomMapping.g:3029:1: ( ( rule__Import__ImportURIAssignment_1 ) )
            {
            // InternalTomMapping.g:3029:1: ( ( rule__Import__ImportURIAssignment_1 ) )
            // InternalTomMapping.g:3030:2: ( rule__Import__ImportURIAssignment_1 )
            {
             before(grammarAccess.getImportAccess().getImportURIAssignment_1()); 
            // InternalTomMapping.g:3031:2: ( rule__Import__ImportURIAssignment_1 )
            // InternalTomMapping.g:3031:3: rule__Import__ImportURIAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Import__ImportURIAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getImportAccess().getImportURIAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__1__Impl"


    // $ANTLR start "rule__Import__Group__2"
    // InternalTomMapping.g:3039:1: rule__Import__Group__2 : rule__Import__Group__2__Impl ;
    public final void rule__Import__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3043:1: ( rule__Import__Group__2__Impl )
            // InternalTomMapping.g:3044:2: rule__Import__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Import__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__2"


    // $ANTLR start "rule__Import__Group__2__Impl"
    // InternalTomMapping.g:3050:1: rule__Import__Group__2__Impl : ( ';' ) ;
    public final void rule__Import__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3054:1: ( ( ';' ) )
            // InternalTomMapping.g:3055:1: ( ';' )
            {
            // InternalTomMapping.g:3055:1: ( ';' )
            // InternalTomMapping.g:3056:2: ';'
            {
             before(grammarAccess.getImportAccess().getSemicolonKeyword_2()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getImportAccess().getSemicolonKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__2__Impl"


    // $ANTLR start "rule__Terminal__Group__0"
    // InternalTomMapping.g:3066:1: rule__Terminal__Group__0 : rule__Terminal__Group__0__Impl rule__Terminal__Group__1 ;
    public final void rule__Terminal__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3070:1: ( rule__Terminal__Group__0__Impl rule__Terminal__Group__1 )
            // InternalTomMapping.g:3071:2: rule__Terminal__Group__0__Impl rule__Terminal__Group__1
            {
            pushFollow(FOLLOW_21);
            rule__Terminal__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Terminal__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Terminal__Group__0"


    // $ANTLR start "rule__Terminal__Group__0__Impl"
    // InternalTomMapping.g:3078:1: rule__Terminal__Group__0__Impl : ( ( rule__Terminal__NameAssignment_0 ) ) ;
    public final void rule__Terminal__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3082:1: ( ( ( rule__Terminal__NameAssignment_0 ) ) )
            // InternalTomMapping.g:3083:1: ( ( rule__Terminal__NameAssignment_0 ) )
            {
            // InternalTomMapping.g:3083:1: ( ( rule__Terminal__NameAssignment_0 ) )
            // InternalTomMapping.g:3084:2: ( rule__Terminal__NameAssignment_0 )
            {
             before(grammarAccess.getTerminalAccess().getNameAssignment_0()); 
            // InternalTomMapping.g:3085:2: ( rule__Terminal__NameAssignment_0 )
            // InternalTomMapping.g:3085:3: rule__Terminal__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Terminal__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getTerminalAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Terminal__Group__0__Impl"


    // $ANTLR start "rule__Terminal__Group__1"
    // InternalTomMapping.g:3093:1: rule__Terminal__Group__1 : rule__Terminal__Group__1__Impl rule__Terminal__Group__2 ;
    public final void rule__Terminal__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3097:1: ( rule__Terminal__Group__1__Impl rule__Terminal__Group__2 )
            // InternalTomMapping.g:3098:2: rule__Terminal__Group__1__Impl rule__Terminal__Group__2
            {
            pushFollow(FOLLOW_3);
            rule__Terminal__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Terminal__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Terminal__Group__1"


    // $ANTLR start "rule__Terminal__Group__1__Impl"
    // InternalTomMapping.g:3105:1: rule__Terminal__Group__1__Impl : ( ':' ) ;
    public final void rule__Terminal__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3109:1: ( ( ':' ) )
            // InternalTomMapping.g:3110:1: ( ':' )
            {
            // InternalTomMapping.g:3110:1: ( ':' )
            // InternalTomMapping.g:3111:2: ':'
            {
             before(grammarAccess.getTerminalAccess().getColonKeyword_1()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getTerminalAccess().getColonKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Terminal__Group__1__Impl"


    // $ANTLR start "rule__Terminal__Group__2"
    // InternalTomMapping.g:3120:1: rule__Terminal__Group__2 : rule__Terminal__Group__2__Impl rule__Terminal__Group__3 ;
    public final void rule__Terminal__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3124:1: ( rule__Terminal__Group__2__Impl rule__Terminal__Group__3 )
            // InternalTomMapping.g:3125:2: rule__Terminal__Group__2__Impl rule__Terminal__Group__3
            {
            pushFollow(FOLLOW_22);
            rule__Terminal__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Terminal__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Terminal__Group__2"


    // $ANTLR start "rule__Terminal__Group__2__Impl"
    // InternalTomMapping.g:3132:1: rule__Terminal__Group__2__Impl : ( ( rule__Terminal__ClassAssignment_2 ) ) ;
    public final void rule__Terminal__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3136:1: ( ( ( rule__Terminal__ClassAssignment_2 ) ) )
            // InternalTomMapping.g:3137:1: ( ( rule__Terminal__ClassAssignment_2 ) )
            {
            // InternalTomMapping.g:3137:1: ( ( rule__Terminal__ClassAssignment_2 ) )
            // InternalTomMapping.g:3138:2: ( rule__Terminal__ClassAssignment_2 )
            {
             before(grammarAccess.getTerminalAccess().getClassAssignment_2()); 
            // InternalTomMapping.g:3139:2: ( rule__Terminal__ClassAssignment_2 )
            // InternalTomMapping.g:3139:3: rule__Terminal__ClassAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Terminal__ClassAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getTerminalAccess().getClassAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Terminal__Group__2__Impl"


    // $ANTLR start "rule__Terminal__Group__3"
    // InternalTomMapping.g:3147:1: rule__Terminal__Group__3 : rule__Terminal__Group__3__Impl ;
    public final void rule__Terminal__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3151:1: ( rule__Terminal__Group__3__Impl )
            // InternalTomMapping.g:3152:2: rule__Terminal__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Terminal__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Terminal__Group__3"


    // $ANTLR start "rule__Terminal__Group__3__Impl"
    // InternalTomMapping.g:3158:1: rule__Terminal__Group__3__Impl : ( ( rule__Terminal__ManyAssignment_3 )? ) ;
    public final void rule__Terminal__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3162:1: ( ( ( rule__Terminal__ManyAssignment_3 )? ) )
            // InternalTomMapping.g:3163:1: ( ( rule__Terminal__ManyAssignment_3 )? )
            {
            // InternalTomMapping.g:3163:1: ( ( rule__Terminal__ManyAssignment_3 )? )
            // InternalTomMapping.g:3164:2: ( rule__Terminal__ManyAssignment_3 )?
            {
             before(grammarAccess.getTerminalAccess().getManyAssignment_3()); 
            // InternalTomMapping.g:3165:2: ( rule__Terminal__ManyAssignment_3 )?
            int alt26=2;
            int LA26_0 = input.LA(1);

            if ( (LA26_0==42) ) {
                alt26=1;
            }
            switch (alt26) {
                case 1 :
                    // InternalTomMapping.g:3165:3: rule__Terminal__ManyAssignment_3
                    {
                    pushFollow(FOLLOW_2);
                    rule__Terminal__ManyAssignment_3();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getTerminalAccess().getManyAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Terminal__Group__3__Impl"


    // $ANTLR start "rule__AliasOperator__Group__0"
    // InternalTomMapping.g:3174:1: rule__AliasOperator__Group__0 : rule__AliasOperator__Group__0__Impl rule__AliasOperator__Group__1 ;
    public final void rule__AliasOperator__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3178:1: ( rule__AliasOperator__Group__0__Impl rule__AliasOperator__Group__1 )
            // InternalTomMapping.g:3179:2: rule__AliasOperator__Group__0__Impl rule__AliasOperator__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__AliasOperator__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AliasOperator__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasOperator__Group__0"


    // $ANTLR start "rule__AliasOperator__Group__0__Impl"
    // InternalTomMapping.g:3186:1: rule__AliasOperator__Group__0__Impl : ( 'alias' ) ;
    public final void rule__AliasOperator__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3190:1: ( ( 'alias' ) )
            // InternalTomMapping.g:3191:1: ( 'alias' )
            {
            // InternalTomMapping.g:3191:1: ( 'alias' )
            // InternalTomMapping.g:3192:2: 'alias'
            {
             before(grammarAccess.getAliasOperatorAccess().getAliasKeyword_0()); 
            match(input,29,FOLLOW_2); 
             after(grammarAccess.getAliasOperatorAccess().getAliasKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasOperator__Group__0__Impl"


    // $ANTLR start "rule__AliasOperator__Group__1"
    // InternalTomMapping.g:3201:1: rule__AliasOperator__Group__1 : rule__AliasOperator__Group__1__Impl rule__AliasOperator__Group__2 ;
    public final void rule__AliasOperator__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3205:1: ( rule__AliasOperator__Group__1__Impl rule__AliasOperator__Group__2 )
            // InternalTomMapping.g:3206:2: rule__AliasOperator__Group__1__Impl rule__AliasOperator__Group__2
            {
            pushFollow(FOLLOW_23);
            rule__AliasOperator__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AliasOperator__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasOperator__Group__1"


    // $ANTLR start "rule__AliasOperator__Group__1__Impl"
    // InternalTomMapping.g:3213:1: rule__AliasOperator__Group__1__Impl : ( ( rule__AliasOperator__NameAssignment_1 ) ) ;
    public final void rule__AliasOperator__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3217:1: ( ( ( rule__AliasOperator__NameAssignment_1 ) ) )
            // InternalTomMapping.g:3218:1: ( ( rule__AliasOperator__NameAssignment_1 ) )
            {
            // InternalTomMapping.g:3218:1: ( ( rule__AliasOperator__NameAssignment_1 ) )
            // InternalTomMapping.g:3219:2: ( rule__AliasOperator__NameAssignment_1 )
            {
             before(grammarAccess.getAliasOperatorAccess().getNameAssignment_1()); 
            // InternalTomMapping.g:3220:2: ( rule__AliasOperator__NameAssignment_1 )
            // InternalTomMapping.g:3220:3: rule__AliasOperator__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__AliasOperator__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getAliasOperatorAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasOperator__Group__1__Impl"


    // $ANTLR start "rule__AliasOperator__Group__2"
    // InternalTomMapping.g:3228:1: rule__AliasOperator__Group__2 : rule__AliasOperator__Group__2__Impl rule__AliasOperator__Group__3 ;
    public final void rule__AliasOperator__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3232:1: ( rule__AliasOperator__Group__2__Impl rule__AliasOperator__Group__3 )
            // InternalTomMapping.g:3233:2: rule__AliasOperator__Group__2__Impl rule__AliasOperator__Group__3
            {
            pushFollow(FOLLOW_3);
            rule__AliasOperator__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AliasOperator__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasOperator__Group__2"


    // $ANTLR start "rule__AliasOperator__Group__2__Impl"
    // InternalTomMapping.g:3240:1: rule__AliasOperator__Group__2__Impl : ( '::' ) ;
    public final void rule__AliasOperator__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3244:1: ( ( '::' ) )
            // InternalTomMapping.g:3245:1: ( '::' )
            {
            // InternalTomMapping.g:3245:1: ( '::' )
            // InternalTomMapping.g:3246:2: '::'
            {
             before(grammarAccess.getAliasOperatorAccess().getColonColonKeyword_2()); 
            match(input,30,FOLLOW_2); 
             after(grammarAccess.getAliasOperatorAccess().getColonColonKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasOperator__Group__2__Impl"


    // $ANTLR start "rule__AliasOperator__Group__3"
    // InternalTomMapping.g:3255:1: rule__AliasOperator__Group__3 : rule__AliasOperator__Group__3__Impl rule__AliasOperator__Group__4 ;
    public final void rule__AliasOperator__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3259:1: ( rule__AliasOperator__Group__3__Impl rule__AliasOperator__Group__4 )
            // InternalTomMapping.g:3260:2: rule__AliasOperator__Group__3__Impl rule__AliasOperator__Group__4
            {
            pushFollow(FOLLOW_24);
            rule__AliasOperator__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AliasOperator__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasOperator__Group__3"


    // $ANTLR start "rule__AliasOperator__Group__3__Impl"
    // InternalTomMapping.g:3267:1: rule__AliasOperator__Group__3__Impl : ( ( rule__AliasOperator__OpAssignment_3 ) ) ;
    public final void rule__AliasOperator__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3271:1: ( ( ( rule__AliasOperator__OpAssignment_3 ) ) )
            // InternalTomMapping.g:3272:1: ( ( rule__AliasOperator__OpAssignment_3 ) )
            {
            // InternalTomMapping.g:3272:1: ( ( rule__AliasOperator__OpAssignment_3 ) )
            // InternalTomMapping.g:3273:2: ( rule__AliasOperator__OpAssignment_3 )
            {
             before(grammarAccess.getAliasOperatorAccess().getOpAssignment_3()); 
            // InternalTomMapping.g:3274:2: ( rule__AliasOperator__OpAssignment_3 )
            // InternalTomMapping.g:3274:3: rule__AliasOperator__OpAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__AliasOperator__OpAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getAliasOperatorAccess().getOpAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasOperator__Group__3__Impl"


    // $ANTLR start "rule__AliasOperator__Group__4"
    // InternalTomMapping.g:3282:1: rule__AliasOperator__Group__4 : rule__AliasOperator__Group__4__Impl rule__AliasOperator__Group__5 ;
    public final void rule__AliasOperator__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3286:1: ( rule__AliasOperator__Group__4__Impl rule__AliasOperator__Group__5 )
            // InternalTomMapping.g:3287:2: rule__AliasOperator__Group__4__Impl rule__AliasOperator__Group__5
            {
            pushFollow(FOLLOW_3);
            rule__AliasOperator__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AliasOperator__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasOperator__Group__4"


    // $ANTLR start "rule__AliasOperator__Group__4__Impl"
    // InternalTomMapping.g:3294:1: rule__AliasOperator__Group__4__Impl : ( '(' ) ;
    public final void rule__AliasOperator__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3298:1: ( ( '(' ) )
            // InternalTomMapping.g:3299:1: ( '(' )
            {
            // InternalTomMapping.g:3299:1: ( '(' )
            // InternalTomMapping.g:3300:2: '('
            {
             before(grammarAccess.getAliasOperatorAccess().getLeftParenthesisKeyword_4()); 
            match(input,31,FOLLOW_2); 
             after(grammarAccess.getAliasOperatorAccess().getLeftParenthesisKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasOperator__Group__4__Impl"


    // $ANTLR start "rule__AliasOperator__Group__5"
    // InternalTomMapping.g:3309:1: rule__AliasOperator__Group__5 : rule__AliasOperator__Group__5__Impl rule__AliasOperator__Group__6 ;
    public final void rule__AliasOperator__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3313:1: ( rule__AliasOperator__Group__5__Impl rule__AliasOperator__Group__6 )
            // InternalTomMapping.g:3314:2: rule__AliasOperator__Group__5__Impl rule__AliasOperator__Group__6
            {
            pushFollow(FOLLOW_25);
            rule__AliasOperator__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AliasOperator__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasOperator__Group__5"


    // $ANTLR start "rule__AliasOperator__Group__5__Impl"
    // InternalTomMapping.g:3321:1: rule__AliasOperator__Group__5__Impl : ( ( rule__AliasOperator__NodesAssignment_5 ) ) ;
    public final void rule__AliasOperator__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3325:1: ( ( ( rule__AliasOperator__NodesAssignment_5 ) ) )
            // InternalTomMapping.g:3326:1: ( ( rule__AliasOperator__NodesAssignment_5 ) )
            {
            // InternalTomMapping.g:3326:1: ( ( rule__AliasOperator__NodesAssignment_5 ) )
            // InternalTomMapping.g:3327:2: ( rule__AliasOperator__NodesAssignment_5 )
            {
             before(grammarAccess.getAliasOperatorAccess().getNodesAssignment_5()); 
            // InternalTomMapping.g:3328:2: ( rule__AliasOperator__NodesAssignment_5 )
            // InternalTomMapping.g:3328:3: rule__AliasOperator__NodesAssignment_5
            {
            pushFollow(FOLLOW_2);
            rule__AliasOperator__NodesAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getAliasOperatorAccess().getNodesAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasOperator__Group__5__Impl"


    // $ANTLR start "rule__AliasOperator__Group__6"
    // InternalTomMapping.g:3336:1: rule__AliasOperator__Group__6 : rule__AliasOperator__Group__6__Impl rule__AliasOperator__Group__7 ;
    public final void rule__AliasOperator__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3340:1: ( rule__AliasOperator__Group__6__Impl rule__AliasOperator__Group__7 )
            // InternalTomMapping.g:3341:2: rule__AliasOperator__Group__6__Impl rule__AliasOperator__Group__7
            {
            pushFollow(FOLLOW_25);
            rule__AliasOperator__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AliasOperator__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasOperator__Group__6"


    // $ANTLR start "rule__AliasOperator__Group__6__Impl"
    // InternalTomMapping.g:3348:1: rule__AliasOperator__Group__6__Impl : ( ( rule__AliasOperator__Group_6__0 )* ) ;
    public final void rule__AliasOperator__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3352:1: ( ( ( rule__AliasOperator__Group_6__0 )* ) )
            // InternalTomMapping.g:3353:1: ( ( rule__AliasOperator__Group_6__0 )* )
            {
            // InternalTomMapping.g:3353:1: ( ( rule__AliasOperator__Group_6__0 )* )
            // InternalTomMapping.g:3354:2: ( rule__AliasOperator__Group_6__0 )*
            {
             before(grammarAccess.getAliasOperatorAccess().getGroup_6()); 
            // InternalTomMapping.g:3355:2: ( rule__AliasOperator__Group_6__0 )*
            loop27:
            do {
                int alt27=2;
                int LA27_0 = input.LA(1);

                if ( (LA27_0==21) ) {
                    alt27=1;
                }


                switch (alt27) {
            	case 1 :
            	    // InternalTomMapping.g:3355:3: rule__AliasOperator__Group_6__0
            	    {
            	    pushFollow(FOLLOW_15);
            	    rule__AliasOperator__Group_6__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop27;
                }
            } while (true);

             after(grammarAccess.getAliasOperatorAccess().getGroup_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasOperator__Group__6__Impl"


    // $ANTLR start "rule__AliasOperator__Group__7"
    // InternalTomMapping.g:3363:1: rule__AliasOperator__Group__7 : rule__AliasOperator__Group__7__Impl ;
    public final void rule__AliasOperator__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3367:1: ( rule__AliasOperator__Group__7__Impl )
            // InternalTomMapping.g:3368:2: rule__AliasOperator__Group__7__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AliasOperator__Group__7__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasOperator__Group__7"


    // $ANTLR start "rule__AliasOperator__Group__7__Impl"
    // InternalTomMapping.g:3374:1: rule__AliasOperator__Group__7__Impl : ( ')' ) ;
    public final void rule__AliasOperator__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3378:1: ( ( ')' ) )
            // InternalTomMapping.g:3379:1: ( ')' )
            {
            // InternalTomMapping.g:3379:1: ( ')' )
            // InternalTomMapping.g:3380:2: ')'
            {
             before(grammarAccess.getAliasOperatorAccess().getRightParenthesisKeyword_7()); 
            match(input,32,FOLLOW_2); 
             after(grammarAccess.getAliasOperatorAccess().getRightParenthesisKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasOperator__Group__7__Impl"


    // $ANTLR start "rule__AliasOperator__Group_6__0"
    // InternalTomMapping.g:3390:1: rule__AliasOperator__Group_6__0 : rule__AliasOperator__Group_6__0__Impl rule__AliasOperator__Group_6__1 ;
    public final void rule__AliasOperator__Group_6__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3394:1: ( rule__AliasOperator__Group_6__0__Impl rule__AliasOperator__Group_6__1 )
            // InternalTomMapping.g:3395:2: rule__AliasOperator__Group_6__0__Impl rule__AliasOperator__Group_6__1
            {
            pushFollow(FOLLOW_3);
            rule__AliasOperator__Group_6__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AliasOperator__Group_6__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasOperator__Group_6__0"


    // $ANTLR start "rule__AliasOperator__Group_6__0__Impl"
    // InternalTomMapping.g:3402:1: rule__AliasOperator__Group_6__0__Impl : ( ',' ) ;
    public final void rule__AliasOperator__Group_6__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3406:1: ( ( ',' ) )
            // InternalTomMapping.g:3407:1: ( ',' )
            {
            // InternalTomMapping.g:3407:1: ( ',' )
            // InternalTomMapping.g:3408:2: ','
            {
             before(grammarAccess.getAliasOperatorAccess().getCommaKeyword_6_0()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getAliasOperatorAccess().getCommaKeyword_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasOperator__Group_6__0__Impl"


    // $ANTLR start "rule__AliasOperator__Group_6__1"
    // InternalTomMapping.g:3417:1: rule__AliasOperator__Group_6__1 : rule__AliasOperator__Group_6__1__Impl ;
    public final void rule__AliasOperator__Group_6__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3421:1: ( rule__AliasOperator__Group_6__1__Impl )
            // InternalTomMapping.g:3422:2: rule__AliasOperator__Group_6__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AliasOperator__Group_6__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasOperator__Group_6__1"


    // $ANTLR start "rule__AliasOperator__Group_6__1__Impl"
    // InternalTomMapping.g:3428:1: rule__AliasOperator__Group_6__1__Impl : ( ( rule__AliasOperator__NodesAssignment_6_1 ) ) ;
    public final void rule__AliasOperator__Group_6__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3432:1: ( ( ( rule__AliasOperator__NodesAssignment_6_1 ) ) )
            // InternalTomMapping.g:3433:1: ( ( rule__AliasOperator__NodesAssignment_6_1 ) )
            {
            // InternalTomMapping.g:3433:1: ( ( rule__AliasOperator__NodesAssignment_6_1 ) )
            // InternalTomMapping.g:3434:2: ( rule__AliasOperator__NodesAssignment_6_1 )
            {
             before(grammarAccess.getAliasOperatorAccess().getNodesAssignment_6_1()); 
            // InternalTomMapping.g:3435:2: ( rule__AliasOperator__NodesAssignment_6_1 )
            // InternalTomMapping.g:3435:3: rule__AliasOperator__NodesAssignment_6_1
            {
            pushFollow(FOLLOW_2);
            rule__AliasOperator__NodesAssignment_6_1();

            state._fsp--;


            }

             after(grammarAccess.getAliasOperatorAccess().getNodesAssignment_6_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasOperator__Group_6__1__Impl"


    // $ANTLR start "rule__OperatorNode__Group__0"
    // InternalTomMapping.g:3444:1: rule__OperatorNode__Group__0 : rule__OperatorNode__Group__0__Impl rule__OperatorNode__Group__1 ;
    public final void rule__OperatorNode__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3448:1: ( rule__OperatorNode__Group__0__Impl rule__OperatorNode__Group__1 )
            // InternalTomMapping.g:3449:2: rule__OperatorNode__Group__0__Impl rule__OperatorNode__Group__1
            {
            pushFollow(FOLLOW_24);
            rule__OperatorNode__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OperatorNode__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OperatorNode__Group__0"


    // $ANTLR start "rule__OperatorNode__Group__0__Impl"
    // InternalTomMapping.g:3456:1: rule__OperatorNode__Group__0__Impl : ( ( rule__OperatorNode__OpAssignment_0 ) ) ;
    public final void rule__OperatorNode__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3460:1: ( ( ( rule__OperatorNode__OpAssignment_0 ) ) )
            // InternalTomMapping.g:3461:1: ( ( rule__OperatorNode__OpAssignment_0 ) )
            {
            // InternalTomMapping.g:3461:1: ( ( rule__OperatorNode__OpAssignment_0 ) )
            // InternalTomMapping.g:3462:2: ( rule__OperatorNode__OpAssignment_0 )
            {
             before(grammarAccess.getOperatorNodeAccess().getOpAssignment_0()); 
            // InternalTomMapping.g:3463:2: ( rule__OperatorNode__OpAssignment_0 )
            // InternalTomMapping.g:3463:3: rule__OperatorNode__OpAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__OperatorNode__OpAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getOperatorNodeAccess().getOpAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OperatorNode__Group__0__Impl"


    // $ANTLR start "rule__OperatorNode__Group__1"
    // InternalTomMapping.g:3471:1: rule__OperatorNode__Group__1 : rule__OperatorNode__Group__1__Impl rule__OperatorNode__Group__2 ;
    public final void rule__OperatorNode__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3475:1: ( rule__OperatorNode__Group__1__Impl rule__OperatorNode__Group__2 )
            // InternalTomMapping.g:3476:2: rule__OperatorNode__Group__1__Impl rule__OperatorNode__Group__2
            {
            pushFollow(FOLLOW_3);
            rule__OperatorNode__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OperatorNode__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OperatorNode__Group__1"


    // $ANTLR start "rule__OperatorNode__Group__1__Impl"
    // InternalTomMapping.g:3483:1: rule__OperatorNode__Group__1__Impl : ( '(' ) ;
    public final void rule__OperatorNode__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3487:1: ( ( '(' ) )
            // InternalTomMapping.g:3488:1: ( '(' )
            {
            // InternalTomMapping.g:3488:1: ( '(' )
            // InternalTomMapping.g:3489:2: '('
            {
             before(grammarAccess.getOperatorNodeAccess().getLeftParenthesisKeyword_1()); 
            match(input,31,FOLLOW_2); 
             after(grammarAccess.getOperatorNodeAccess().getLeftParenthesisKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OperatorNode__Group__1__Impl"


    // $ANTLR start "rule__OperatorNode__Group__2"
    // InternalTomMapping.g:3498:1: rule__OperatorNode__Group__2 : rule__OperatorNode__Group__2__Impl rule__OperatorNode__Group__3 ;
    public final void rule__OperatorNode__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3502:1: ( rule__OperatorNode__Group__2__Impl rule__OperatorNode__Group__3 )
            // InternalTomMapping.g:3503:2: rule__OperatorNode__Group__2__Impl rule__OperatorNode__Group__3
            {
            pushFollow(FOLLOW_25);
            rule__OperatorNode__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OperatorNode__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OperatorNode__Group__2"


    // $ANTLR start "rule__OperatorNode__Group__2__Impl"
    // InternalTomMapping.g:3510:1: rule__OperatorNode__Group__2__Impl : ( ( rule__OperatorNode__NodesAssignment_2 ) ) ;
    public final void rule__OperatorNode__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3514:1: ( ( ( rule__OperatorNode__NodesAssignment_2 ) ) )
            // InternalTomMapping.g:3515:1: ( ( rule__OperatorNode__NodesAssignment_2 ) )
            {
            // InternalTomMapping.g:3515:1: ( ( rule__OperatorNode__NodesAssignment_2 ) )
            // InternalTomMapping.g:3516:2: ( rule__OperatorNode__NodesAssignment_2 )
            {
             before(grammarAccess.getOperatorNodeAccess().getNodesAssignment_2()); 
            // InternalTomMapping.g:3517:2: ( rule__OperatorNode__NodesAssignment_2 )
            // InternalTomMapping.g:3517:3: rule__OperatorNode__NodesAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__OperatorNode__NodesAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getOperatorNodeAccess().getNodesAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OperatorNode__Group__2__Impl"


    // $ANTLR start "rule__OperatorNode__Group__3"
    // InternalTomMapping.g:3525:1: rule__OperatorNode__Group__3 : rule__OperatorNode__Group__3__Impl rule__OperatorNode__Group__4 ;
    public final void rule__OperatorNode__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3529:1: ( rule__OperatorNode__Group__3__Impl rule__OperatorNode__Group__4 )
            // InternalTomMapping.g:3530:2: rule__OperatorNode__Group__3__Impl rule__OperatorNode__Group__4
            {
            pushFollow(FOLLOW_25);
            rule__OperatorNode__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OperatorNode__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OperatorNode__Group__3"


    // $ANTLR start "rule__OperatorNode__Group__3__Impl"
    // InternalTomMapping.g:3537:1: rule__OperatorNode__Group__3__Impl : ( ( rule__OperatorNode__Group_3__0 )* ) ;
    public final void rule__OperatorNode__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3541:1: ( ( ( rule__OperatorNode__Group_3__0 )* ) )
            // InternalTomMapping.g:3542:1: ( ( rule__OperatorNode__Group_3__0 )* )
            {
            // InternalTomMapping.g:3542:1: ( ( rule__OperatorNode__Group_3__0 )* )
            // InternalTomMapping.g:3543:2: ( rule__OperatorNode__Group_3__0 )*
            {
             before(grammarAccess.getOperatorNodeAccess().getGroup_3()); 
            // InternalTomMapping.g:3544:2: ( rule__OperatorNode__Group_3__0 )*
            loop28:
            do {
                int alt28=2;
                int LA28_0 = input.LA(1);

                if ( (LA28_0==21) ) {
                    alt28=1;
                }


                switch (alt28) {
            	case 1 :
            	    // InternalTomMapping.g:3544:3: rule__OperatorNode__Group_3__0
            	    {
            	    pushFollow(FOLLOW_15);
            	    rule__OperatorNode__Group_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop28;
                }
            } while (true);

             after(grammarAccess.getOperatorNodeAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OperatorNode__Group__3__Impl"


    // $ANTLR start "rule__OperatorNode__Group__4"
    // InternalTomMapping.g:3552:1: rule__OperatorNode__Group__4 : rule__OperatorNode__Group__4__Impl ;
    public final void rule__OperatorNode__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3556:1: ( rule__OperatorNode__Group__4__Impl )
            // InternalTomMapping.g:3557:2: rule__OperatorNode__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__OperatorNode__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OperatorNode__Group__4"


    // $ANTLR start "rule__OperatorNode__Group__4__Impl"
    // InternalTomMapping.g:3563:1: rule__OperatorNode__Group__4__Impl : ( ')' ) ;
    public final void rule__OperatorNode__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3567:1: ( ( ')' ) )
            // InternalTomMapping.g:3568:1: ( ')' )
            {
            // InternalTomMapping.g:3568:1: ( ')' )
            // InternalTomMapping.g:3569:2: ')'
            {
             before(grammarAccess.getOperatorNodeAccess().getRightParenthesisKeyword_4()); 
            match(input,32,FOLLOW_2); 
             after(grammarAccess.getOperatorNodeAccess().getRightParenthesisKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OperatorNode__Group__4__Impl"


    // $ANTLR start "rule__OperatorNode__Group_3__0"
    // InternalTomMapping.g:3579:1: rule__OperatorNode__Group_3__0 : rule__OperatorNode__Group_3__0__Impl rule__OperatorNode__Group_3__1 ;
    public final void rule__OperatorNode__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3583:1: ( rule__OperatorNode__Group_3__0__Impl rule__OperatorNode__Group_3__1 )
            // InternalTomMapping.g:3584:2: rule__OperatorNode__Group_3__0__Impl rule__OperatorNode__Group_3__1
            {
            pushFollow(FOLLOW_3);
            rule__OperatorNode__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OperatorNode__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OperatorNode__Group_3__0"


    // $ANTLR start "rule__OperatorNode__Group_3__0__Impl"
    // InternalTomMapping.g:3591:1: rule__OperatorNode__Group_3__0__Impl : ( ',' ) ;
    public final void rule__OperatorNode__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3595:1: ( ( ',' ) )
            // InternalTomMapping.g:3596:1: ( ',' )
            {
            // InternalTomMapping.g:3596:1: ( ',' )
            // InternalTomMapping.g:3597:2: ','
            {
             before(grammarAccess.getOperatorNodeAccess().getCommaKeyword_3_0()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getOperatorNodeAccess().getCommaKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OperatorNode__Group_3__0__Impl"


    // $ANTLR start "rule__OperatorNode__Group_3__1"
    // InternalTomMapping.g:3606:1: rule__OperatorNode__Group_3__1 : rule__OperatorNode__Group_3__1__Impl ;
    public final void rule__OperatorNode__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3610:1: ( rule__OperatorNode__Group_3__1__Impl )
            // InternalTomMapping.g:3611:2: rule__OperatorNode__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__OperatorNode__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OperatorNode__Group_3__1"


    // $ANTLR start "rule__OperatorNode__Group_3__1__Impl"
    // InternalTomMapping.g:3617:1: rule__OperatorNode__Group_3__1__Impl : ( ( rule__OperatorNode__NodesAssignment_3_1 ) ) ;
    public final void rule__OperatorNode__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3621:1: ( ( ( rule__OperatorNode__NodesAssignment_3_1 ) ) )
            // InternalTomMapping.g:3622:1: ( ( rule__OperatorNode__NodesAssignment_3_1 ) )
            {
            // InternalTomMapping.g:3622:1: ( ( rule__OperatorNode__NodesAssignment_3_1 ) )
            // InternalTomMapping.g:3623:2: ( rule__OperatorNode__NodesAssignment_3_1 )
            {
             before(grammarAccess.getOperatorNodeAccess().getNodesAssignment_3_1()); 
            // InternalTomMapping.g:3624:2: ( rule__OperatorNode__NodesAssignment_3_1 )
            // InternalTomMapping.g:3624:3: rule__OperatorNode__NodesAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__OperatorNode__NodesAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getOperatorNodeAccess().getNodesAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OperatorNode__Group_3__1__Impl"


    // $ANTLR start "rule__ClassOperator__Group__0"
    // InternalTomMapping.g:3633:1: rule__ClassOperator__Group__0 : rule__ClassOperator__Group__0__Impl rule__ClassOperator__Group__1 ;
    public final void rule__ClassOperator__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3637:1: ( rule__ClassOperator__Group__0__Impl rule__ClassOperator__Group__1 )
            // InternalTomMapping.g:3638:2: rule__ClassOperator__Group__0__Impl rule__ClassOperator__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__ClassOperator__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ClassOperator__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassOperator__Group__0"


    // $ANTLR start "rule__ClassOperator__Group__0__Impl"
    // InternalTomMapping.g:3645:1: rule__ClassOperator__Group__0__Impl : ( 'op' ) ;
    public final void rule__ClassOperator__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3649:1: ( ( 'op' ) )
            // InternalTomMapping.g:3650:1: ( 'op' )
            {
            // InternalTomMapping.g:3650:1: ( 'op' )
            // InternalTomMapping.g:3651:2: 'op'
            {
             before(grammarAccess.getClassOperatorAccess().getOpKeyword_0()); 
            match(input,33,FOLLOW_2); 
             after(grammarAccess.getClassOperatorAccess().getOpKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassOperator__Group__0__Impl"


    // $ANTLR start "rule__ClassOperator__Group__1"
    // InternalTomMapping.g:3660:1: rule__ClassOperator__Group__1 : rule__ClassOperator__Group__1__Impl rule__ClassOperator__Group__2 ;
    public final void rule__ClassOperator__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3664:1: ( rule__ClassOperator__Group__1__Impl rule__ClassOperator__Group__2 )
            // InternalTomMapping.g:3665:2: rule__ClassOperator__Group__1__Impl rule__ClassOperator__Group__2
            {
            pushFollow(FOLLOW_23);
            rule__ClassOperator__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ClassOperator__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassOperator__Group__1"


    // $ANTLR start "rule__ClassOperator__Group__1__Impl"
    // InternalTomMapping.g:3672:1: rule__ClassOperator__Group__1__Impl : ( ( rule__ClassOperator__NameAssignment_1 ) ) ;
    public final void rule__ClassOperator__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3676:1: ( ( ( rule__ClassOperator__NameAssignment_1 ) ) )
            // InternalTomMapping.g:3677:1: ( ( rule__ClassOperator__NameAssignment_1 ) )
            {
            // InternalTomMapping.g:3677:1: ( ( rule__ClassOperator__NameAssignment_1 ) )
            // InternalTomMapping.g:3678:2: ( rule__ClassOperator__NameAssignment_1 )
            {
             before(grammarAccess.getClassOperatorAccess().getNameAssignment_1()); 
            // InternalTomMapping.g:3679:2: ( rule__ClassOperator__NameAssignment_1 )
            // InternalTomMapping.g:3679:3: rule__ClassOperator__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__ClassOperator__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getClassOperatorAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassOperator__Group__1__Impl"


    // $ANTLR start "rule__ClassOperator__Group__2"
    // InternalTomMapping.g:3687:1: rule__ClassOperator__Group__2 : rule__ClassOperator__Group__2__Impl rule__ClassOperator__Group__3 ;
    public final void rule__ClassOperator__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3691:1: ( rule__ClassOperator__Group__2__Impl rule__ClassOperator__Group__3 )
            // InternalTomMapping.g:3692:2: rule__ClassOperator__Group__2__Impl rule__ClassOperator__Group__3
            {
            pushFollow(FOLLOW_3);
            rule__ClassOperator__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ClassOperator__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassOperator__Group__2"


    // $ANTLR start "rule__ClassOperator__Group__2__Impl"
    // InternalTomMapping.g:3699:1: rule__ClassOperator__Group__2__Impl : ( '::' ) ;
    public final void rule__ClassOperator__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3703:1: ( ( '::' ) )
            // InternalTomMapping.g:3704:1: ( '::' )
            {
            // InternalTomMapping.g:3704:1: ( '::' )
            // InternalTomMapping.g:3705:2: '::'
            {
             before(grammarAccess.getClassOperatorAccess().getColonColonKeyword_2()); 
            match(input,30,FOLLOW_2); 
             after(grammarAccess.getClassOperatorAccess().getColonColonKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassOperator__Group__2__Impl"


    // $ANTLR start "rule__ClassOperator__Group__3"
    // InternalTomMapping.g:3714:1: rule__ClassOperator__Group__3 : rule__ClassOperator__Group__3__Impl ;
    public final void rule__ClassOperator__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3718:1: ( rule__ClassOperator__Group__3__Impl )
            // InternalTomMapping.g:3719:2: rule__ClassOperator__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ClassOperator__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassOperator__Group__3"


    // $ANTLR start "rule__ClassOperator__Group__3__Impl"
    // InternalTomMapping.g:3725:1: rule__ClassOperator__Group__3__Impl : ( ( rule__ClassOperator__ClassAssignment_3 ) ) ;
    public final void rule__ClassOperator__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3729:1: ( ( ( rule__ClassOperator__ClassAssignment_3 ) ) )
            // InternalTomMapping.g:3730:1: ( ( rule__ClassOperator__ClassAssignment_3 ) )
            {
            // InternalTomMapping.g:3730:1: ( ( rule__ClassOperator__ClassAssignment_3 ) )
            // InternalTomMapping.g:3731:2: ( rule__ClassOperator__ClassAssignment_3 )
            {
             before(grammarAccess.getClassOperatorAccess().getClassAssignment_3()); 
            // InternalTomMapping.g:3732:2: ( rule__ClassOperator__ClassAssignment_3 )
            // InternalTomMapping.g:3732:3: rule__ClassOperator__ClassAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__ClassOperator__ClassAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getClassOperatorAccess().getClassAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassOperator__Group__3__Impl"


    // $ANTLR start "rule__ClassOperatorWithExceptions__Group__0"
    // InternalTomMapping.g:3741:1: rule__ClassOperatorWithExceptions__Group__0 : rule__ClassOperatorWithExceptions__Group__0__Impl rule__ClassOperatorWithExceptions__Group__1 ;
    public final void rule__ClassOperatorWithExceptions__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3745:1: ( rule__ClassOperatorWithExceptions__Group__0__Impl rule__ClassOperatorWithExceptions__Group__1 )
            // InternalTomMapping.g:3746:2: rule__ClassOperatorWithExceptions__Group__0__Impl rule__ClassOperatorWithExceptions__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__ClassOperatorWithExceptions__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ClassOperatorWithExceptions__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassOperatorWithExceptions__Group__0"


    // $ANTLR start "rule__ClassOperatorWithExceptions__Group__0__Impl"
    // InternalTomMapping.g:3753:1: rule__ClassOperatorWithExceptions__Group__0__Impl : ( 'op' ) ;
    public final void rule__ClassOperatorWithExceptions__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3757:1: ( ( 'op' ) )
            // InternalTomMapping.g:3758:1: ( 'op' )
            {
            // InternalTomMapping.g:3758:1: ( 'op' )
            // InternalTomMapping.g:3759:2: 'op'
            {
             before(grammarAccess.getClassOperatorWithExceptionsAccess().getOpKeyword_0()); 
            match(input,33,FOLLOW_2); 
             after(grammarAccess.getClassOperatorWithExceptionsAccess().getOpKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassOperatorWithExceptions__Group__0__Impl"


    // $ANTLR start "rule__ClassOperatorWithExceptions__Group__1"
    // InternalTomMapping.g:3768:1: rule__ClassOperatorWithExceptions__Group__1 : rule__ClassOperatorWithExceptions__Group__1__Impl rule__ClassOperatorWithExceptions__Group__2 ;
    public final void rule__ClassOperatorWithExceptions__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3772:1: ( rule__ClassOperatorWithExceptions__Group__1__Impl rule__ClassOperatorWithExceptions__Group__2 )
            // InternalTomMapping.g:3773:2: rule__ClassOperatorWithExceptions__Group__1__Impl rule__ClassOperatorWithExceptions__Group__2
            {
            pushFollow(FOLLOW_23);
            rule__ClassOperatorWithExceptions__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ClassOperatorWithExceptions__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassOperatorWithExceptions__Group__1"


    // $ANTLR start "rule__ClassOperatorWithExceptions__Group__1__Impl"
    // InternalTomMapping.g:3780:1: rule__ClassOperatorWithExceptions__Group__1__Impl : ( ( rule__ClassOperatorWithExceptions__NameAssignment_1 ) ) ;
    public final void rule__ClassOperatorWithExceptions__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3784:1: ( ( ( rule__ClassOperatorWithExceptions__NameAssignment_1 ) ) )
            // InternalTomMapping.g:3785:1: ( ( rule__ClassOperatorWithExceptions__NameAssignment_1 ) )
            {
            // InternalTomMapping.g:3785:1: ( ( rule__ClassOperatorWithExceptions__NameAssignment_1 ) )
            // InternalTomMapping.g:3786:2: ( rule__ClassOperatorWithExceptions__NameAssignment_1 )
            {
             before(grammarAccess.getClassOperatorWithExceptionsAccess().getNameAssignment_1()); 
            // InternalTomMapping.g:3787:2: ( rule__ClassOperatorWithExceptions__NameAssignment_1 )
            // InternalTomMapping.g:3787:3: rule__ClassOperatorWithExceptions__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__ClassOperatorWithExceptions__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getClassOperatorWithExceptionsAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassOperatorWithExceptions__Group__1__Impl"


    // $ANTLR start "rule__ClassOperatorWithExceptions__Group__2"
    // InternalTomMapping.g:3795:1: rule__ClassOperatorWithExceptions__Group__2 : rule__ClassOperatorWithExceptions__Group__2__Impl rule__ClassOperatorWithExceptions__Group__3 ;
    public final void rule__ClassOperatorWithExceptions__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3799:1: ( rule__ClassOperatorWithExceptions__Group__2__Impl rule__ClassOperatorWithExceptions__Group__3 )
            // InternalTomMapping.g:3800:2: rule__ClassOperatorWithExceptions__Group__2__Impl rule__ClassOperatorWithExceptions__Group__3
            {
            pushFollow(FOLLOW_3);
            rule__ClassOperatorWithExceptions__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ClassOperatorWithExceptions__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassOperatorWithExceptions__Group__2"


    // $ANTLR start "rule__ClassOperatorWithExceptions__Group__2__Impl"
    // InternalTomMapping.g:3807:1: rule__ClassOperatorWithExceptions__Group__2__Impl : ( '::' ) ;
    public final void rule__ClassOperatorWithExceptions__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3811:1: ( ( '::' ) )
            // InternalTomMapping.g:3812:1: ( '::' )
            {
            // InternalTomMapping.g:3812:1: ( '::' )
            // InternalTomMapping.g:3813:2: '::'
            {
             before(grammarAccess.getClassOperatorWithExceptionsAccess().getColonColonKeyword_2()); 
            match(input,30,FOLLOW_2); 
             after(grammarAccess.getClassOperatorWithExceptionsAccess().getColonColonKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassOperatorWithExceptions__Group__2__Impl"


    // $ANTLR start "rule__ClassOperatorWithExceptions__Group__3"
    // InternalTomMapping.g:3822:1: rule__ClassOperatorWithExceptions__Group__3 : rule__ClassOperatorWithExceptions__Group__3__Impl rule__ClassOperatorWithExceptions__Group__4 ;
    public final void rule__ClassOperatorWithExceptions__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3826:1: ( rule__ClassOperatorWithExceptions__Group__3__Impl rule__ClassOperatorWithExceptions__Group__4 )
            // InternalTomMapping.g:3827:2: rule__ClassOperatorWithExceptions__Group__3__Impl rule__ClassOperatorWithExceptions__Group__4
            {
            pushFollow(FOLLOW_24);
            rule__ClassOperatorWithExceptions__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ClassOperatorWithExceptions__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassOperatorWithExceptions__Group__3"


    // $ANTLR start "rule__ClassOperatorWithExceptions__Group__3__Impl"
    // InternalTomMapping.g:3834:1: rule__ClassOperatorWithExceptions__Group__3__Impl : ( ( rule__ClassOperatorWithExceptions__ClassAssignment_3 ) ) ;
    public final void rule__ClassOperatorWithExceptions__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3838:1: ( ( ( rule__ClassOperatorWithExceptions__ClassAssignment_3 ) ) )
            // InternalTomMapping.g:3839:1: ( ( rule__ClassOperatorWithExceptions__ClassAssignment_3 ) )
            {
            // InternalTomMapping.g:3839:1: ( ( rule__ClassOperatorWithExceptions__ClassAssignment_3 ) )
            // InternalTomMapping.g:3840:2: ( rule__ClassOperatorWithExceptions__ClassAssignment_3 )
            {
             before(grammarAccess.getClassOperatorWithExceptionsAccess().getClassAssignment_3()); 
            // InternalTomMapping.g:3841:2: ( rule__ClassOperatorWithExceptions__ClassAssignment_3 )
            // InternalTomMapping.g:3841:3: rule__ClassOperatorWithExceptions__ClassAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__ClassOperatorWithExceptions__ClassAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getClassOperatorWithExceptionsAccess().getClassAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassOperatorWithExceptions__Group__3__Impl"


    // $ANTLR start "rule__ClassOperatorWithExceptions__Group__4"
    // InternalTomMapping.g:3849:1: rule__ClassOperatorWithExceptions__Group__4 : rule__ClassOperatorWithExceptions__Group__4__Impl rule__ClassOperatorWithExceptions__Group__5 ;
    public final void rule__ClassOperatorWithExceptions__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3853:1: ( rule__ClassOperatorWithExceptions__Group__4__Impl rule__ClassOperatorWithExceptions__Group__5 )
            // InternalTomMapping.g:3854:2: rule__ClassOperatorWithExceptions__Group__4__Impl rule__ClassOperatorWithExceptions__Group__5
            {
            pushFollow(FOLLOW_26);
            rule__ClassOperatorWithExceptions__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ClassOperatorWithExceptions__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassOperatorWithExceptions__Group__4"


    // $ANTLR start "rule__ClassOperatorWithExceptions__Group__4__Impl"
    // InternalTomMapping.g:3861:1: rule__ClassOperatorWithExceptions__Group__4__Impl : ( '(' ) ;
    public final void rule__ClassOperatorWithExceptions__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3865:1: ( ( '(' ) )
            // InternalTomMapping.g:3866:1: ( '(' )
            {
            // InternalTomMapping.g:3866:1: ( '(' )
            // InternalTomMapping.g:3867:2: '('
            {
             before(grammarAccess.getClassOperatorWithExceptionsAccess().getLeftParenthesisKeyword_4()); 
            match(input,31,FOLLOW_2); 
             after(grammarAccess.getClassOperatorWithExceptionsAccess().getLeftParenthesisKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassOperatorWithExceptions__Group__4__Impl"


    // $ANTLR start "rule__ClassOperatorWithExceptions__Group__5"
    // InternalTomMapping.g:3876:1: rule__ClassOperatorWithExceptions__Group__5 : rule__ClassOperatorWithExceptions__Group__5__Impl rule__ClassOperatorWithExceptions__Group__6 ;
    public final void rule__ClassOperatorWithExceptions__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3880:1: ( rule__ClassOperatorWithExceptions__Group__5__Impl rule__ClassOperatorWithExceptions__Group__6 )
            // InternalTomMapping.g:3881:2: rule__ClassOperatorWithExceptions__Group__5__Impl rule__ClassOperatorWithExceptions__Group__6
            {
            pushFollow(FOLLOW_25);
            rule__ClassOperatorWithExceptions__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ClassOperatorWithExceptions__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassOperatorWithExceptions__Group__5"


    // $ANTLR start "rule__ClassOperatorWithExceptions__Group__5__Impl"
    // InternalTomMapping.g:3888:1: rule__ClassOperatorWithExceptions__Group__5__Impl : ( ( rule__ClassOperatorWithExceptions__ParametersAssignment_5 ) ) ;
    public final void rule__ClassOperatorWithExceptions__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3892:1: ( ( ( rule__ClassOperatorWithExceptions__ParametersAssignment_5 ) ) )
            // InternalTomMapping.g:3893:1: ( ( rule__ClassOperatorWithExceptions__ParametersAssignment_5 ) )
            {
            // InternalTomMapping.g:3893:1: ( ( rule__ClassOperatorWithExceptions__ParametersAssignment_5 ) )
            // InternalTomMapping.g:3894:2: ( rule__ClassOperatorWithExceptions__ParametersAssignment_5 )
            {
             before(grammarAccess.getClassOperatorWithExceptionsAccess().getParametersAssignment_5()); 
            // InternalTomMapping.g:3895:2: ( rule__ClassOperatorWithExceptions__ParametersAssignment_5 )
            // InternalTomMapping.g:3895:3: rule__ClassOperatorWithExceptions__ParametersAssignment_5
            {
            pushFollow(FOLLOW_2);
            rule__ClassOperatorWithExceptions__ParametersAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getClassOperatorWithExceptionsAccess().getParametersAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassOperatorWithExceptions__Group__5__Impl"


    // $ANTLR start "rule__ClassOperatorWithExceptions__Group__6"
    // InternalTomMapping.g:3903:1: rule__ClassOperatorWithExceptions__Group__6 : rule__ClassOperatorWithExceptions__Group__6__Impl rule__ClassOperatorWithExceptions__Group__7 ;
    public final void rule__ClassOperatorWithExceptions__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3907:1: ( rule__ClassOperatorWithExceptions__Group__6__Impl rule__ClassOperatorWithExceptions__Group__7 )
            // InternalTomMapping.g:3908:2: rule__ClassOperatorWithExceptions__Group__6__Impl rule__ClassOperatorWithExceptions__Group__7
            {
            pushFollow(FOLLOW_25);
            rule__ClassOperatorWithExceptions__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ClassOperatorWithExceptions__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassOperatorWithExceptions__Group__6"


    // $ANTLR start "rule__ClassOperatorWithExceptions__Group__6__Impl"
    // InternalTomMapping.g:3915:1: rule__ClassOperatorWithExceptions__Group__6__Impl : ( ( rule__ClassOperatorWithExceptions__Group_6__0 )* ) ;
    public final void rule__ClassOperatorWithExceptions__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3919:1: ( ( ( rule__ClassOperatorWithExceptions__Group_6__0 )* ) )
            // InternalTomMapping.g:3920:1: ( ( rule__ClassOperatorWithExceptions__Group_6__0 )* )
            {
            // InternalTomMapping.g:3920:1: ( ( rule__ClassOperatorWithExceptions__Group_6__0 )* )
            // InternalTomMapping.g:3921:2: ( rule__ClassOperatorWithExceptions__Group_6__0 )*
            {
             before(grammarAccess.getClassOperatorWithExceptionsAccess().getGroup_6()); 
            // InternalTomMapping.g:3922:2: ( rule__ClassOperatorWithExceptions__Group_6__0 )*
            loop29:
            do {
                int alt29=2;
                int LA29_0 = input.LA(1);

                if ( (LA29_0==21) ) {
                    alt29=1;
                }


                switch (alt29) {
            	case 1 :
            	    // InternalTomMapping.g:3922:3: rule__ClassOperatorWithExceptions__Group_6__0
            	    {
            	    pushFollow(FOLLOW_15);
            	    rule__ClassOperatorWithExceptions__Group_6__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop29;
                }
            } while (true);

             after(grammarAccess.getClassOperatorWithExceptionsAccess().getGroup_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassOperatorWithExceptions__Group__6__Impl"


    // $ANTLR start "rule__ClassOperatorWithExceptions__Group__7"
    // InternalTomMapping.g:3930:1: rule__ClassOperatorWithExceptions__Group__7 : rule__ClassOperatorWithExceptions__Group__7__Impl ;
    public final void rule__ClassOperatorWithExceptions__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3934:1: ( rule__ClassOperatorWithExceptions__Group__7__Impl )
            // InternalTomMapping.g:3935:2: rule__ClassOperatorWithExceptions__Group__7__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ClassOperatorWithExceptions__Group__7__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassOperatorWithExceptions__Group__7"


    // $ANTLR start "rule__ClassOperatorWithExceptions__Group__7__Impl"
    // InternalTomMapping.g:3941:1: rule__ClassOperatorWithExceptions__Group__7__Impl : ( ')' ) ;
    public final void rule__ClassOperatorWithExceptions__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3945:1: ( ( ')' ) )
            // InternalTomMapping.g:3946:1: ( ')' )
            {
            // InternalTomMapping.g:3946:1: ( ')' )
            // InternalTomMapping.g:3947:2: ')'
            {
             before(grammarAccess.getClassOperatorWithExceptionsAccess().getRightParenthesisKeyword_7()); 
            match(input,32,FOLLOW_2); 
             after(grammarAccess.getClassOperatorWithExceptionsAccess().getRightParenthesisKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassOperatorWithExceptions__Group__7__Impl"


    // $ANTLR start "rule__ClassOperatorWithExceptions__Group_6__0"
    // InternalTomMapping.g:3957:1: rule__ClassOperatorWithExceptions__Group_6__0 : rule__ClassOperatorWithExceptions__Group_6__0__Impl rule__ClassOperatorWithExceptions__Group_6__1 ;
    public final void rule__ClassOperatorWithExceptions__Group_6__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3961:1: ( rule__ClassOperatorWithExceptions__Group_6__0__Impl rule__ClassOperatorWithExceptions__Group_6__1 )
            // InternalTomMapping.g:3962:2: rule__ClassOperatorWithExceptions__Group_6__0__Impl rule__ClassOperatorWithExceptions__Group_6__1
            {
            pushFollow(FOLLOW_26);
            rule__ClassOperatorWithExceptions__Group_6__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ClassOperatorWithExceptions__Group_6__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassOperatorWithExceptions__Group_6__0"


    // $ANTLR start "rule__ClassOperatorWithExceptions__Group_6__0__Impl"
    // InternalTomMapping.g:3969:1: rule__ClassOperatorWithExceptions__Group_6__0__Impl : ( ',' ) ;
    public final void rule__ClassOperatorWithExceptions__Group_6__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3973:1: ( ( ',' ) )
            // InternalTomMapping.g:3974:1: ( ',' )
            {
            // InternalTomMapping.g:3974:1: ( ',' )
            // InternalTomMapping.g:3975:2: ','
            {
             before(grammarAccess.getClassOperatorWithExceptionsAccess().getCommaKeyword_6_0()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getClassOperatorWithExceptionsAccess().getCommaKeyword_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassOperatorWithExceptions__Group_6__0__Impl"


    // $ANTLR start "rule__ClassOperatorWithExceptions__Group_6__1"
    // InternalTomMapping.g:3984:1: rule__ClassOperatorWithExceptions__Group_6__1 : rule__ClassOperatorWithExceptions__Group_6__1__Impl ;
    public final void rule__ClassOperatorWithExceptions__Group_6__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3988:1: ( rule__ClassOperatorWithExceptions__Group_6__1__Impl )
            // InternalTomMapping.g:3989:2: rule__ClassOperatorWithExceptions__Group_6__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ClassOperatorWithExceptions__Group_6__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassOperatorWithExceptions__Group_6__1"


    // $ANTLR start "rule__ClassOperatorWithExceptions__Group_6__1__Impl"
    // InternalTomMapping.g:3995:1: rule__ClassOperatorWithExceptions__Group_6__1__Impl : ( ( rule__ClassOperatorWithExceptions__ParametersAssignment_6_1 ) ) ;
    public final void rule__ClassOperatorWithExceptions__Group_6__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:3999:1: ( ( ( rule__ClassOperatorWithExceptions__ParametersAssignment_6_1 ) ) )
            // InternalTomMapping.g:4000:1: ( ( rule__ClassOperatorWithExceptions__ParametersAssignment_6_1 ) )
            {
            // InternalTomMapping.g:4000:1: ( ( rule__ClassOperatorWithExceptions__ParametersAssignment_6_1 ) )
            // InternalTomMapping.g:4001:2: ( rule__ClassOperatorWithExceptions__ParametersAssignment_6_1 )
            {
             before(grammarAccess.getClassOperatorWithExceptionsAccess().getParametersAssignment_6_1()); 
            // InternalTomMapping.g:4002:2: ( rule__ClassOperatorWithExceptions__ParametersAssignment_6_1 )
            // InternalTomMapping.g:4002:3: rule__ClassOperatorWithExceptions__ParametersAssignment_6_1
            {
            pushFollow(FOLLOW_2);
            rule__ClassOperatorWithExceptions__ParametersAssignment_6_1();

            state._fsp--;


            }

             after(grammarAccess.getClassOperatorWithExceptionsAccess().getParametersAssignment_6_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassOperatorWithExceptions__Group_6__1__Impl"


    // $ANTLR start "rule__UserOperator__Group__0"
    // InternalTomMapping.g:4011:1: rule__UserOperator__Group__0 : rule__UserOperator__Group__0__Impl rule__UserOperator__Group__1 ;
    public final void rule__UserOperator__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4015:1: ( rule__UserOperator__Group__0__Impl rule__UserOperator__Group__1 )
            // InternalTomMapping.g:4016:2: rule__UserOperator__Group__0__Impl rule__UserOperator__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__UserOperator__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UserOperator__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__Group__0"


    // $ANTLR start "rule__UserOperator__Group__0__Impl"
    // InternalTomMapping.g:4023:1: rule__UserOperator__Group__0__Impl : ( 'op' ) ;
    public final void rule__UserOperator__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4027:1: ( ( 'op' ) )
            // InternalTomMapping.g:4028:1: ( 'op' )
            {
            // InternalTomMapping.g:4028:1: ( 'op' )
            // InternalTomMapping.g:4029:2: 'op'
            {
             before(grammarAccess.getUserOperatorAccess().getOpKeyword_0()); 
            match(input,33,FOLLOW_2); 
             after(grammarAccess.getUserOperatorAccess().getOpKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__Group__0__Impl"


    // $ANTLR start "rule__UserOperator__Group__1"
    // InternalTomMapping.g:4038:1: rule__UserOperator__Group__1 : rule__UserOperator__Group__1__Impl rule__UserOperator__Group__2 ;
    public final void rule__UserOperator__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4042:1: ( rule__UserOperator__Group__1__Impl rule__UserOperator__Group__2 )
            // InternalTomMapping.g:4043:2: rule__UserOperator__Group__1__Impl rule__UserOperator__Group__2
            {
            pushFollow(FOLLOW_24);
            rule__UserOperator__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UserOperator__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__Group__1"


    // $ANTLR start "rule__UserOperator__Group__1__Impl"
    // InternalTomMapping.g:4050:1: rule__UserOperator__Group__1__Impl : ( ( rule__UserOperator__NameAssignment_1 ) ) ;
    public final void rule__UserOperator__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4054:1: ( ( ( rule__UserOperator__NameAssignment_1 ) ) )
            // InternalTomMapping.g:4055:1: ( ( rule__UserOperator__NameAssignment_1 ) )
            {
            // InternalTomMapping.g:4055:1: ( ( rule__UserOperator__NameAssignment_1 ) )
            // InternalTomMapping.g:4056:2: ( rule__UserOperator__NameAssignment_1 )
            {
             before(grammarAccess.getUserOperatorAccess().getNameAssignment_1()); 
            // InternalTomMapping.g:4057:2: ( rule__UserOperator__NameAssignment_1 )
            // InternalTomMapping.g:4057:3: rule__UserOperator__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__UserOperator__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getUserOperatorAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__Group__1__Impl"


    // $ANTLR start "rule__UserOperator__Group__2"
    // InternalTomMapping.g:4065:1: rule__UserOperator__Group__2 : rule__UserOperator__Group__2__Impl rule__UserOperator__Group__3 ;
    public final void rule__UserOperator__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4069:1: ( rule__UserOperator__Group__2__Impl rule__UserOperator__Group__3 )
            // InternalTomMapping.g:4070:2: rule__UserOperator__Group__2__Impl rule__UserOperator__Group__3
            {
            pushFollow(FOLLOW_3);
            rule__UserOperator__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UserOperator__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__Group__2"


    // $ANTLR start "rule__UserOperator__Group__2__Impl"
    // InternalTomMapping.g:4077:1: rule__UserOperator__Group__2__Impl : ( '(' ) ;
    public final void rule__UserOperator__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4081:1: ( ( '(' ) )
            // InternalTomMapping.g:4082:1: ( '(' )
            {
            // InternalTomMapping.g:4082:1: ( '(' )
            // InternalTomMapping.g:4083:2: '('
            {
             before(grammarAccess.getUserOperatorAccess().getLeftParenthesisKeyword_2()); 
            match(input,31,FOLLOW_2); 
             after(grammarAccess.getUserOperatorAccess().getLeftParenthesisKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__Group__2__Impl"


    // $ANTLR start "rule__UserOperator__Group__3"
    // InternalTomMapping.g:4092:1: rule__UserOperator__Group__3 : rule__UserOperator__Group__3__Impl rule__UserOperator__Group__4 ;
    public final void rule__UserOperator__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4096:1: ( rule__UserOperator__Group__3__Impl rule__UserOperator__Group__4 )
            // InternalTomMapping.g:4097:2: rule__UserOperator__Group__3__Impl rule__UserOperator__Group__4
            {
            pushFollow(FOLLOW_25);
            rule__UserOperator__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UserOperator__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__Group__3"


    // $ANTLR start "rule__UserOperator__Group__3__Impl"
    // InternalTomMapping.g:4104:1: rule__UserOperator__Group__3__Impl : ( ( rule__UserOperator__ParametersAssignment_3 ) ) ;
    public final void rule__UserOperator__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4108:1: ( ( ( rule__UserOperator__ParametersAssignment_3 ) ) )
            // InternalTomMapping.g:4109:1: ( ( rule__UserOperator__ParametersAssignment_3 ) )
            {
            // InternalTomMapping.g:4109:1: ( ( rule__UserOperator__ParametersAssignment_3 ) )
            // InternalTomMapping.g:4110:2: ( rule__UserOperator__ParametersAssignment_3 )
            {
             before(grammarAccess.getUserOperatorAccess().getParametersAssignment_3()); 
            // InternalTomMapping.g:4111:2: ( rule__UserOperator__ParametersAssignment_3 )
            // InternalTomMapping.g:4111:3: rule__UserOperator__ParametersAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__UserOperator__ParametersAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getUserOperatorAccess().getParametersAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__Group__3__Impl"


    // $ANTLR start "rule__UserOperator__Group__4"
    // InternalTomMapping.g:4119:1: rule__UserOperator__Group__4 : rule__UserOperator__Group__4__Impl rule__UserOperator__Group__5 ;
    public final void rule__UserOperator__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4123:1: ( rule__UserOperator__Group__4__Impl rule__UserOperator__Group__5 )
            // InternalTomMapping.g:4124:2: rule__UserOperator__Group__4__Impl rule__UserOperator__Group__5
            {
            pushFollow(FOLLOW_25);
            rule__UserOperator__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UserOperator__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__Group__4"


    // $ANTLR start "rule__UserOperator__Group__4__Impl"
    // InternalTomMapping.g:4131:1: rule__UserOperator__Group__4__Impl : ( ( rule__UserOperator__Group_4__0 )* ) ;
    public final void rule__UserOperator__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4135:1: ( ( ( rule__UserOperator__Group_4__0 )* ) )
            // InternalTomMapping.g:4136:1: ( ( rule__UserOperator__Group_4__0 )* )
            {
            // InternalTomMapping.g:4136:1: ( ( rule__UserOperator__Group_4__0 )* )
            // InternalTomMapping.g:4137:2: ( rule__UserOperator__Group_4__0 )*
            {
             before(grammarAccess.getUserOperatorAccess().getGroup_4()); 
            // InternalTomMapping.g:4138:2: ( rule__UserOperator__Group_4__0 )*
            loop30:
            do {
                int alt30=2;
                int LA30_0 = input.LA(1);

                if ( (LA30_0==21) ) {
                    alt30=1;
                }


                switch (alt30) {
            	case 1 :
            	    // InternalTomMapping.g:4138:3: rule__UserOperator__Group_4__0
            	    {
            	    pushFollow(FOLLOW_15);
            	    rule__UserOperator__Group_4__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop30;
                }
            } while (true);

             after(grammarAccess.getUserOperatorAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__Group__4__Impl"


    // $ANTLR start "rule__UserOperator__Group__5"
    // InternalTomMapping.g:4146:1: rule__UserOperator__Group__5 : rule__UserOperator__Group__5__Impl rule__UserOperator__Group__6 ;
    public final void rule__UserOperator__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4150:1: ( rule__UserOperator__Group__5__Impl rule__UserOperator__Group__6 )
            // InternalTomMapping.g:4151:2: rule__UserOperator__Group__5__Impl rule__UserOperator__Group__6
            {
            pushFollow(FOLLOW_23);
            rule__UserOperator__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UserOperator__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__Group__5"


    // $ANTLR start "rule__UserOperator__Group__5__Impl"
    // InternalTomMapping.g:4158:1: rule__UserOperator__Group__5__Impl : ( ')' ) ;
    public final void rule__UserOperator__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4162:1: ( ( ')' ) )
            // InternalTomMapping.g:4163:1: ( ')' )
            {
            // InternalTomMapping.g:4163:1: ( ')' )
            // InternalTomMapping.g:4164:2: ')'
            {
             before(grammarAccess.getUserOperatorAccess().getRightParenthesisKeyword_5()); 
            match(input,32,FOLLOW_2); 
             after(grammarAccess.getUserOperatorAccess().getRightParenthesisKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__Group__5__Impl"


    // $ANTLR start "rule__UserOperator__Group__6"
    // InternalTomMapping.g:4173:1: rule__UserOperator__Group__6 : rule__UserOperator__Group__6__Impl rule__UserOperator__Group__7 ;
    public final void rule__UserOperator__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4177:1: ( rule__UserOperator__Group__6__Impl rule__UserOperator__Group__7 )
            // InternalTomMapping.g:4178:2: rule__UserOperator__Group__6__Impl rule__UserOperator__Group__7
            {
            pushFollow(FOLLOW_3);
            rule__UserOperator__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UserOperator__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__Group__6"


    // $ANTLR start "rule__UserOperator__Group__6__Impl"
    // InternalTomMapping.g:4185:1: rule__UserOperator__Group__6__Impl : ( '::' ) ;
    public final void rule__UserOperator__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4189:1: ( ( '::' ) )
            // InternalTomMapping.g:4190:1: ( '::' )
            {
            // InternalTomMapping.g:4190:1: ( '::' )
            // InternalTomMapping.g:4191:2: '::'
            {
             before(grammarAccess.getUserOperatorAccess().getColonColonKeyword_6()); 
            match(input,30,FOLLOW_2); 
             after(grammarAccess.getUserOperatorAccess().getColonColonKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__Group__6__Impl"


    // $ANTLR start "rule__UserOperator__Group__7"
    // InternalTomMapping.g:4200:1: rule__UserOperator__Group__7 : rule__UserOperator__Group__7__Impl rule__UserOperator__Group__8 ;
    public final void rule__UserOperator__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4204:1: ( rule__UserOperator__Group__7__Impl rule__UserOperator__Group__8 )
            // InternalTomMapping.g:4205:2: rule__UserOperator__Group__7__Impl rule__UserOperator__Group__8
            {
            pushFollow(FOLLOW_10);
            rule__UserOperator__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UserOperator__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__Group__7"


    // $ANTLR start "rule__UserOperator__Group__7__Impl"
    // InternalTomMapping.g:4212:1: rule__UserOperator__Group__7__Impl : ( ( rule__UserOperator__TypeAssignment_7 ) ) ;
    public final void rule__UserOperator__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4216:1: ( ( ( rule__UserOperator__TypeAssignment_7 ) ) )
            // InternalTomMapping.g:4217:1: ( ( rule__UserOperator__TypeAssignment_7 ) )
            {
            // InternalTomMapping.g:4217:1: ( ( rule__UserOperator__TypeAssignment_7 ) )
            // InternalTomMapping.g:4218:2: ( rule__UserOperator__TypeAssignment_7 )
            {
             before(grammarAccess.getUserOperatorAccess().getTypeAssignment_7()); 
            // InternalTomMapping.g:4219:2: ( rule__UserOperator__TypeAssignment_7 )
            // InternalTomMapping.g:4219:3: rule__UserOperator__TypeAssignment_7
            {
            pushFollow(FOLLOW_2);
            rule__UserOperator__TypeAssignment_7();

            state._fsp--;


            }

             after(grammarAccess.getUserOperatorAccess().getTypeAssignment_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__Group__7__Impl"


    // $ANTLR start "rule__UserOperator__Group__8"
    // InternalTomMapping.g:4227:1: rule__UserOperator__Group__8 : rule__UserOperator__Group__8__Impl rule__UserOperator__Group__9 ;
    public final void rule__UserOperator__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4231:1: ( rule__UserOperator__Group__8__Impl rule__UserOperator__Group__9 )
            // InternalTomMapping.g:4232:2: rule__UserOperator__Group__8__Impl rule__UserOperator__Group__9
            {
            pushFollow(FOLLOW_27);
            rule__UserOperator__Group__8__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UserOperator__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__Group__8"


    // $ANTLR start "rule__UserOperator__Group__8__Impl"
    // InternalTomMapping.g:4239:1: rule__UserOperator__Group__8__Impl : ( '{' ) ;
    public final void rule__UserOperator__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4243:1: ( ( '{' ) )
            // InternalTomMapping.g:4244:1: ( '{' )
            {
            // InternalTomMapping.g:4244:1: ( '{' )
            // InternalTomMapping.g:4245:2: '{'
            {
             before(grammarAccess.getUserOperatorAccess().getLeftCurlyBracketKeyword_8()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getUserOperatorAccess().getLeftCurlyBracketKeyword_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__Group__8__Impl"


    // $ANTLR start "rule__UserOperator__Group__9"
    // InternalTomMapping.g:4254:1: rule__UserOperator__Group__9 : rule__UserOperator__Group__9__Impl rule__UserOperator__Group__10 ;
    public final void rule__UserOperator__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4258:1: ( rule__UserOperator__Group__9__Impl rule__UserOperator__Group__10 )
            // InternalTomMapping.g:4259:2: rule__UserOperator__Group__9__Impl rule__UserOperator__Group__10
            {
            pushFollow(FOLLOW_4);
            rule__UserOperator__Group__9__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UserOperator__Group__10();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__Group__9"


    // $ANTLR start "rule__UserOperator__Group__9__Impl"
    // InternalTomMapping.g:4266:1: rule__UserOperator__Group__9__Impl : ( ( rule__UserOperator__AccessorsAssignment_9 ) ) ;
    public final void rule__UserOperator__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4270:1: ( ( ( rule__UserOperator__AccessorsAssignment_9 ) ) )
            // InternalTomMapping.g:4271:1: ( ( rule__UserOperator__AccessorsAssignment_9 ) )
            {
            // InternalTomMapping.g:4271:1: ( ( rule__UserOperator__AccessorsAssignment_9 ) )
            // InternalTomMapping.g:4272:2: ( rule__UserOperator__AccessorsAssignment_9 )
            {
             before(grammarAccess.getUserOperatorAccess().getAccessorsAssignment_9()); 
            // InternalTomMapping.g:4273:2: ( rule__UserOperator__AccessorsAssignment_9 )
            // InternalTomMapping.g:4273:3: rule__UserOperator__AccessorsAssignment_9
            {
            pushFollow(FOLLOW_2);
            rule__UserOperator__AccessorsAssignment_9();

            state._fsp--;


            }

             after(grammarAccess.getUserOperatorAccess().getAccessorsAssignment_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__Group__9__Impl"


    // $ANTLR start "rule__UserOperator__Group__10"
    // InternalTomMapping.g:4281:1: rule__UserOperator__Group__10 : rule__UserOperator__Group__10__Impl rule__UserOperator__Group__11 ;
    public final void rule__UserOperator__Group__10() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4285:1: ( rule__UserOperator__Group__10__Impl rule__UserOperator__Group__11 )
            // InternalTomMapping.g:4286:2: rule__UserOperator__Group__10__Impl rule__UserOperator__Group__11
            {
            pushFollow(FOLLOW_4);
            rule__UserOperator__Group__10__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UserOperator__Group__11();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__Group__10"


    // $ANTLR start "rule__UserOperator__Group__10__Impl"
    // InternalTomMapping.g:4293:1: rule__UserOperator__Group__10__Impl : ( ( rule__UserOperator__Group_10__0 )* ) ;
    public final void rule__UserOperator__Group__10__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4297:1: ( ( ( rule__UserOperator__Group_10__0 )* ) )
            // InternalTomMapping.g:4298:1: ( ( rule__UserOperator__Group_10__0 )* )
            {
            // InternalTomMapping.g:4298:1: ( ( rule__UserOperator__Group_10__0 )* )
            // InternalTomMapping.g:4299:2: ( rule__UserOperator__Group_10__0 )*
            {
             before(grammarAccess.getUserOperatorAccess().getGroup_10()); 
            // InternalTomMapping.g:4300:2: ( rule__UserOperator__Group_10__0 )*
            loop31:
            do {
                int alt31=2;
                int LA31_0 = input.LA(1);

                if ( (LA31_0==14) ) {
                    int LA31_1 = input.LA(2);

                    if ( (LA31_1==37) ) {
                        alt31=1;
                    }


                }


                switch (alt31) {
            	case 1 :
            	    // InternalTomMapping.g:4300:3: rule__UserOperator__Group_10__0
            	    {
            	    pushFollow(FOLLOW_18);
            	    rule__UserOperator__Group_10__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop31;
                }
            } while (true);

             after(grammarAccess.getUserOperatorAccess().getGroup_10()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__Group__10__Impl"


    // $ANTLR start "rule__UserOperator__Group__11"
    // InternalTomMapping.g:4308:1: rule__UserOperator__Group__11 : rule__UserOperator__Group__11__Impl rule__UserOperator__Group__12 ;
    public final void rule__UserOperator__Group__11() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4312:1: ( rule__UserOperator__Group__11__Impl rule__UserOperator__Group__12 )
            // InternalTomMapping.g:4313:2: rule__UserOperator__Group__11__Impl rule__UserOperator__Group__12
            {
            pushFollow(FOLLOW_28);
            rule__UserOperator__Group__11__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UserOperator__Group__12();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__Group__11"


    // $ANTLR start "rule__UserOperator__Group__11__Impl"
    // InternalTomMapping.g:4320:1: rule__UserOperator__Group__11__Impl : ( ';' ) ;
    public final void rule__UserOperator__Group__11__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4324:1: ( ( ';' ) )
            // InternalTomMapping.g:4325:1: ( ';' )
            {
            // InternalTomMapping.g:4325:1: ( ';' )
            // InternalTomMapping.g:4326:2: ';'
            {
             before(grammarAccess.getUserOperatorAccess().getSemicolonKeyword_11()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getUserOperatorAccess().getSemicolonKeyword_11()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__Group__11__Impl"


    // $ANTLR start "rule__UserOperator__Group__12"
    // InternalTomMapping.g:4335:1: rule__UserOperator__Group__12 : rule__UserOperator__Group__12__Impl rule__UserOperator__Group__13 ;
    public final void rule__UserOperator__Group__12() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4339:1: ( rule__UserOperator__Group__12__Impl rule__UserOperator__Group__13 )
            // InternalTomMapping.g:4340:2: rule__UserOperator__Group__12__Impl rule__UserOperator__Group__13
            {
            pushFollow(FOLLOW_29);
            rule__UserOperator__Group__12__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UserOperator__Group__13();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__Group__12"


    // $ANTLR start "rule__UserOperator__Group__12__Impl"
    // InternalTomMapping.g:4347:1: rule__UserOperator__Group__12__Impl : ( 'make' ) ;
    public final void rule__UserOperator__Group__12__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4351:1: ( ( 'make' ) )
            // InternalTomMapping.g:4352:1: ( 'make' )
            {
            // InternalTomMapping.g:4352:1: ( 'make' )
            // InternalTomMapping.g:4353:2: 'make'
            {
             before(grammarAccess.getUserOperatorAccess().getMakeKeyword_12()); 
            match(input,34,FOLLOW_2); 
             after(grammarAccess.getUserOperatorAccess().getMakeKeyword_12()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__Group__12__Impl"


    // $ANTLR start "rule__UserOperator__Group__13"
    // InternalTomMapping.g:4362:1: rule__UserOperator__Group__13 : rule__UserOperator__Group__13__Impl rule__UserOperator__Group__14 ;
    public final void rule__UserOperator__Group__13() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4366:1: ( rule__UserOperator__Group__13__Impl rule__UserOperator__Group__14 )
            // InternalTomMapping.g:4367:2: rule__UserOperator__Group__13__Impl rule__UserOperator__Group__14
            {
            pushFollow(FOLLOW_9);
            rule__UserOperator__Group__13__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UserOperator__Group__14();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__Group__13"


    // $ANTLR start "rule__UserOperator__Group__13__Impl"
    // InternalTomMapping.g:4374:1: rule__UserOperator__Group__13__Impl : ( '=' ) ;
    public final void rule__UserOperator__Group__13__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4378:1: ( ( '=' ) )
            // InternalTomMapping.g:4379:1: ( '=' )
            {
            // InternalTomMapping.g:4379:1: ( '=' )
            // InternalTomMapping.g:4380:2: '='
            {
             before(grammarAccess.getUserOperatorAccess().getEqualsSignKeyword_13()); 
            match(input,35,FOLLOW_2); 
             after(grammarAccess.getUserOperatorAccess().getEqualsSignKeyword_13()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__Group__13__Impl"


    // $ANTLR start "rule__UserOperator__Group__14"
    // InternalTomMapping.g:4389:1: rule__UserOperator__Group__14 : rule__UserOperator__Group__14__Impl rule__UserOperator__Group__15 ;
    public final void rule__UserOperator__Group__14() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4393:1: ( rule__UserOperator__Group__14__Impl rule__UserOperator__Group__15 )
            // InternalTomMapping.g:4394:2: rule__UserOperator__Group__14__Impl rule__UserOperator__Group__15
            {
            pushFollow(FOLLOW_4);
            rule__UserOperator__Group__14__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UserOperator__Group__15();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__Group__14"


    // $ANTLR start "rule__UserOperator__Group__14__Impl"
    // InternalTomMapping.g:4401:1: rule__UserOperator__Group__14__Impl : ( ( rule__UserOperator__MakeAssignment_14 ) ) ;
    public final void rule__UserOperator__Group__14__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4405:1: ( ( ( rule__UserOperator__MakeAssignment_14 ) ) )
            // InternalTomMapping.g:4406:1: ( ( rule__UserOperator__MakeAssignment_14 ) )
            {
            // InternalTomMapping.g:4406:1: ( ( rule__UserOperator__MakeAssignment_14 ) )
            // InternalTomMapping.g:4407:2: ( rule__UserOperator__MakeAssignment_14 )
            {
             before(grammarAccess.getUserOperatorAccess().getMakeAssignment_14()); 
            // InternalTomMapping.g:4408:2: ( rule__UserOperator__MakeAssignment_14 )
            // InternalTomMapping.g:4408:3: rule__UserOperator__MakeAssignment_14
            {
            pushFollow(FOLLOW_2);
            rule__UserOperator__MakeAssignment_14();

            state._fsp--;


            }

             after(grammarAccess.getUserOperatorAccess().getMakeAssignment_14()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__Group__14__Impl"


    // $ANTLR start "rule__UserOperator__Group__15"
    // InternalTomMapping.g:4416:1: rule__UserOperator__Group__15 : rule__UserOperator__Group__15__Impl rule__UserOperator__Group__16 ;
    public final void rule__UserOperator__Group__15() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4420:1: ( rule__UserOperator__Group__15__Impl rule__UserOperator__Group__16 )
            // InternalTomMapping.g:4421:2: rule__UserOperator__Group__15__Impl rule__UserOperator__Group__16
            {
            pushFollow(FOLLOW_30);
            rule__UserOperator__Group__15__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UserOperator__Group__16();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__Group__15"


    // $ANTLR start "rule__UserOperator__Group__15__Impl"
    // InternalTomMapping.g:4428:1: rule__UserOperator__Group__15__Impl : ( ';' ) ;
    public final void rule__UserOperator__Group__15__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4432:1: ( ( ';' ) )
            // InternalTomMapping.g:4433:1: ( ';' )
            {
            // InternalTomMapping.g:4433:1: ( ';' )
            // InternalTomMapping.g:4434:2: ';'
            {
             before(grammarAccess.getUserOperatorAccess().getSemicolonKeyword_15()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getUserOperatorAccess().getSemicolonKeyword_15()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__Group__15__Impl"


    // $ANTLR start "rule__UserOperator__Group__16"
    // InternalTomMapping.g:4443:1: rule__UserOperator__Group__16 : rule__UserOperator__Group__16__Impl rule__UserOperator__Group__17 ;
    public final void rule__UserOperator__Group__16() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4447:1: ( rule__UserOperator__Group__16__Impl rule__UserOperator__Group__17 )
            // InternalTomMapping.g:4448:2: rule__UserOperator__Group__16__Impl rule__UserOperator__Group__17
            {
            pushFollow(FOLLOW_29);
            rule__UserOperator__Group__16__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UserOperator__Group__17();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__Group__16"


    // $ANTLR start "rule__UserOperator__Group__16__Impl"
    // InternalTomMapping.g:4455:1: rule__UserOperator__Group__16__Impl : ( 'is_fsym' ) ;
    public final void rule__UserOperator__Group__16__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4459:1: ( ( 'is_fsym' ) )
            // InternalTomMapping.g:4460:1: ( 'is_fsym' )
            {
            // InternalTomMapping.g:4460:1: ( 'is_fsym' )
            // InternalTomMapping.g:4461:2: 'is_fsym'
            {
             before(grammarAccess.getUserOperatorAccess().getIs_fsymKeyword_16()); 
            match(input,36,FOLLOW_2); 
             after(grammarAccess.getUserOperatorAccess().getIs_fsymKeyword_16()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__Group__16__Impl"


    // $ANTLR start "rule__UserOperator__Group__17"
    // InternalTomMapping.g:4470:1: rule__UserOperator__Group__17 : rule__UserOperator__Group__17__Impl rule__UserOperator__Group__18 ;
    public final void rule__UserOperator__Group__17() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4474:1: ( rule__UserOperator__Group__17__Impl rule__UserOperator__Group__18 )
            // InternalTomMapping.g:4475:2: rule__UserOperator__Group__17__Impl rule__UserOperator__Group__18
            {
            pushFollow(FOLLOW_9);
            rule__UserOperator__Group__17__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UserOperator__Group__18();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__Group__17"


    // $ANTLR start "rule__UserOperator__Group__17__Impl"
    // InternalTomMapping.g:4482:1: rule__UserOperator__Group__17__Impl : ( '=' ) ;
    public final void rule__UserOperator__Group__17__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4486:1: ( ( '=' ) )
            // InternalTomMapping.g:4487:1: ( '=' )
            {
            // InternalTomMapping.g:4487:1: ( '=' )
            // InternalTomMapping.g:4488:2: '='
            {
             before(grammarAccess.getUserOperatorAccess().getEqualsSignKeyword_17()); 
            match(input,35,FOLLOW_2); 
             after(grammarAccess.getUserOperatorAccess().getEqualsSignKeyword_17()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__Group__17__Impl"


    // $ANTLR start "rule__UserOperator__Group__18"
    // InternalTomMapping.g:4497:1: rule__UserOperator__Group__18 : rule__UserOperator__Group__18__Impl rule__UserOperator__Group__19 ;
    public final void rule__UserOperator__Group__18() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4501:1: ( rule__UserOperator__Group__18__Impl rule__UserOperator__Group__19 )
            // InternalTomMapping.g:4502:2: rule__UserOperator__Group__18__Impl rule__UserOperator__Group__19
            {
            pushFollow(FOLLOW_4);
            rule__UserOperator__Group__18__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UserOperator__Group__19();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__Group__18"


    // $ANTLR start "rule__UserOperator__Group__18__Impl"
    // InternalTomMapping.g:4509:1: rule__UserOperator__Group__18__Impl : ( ( rule__UserOperator__TestAssignment_18 ) ) ;
    public final void rule__UserOperator__Group__18__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4513:1: ( ( ( rule__UserOperator__TestAssignment_18 ) ) )
            // InternalTomMapping.g:4514:1: ( ( rule__UserOperator__TestAssignment_18 ) )
            {
            // InternalTomMapping.g:4514:1: ( ( rule__UserOperator__TestAssignment_18 ) )
            // InternalTomMapping.g:4515:2: ( rule__UserOperator__TestAssignment_18 )
            {
             before(grammarAccess.getUserOperatorAccess().getTestAssignment_18()); 
            // InternalTomMapping.g:4516:2: ( rule__UserOperator__TestAssignment_18 )
            // InternalTomMapping.g:4516:3: rule__UserOperator__TestAssignment_18
            {
            pushFollow(FOLLOW_2);
            rule__UserOperator__TestAssignment_18();

            state._fsp--;


            }

             after(grammarAccess.getUserOperatorAccess().getTestAssignment_18()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__Group__18__Impl"


    // $ANTLR start "rule__UserOperator__Group__19"
    // InternalTomMapping.g:4524:1: rule__UserOperator__Group__19 : rule__UserOperator__Group__19__Impl rule__UserOperator__Group__20 ;
    public final void rule__UserOperator__Group__19() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4528:1: ( rule__UserOperator__Group__19__Impl rule__UserOperator__Group__20 )
            // InternalTomMapping.g:4529:2: rule__UserOperator__Group__19__Impl rule__UserOperator__Group__20
            {
            pushFollow(FOLLOW_12);
            rule__UserOperator__Group__19__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UserOperator__Group__20();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__Group__19"


    // $ANTLR start "rule__UserOperator__Group__19__Impl"
    // InternalTomMapping.g:4536:1: rule__UserOperator__Group__19__Impl : ( ';' ) ;
    public final void rule__UserOperator__Group__19__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4540:1: ( ( ';' ) )
            // InternalTomMapping.g:4541:1: ( ';' )
            {
            // InternalTomMapping.g:4541:1: ( ';' )
            // InternalTomMapping.g:4542:2: ';'
            {
             before(grammarAccess.getUserOperatorAccess().getSemicolonKeyword_19()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getUserOperatorAccess().getSemicolonKeyword_19()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__Group__19__Impl"


    // $ANTLR start "rule__UserOperator__Group__20"
    // InternalTomMapping.g:4551:1: rule__UserOperator__Group__20 : rule__UserOperator__Group__20__Impl ;
    public final void rule__UserOperator__Group__20() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4555:1: ( rule__UserOperator__Group__20__Impl )
            // InternalTomMapping.g:4556:2: rule__UserOperator__Group__20__Impl
            {
            pushFollow(FOLLOW_2);
            rule__UserOperator__Group__20__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__Group__20"


    // $ANTLR start "rule__UserOperator__Group__20__Impl"
    // InternalTomMapping.g:4562:1: rule__UserOperator__Group__20__Impl : ( '}' ) ;
    public final void rule__UserOperator__Group__20__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4566:1: ( ( '}' ) )
            // InternalTomMapping.g:4567:1: ( '}' )
            {
            // InternalTomMapping.g:4567:1: ( '}' )
            // InternalTomMapping.g:4568:2: '}'
            {
             before(grammarAccess.getUserOperatorAccess().getRightCurlyBracketKeyword_20()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getUserOperatorAccess().getRightCurlyBracketKeyword_20()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__Group__20__Impl"


    // $ANTLR start "rule__UserOperator__Group_4__0"
    // InternalTomMapping.g:4578:1: rule__UserOperator__Group_4__0 : rule__UserOperator__Group_4__0__Impl rule__UserOperator__Group_4__1 ;
    public final void rule__UserOperator__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4582:1: ( rule__UserOperator__Group_4__0__Impl rule__UserOperator__Group_4__1 )
            // InternalTomMapping.g:4583:2: rule__UserOperator__Group_4__0__Impl rule__UserOperator__Group_4__1
            {
            pushFollow(FOLLOW_3);
            rule__UserOperator__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UserOperator__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__Group_4__0"


    // $ANTLR start "rule__UserOperator__Group_4__0__Impl"
    // InternalTomMapping.g:4590:1: rule__UserOperator__Group_4__0__Impl : ( ',' ) ;
    public final void rule__UserOperator__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4594:1: ( ( ',' ) )
            // InternalTomMapping.g:4595:1: ( ',' )
            {
            // InternalTomMapping.g:4595:1: ( ',' )
            // InternalTomMapping.g:4596:2: ','
            {
             before(grammarAccess.getUserOperatorAccess().getCommaKeyword_4_0()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getUserOperatorAccess().getCommaKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__Group_4__0__Impl"


    // $ANTLR start "rule__UserOperator__Group_4__1"
    // InternalTomMapping.g:4605:1: rule__UserOperator__Group_4__1 : rule__UserOperator__Group_4__1__Impl ;
    public final void rule__UserOperator__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4609:1: ( rule__UserOperator__Group_4__1__Impl )
            // InternalTomMapping.g:4610:2: rule__UserOperator__Group_4__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__UserOperator__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__Group_4__1"


    // $ANTLR start "rule__UserOperator__Group_4__1__Impl"
    // InternalTomMapping.g:4616:1: rule__UserOperator__Group_4__1__Impl : ( ( rule__UserOperator__ParametersAssignment_4_1 ) ) ;
    public final void rule__UserOperator__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4620:1: ( ( ( rule__UserOperator__ParametersAssignment_4_1 ) ) )
            // InternalTomMapping.g:4621:1: ( ( rule__UserOperator__ParametersAssignment_4_1 ) )
            {
            // InternalTomMapping.g:4621:1: ( ( rule__UserOperator__ParametersAssignment_4_1 ) )
            // InternalTomMapping.g:4622:2: ( rule__UserOperator__ParametersAssignment_4_1 )
            {
             before(grammarAccess.getUserOperatorAccess().getParametersAssignment_4_1()); 
            // InternalTomMapping.g:4623:2: ( rule__UserOperator__ParametersAssignment_4_1 )
            // InternalTomMapping.g:4623:3: rule__UserOperator__ParametersAssignment_4_1
            {
            pushFollow(FOLLOW_2);
            rule__UserOperator__ParametersAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getUserOperatorAccess().getParametersAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__Group_4__1__Impl"


    // $ANTLR start "rule__UserOperator__Group_10__0"
    // InternalTomMapping.g:4632:1: rule__UserOperator__Group_10__0 : rule__UserOperator__Group_10__0__Impl rule__UserOperator__Group_10__1 ;
    public final void rule__UserOperator__Group_10__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4636:1: ( rule__UserOperator__Group_10__0__Impl rule__UserOperator__Group_10__1 )
            // InternalTomMapping.g:4637:2: rule__UserOperator__Group_10__0__Impl rule__UserOperator__Group_10__1
            {
            pushFollow(FOLLOW_27);
            rule__UserOperator__Group_10__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UserOperator__Group_10__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__Group_10__0"


    // $ANTLR start "rule__UserOperator__Group_10__0__Impl"
    // InternalTomMapping.g:4644:1: rule__UserOperator__Group_10__0__Impl : ( ';' ) ;
    public final void rule__UserOperator__Group_10__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4648:1: ( ( ';' ) )
            // InternalTomMapping.g:4649:1: ( ';' )
            {
            // InternalTomMapping.g:4649:1: ( ';' )
            // InternalTomMapping.g:4650:2: ';'
            {
             before(grammarAccess.getUserOperatorAccess().getSemicolonKeyword_10_0()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getUserOperatorAccess().getSemicolonKeyword_10_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__Group_10__0__Impl"


    // $ANTLR start "rule__UserOperator__Group_10__1"
    // InternalTomMapping.g:4659:1: rule__UserOperator__Group_10__1 : rule__UserOperator__Group_10__1__Impl ;
    public final void rule__UserOperator__Group_10__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4663:1: ( rule__UserOperator__Group_10__1__Impl )
            // InternalTomMapping.g:4664:2: rule__UserOperator__Group_10__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__UserOperator__Group_10__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__Group_10__1"


    // $ANTLR start "rule__UserOperator__Group_10__1__Impl"
    // InternalTomMapping.g:4670:1: rule__UserOperator__Group_10__1__Impl : ( ( rule__UserOperator__AccessorsAssignment_10_1 ) ) ;
    public final void rule__UserOperator__Group_10__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4674:1: ( ( ( rule__UserOperator__AccessorsAssignment_10_1 ) ) )
            // InternalTomMapping.g:4675:1: ( ( rule__UserOperator__AccessorsAssignment_10_1 ) )
            {
            // InternalTomMapping.g:4675:1: ( ( rule__UserOperator__AccessorsAssignment_10_1 ) )
            // InternalTomMapping.g:4676:2: ( rule__UserOperator__AccessorsAssignment_10_1 )
            {
             before(grammarAccess.getUserOperatorAccess().getAccessorsAssignment_10_1()); 
            // InternalTomMapping.g:4677:2: ( rule__UserOperator__AccessorsAssignment_10_1 )
            // InternalTomMapping.g:4677:3: rule__UserOperator__AccessorsAssignment_10_1
            {
            pushFollow(FOLLOW_2);
            rule__UserOperator__AccessorsAssignment_10_1();

            state._fsp--;


            }

             after(grammarAccess.getUserOperatorAccess().getAccessorsAssignment_10_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__Group_10__1__Impl"


    // $ANTLR start "rule__Parameter__Group__0"
    // InternalTomMapping.g:4686:1: rule__Parameter__Group__0 : rule__Parameter__Group__0__Impl rule__Parameter__Group__1 ;
    public final void rule__Parameter__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4690:1: ( rule__Parameter__Group__0__Impl rule__Parameter__Group__1 )
            // InternalTomMapping.g:4691:2: rule__Parameter__Group__0__Impl rule__Parameter__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Parameter__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Parameter__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__Group__0"


    // $ANTLR start "rule__Parameter__Group__0__Impl"
    // InternalTomMapping.g:4698:1: rule__Parameter__Group__0__Impl : ( ( rule__Parameter__TypeAssignment_0 ) ) ;
    public final void rule__Parameter__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4702:1: ( ( ( rule__Parameter__TypeAssignment_0 ) ) )
            // InternalTomMapping.g:4703:1: ( ( rule__Parameter__TypeAssignment_0 ) )
            {
            // InternalTomMapping.g:4703:1: ( ( rule__Parameter__TypeAssignment_0 ) )
            // InternalTomMapping.g:4704:2: ( rule__Parameter__TypeAssignment_0 )
            {
             before(grammarAccess.getParameterAccess().getTypeAssignment_0()); 
            // InternalTomMapping.g:4705:2: ( rule__Parameter__TypeAssignment_0 )
            // InternalTomMapping.g:4705:3: rule__Parameter__TypeAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Parameter__TypeAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getParameterAccess().getTypeAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__Group__0__Impl"


    // $ANTLR start "rule__Parameter__Group__1"
    // InternalTomMapping.g:4713:1: rule__Parameter__Group__1 : rule__Parameter__Group__1__Impl ;
    public final void rule__Parameter__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4717:1: ( rule__Parameter__Group__1__Impl )
            // InternalTomMapping.g:4718:2: rule__Parameter__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Parameter__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__Group__1"


    // $ANTLR start "rule__Parameter__Group__1__Impl"
    // InternalTomMapping.g:4724:1: rule__Parameter__Group__1__Impl : ( ( rule__Parameter__NameAssignment_1 ) ) ;
    public final void rule__Parameter__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4728:1: ( ( ( rule__Parameter__NameAssignment_1 ) ) )
            // InternalTomMapping.g:4729:1: ( ( rule__Parameter__NameAssignment_1 ) )
            {
            // InternalTomMapping.g:4729:1: ( ( rule__Parameter__NameAssignment_1 ) )
            // InternalTomMapping.g:4730:2: ( rule__Parameter__NameAssignment_1 )
            {
             before(grammarAccess.getParameterAccess().getNameAssignment_1()); 
            // InternalTomMapping.g:4731:2: ( rule__Parameter__NameAssignment_1 )
            // InternalTomMapping.g:4731:3: rule__Parameter__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Parameter__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getParameterAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__Group__1__Impl"


    // $ANTLR start "rule__Accessor__Group__0"
    // InternalTomMapping.g:4740:1: rule__Accessor__Group__0 : rule__Accessor__Group__0__Impl rule__Accessor__Group__1 ;
    public final void rule__Accessor__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4744:1: ( rule__Accessor__Group__0__Impl rule__Accessor__Group__1 )
            // InternalTomMapping.g:4745:2: rule__Accessor__Group__0__Impl rule__Accessor__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Accessor__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Accessor__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Accessor__Group__0"


    // $ANTLR start "rule__Accessor__Group__0__Impl"
    // InternalTomMapping.g:4752:1: rule__Accessor__Group__0__Impl : ( 'slot' ) ;
    public final void rule__Accessor__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4756:1: ( ( 'slot' ) )
            // InternalTomMapping.g:4757:1: ( 'slot' )
            {
            // InternalTomMapping.g:4757:1: ( 'slot' )
            // InternalTomMapping.g:4758:2: 'slot'
            {
             before(grammarAccess.getAccessorAccess().getSlotKeyword_0()); 
            match(input,37,FOLLOW_2); 
             after(grammarAccess.getAccessorAccess().getSlotKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Accessor__Group__0__Impl"


    // $ANTLR start "rule__Accessor__Group__1"
    // InternalTomMapping.g:4767:1: rule__Accessor__Group__1 : rule__Accessor__Group__1__Impl rule__Accessor__Group__2 ;
    public final void rule__Accessor__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4771:1: ( rule__Accessor__Group__1__Impl rule__Accessor__Group__2 )
            // InternalTomMapping.g:4772:2: rule__Accessor__Group__1__Impl rule__Accessor__Group__2
            {
            pushFollow(FOLLOW_29);
            rule__Accessor__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Accessor__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Accessor__Group__1"


    // $ANTLR start "rule__Accessor__Group__1__Impl"
    // InternalTomMapping.g:4779:1: rule__Accessor__Group__1__Impl : ( ( rule__Accessor__SlotAssignment_1 ) ) ;
    public final void rule__Accessor__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4783:1: ( ( ( rule__Accessor__SlotAssignment_1 ) ) )
            // InternalTomMapping.g:4784:1: ( ( rule__Accessor__SlotAssignment_1 ) )
            {
            // InternalTomMapping.g:4784:1: ( ( rule__Accessor__SlotAssignment_1 ) )
            // InternalTomMapping.g:4785:2: ( rule__Accessor__SlotAssignment_1 )
            {
             before(grammarAccess.getAccessorAccess().getSlotAssignment_1()); 
            // InternalTomMapping.g:4786:2: ( rule__Accessor__SlotAssignment_1 )
            // InternalTomMapping.g:4786:3: rule__Accessor__SlotAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Accessor__SlotAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getAccessorAccess().getSlotAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Accessor__Group__1__Impl"


    // $ANTLR start "rule__Accessor__Group__2"
    // InternalTomMapping.g:4794:1: rule__Accessor__Group__2 : rule__Accessor__Group__2__Impl rule__Accessor__Group__3 ;
    public final void rule__Accessor__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4798:1: ( rule__Accessor__Group__2__Impl rule__Accessor__Group__3 )
            // InternalTomMapping.g:4799:2: rule__Accessor__Group__2__Impl rule__Accessor__Group__3
            {
            pushFollow(FOLLOW_9);
            rule__Accessor__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Accessor__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Accessor__Group__2"


    // $ANTLR start "rule__Accessor__Group__2__Impl"
    // InternalTomMapping.g:4806:1: rule__Accessor__Group__2__Impl : ( '=' ) ;
    public final void rule__Accessor__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4810:1: ( ( '=' ) )
            // InternalTomMapping.g:4811:1: ( '=' )
            {
            // InternalTomMapping.g:4811:1: ( '=' )
            // InternalTomMapping.g:4812:2: '='
            {
             before(grammarAccess.getAccessorAccess().getEqualsSignKeyword_2()); 
            match(input,35,FOLLOW_2); 
             after(grammarAccess.getAccessorAccess().getEqualsSignKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Accessor__Group__2__Impl"


    // $ANTLR start "rule__Accessor__Group__3"
    // InternalTomMapping.g:4821:1: rule__Accessor__Group__3 : rule__Accessor__Group__3__Impl ;
    public final void rule__Accessor__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4825:1: ( rule__Accessor__Group__3__Impl )
            // InternalTomMapping.g:4826:2: rule__Accessor__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Accessor__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Accessor__Group__3"


    // $ANTLR start "rule__Accessor__Group__3__Impl"
    // InternalTomMapping.g:4832:1: rule__Accessor__Group__3__Impl : ( ( rule__Accessor__JavaAssignment_3 ) ) ;
    public final void rule__Accessor__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4836:1: ( ( ( rule__Accessor__JavaAssignment_3 ) ) )
            // InternalTomMapping.g:4837:1: ( ( rule__Accessor__JavaAssignment_3 ) )
            {
            // InternalTomMapping.g:4837:1: ( ( rule__Accessor__JavaAssignment_3 ) )
            // InternalTomMapping.g:4838:2: ( rule__Accessor__JavaAssignment_3 )
            {
             before(grammarAccess.getAccessorAccess().getJavaAssignment_3()); 
            // InternalTomMapping.g:4839:2: ( rule__Accessor__JavaAssignment_3 )
            // InternalTomMapping.g:4839:3: rule__Accessor__JavaAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Accessor__JavaAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getAccessorAccess().getJavaAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Accessor__Group__3__Impl"


    // $ANTLR start "rule__FeatureException__Group__0"
    // InternalTomMapping.g:4848:1: rule__FeatureException__Group__0 : rule__FeatureException__Group__0__Impl rule__FeatureException__Group__1 ;
    public final void rule__FeatureException__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4852:1: ( rule__FeatureException__Group__0__Impl rule__FeatureException__Group__1 )
            // InternalTomMapping.g:4853:2: rule__FeatureException__Group__0__Impl rule__FeatureException__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__FeatureException__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FeatureException__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureException__Group__0"


    // $ANTLR start "rule__FeatureException__Group__0__Impl"
    // InternalTomMapping.g:4860:1: rule__FeatureException__Group__0__Impl : ( 'ignore' ) ;
    public final void rule__FeatureException__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4864:1: ( ( 'ignore' ) )
            // InternalTomMapping.g:4865:1: ( 'ignore' )
            {
            // InternalTomMapping.g:4865:1: ( 'ignore' )
            // InternalTomMapping.g:4866:2: 'ignore'
            {
             before(grammarAccess.getFeatureExceptionAccess().getIgnoreKeyword_0()); 
            match(input,38,FOLLOW_2); 
             after(grammarAccess.getFeatureExceptionAccess().getIgnoreKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureException__Group__0__Impl"


    // $ANTLR start "rule__FeatureException__Group__1"
    // InternalTomMapping.g:4875:1: rule__FeatureException__Group__1 : rule__FeatureException__Group__1__Impl ;
    public final void rule__FeatureException__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4879:1: ( rule__FeatureException__Group__1__Impl )
            // InternalTomMapping.g:4880:2: rule__FeatureException__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__FeatureException__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureException__Group__1"


    // $ANTLR start "rule__FeatureException__Group__1__Impl"
    // InternalTomMapping.g:4886:1: rule__FeatureException__Group__1__Impl : ( ( rule__FeatureException__FeatureAssignment_1 ) ) ;
    public final void rule__FeatureException__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4890:1: ( ( ( rule__FeatureException__FeatureAssignment_1 ) ) )
            // InternalTomMapping.g:4891:1: ( ( rule__FeatureException__FeatureAssignment_1 ) )
            {
            // InternalTomMapping.g:4891:1: ( ( rule__FeatureException__FeatureAssignment_1 ) )
            // InternalTomMapping.g:4892:2: ( rule__FeatureException__FeatureAssignment_1 )
            {
             before(grammarAccess.getFeatureExceptionAccess().getFeatureAssignment_1()); 
            // InternalTomMapping.g:4893:2: ( rule__FeatureException__FeatureAssignment_1 )
            // InternalTomMapping.g:4893:3: rule__FeatureException__FeatureAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__FeatureException__FeatureAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getFeatureExceptionAccess().getFeatureAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureException__Group__1__Impl"


    // $ANTLR start "rule__SettedFeatureParameter__Group__0"
    // InternalTomMapping.g:4902:1: rule__SettedFeatureParameter__Group__0 : rule__SettedFeatureParameter__Group__0__Impl rule__SettedFeatureParameter__Group__1 ;
    public final void rule__SettedFeatureParameter__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4906:1: ( rule__SettedFeatureParameter__Group__0__Impl rule__SettedFeatureParameter__Group__1 )
            // InternalTomMapping.g:4907:2: rule__SettedFeatureParameter__Group__0__Impl rule__SettedFeatureParameter__Group__1
            {
            pushFollow(FOLLOW_29);
            rule__SettedFeatureParameter__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SettedFeatureParameter__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SettedFeatureParameter__Group__0"


    // $ANTLR start "rule__SettedFeatureParameter__Group__0__Impl"
    // InternalTomMapping.g:4914:1: rule__SettedFeatureParameter__Group__0__Impl : ( ( rule__SettedFeatureParameter__FeatureAssignment_0 ) ) ;
    public final void rule__SettedFeatureParameter__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4918:1: ( ( ( rule__SettedFeatureParameter__FeatureAssignment_0 ) ) )
            // InternalTomMapping.g:4919:1: ( ( rule__SettedFeatureParameter__FeatureAssignment_0 ) )
            {
            // InternalTomMapping.g:4919:1: ( ( rule__SettedFeatureParameter__FeatureAssignment_0 ) )
            // InternalTomMapping.g:4920:2: ( rule__SettedFeatureParameter__FeatureAssignment_0 )
            {
             before(grammarAccess.getSettedFeatureParameterAccess().getFeatureAssignment_0()); 
            // InternalTomMapping.g:4921:2: ( rule__SettedFeatureParameter__FeatureAssignment_0 )
            // InternalTomMapping.g:4921:3: rule__SettedFeatureParameter__FeatureAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__SettedFeatureParameter__FeatureAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getSettedFeatureParameterAccess().getFeatureAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SettedFeatureParameter__Group__0__Impl"


    // $ANTLR start "rule__SettedFeatureParameter__Group__1"
    // InternalTomMapping.g:4929:1: rule__SettedFeatureParameter__Group__1 : rule__SettedFeatureParameter__Group__1__Impl rule__SettedFeatureParameter__Group__2 ;
    public final void rule__SettedFeatureParameter__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4933:1: ( rule__SettedFeatureParameter__Group__1__Impl rule__SettedFeatureParameter__Group__2 )
            // InternalTomMapping.g:4934:2: rule__SettedFeatureParameter__Group__1__Impl rule__SettedFeatureParameter__Group__2
            {
            pushFollow(FOLLOW_31);
            rule__SettedFeatureParameter__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SettedFeatureParameter__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SettedFeatureParameter__Group__1"


    // $ANTLR start "rule__SettedFeatureParameter__Group__1__Impl"
    // InternalTomMapping.g:4941:1: rule__SettedFeatureParameter__Group__1__Impl : ( '=' ) ;
    public final void rule__SettedFeatureParameter__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4945:1: ( ( '=' ) )
            // InternalTomMapping.g:4946:1: ( '=' )
            {
            // InternalTomMapping.g:4946:1: ( '=' )
            // InternalTomMapping.g:4947:2: '='
            {
             before(grammarAccess.getSettedFeatureParameterAccess().getEqualsSignKeyword_1()); 
            match(input,35,FOLLOW_2); 
             after(grammarAccess.getSettedFeatureParameterAccess().getEqualsSignKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SettedFeatureParameter__Group__1__Impl"


    // $ANTLR start "rule__SettedFeatureParameter__Group__2"
    // InternalTomMapping.g:4956:1: rule__SettedFeatureParameter__Group__2 : rule__SettedFeatureParameter__Group__2__Impl ;
    public final void rule__SettedFeatureParameter__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4960:1: ( rule__SettedFeatureParameter__Group__2__Impl )
            // InternalTomMapping.g:4961:2: rule__SettedFeatureParameter__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__SettedFeatureParameter__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SettedFeatureParameter__Group__2"


    // $ANTLR start "rule__SettedFeatureParameter__Group__2__Impl"
    // InternalTomMapping.g:4967:1: rule__SettedFeatureParameter__Group__2__Impl : ( ( rule__SettedFeatureParameter__ValueAssignment_2 ) ) ;
    public final void rule__SettedFeatureParameter__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4971:1: ( ( ( rule__SettedFeatureParameter__ValueAssignment_2 ) ) )
            // InternalTomMapping.g:4972:1: ( ( rule__SettedFeatureParameter__ValueAssignment_2 ) )
            {
            // InternalTomMapping.g:4972:1: ( ( rule__SettedFeatureParameter__ValueAssignment_2 ) )
            // InternalTomMapping.g:4973:2: ( rule__SettedFeatureParameter__ValueAssignment_2 )
            {
             before(grammarAccess.getSettedFeatureParameterAccess().getValueAssignment_2()); 
            // InternalTomMapping.g:4974:2: ( rule__SettedFeatureParameter__ValueAssignment_2 )
            // InternalTomMapping.g:4974:3: rule__SettedFeatureParameter__ValueAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__SettedFeatureParameter__ValueAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getSettedFeatureParameterAccess().getValueAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SettedFeatureParameter__Group__2__Impl"


    // $ANTLR start "rule__FQN__Group__0"
    // InternalTomMapping.g:4983:1: rule__FQN__Group__0 : rule__FQN__Group__0__Impl rule__FQN__Group__1 ;
    public final void rule__FQN__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4987:1: ( rule__FQN__Group__0__Impl rule__FQN__Group__1 )
            // InternalTomMapping.g:4988:2: rule__FQN__Group__0__Impl rule__FQN__Group__1
            {
            pushFollow(FOLLOW_32);
            rule__FQN__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FQN__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FQN__Group__0"


    // $ANTLR start "rule__FQN__Group__0__Impl"
    // InternalTomMapping.g:4995:1: rule__FQN__Group__0__Impl : ( RULE_ID ) ;
    public final void rule__FQN__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:4999:1: ( ( RULE_ID ) )
            // InternalTomMapping.g:5000:1: ( RULE_ID )
            {
            // InternalTomMapping.g:5000:1: ( RULE_ID )
            // InternalTomMapping.g:5001:2: RULE_ID
            {
             before(grammarAccess.getFQNAccess().getIDTerminalRuleCall_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getFQNAccess().getIDTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FQN__Group__0__Impl"


    // $ANTLR start "rule__FQN__Group__1"
    // InternalTomMapping.g:5010:1: rule__FQN__Group__1 : rule__FQN__Group__1__Impl ;
    public final void rule__FQN__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5014:1: ( rule__FQN__Group__1__Impl )
            // InternalTomMapping.g:5015:2: rule__FQN__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__FQN__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FQN__Group__1"


    // $ANTLR start "rule__FQN__Group__1__Impl"
    // InternalTomMapping.g:5021:1: rule__FQN__Group__1__Impl : ( ( rule__FQN__Group_1__0 )* ) ;
    public final void rule__FQN__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5025:1: ( ( ( rule__FQN__Group_1__0 )* ) )
            // InternalTomMapping.g:5026:1: ( ( rule__FQN__Group_1__0 )* )
            {
            // InternalTomMapping.g:5026:1: ( ( rule__FQN__Group_1__0 )* )
            // InternalTomMapping.g:5027:2: ( rule__FQN__Group_1__0 )*
            {
             before(grammarAccess.getFQNAccess().getGroup_1()); 
            // InternalTomMapping.g:5028:2: ( rule__FQN__Group_1__0 )*
            loop32:
            do {
                int alt32=2;
                int LA32_0 = input.LA(1);

                if ( (LA32_0==39) ) {
                    alt32=1;
                }


                switch (alt32) {
            	case 1 :
            	    // InternalTomMapping.g:5028:3: rule__FQN__Group_1__0
            	    {
            	    pushFollow(FOLLOW_33);
            	    rule__FQN__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop32;
                }
            } while (true);

             after(grammarAccess.getFQNAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FQN__Group__1__Impl"


    // $ANTLR start "rule__FQN__Group_1__0"
    // InternalTomMapping.g:5037:1: rule__FQN__Group_1__0 : rule__FQN__Group_1__0__Impl rule__FQN__Group_1__1 ;
    public final void rule__FQN__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5041:1: ( rule__FQN__Group_1__0__Impl rule__FQN__Group_1__1 )
            // InternalTomMapping.g:5042:2: rule__FQN__Group_1__0__Impl rule__FQN__Group_1__1
            {
            pushFollow(FOLLOW_3);
            rule__FQN__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FQN__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FQN__Group_1__0"


    // $ANTLR start "rule__FQN__Group_1__0__Impl"
    // InternalTomMapping.g:5049:1: rule__FQN__Group_1__0__Impl : ( '.' ) ;
    public final void rule__FQN__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5053:1: ( ( '.' ) )
            // InternalTomMapping.g:5054:1: ( '.' )
            {
            // InternalTomMapping.g:5054:1: ( '.' )
            // InternalTomMapping.g:5055:2: '.'
            {
             before(grammarAccess.getFQNAccess().getFullStopKeyword_1_0()); 
            match(input,39,FOLLOW_2); 
             after(grammarAccess.getFQNAccess().getFullStopKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FQN__Group_1__0__Impl"


    // $ANTLR start "rule__FQN__Group_1__1"
    // InternalTomMapping.g:5064:1: rule__FQN__Group_1__1 : rule__FQN__Group_1__1__Impl ;
    public final void rule__FQN__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5068:1: ( rule__FQN__Group_1__1__Impl )
            // InternalTomMapping.g:5069:2: rule__FQN__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__FQN__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FQN__Group_1__1"


    // $ANTLR start "rule__FQN__Group_1__1__Impl"
    // InternalTomMapping.g:5075:1: rule__FQN__Group_1__1__Impl : ( RULE_ID ) ;
    public final void rule__FQN__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5079:1: ( ( RULE_ID ) )
            // InternalTomMapping.g:5080:1: ( RULE_ID )
            {
            // InternalTomMapping.g:5080:1: ( RULE_ID )
            // InternalTomMapping.g:5081:2: RULE_ID
            {
             before(grammarAccess.getFQNAccess().getIDTerminalRuleCall_1_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getFQNAccess().getIDTerminalRuleCall_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FQN__Group_1__1__Impl"


    // $ANTLR start "rule__Mapping__NameAssignment_1"
    // InternalTomMapping.g:5091:1: rule__Mapping__NameAssignment_1 : ( ruleSN ) ;
    public final void rule__Mapping__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5095:1: ( ( ruleSN ) )
            // InternalTomMapping.g:5096:2: ( ruleSN )
            {
            // InternalTomMapping.g:5096:2: ( ruleSN )
            // InternalTomMapping.g:5097:3: ruleSN
            {
             before(grammarAccess.getMappingAccess().getNameSNParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleSN();

            state._fsp--;

             after(grammarAccess.getMappingAccess().getNameSNParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__NameAssignment_1"


    // $ANTLR start "rule__Mapping__PrefixAssignment_3_1"
    // InternalTomMapping.g:5106:1: rule__Mapping__PrefixAssignment_3_1 : ( RULE_STRING ) ;
    public final void rule__Mapping__PrefixAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5110:1: ( ( RULE_STRING ) )
            // InternalTomMapping.g:5111:2: ( RULE_STRING )
            {
            // InternalTomMapping.g:5111:2: ( RULE_STRING )
            // InternalTomMapping.g:5112:3: RULE_STRING
            {
             before(grammarAccess.getMappingAccess().getPrefixSTRINGTerminalRuleCall_3_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getMappingAccess().getPrefixSTRINGTerminalRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__PrefixAssignment_3_1"


    // $ANTLR start "rule__Mapping__PackageNameAssignment_4_1"
    // InternalTomMapping.g:5121:1: rule__Mapping__PackageNameAssignment_4_1 : ( RULE_STRING ) ;
    public final void rule__Mapping__PackageNameAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5125:1: ( ( RULE_STRING ) )
            // InternalTomMapping.g:5126:2: ( RULE_STRING )
            {
            // InternalTomMapping.g:5126:2: ( RULE_STRING )
            // InternalTomMapping.g:5127:3: RULE_STRING
            {
             before(grammarAccess.getMappingAccess().getPackageNameSTRINGTerminalRuleCall_4_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getMappingAccess().getPackageNameSTRINGTerminalRuleCall_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__PackageNameAssignment_4_1"


    // $ANTLR start "rule__Mapping__UseFullyQualifiedTypeNameAssignment_5_0"
    // InternalTomMapping.g:5136:1: rule__Mapping__UseFullyQualifiedTypeNameAssignment_5_0 : ( ( 'fullyQualified' ) ) ;
    public final void rule__Mapping__UseFullyQualifiedTypeNameAssignment_5_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5140:1: ( ( ( 'fullyQualified' ) ) )
            // InternalTomMapping.g:5141:2: ( ( 'fullyQualified' ) )
            {
            // InternalTomMapping.g:5141:2: ( ( 'fullyQualified' ) )
            // InternalTomMapping.g:5142:3: ( 'fullyQualified' )
            {
             before(grammarAccess.getMappingAccess().getUseFullyQualifiedTypeNameFullyQualifiedKeyword_5_0_0()); 
            // InternalTomMapping.g:5143:3: ( 'fullyQualified' )
            // InternalTomMapping.g:5144:4: 'fullyQualified'
            {
             before(grammarAccess.getMappingAccess().getUseFullyQualifiedTypeNameFullyQualifiedKeyword_5_0_0()); 
            match(input,40,FOLLOW_2); 
             after(grammarAccess.getMappingAccess().getUseFullyQualifiedTypeNameFullyQualifiedKeyword_5_0_0()); 

            }

             after(grammarAccess.getMappingAccess().getUseFullyQualifiedTypeNameFullyQualifiedKeyword_5_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__UseFullyQualifiedTypeNameAssignment_5_0"


    // $ANTLR start "rule__Mapping__GenerateUserFactoryAssignment_6_0"
    // InternalTomMapping.g:5155:1: rule__Mapping__GenerateUserFactoryAssignment_6_0 : ( ( 'userFactory' ) ) ;
    public final void rule__Mapping__GenerateUserFactoryAssignment_6_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5159:1: ( ( ( 'userFactory' ) ) )
            // InternalTomMapping.g:5160:2: ( ( 'userFactory' ) )
            {
            // InternalTomMapping.g:5160:2: ( ( 'userFactory' ) )
            // InternalTomMapping.g:5161:3: ( 'userFactory' )
            {
             before(grammarAccess.getMappingAccess().getGenerateUserFactoryUserFactoryKeyword_6_0_0()); 
            // InternalTomMapping.g:5162:3: ( 'userFactory' )
            // InternalTomMapping.g:5163:4: 'userFactory'
            {
             before(grammarAccess.getMappingAccess().getGenerateUserFactoryUserFactoryKeyword_6_0_0()); 
            match(input,41,FOLLOW_2); 
             after(grammarAccess.getMappingAccess().getGenerateUserFactoryUserFactoryKeyword_6_0_0()); 

            }

             after(grammarAccess.getMappingAccess().getGenerateUserFactoryUserFactoryKeyword_6_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__GenerateUserFactoryAssignment_6_0"


    // $ANTLR start "rule__Mapping__ImportsAssignment_7"
    // InternalTomMapping.g:5174:1: rule__Mapping__ImportsAssignment_7 : ( ruleImport ) ;
    public final void rule__Mapping__ImportsAssignment_7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5178:1: ( ( ruleImport ) )
            // InternalTomMapping.g:5179:2: ( ruleImport )
            {
            // InternalTomMapping.g:5179:2: ( ruleImport )
            // InternalTomMapping.g:5180:3: ruleImport
            {
             before(grammarAccess.getMappingAccess().getImportsImportParserRuleCall_7_0()); 
            pushFollow(FOLLOW_2);
            ruleImport();

            state._fsp--;

             after(grammarAccess.getMappingAccess().getImportsImportParserRuleCall_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__ImportsAssignment_7"


    // $ANTLR start "rule__Mapping__InstanceMappingsAssignment_8"
    // InternalTomMapping.g:5189:1: rule__Mapping__InstanceMappingsAssignment_8 : ( ruleInstanceMapping ) ;
    public final void rule__Mapping__InstanceMappingsAssignment_8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5193:1: ( ( ruleInstanceMapping ) )
            // InternalTomMapping.g:5194:2: ( ruleInstanceMapping )
            {
            // InternalTomMapping.g:5194:2: ( ruleInstanceMapping )
            // InternalTomMapping.g:5195:3: ruleInstanceMapping
            {
             before(grammarAccess.getMappingAccess().getInstanceMappingsInstanceMappingParserRuleCall_8_0()); 
            pushFollow(FOLLOW_2);
            ruleInstanceMapping();

            state._fsp--;

             after(grammarAccess.getMappingAccess().getInstanceMappingsInstanceMappingParserRuleCall_8_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__InstanceMappingsAssignment_8"


    // $ANTLR start "rule__Mapping__TerminalsAssignment_9_2_0_0_2"
    // InternalTomMapping.g:5204:1: rule__Mapping__TerminalsAssignment_9_2_0_0_2 : ( ruleTerminal ) ;
    public final void rule__Mapping__TerminalsAssignment_9_2_0_0_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5208:1: ( ( ruleTerminal ) )
            // InternalTomMapping.g:5209:2: ( ruleTerminal )
            {
            // InternalTomMapping.g:5209:2: ( ruleTerminal )
            // InternalTomMapping.g:5210:3: ruleTerminal
            {
             before(grammarAccess.getMappingAccess().getTerminalsTerminalParserRuleCall_9_2_0_0_2_0()); 
            pushFollow(FOLLOW_2);
            ruleTerminal();

            state._fsp--;

             after(grammarAccess.getMappingAccess().getTerminalsTerminalParserRuleCall_9_2_0_0_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__TerminalsAssignment_9_2_0_0_2"


    // $ANTLR start "rule__Mapping__TerminalsAssignment_9_2_0_0_3_1"
    // InternalTomMapping.g:5219:1: rule__Mapping__TerminalsAssignment_9_2_0_0_3_1 : ( ruleTerminal ) ;
    public final void rule__Mapping__TerminalsAssignment_9_2_0_0_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5223:1: ( ( ruleTerminal ) )
            // InternalTomMapping.g:5224:2: ( ruleTerminal )
            {
            // InternalTomMapping.g:5224:2: ( ruleTerminal )
            // InternalTomMapping.g:5225:3: ruleTerminal
            {
             before(grammarAccess.getMappingAccess().getTerminalsTerminalParserRuleCall_9_2_0_0_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleTerminal();

            state._fsp--;

             after(grammarAccess.getMappingAccess().getTerminalsTerminalParserRuleCall_9_2_0_0_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__TerminalsAssignment_9_2_0_0_3_1"


    // $ANTLR start "rule__Mapping__ExternalTerminalsAssignment_9_2_0_1_2"
    // InternalTomMapping.g:5234:1: rule__Mapping__ExternalTerminalsAssignment_9_2_0_1_2 : ( ( ruleFQN ) ) ;
    public final void rule__Mapping__ExternalTerminalsAssignment_9_2_0_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5238:1: ( ( ( ruleFQN ) ) )
            // InternalTomMapping.g:5239:2: ( ( ruleFQN ) )
            {
            // InternalTomMapping.g:5239:2: ( ( ruleFQN ) )
            // InternalTomMapping.g:5240:3: ( ruleFQN )
            {
             before(grammarAccess.getMappingAccess().getExternalTerminalsTerminalCrossReference_9_2_0_1_2_0()); 
            // InternalTomMapping.g:5241:3: ( ruleFQN )
            // InternalTomMapping.g:5242:4: ruleFQN
            {
             before(grammarAccess.getMappingAccess().getExternalTerminalsTerminalFQNParserRuleCall_9_2_0_1_2_0_1()); 
            pushFollow(FOLLOW_2);
            ruleFQN();

            state._fsp--;

             after(grammarAccess.getMappingAccess().getExternalTerminalsTerminalFQNParserRuleCall_9_2_0_1_2_0_1()); 

            }

             after(grammarAccess.getMappingAccess().getExternalTerminalsTerminalCrossReference_9_2_0_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__ExternalTerminalsAssignment_9_2_0_1_2"


    // $ANTLR start "rule__Mapping__ExternalTerminalsAssignment_9_2_0_1_3_1"
    // InternalTomMapping.g:5253:1: rule__Mapping__ExternalTerminalsAssignment_9_2_0_1_3_1 : ( ( ruleFQN ) ) ;
    public final void rule__Mapping__ExternalTerminalsAssignment_9_2_0_1_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5257:1: ( ( ( ruleFQN ) ) )
            // InternalTomMapping.g:5258:2: ( ( ruleFQN ) )
            {
            // InternalTomMapping.g:5258:2: ( ( ruleFQN ) )
            // InternalTomMapping.g:5259:3: ( ruleFQN )
            {
             before(grammarAccess.getMappingAccess().getExternalTerminalsTerminalCrossReference_9_2_0_1_3_1_0()); 
            // InternalTomMapping.g:5260:3: ( ruleFQN )
            // InternalTomMapping.g:5261:4: ruleFQN
            {
             before(grammarAccess.getMappingAccess().getExternalTerminalsTerminalFQNParserRuleCall_9_2_0_1_3_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleFQN();

            state._fsp--;

             after(grammarAccess.getMappingAccess().getExternalTerminalsTerminalFQNParserRuleCall_9_2_0_1_3_1_0_1()); 

            }

             after(grammarAccess.getMappingAccess().getExternalTerminalsTerminalCrossReference_9_2_0_1_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__ExternalTerminalsAssignment_9_2_0_1_3_1"


    // $ANTLR start "rule__Mapping__TerminalsAssignment_9_2_1_0"
    // InternalTomMapping.g:5272:1: rule__Mapping__TerminalsAssignment_9_2_1_0 : ( ruleTerminal ) ;
    public final void rule__Mapping__TerminalsAssignment_9_2_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5276:1: ( ( ruleTerminal ) )
            // InternalTomMapping.g:5277:2: ( ruleTerminal )
            {
            // InternalTomMapping.g:5277:2: ( ruleTerminal )
            // InternalTomMapping.g:5278:3: ruleTerminal
            {
             before(grammarAccess.getMappingAccess().getTerminalsTerminalParserRuleCall_9_2_1_0_0()); 
            pushFollow(FOLLOW_2);
            ruleTerminal();

            state._fsp--;

             after(grammarAccess.getMappingAccess().getTerminalsTerminalParserRuleCall_9_2_1_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__TerminalsAssignment_9_2_1_0"


    // $ANTLR start "rule__Mapping__TerminalsAssignment_9_2_1_1_1"
    // InternalTomMapping.g:5287:1: rule__Mapping__TerminalsAssignment_9_2_1_1_1 : ( ruleTerminal ) ;
    public final void rule__Mapping__TerminalsAssignment_9_2_1_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5291:1: ( ( ruleTerminal ) )
            // InternalTomMapping.g:5292:2: ( ruleTerminal )
            {
            // InternalTomMapping.g:5292:2: ( ruleTerminal )
            // InternalTomMapping.g:5293:3: ruleTerminal
            {
             before(grammarAccess.getMappingAccess().getTerminalsTerminalParserRuleCall_9_2_1_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleTerminal();

            state._fsp--;

             after(grammarAccess.getMappingAccess().getTerminalsTerminalParserRuleCall_9_2_1_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__TerminalsAssignment_9_2_1_1_1"


    // $ANTLR start "rule__Mapping__OperatorsAssignment_10_0_2"
    // InternalTomMapping.g:5302:1: rule__Mapping__OperatorsAssignment_10_0_2 : ( ruleOperator ) ;
    public final void rule__Mapping__OperatorsAssignment_10_0_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5306:1: ( ( ruleOperator ) )
            // InternalTomMapping.g:5307:2: ( ruleOperator )
            {
            // InternalTomMapping.g:5307:2: ( ruleOperator )
            // InternalTomMapping.g:5308:3: ruleOperator
            {
             before(grammarAccess.getMappingAccess().getOperatorsOperatorParserRuleCall_10_0_2_0()); 
            pushFollow(FOLLOW_2);
            ruleOperator();

            state._fsp--;

             after(grammarAccess.getMappingAccess().getOperatorsOperatorParserRuleCall_10_0_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__OperatorsAssignment_10_0_2"


    // $ANTLR start "rule__Mapping__OperatorsAssignment_10_0_3_1"
    // InternalTomMapping.g:5317:1: rule__Mapping__OperatorsAssignment_10_0_3_1 : ( ruleOperator ) ;
    public final void rule__Mapping__OperatorsAssignment_10_0_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5321:1: ( ( ruleOperator ) )
            // InternalTomMapping.g:5322:2: ( ruleOperator )
            {
            // InternalTomMapping.g:5322:2: ( ruleOperator )
            // InternalTomMapping.g:5323:3: ruleOperator
            {
             before(grammarAccess.getMappingAccess().getOperatorsOperatorParserRuleCall_10_0_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleOperator();

            state._fsp--;

             after(grammarAccess.getMappingAccess().getOperatorsOperatorParserRuleCall_10_0_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__OperatorsAssignment_10_0_3_1"


    // $ANTLR start "rule__Mapping__ModulesAssignment_10_1"
    // InternalTomMapping.g:5332:1: rule__Mapping__ModulesAssignment_10_1 : ( ruleModule ) ;
    public final void rule__Mapping__ModulesAssignment_10_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5336:1: ( ( ruleModule ) )
            // InternalTomMapping.g:5337:2: ( ruleModule )
            {
            // InternalTomMapping.g:5337:2: ( ruleModule )
            // InternalTomMapping.g:5338:3: ruleModule
            {
             before(grammarAccess.getMappingAccess().getModulesModuleParserRuleCall_10_1_0()); 
            pushFollow(FOLLOW_2);
            ruleModule();

            state._fsp--;

             after(grammarAccess.getMappingAccess().getModulesModuleParserRuleCall_10_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__ModulesAssignment_10_1"


    // $ANTLR start "rule__InstanceMapping__EpackageAssignment_1"
    // InternalTomMapping.g:5347:1: rule__InstanceMapping__EpackageAssignment_1 : ( ( ruleFQN ) ) ;
    public final void rule__InstanceMapping__EpackageAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5351:1: ( ( ( ruleFQN ) ) )
            // InternalTomMapping.g:5352:2: ( ( ruleFQN ) )
            {
            // InternalTomMapping.g:5352:2: ( ( ruleFQN ) )
            // InternalTomMapping.g:5353:3: ( ruleFQN )
            {
             before(grammarAccess.getInstanceMappingAccess().getEpackageEPackageCrossReference_1_0()); 
            // InternalTomMapping.g:5354:3: ( ruleFQN )
            // InternalTomMapping.g:5355:4: ruleFQN
            {
             before(grammarAccess.getInstanceMappingAccess().getEpackageEPackageFQNParserRuleCall_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleFQN();

            state._fsp--;

             after(grammarAccess.getInstanceMappingAccess().getEpackageEPackageFQNParserRuleCall_1_0_1()); 

            }

             after(grammarAccess.getInstanceMappingAccess().getEpackageEPackageCrossReference_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InstanceMapping__EpackageAssignment_1"


    // $ANTLR start "rule__InstanceMapping__InstancePackageNameAssignment_3_1"
    // InternalTomMapping.g:5366:1: rule__InstanceMapping__InstancePackageNameAssignment_3_1 : ( RULE_STRING ) ;
    public final void rule__InstanceMapping__InstancePackageNameAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5370:1: ( ( RULE_STRING ) )
            // InternalTomMapping.g:5371:2: ( RULE_STRING )
            {
            // InternalTomMapping.g:5371:2: ( RULE_STRING )
            // InternalTomMapping.g:5372:3: RULE_STRING
            {
             before(grammarAccess.getInstanceMappingAccess().getInstancePackageNameSTRINGTerminalRuleCall_3_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getInstanceMappingAccess().getInstancePackageNameSTRINGTerminalRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InstanceMapping__InstancePackageNameAssignment_3_1"


    // $ANTLR start "rule__InstanceMapping__PackageFactoryNameAssignment_4_1"
    // InternalTomMapping.g:5381:1: rule__InstanceMapping__PackageFactoryNameAssignment_4_1 : ( RULE_STRING ) ;
    public final void rule__InstanceMapping__PackageFactoryNameAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5385:1: ( ( RULE_STRING ) )
            // InternalTomMapping.g:5386:2: ( RULE_STRING )
            {
            // InternalTomMapping.g:5386:2: ( RULE_STRING )
            // InternalTomMapping.g:5387:3: RULE_STRING
            {
             before(grammarAccess.getInstanceMappingAccess().getPackageFactoryNameSTRINGTerminalRuleCall_4_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getInstanceMappingAccess().getPackageFactoryNameSTRINGTerminalRuleCall_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InstanceMapping__PackageFactoryNameAssignment_4_1"


    // $ANTLR start "rule__Module__NameAssignment_1"
    // InternalTomMapping.g:5396:1: rule__Module__NameAssignment_1 : ( ruleSN ) ;
    public final void rule__Module__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5400:1: ( ( ruleSN ) )
            // InternalTomMapping.g:5401:2: ( ruleSN )
            {
            // InternalTomMapping.g:5401:2: ( ruleSN )
            // InternalTomMapping.g:5402:3: ruleSN
            {
             before(grammarAccess.getModuleAccess().getNameSNParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleSN();

            state._fsp--;

             after(grammarAccess.getModuleAccess().getNameSNParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__NameAssignment_1"


    // $ANTLR start "rule__Module__OperatorsAssignment_3_2"
    // InternalTomMapping.g:5411:1: rule__Module__OperatorsAssignment_3_2 : ( ruleOperator ) ;
    public final void rule__Module__OperatorsAssignment_3_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5415:1: ( ( ruleOperator ) )
            // InternalTomMapping.g:5416:2: ( ruleOperator )
            {
            // InternalTomMapping.g:5416:2: ( ruleOperator )
            // InternalTomMapping.g:5417:3: ruleOperator
            {
             before(grammarAccess.getModuleAccess().getOperatorsOperatorParserRuleCall_3_2_0()); 
            pushFollow(FOLLOW_2);
            ruleOperator();

            state._fsp--;

             after(grammarAccess.getModuleAccess().getOperatorsOperatorParserRuleCall_3_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__OperatorsAssignment_3_2"


    // $ANTLR start "rule__Module__OperatorsAssignment_3_3_1"
    // InternalTomMapping.g:5426:1: rule__Module__OperatorsAssignment_3_3_1 : ( ruleOperator ) ;
    public final void rule__Module__OperatorsAssignment_3_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5430:1: ( ( ruleOperator ) )
            // InternalTomMapping.g:5431:2: ( ruleOperator )
            {
            // InternalTomMapping.g:5431:2: ( ruleOperator )
            // InternalTomMapping.g:5432:3: ruleOperator
            {
             before(grammarAccess.getModuleAccess().getOperatorsOperatorParserRuleCall_3_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleOperator();

            state._fsp--;

             after(grammarAccess.getModuleAccess().getOperatorsOperatorParserRuleCall_3_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__OperatorsAssignment_3_3_1"


    // $ANTLR start "rule__Import__ImportURIAssignment_1"
    // InternalTomMapping.g:5441:1: rule__Import__ImportURIAssignment_1 : ( RULE_STRING ) ;
    public final void rule__Import__ImportURIAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5445:1: ( ( RULE_STRING ) )
            // InternalTomMapping.g:5446:2: ( RULE_STRING )
            {
            // InternalTomMapping.g:5446:2: ( RULE_STRING )
            // InternalTomMapping.g:5447:3: RULE_STRING
            {
             before(grammarAccess.getImportAccess().getImportURISTRINGTerminalRuleCall_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getImportAccess().getImportURISTRINGTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__ImportURIAssignment_1"


    // $ANTLR start "rule__Terminal__NameAssignment_0"
    // InternalTomMapping.g:5456:1: rule__Terminal__NameAssignment_0 : ( ruleSN ) ;
    public final void rule__Terminal__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5460:1: ( ( ruleSN ) )
            // InternalTomMapping.g:5461:2: ( ruleSN )
            {
            // InternalTomMapping.g:5461:2: ( ruleSN )
            // InternalTomMapping.g:5462:3: ruleSN
            {
             before(grammarAccess.getTerminalAccess().getNameSNParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleSN();

            state._fsp--;

             after(grammarAccess.getTerminalAccess().getNameSNParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Terminal__NameAssignment_0"


    // $ANTLR start "rule__Terminal__ClassAssignment_2"
    // InternalTomMapping.g:5471:1: rule__Terminal__ClassAssignment_2 : ( ( ruleFQN ) ) ;
    public final void rule__Terminal__ClassAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5475:1: ( ( ( ruleFQN ) ) )
            // InternalTomMapping.g:5476:2: ( ( ruleFQN ) )
            {
            // InternalTomMapping.g:5476:2: ( ( ruleFQN ) )
            // InternalTomMapping.g:5477:3: ( ruleFQN )
            {
             before(grammarAccess.getTerminalAccess().getClassEClassCrossReference_2_0()); 
            // InternalTomMapping.g:5478:3: ( ruleFQN )
            // InternalTomMapping.g:5479:4: ruleFQN
            {
             before(grammarAccess.getTerminalAccess().getClassEClassFQNParserRuleCall_2_0_1()); 
            pushFollow(FOLLOW_2);
            ruleFQN();

            state._fsp--;

             after(grammarAccess.getTerminalAccess().getClassEClassFQNParserRuleCall_2_0_1()); 

            }

             after(grammarAccess.getTerminalAccess().getClassEClassCrossReference_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Terminal__ClassAssignment_2"


    // $ANTLR start "rule__Terminal__ManyAssignment_3"
    // InternalTomMapping.g:5490:1: rule__Terminal__ManyAssignment_3 : ( ( '[]' ) ) ;
    public final void rule__Terminal__ManyAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5494:1: ( ( ( '[]' ) ) )
            // InternalTomMapping.g:5495:2: ( ( '[]' ) )
            {
            // InternalTomMapping.g:5495:2: ( ( '[]' ) )
            // InternalTomMapping.g:5496:3: ( '[]' )
            {
             before(grammarAccess.getTerminalAccess().getManyLeftSquareBracketRightSquareBracketKeyword_3_0()); 
            // InternalTomMapping.g:5497:3: ( '[]' )
            // InternalTomMapping.g:5498:4: '[]'
            {
             before(grammarAccess.getTerminalAccess().getManyLeftSquareBracketRightSquareBracketKeyword_3_0()); 
            match(input,42,FOLLOW_2); 
             after(grammarAccess.getTerminalAccess().getManyLeftSquareBracketRightSquareBracketKeyword_3_0()); 

            }

             after(grammarAccess.getTerminalAccess().getManyLeftSquareBracketRightSquareBracketKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Terminal__ManyAssignment_3"


    // $ANTLR start "rule__AliasOperator__NameAssignment_1"
    // InternalTomMapping.g:5509:1: rule__AliasOperator__NameAssignment_1 : ( ruleSN ) ;
    public final void rule__AliasOperator__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5513:1: ( ( ruleSN ) )
            // InternalTomMapping.g:5514:2: ( ruleSN )
            {
            // InternalTomMapping.g:5514:2: ( ruleSN )
            // InternalTomMapping.g:5515:3: ruleSN
            {
             before(grammarAccess.getAliasOperatorAccess().getNameSNParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleSN();

            state._fsp--;

             after(grammarAccess.getAliasOperatorAccess().getNameSNParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasOperator__NameAssignment_1"


    // $ANTLR start "rule__AliasOperator__OpAssignment_3"
    // InternalTomMapping.g:5524:1: rule__AliasOperator__OpAssignment_3 : ( ( RULE_ID ) ) ;
    public final void rule__AliasOperator__OpAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5528:1: ( ( ( RULE_ID ) ) )
            // InternalTomMapping.g:5529:2: ( ( RULE_ID ) )
            {
            // InternalTomMapping.g:5529:2: ( ( RULE_ID ) )
            // InternalTomMapping.g:5530:3: ( RULE_ID )
            {
             before(grammarAccess.getAliasOperatorAccess().getOpOperatorCrossReference_3_0()); 
            // InternalTomMapping.g:5531:3: ( RULE_ID )
            // InternalTomMapping.g:5532:4: RULE_ID
            {
             before(grammarAccess.getAliasOperatorAccess().getOpOperatorIDTerminalRuleCall_3_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getAliasOperatorAccess().getOpOperatorIDTerminalRuleCall_3_0_1()); 

            }

             after(grammarAccess.getAliasOperatorAccess().getOpOperatorCrossReference_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasOperator__OpAssignment_3"


    // $ANTLR start "rule__AliasOperator__NodesAssignment_5"
    // InternalTomMapping.g:5543:1: rule__AliasOperator__NodesAssignment_5 : ( ruleAliasNode ) ;
    public final void rule__AliasOperator__NodesAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5547:1: ( ( ruleAliasNode ) )
            // InternalTomMapping.g:5548:2: ( ruleAliasNode )
            {
            // InternalTomMapping.g:5548:2: ( ruleAliasNode )
            // InternalTomMapping.g:5549:3: ruleAliasNode
            {
             before(grammarAccess.getAliasOperatorAccess().getNodesAliasNodeParserRuleCall_5_0()); 
            pushFollow(FOLLOW_2);
            ruleAliasNode();

            state._fsp--;

             after(grammarAccess.getAliasOperatorAccess().getNodesAliasNodeParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasOperator__NodesAssignment_5"


    // $ANTLR start "rule__AliasOperator__NodesAssignment_6_1"
    // InternalTomMapping.g:5558:1: rule__AliasOperator__NodesAssignment_6_1 : ( ruleAliasNode ) ;
    public final void rule__AliasOperator__NodesAssignment_6_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5562:1: ( ( ruleAliasNode ) )
            // InternalTomMapping.g:5563:2: ( ruleAliasNode )
            {
            // InternalTomMapping.g:5563:2: ( ruleAliasNode )
            // InternalTomMapping.g:5564:3: ruleAliasNode
            {
             before(grammarAccess.getAliasOperatorAccess().getNodesAliasNodeParserRuleCall_6_1_0()); 
            pushFollow(FOLLOW_2);
            ruleAliasNode();

            state._fsp--;

             after(grammarAccess.getAliasOperatorAccess().getNodesAliasNodeParserRuleCall_6_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AliasOperator__NodesAssignment_6_1"


    // $ANTLR start "rule__FeatureNode__FeatureAssignment"
    // InternalTomMapping.g:5573:1: rule__FeatureNode__FeatureAssignment : ( ruleSN ) ;
    public final void rule__FeatureNode__FeatureAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5577:1: ( ( ruleSN ) )
            // InternalTomMapping.g:5578:2: ( ruleSN )
            {
            // InternalTomMapping.g:5578:2: ( ruleSN )
            // InternalTomMapping.g:5579:3: ruleSN
            {
             before(grammarAccess.getFeatureNodeAccess().getFeatureSNParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleSN();

            state._fsp--;

             after(grammarAccess.getFeatureNodeAccess().getFeatureSNParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureNode__FeatureAssignment"


    // $ANTLR start "rule__OperatorNode__OpAssignment_0"
    // InternalTomMapping.g:5588:1: rule__OperatorNode__OpAssignment_0 : ( ( RULE_ID ) ) ;
    public final void rule__OperatorNode__OpAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5592:1: ( ( ( RULE_ID ) ) )
            // InternalTomMapping.g:5593:2: ( ( RULE_ID ) )
            {
            // InternalTomMapping.g:5593:2: ( ( RULE_ID ) )
            // InternalTomMapping.g:5594:3: ( RULE_ID )
            {
             before(grammarAccess.getOperatorNodeAccess().getOpOperatorCrossReference_0_0()); 
            // InternalTomMapping.g:5595:3: ( RULE_ID )
            // InternalTomMapping.g:5596:4: RULE_ID
            {
             before(grammarAccess.getOperatorNodeAccess().getOpOperatorIDTerminalRuleCall_0_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getOperatorNodeAccess().getOpOperatorIDTerminalRuleCall_0_0_1()); 

            }

             after(grammarAccess.getOperatorNodeAccess().getOpOperatorCrossReference_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OperatorNode__OpAssignment_0"


    // $ANTLR start "rule__OperatorNode__NodesAssignment_2"
    // InternalTomMapping.g:5607:1: rule__OperatorNode__NodesAssignment_2 : ( ruleAliasNode ) ;
    public final void rule__OperatorNode__NodesAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5611:1: ( ( ruleAliasNode ) )
            // InternalTomMapping.g:5612:2: ( ruleAliasNode )
            {
            // InternalTomMapping.g:5612:2: ( ruleAliasNode )
            // InternalTomMapping.g:5613:3: ruleAliasNode
            {
             before(grammarAccess.getOperatorNodeAccess().getNodesAliasNodeParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleAliasNode();

            state._fsp--;

             after(grammarAccess.getOperatorNodeAccess().getNodesAliasNodeParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OperatorNode__NodesAssignment_2"


    // $ANTLR start "rule__OperatorNode__NodesAssignment_3_1"
    // InternalTomMapping.g:5622:1: rule__OperatorNode__NodesAssignment_3_1 : ( ruleAliasNode ) ;
    public final void rule__OperatorNode__NodesAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5626:1: ( ( ruleAliasNode ) )
            // InternalTomMapping.g:5627:2: ( ruleAliasNode )
            {
            // InternalTomMapping.g:5627:2: ( ruleAliasNode )
            // InternalTomMapping.g:5628:3: ruleAliasNode
            {
             before(grammarAccess.getOperatorNodeAccess().getNodesAliasNodeParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleAliasNode();

            state._fsp--;

             after(grammarAccess.getOperatorNodeAccess().getNodesAliasNodeParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OperatorNode__NodesAssignment_3_1"


    // $ANTLR start "rule__ClassOperator__NameAssignment_1"
    // InternalTomMapping.g:5637:1: rule__ClassOperator__NameAssignment_1 : ( ruleSN ) ;
    public final void rule__ClassOperator__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5641:1: ( ( ruleSN ) )
            // InternalTomMapping.g:5642:2: ( ruleSN )
            {
            // InternalTomMapping.g:5642:2: ( ruleSN )
            // InternalTomMapping.g:5643:3: ruleSN
            {
             before(grammarAccess.getClassOperatorAccess().getNameSNParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleSN();

            state._fsp--;

             after(grammarAccess.getClassOperatorAccess().getNameSNParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassOperator__NameAssignment_1"


    // $ANTLR start "rule__ClassOperator__ClassAssignment_3"
    // InternalTomMapping.g:5652:1: rule__ClassOperator__ClassAssignment_3 : ( ( ruleFQN ) ) ;
    public final void rule__ClassOperator__ClassAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5656:1: ( ( ( ruleFQN ) ) )
            // InternalTomMapping.g:5657:2: ( ( ruleFQN ) )
            {
            // InternalTomMapping.g:5657:2: ( ( ruleFQN ) )
            // InternalTomMapping.g:5658:3: ( ruleFQN )
            {
             before(grammarAccess.getClassOperatorAccess().getClassEClassCrossReference_3_0()); 
            // InternalTomMapping.g:5659:3: ( ruleFQN )
            // InternalTomMapping.g:5660:4: ruleFQN
            {
             before(grammarAccess.getClassOperatorAccess().getClassEClassFQNParserRuleCall_3_0_1()); 
            pushFollow(FOLLOW_2);
            ruleFQN();

            state._fsp--;

             after(grammarAccess.getClassOperatorAccess().getClassEClassFQNParserRuleCall_3_0_1()); 

            }

             after(grammarAccess.getClassOperatorAccess().getClassEClassCrossReference_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassOperator__ClassAssignment_3"


    // $ANTLR start "rule__ClassOperatorWithExceptions__NameAssignment_1"
    // InternalTomMapping.g:5671:1: rule__ClassOperatorWithExceptions__NameAssignment_1 : ( ruleSN ) ;
    public final void rule__ClassOperatorWithExceptions__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5675:1: ( ( ruleSN ) )
            // InternalTomMapping.g:5676:2: ( ruleSN )
            {
            // InternalTomMapping.g:5676:2: ( ruleSN )
            // InternalTomMapping.g:5677:3: ruleSN
            {
             before(grammarAccess.getClassOperatorWithExceptionsAccess().getNameSNParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleSN();

            state._fsp--;

             after(grammarAccess.getClassOperatorWithExceptionsAccess().getNameSNParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassOperatorWithExceptions__NameAssignment_1"


    // $ANTLR start "rule__ClassOperatorWithExceptions__ClassAssignment_3"
    // InternalTomMapping.g:5686:1: rule__ClassOperatorWithExceptions__ClassAssignment_3 : ( ( ruleFQN ) ) ;
    public final void rule__ClassOperatorWithExceptions__ClassAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5690:1: ( ( ( ruleFQN ) ) )
            // InternalTomMapping.g:5691:2: ( ( ruleFQN ) )
            {
            // InternalTomMapping.g:5691:2: ( ( ruleFQN ) )
            // InternalTomMapping.g:5692:3: ( ruleFQN )
            {
             before(grammarAccess.getClassOperatorWithExceptionsAccess().getClassEClassCrossReference_3_0()); 
            // InternalTomMapping.g:5693:3: ( ruleFQN )
            // InternalTomMapping.g:5694:4: ruleFQN
            {
             before(grammarAccess.getClassOperatorWithExceptionsAccess().getClassEClassFQNParserRuleCall_3_0_1()); 
            pushFollow(FOLLOW_2);
            ruleFQN();

            state._fsp--;

             after(grammarAccess.getClassOperatorWithExceptionsAccess().getClassEClassFQNParserRuleCall_3_0_1()); 

            }

             after(grammarAccess.getClassOperatorWithExceptionsAccess().getClassEClassCrossReference_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassOperatorWithExceptions__ClassAssignment_3"


    // $ANTLR start "rule__ClassOperatorWithExceptions__ParametersAssignment_5"
    // InternalTomMapping.g:5705:1: rule__ClassOperatorWithExceptions__ParametersAssignment_5 : ( ruleFeatureParameter ) ;
    public final void rule__ClassOperatorWithExceptions__ParametersAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5709:1: ( ( ruleFeatureParameter ) )
            // InternalTomMapping.g:5710:2: ( ruleFeatureParameter )
            {
            // InternalTomMapping.g:5710:2: ( ruleFeatureParameter )
            // InternalTomMapping.g:5711:3: ruleFeatureParameter
            {
             before(grammarAccess.getClassOperatorWithExceptionsAccess().getParametersFeatureParameterParserRuleCall_5_0()); 
            pushFollow(FOLLOW_2);
            ruleFeatureParameter();

            state._fsp--;

             after(grammarAccess.getClassOperatorWithExceptionsAccess().getParametersFeatureParameterParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassOperatorWithExceptions__ParametersAssignment_5"


    // $ANTLR start "rule__ClassOperatorWithExceptions__ParametersAssignment_6_1"
    // InternalTomMapping.g:5720:1: rule__ClassOperatorWithExceptions__ParametersAssignment_6_1 : ( ruleFeatureParameter ) ;
    public final void rule__ClassOperatorWithExceptions__ParametersAssignment_6_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5724:1: ( ( ruleFeatureParameter ) )
            // InternalTomMapping.g:5725:2: ( ruleFeatureParameter )
            {
            // InternalTomMapping.g:5725:2: ( ruleFeatureParameter )
            // InternalTomMapping.g:5726:3: ruleFeatureParameter
            {
             before(grammarAccess.getClassOperatorWithExceptionsAccess().getParametersFeatureParameterParserRuleCall_6_1_0()); 
            pushFollow(FOLLOW_2);
            ruleFeatureParameter();

            state._fsp--;

             after(grammarAccess.getClassOperatorWithExceptionsAccess().getParametersFeatureParameterParserRuleCall_6_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassOperatorWithExceptions__ParametersAssignment_6_1"


    // $ANTLR start "rule__UserOperator__NameAssignment_1"
    // InternalTomMapping.g:5735:1: rule__UserOperator__NameAssignment_1 : ( ruleSN ) ;
    public final void rule__UserOperator__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5739:1: ( ( ruleSN ) )
            // InternalTomMapping.g:5740:2: ( ruleSN )
            {
            // InternalTomMapping.g:5740:2: ( ruleSN )
            // InternalTomMapping.g:5741:3: ruleSN
            {
             before(grammarAccess.getUserOperatorAccess().getNameSNParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleSN();

            state._fsp--;

             after(grammarAccess.getUserOperatorAccess().getNameSNParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__NameAssignment_1"


    // $ANTLR start "rule__UserOperator__ParametersAssignment_3"
    // InternalTomMapping.g:5750:1: rule__UserOperator__ParametersAssignment_3 : ( ruleParameter ) ;
    public final void rule__UserOperator__ParametersAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5754:1: ( ( ruleParameter ) )
            // InternalTomMapping.g:5755:2: ( ruleParameter )
            {
            // InternalTomMapping.g:5755:2: ( ruleParameter )
            // InternalTomMapping.g:5756:3: ruleParameter
            {
             before(grammarAccess.getUserOperatorAccess().getParametersParameterParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleParameter();

            state._fsp--;

             after(grammarAccess.getUserOperatorAccess().getParametersParameterParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__ParametersAssignment_3"


    // $ANTLR start "rule__UserOperator__ParametersAssignment_4_1"
    // InternalTomMapping.g:5765:1: rule__UserOperator__ParametersAssignment_4_1 : ( ruleParameter ) ;
    public final void rule__UserOperator__ParametersAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5769:1: ( ( ruleParameter ) )
            // InternalTomMapping.g:5770:2: ( ruleParameter )
            {
            // InternalTomMapping.g:5770:2: ( ruleParameter )
            // InternalTomMapping.g:5771:3: ruleParameter
            {
             before(grammarAccess.getUserOperatorAccess().getParametersParameterParserRuleCall_4_1_0()); 
            pushFollow(FOLLOW_2);
            ruleParameter();

            state._fsp--;

             after(grammarAccess.getUserOperatorAccess().getParametersParameterParserRuleCall_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__ParametersAssignment_4_1"


    // $ANTLR start "rule__UserOperator__TypeAssignment_7"
    // InternalTomMapping.g:5780:1: rule__UserOperator__TypeAssignment_7 : ( ( RULE_ID ) ) ;
    public final void rule__UserOperator__TypeAssignment_7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5784:1: ( ( ( RULE_ID ) ) )
            // InternalTomMapping.g:5785:2: ( ( RULE_ID ) )
            {
            // InternalTomMapping.g:5785:2: ( ( RULE_ID ) )
            // InternalTomMapping.g:5786:3: ( RULE_ID )
            {
             before(grammarAccess.getUserOperatorAccess().getTypeTerminalCrossReference_7_0()); 
            // InternalTomMapping.g:5787:3: ( RULE_ID )
            // InternalTomMapping.g:5788:4: RULE_ID
            {
             before(grammarAccess.getUserOperatorAccess().getTypeTerminalIDTerminalRuleCall_7_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getUserOperatorAccess().getTypeTerminalIDTerminalRuleCall_7_0_1()); 

            }

             after(grammarAccess.getUserOperatorAccess().getTypeTerminalCrossReference_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__TypeAssignment_7"


    // $ANTLR start "rule__UserOperator__AccessorsAssignment_9"
    // InternalTomMapping.g:5799:1: rule__UserOperator__AccessorsAssignment_9 : ( ruleAccessor ) ;
    public final void rule__UserOperator__AccessorsAssignment_9() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5803:1: ( ( ruleAccessor ) )
            // InternalTomMapping.g:5804:2: ( ruleAccessor )
            {
            // InternalTomMapping.g:5804:2: ( ruleAccessor )
            // InternalTomMapping.g:5805:3: ruleAccessor
            {
             before(grammarAccess.getUserOperatorAccess().getAccessorsAccessorParserRuleCall_9_0()); 
            pushFollow(FOLLOW_2);
            ruleAccessor();

            state._fsp--;

             after(grammarAccess.getUserOperatorAccess().getAccessorsAccessorParserRuleCall_9_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__AccessorsAssignment_9"


    // $ANTLR start "rule__UserOperator__AccessorsAssignment_10_1"
    // InternalTomMapping.g:5814:1: rule__UserOperator__AccessorsAssignment_10_1 : ( ruleAccessor ) ;
    public final void rule__UserOperator__AccessorsAssignment_10_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5818:1: ( ( ruleAccessor ) )
            // InternalTomMapping.g:5819:2: ( ruleAccessor )
            {
            // InternalTomMapping.g:5819:2: ( ruleAccessor )
            // InternalTomMapping.g:5820:3: ruleAccessor
            {
             before(grammarAccess.getUserOperatorAccess().getAccessorsAccessorParserRuleCall_10_1_0()); 
            pushFollow(FOLLOW_2);
            ruleAccessor();

            state._fsp--;

             after(grammarAccess.getUserOperatorAccess().getAccessorsAccessorParserRuleCall_10_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__AccessorsAssignment_10_1"


    // $ANTLR start "rule__UserOperator__MakeAssignment_14"
    // InternalTomMapping.g:5829:1: rule__UserOperator__MakeAssignment_14 : ( RULE_STRING ) ;
    public final void rule__UserOperator__MakeAssignment_14() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5833:1: ( ( RULE_STRING ) )
            // InternalTomMapping.g:5834:2: ( RULE_STRING )
            {
            // InternalTomMapping.g:5834:2: ( RULE_STRING )
            // InternalTomMapping.g:5835:3: RULE_STRING
            {
             before(grammarAccess.getUserOperatorAccess().getMakeSTRINGTerminalRuleCall_14_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getUserOperatorAccess().getMakeSTRINGTerminalRuleCall_14_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__MakeAssignment_14"


    // $ANTLR start "rule__UserOperator__TestAssignment_18"
    // InternalTomMapping.g:5844:1: rule__UserOperator__TestAssignment_18 : ( RULE_STRING ) ;
    public final void rule__UserOperator__TestAssignment_18() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5848:1: ( ( RULE_STRING ) )
            // InternalTomMapping.g:5849:2: ( RULE_STRING )
            {
            // InternalTomMapping.g:5849:2: ( RULE_STRING )
            // InternalTomMapping.g:5850:3: RULE_STRING
            {
             before(grammarAccess.getUserOperatorAccess().getTestSTRINGTerminalRuleCall_18_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getUserOperatorAccess().getTestSTRINGTerminalRuleCall_18_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserOperator__TestAssignment_18"


    // $ANTLR start "rule__Parameter__TypeAssignment_0"
    // InternalTomMapping.g:5859:1: rule__Parameter__TypeAssignment_0 : ( ( RULE_ID ) ) ;
    public final void rule__Parameter__TypeAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5863:1: ( ( ( RULE_ID ) ) )
            // InternalTomMapping.g:5864:2: ( ( RULE_ID ) )
            {
            // InternalTomMapping.g:5864:2: ( ( RULE_ID ) )
            // InternalTomMapping.g:5865:3: ( RULE_ID )
            {
             before(grammarAccess.getParameterAccess().getTypeTerminalCrossReference_0_0()); 
            // InternalTomMapping.g:5866:3: ( RULE_ID )
            // InternalTomMapping.g:5867:4: RULE_ID
            {
             before(grammarAccess.getParameterAccess().getTypeTerminalIDTerminalRuleCall_0_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getParameterAccess().getTypeTerminalIDTerminalRuleCall_0_0_1()); 

            }

             after(grammarAccess.getParameterAccess().getTypeTerminalCrossReference_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__TypeAssignment_0"


    // $ANTLR start "rule__Parameter__NameAssignment_1"
    // InternalTomMapping.g:5878:1: rule__Parameter__NameAssignment_1 : ( ruleSN ) ;
    public final void rule__Parameter__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5882:1: ( ( ruleSN ) )
            // InternalTomMapping.g:5883:2: ( ruleSN )
            {
            // InternalTomMapping.g:5883:2: ( ruleSN )
            // InternalTomMapping.g:5884:3: ruleSN
            {
             before(grammarAccess.getParameterAccess().getNameSNParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleSN();

            state._fsp--;

             after(grammarAccess.getParameterAccess().getNameSNParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__NameAssignment_1"


    // $ANTLR start "rule__Accessor__SlotAssignment_1"
    // InternalTomMapping.g:5893:1: rule__Accessor__SlotAssignment_1 : ( ( RULE_ID ) ) ;
    public final void rule__Accessor__SlotAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5897:1: ( ( ( RULE_ID ) ) )
            // InternalTomMapping.g:5898:2: ( ( RULE_ID ) )
            {
            // InternalTomMapping.g:5898:2: ( ( RULE_ID ) )
            // InternalTomMapping.g:5899:3: ( RULE_ID )
            {
             before(grammarAccess.getAccessorAccess().getSlotParameterCrossReference_1_0()); 
            // InternalTomMapping.g:5900:3: ( RULE_ID )
            // InternalTomMapping.g:5901:4: RULE_ID
            {
             before(grammarAccess.getAccessorAccess().getSlotParameterIDTerminalRuleCall_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getAccessorAccess().getSlotParameterIDTerminalRuleCall_1_0_1()); 

            }

             after(grammarAccess.getAccessorAccess().getSlotParameterCrossReference_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Accessor__SlotAssignment_1"


    // $ANTLR start "rule__Accessor__JavaAssignment_3"
    // InternalTomMapping.g:5912:1: rule__Accessor__JavaAssignment_3 : ( RULE_STRING ) ;
    public final void rule__Accessor__JavaAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5916:1: ( ( RULE_STRING ) )
            // InternalTomMapping.g:5917:2: ( RULE_STRING )
            {
            // InternalTomMapping.g:5917:2: ( RULE_STRING )
            // InternalTomMapping.g:5918:3: RULE_STRING
            {
             before(grammarAccess.getAccessorAccess().getJavaSTRINGTerminalRuleCall_3_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getAccessorAccess().getJavaSTRINGTerminalRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Accessor__JavaAssignment_3"


    // $ANTLR start "rule__FeatureException__FeatureAssignment_1"
    // InternalTomMapping.g:5927:1: rule__FeatureException__FeatureAssignment_1 : ( ( RULE_ID ) ) ;
    public final void rule__FeatureException__FeatureAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5931:1: ( ( ( RULE_ID ) ) )
            // InternalTomMapping.g:5932:2: ( ( RULE_ID ) )
            {
            // InternalTomMapping.g:5932:2: ( ( RULE_ID ) )
            // InternalTomMapping.g:5933:3: ( RULE_ID )
            {
             before(grammarAccess.getFeatureExceptionAccess().getFeatureEStructuralFeatureCrossReference_1_0()); 
            // InternalTomMapping.g:5934:3: ( RULE_ID )
            // InternalTomMapping.g:5935:4: RULE_ID
            {
             before(grammarAccess.getFeatureExceptionAccess().getFeatureEStructuralFeatureIDTerminalRuleCall_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getFeatureExceptionAccess().getFeatureEStructuralFeatureIDTerminalRuleCall_1_0_1()); 

            }

             after(grammarAccess.getFeatureExceptionAccess().getFeatureEStructuralFeatureCrossReference_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureException__FeatureAssignment_1"


    // $ANTLR start "rule__FeatureParameter__FeatureAssignment_0"
    // InternalTomMapping.g:5946:1: rule__FeatureParameter__FeatureAssignment_0 : ( ( RULE_ID ) ) ;
    public final void rule__FeatureParameter__FeatureAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5950:1: ( ( ( RULE_ID ) ) )
            // InternalTomMapping.g:5951:2: ( ( RULE_ID ) )
            {
            // InternalTomMapping.g:5951:2: ( ( RULE_ID ) )
            // InternalTomMapping.g:5952:3: ( RULE_ID )
            {
             before(grammarAccess.getFeatureParameterAccess().getFeatureEStructuralFeatureCrossReference_0_0()); 
            // InternalTomMapping.g:5953:3: ( RULE_ID )
            // InternalTomMapping.g:5954:4: RULE_ID
            {
             before(grammarAccess.getFeatureParameterAccess().getFeatureEStructuralFeatureIDTerminalRuleCall_0_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getFeatureParameterAccess().getFeatureEStructuralFeatureIDTerminalRuleCall_0_0_1()); 

            }

             after(grammarAccess.getFeatureParameterAccess().getFeatureEStructuralFeatureCrossReference_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureParameter__FeatureAssignment_0"


    // $ANTLR start "rule__SettedFeatureParameter__FeatureAssignment_0"
    // InternalTomMapping.g:5965:1: rule__SettedFeatureParameter__FeatureAssignment_0 : ( ( RULE_ID ) ) ;
    public final void rule__SettedFeatureParameter__FeatureAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5969:1: ( ( ( RULE_ID ) ) )
            // InternalTomMapping.g:5970:2: ( ( RULE_ID ) )
            {
            // InternalTomMapping.g:5970:2: ( ( RULE_ID ) )
            // InternalTomMapping.g:5971:3: ( RULE_ID )
            {
             before(grammarAccess.getSettedFeatureParameterAccess().getFeatureEStructuralFeatureCrossReference_0_0()); 
            // InternalTomMapping.g:5972:3: ( RULE_ID )
            // InternalTomMapping.g:5973:4: RULE_ID
            {
             before(grammarAccess.getSettedFeatureParameterAccess().getFeatureEStructuralFeatureIDTerminalRuleCall_0_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getSettedFeatureParameterAccess().getFeatureEStructuralFeatureIDTerminalRuleCall_0_0_1()); 

            }

             after(grammarAccess.getSettedFeatureParameterAccess().getFeatureEStructuralFeatureCrossReference_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SettedFeatureParameter__FeatureAssignment_0"


    // $ANTLR start "rule__SettedFeatureParameter__ValueAssignment_2"
    // InternalTomMapping.g:5984:1: rule__SettedFeatureParameter__ValueAssignment_2 : ( ruleSettedValue ) ;
    public final void rule__SettedFeatureParameter__ValueAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:5988:1: ( ( ruleSettedValue ) )
            // InternalTomMapping.g:5989:2: ( ruleSettedValue )
            {
            // InternalTomMapping.g:5989:2: ( ruleSettedValue )
            // InternalTomMapping.g:5990:3: ruleSettedValue
            {
             before(grammarAccess.getSettedFeatureParameterAccess().getValueSettedValueParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleSettedValue();

            state._fsp--;

             after(grammarAccess.getSettedFeatureParameterAccess().getValueSettedValueParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SettedFeatureParameter__ValueAssignment_2"


    // $ANTLR start "rule__IntSettedValue__JavaAssignment"
    // InternalTomMapping.g:5999:1: rule__IntSettedValue__JavaAssignment : ( RULE_INT ) ;
    public final void rule__IntSettedValue__JavaAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:6003:1: ( ( RULE_INT ) )
            // InternalTomMapping.g:6004:2: ( RULE_INT )
            {
            // InternalTomMapping.g:6004:2: ( RULE_INT )
            // InternalTomMapping.g:6005:3: RULE_INT
            {
             before(grammarAccess.getIntSettedValueAccess().getJavaINTTerminalRuleCall_0()); 
            match(input,RULE_INT,FOLLOW_2); 
             after(grammarAccess.getIntSettedValueAccess().getJavaINTTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntSettedValue__JavaAssignment"


    // $ANTLR start "rule__BooleanSettedValue__JavaAssignment"
    // InternalTomMapping.g:6014:1: rule__BooleanSettedValue__JavaAssignment : ( ruleEBoolean ) ;
    public final void rule__BooleanSettedValue__JavaAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:6018:1: ( ( ruleEBoolean ) )
            // InternalTomMapping.g:6019:2: ( ruleEBoolean )
            {
            // InternalTomMapping.g:6019:2: ( ruleEBoolean )
            // InternalTomMapping.g:6020:3: ruleEBoolean
            {
             before(grammarAccess.getBooleanSettedValueAccess().getJavaEBooleanParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleEBoolean();

            state._fsp--;

             after(grammarAccess.getBooleanSettedValueAccess().getJavaEBooleanParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanSettedValue__JavaAssignment"


    // $ANTLR start "rule__JavaCodeValue__JavaAssignment"
    // InternalTomMapping.g:6029:1: rule__JavaCodeValue__JavaAssignment : ( RULE_STRING ) ;
    public final void rule__JavaCodeValue__JavaAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:6033:1: ( ( RULE_STRING ) )
            // InternalTomMapping.g:6034:2: ( RULE_STRING )
            {
            // InternalTomMapping.g:6034:2: ( RULE_STRING )
            // InternalTomMapping.g:6035:3: RULE_STRING
            {
             before(grammarAccess.getJavaCodeValueAccess().getJavaSTRINGTerminalRuleCall_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getJavaCodeValueAccess().getJavaSTRINGTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__JavaCodeValue__JavaAssignment"


    // $ANTLR start "rule__LiteralValue__LiteralAssignment"
    // InternalTomMapping.g:6044:1: rule__LiteralValue__LiteralAssignment : ( ( ruleFQN ) ) ;
    public final void rule__LiteralValue__LiteralAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalTomMapping.g:6048:1: ( ( ( ruleFQN ) ) )
            // InternalTomMapping.g:6049:2: ( ( ruleFQN ) )
            {
            // InternalTomMapping.g:6049:2: ( ( ruleFQN ) )
            // InternalTomMapping.g:6050:3: ( ruleFQN )
            {
             before(grammarAccess.getLiteralValueAccess().getLiteralEEnumLiteralCrossReference_0()); 
            // InternalTomMapping.g:6051:3: ( ruleFQN )
            // InternalTomMapping.g:6052:4: ruleFQN
            {
             before(grammarAccess.getLiteralValueAccess().getLiteralEEnumLiteralFQNParserRuleCall_0_1()); 
            pushFollow(FOLLOW_2);
            ruleFQN();

            state._fsp--;

             after(grammarAccess.getLiteralValueAccess().getLiteralEEnumLiteralFQNParserRuleCall_0_1()); 

            }

             after(grammarAccess.getLiteralValueAccess().getLiteralEEnumLiteralCrossReference_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LiteralValue__LiteralAssignment"

    // Delegated rules


    protected DFA3 dfa3 = new DFA3(this);
    static final String dfa_1s = "\13\uffff";
    static final String dfa_2s = "\6\uffff\1\11\3\uffff\1\11";
    static final String dfa_3s = "\1\35\1\4\1\uffff\1\36\1\4\1\uffff\1\16\1\4\2\uffff\1\16";
    static final String dfa_4s = "\1\41\1\4\1\uffff\1\37\1\4\1\uffff\1\47\1\4\2\uffff\1\47";
    static final String dfa_5s = "\2\uffff\1\4\2\uffff\1\3\2\uffff\1\2\1\1\1\uffff";
    static final String dfa_6s = "\13\uffff}>";
    static final String[] dfa_7s = {
            "\1\2\3\uffff\1\1",
            "\1\3",
            "",
            "\1\4\1\5",
            "\1\6",
            "",
            "\1\11\20\uffff\1\10\7\uffff\1\7",
            "\1\12",
            "",
            "",
            "\1\11\20\uffff\1\10\7\uffff\1\7"
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final short[] dfa_2 = DFA.unpackEncodedString(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final char[] dfa_4 = DFA.unpackEncodedStringToUnsignedChars(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[] dfa_6 = DFA.unpackEncodedString(dfa_6s);
    static final short[][] dfa_7 = unpackEncodedStringArray(dfa_7s);

    class DFA3 extends DFA {

        public DFA3(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 3;
            this.eot = dfa_1;
            this.eof = dfa_2;
            this.min = dfa_3;
            this.max = dfa_4;
            this.accept = dfa_5;
            this.special = dfa_6;
            this.transition = dfa_7;
        }
        public String getDescription() {
            return "744:1: rule__Operator__Alternatives : ( ( ruleClassOperator ) | ( ruleClassOperatorWithExceptions ) | ( ruleUserOperator ) | ( ruleAliasOperator ) );";
        }
    }
 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x000003000D838000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000008000002L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000001000002L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000004800002L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000500010L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000500000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000280000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000200002L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000220000000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000004002L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000002090000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000000880000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000040000000000L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000100200000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000004000000010L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000002000000000L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000000400000000L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000000800000000L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000001000000000L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000000000001870L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000008000000000L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000008000000002L});

}