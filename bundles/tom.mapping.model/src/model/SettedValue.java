/**
 */
package model;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Setted Value</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see model.ModelPackage#getSettedValue()
 * @model
 * @generated
 */
public interface SettedValue extends EObject {
} // SettedValue
