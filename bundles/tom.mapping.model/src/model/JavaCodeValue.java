/**
 */
package model;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Java Code Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link model.JavaCodeValue#getJava <em>Java</em>}</li>
 * </ul>
 * </p>
 *
 * @see model.ModelPackage#getJavaCodeValue()
 * @model
 * @generated
 */
public interface JavaCodeValue extends SettedValue {
	/**
	 * Returns the value of the '<em><b>Java</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Java</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Java</em>' attribute.
	 * @see #setJava(String)
	 * @see model.ModelPackage#getJavaCodeValue_Java()
	 * @model
	 * @generated
	 */
	String getJava();

	/**
	 * Sets the value of the '{@link model.JavaCodeValue#getJava <em>Java</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Java</em>' attribute.
	 * @see #getJava()
	 * @generated
	 */
	void setJava(String value);

} // JavaCodeValue
