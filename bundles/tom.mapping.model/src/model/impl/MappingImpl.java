/**
 */
package model.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import model.Import;
import model.InstanceMapping;
import model.Mapping;
import model.ModelFactory;
import model.ModelPackage;
import model.Module;
import model.Operator;
import model.Terminal;


/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Mapping</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link model.impl.MappingImpl#getImports <em>Imports</em>}</li>
 *   <li>{@link model.impl.MappingImpl#getTerminals <em>Terminals</em>}</li>
 *   <li>{@link model.impl.MappingImpl#getExternalTerminals <em>External Terminals</em>}</li>
 *   <li>{@link model.impl.MappingImpl#getOperators <em>Operators</em>}</li>
 *   <li>{@link model.impl.MappingImpl#getName <em>Name</em>}</li>
 *   <li>{@link model.impl.MappingImpl#getPrefix <em>Prefix</em>}</li>
 *   <li>{@link model.impl.MappingImpl#getPackageName <em>Package Name</em>}</li>
 *   <li>{@link model.impl.MappingImpl#isUseFullyQualifiedTypeName <em>Use Fully Qualified Type Name</em>}</li>
 *   <li>{@link model.impl.MappingImpl#isGenerateUserFactory <em>Generate User Factory</em>}</li>
 *   <li>{@link model.impl.MappingImpl#getModules <em>Modules</em>}</li>
 *   <li>{@link model.impl.MappingImpl#getInstanceMappings <em>Instance Mappings</em>}</li>
 *   <li>{@link model.impl.MappingImpl#getExternalMappings <em>External Mappings</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class MappingImpl extends EObjectImpl implements Mapping {
	/**
	 * The cached value of the '{@link #getImports() <em>Imports</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImports()
	 * @generated
	 * @ordered
	 */
	protected EList<Import> imports;

	/**
	 * The cached value of the '{@link #getTerminals() <em>Terminals</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTerminals()
	 * @generated
	 * @ordered
	 */
	protected EList<Terminal> terminals;

	/**
	 * The cached value of the '{@link #getExternalTerminals() <em>External Terminals</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExternalTerminals()
	 * @generated
	 * @ordered
	 */
	protected EList<Terminal> externalTerminals;

	/**
	 * The cached value of the '{@link #getOperators() <em>Operators</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperators()
	 * @generated
	 * @ordered
	 */
	protected EList<Operator> operators;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getPrefix() <em>Prefix</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrefix()
	 * @generated
	 * @ordered
	 */
	protected static final String PREFIX_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPrefix() <em>Prefix</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrefix()
	 * @generated
	 * @ordered
	 */
	protected String prefix = PREFIX_EDEFAULT;

	/**
	 * The default value of the '{@link #getPackageName() <em>Package Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPackageName()
	 * @generated
	 * @ordered
	 */
	protected static final String PACKAGE_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPackageName() <em>Package Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPackageName()
	 * @generated
	 * @ordered
	 */
	protected String packageName = PACKAGE_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #isUseFullyQualifiedTypeName() <em>Use Fully Qualified Type Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isUseFullyQualifiedTypeName()
	 * @generated
	 * @ordered
	 */
	protected static final boolean USE_FULLY_QUALIFIED_TYPE_NAME_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isUseFullyQualifiedTypeName() <em>Use Fully Qualified Type Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isUseFullyQualifiedTypeName()
	 * @generated
	 * @ordered
	 */
	protected boolean useFullyQualifiedTypeName = USE_FULLY_QUALIFIED_TYPE_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #isGenerateUserFactory() <em>Generate User Factory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isGenerateUserFactory()
	 * @generated
	 * @ordered
	 */
	protected static final boolean GENERATE_USER_FACTORY_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isGenerateUserFactory() <em>Generate User Factory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isGenerateUserFactory()
	 * @generated
	 * @ordered
	 */
	protected boolean generateUserFactory = GENERATE_USER_FACTORY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getModules() <em>Modules</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModules()
	 * @generated
	 * @ordered
	 */
	protected EList<Module> modules;

	/**
	 * The cached value of the '{@link #getInstanceMappings() <em>Instance Mappings</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInstanceMappings()
	 * @generated
	 * @ordered
	 */
	protected EList<InstanceMapping> instanceMappings;

	/**
	 * The cached value of the '{@link #getExternalMappings() <em>External Mappings</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExternalMappings()
	 * @generated
	 * @ordered
	 */
	protected EList<Mapping> externalMappings;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MappingImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModelPackage.Literals.MAPPING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Import> getImports() {
		if (imports == null) {
			imports = new EObjectContainmentEList<Import>(Import.class, this, ModelPackage.MAPPING__IMPORTS);
		}
		return imports;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Terminal> getTerminals() {
		if (terminals == null) {
			terminals = new EObjectContainmentEList<Terminal>(Terminal.class, this, ModelPackage.MAPPING__TERMINALS);
		}
		return terminals;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Terminal> getExternalTerminals() {
		if (externalTerminals == null) {
			externalTerminals = new EObjectResolvingEList<Terminal>(Terminal.class, this, ModelPackage.MAPPING__EXTERNAL_TERMINALS);
		}
		return externalTerminals;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Operator> getOperators() {
		if (operators == null) {
			operators = new EObjectContainmentEList<Operator>(Operator.class, this, ModelPackage.MAPPING__OPERATORS);
		}
		return operators;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.MAPPING__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPrefix() {
		return prefix;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPrefix(String newPrefix) {
		String oldPrefix = prefix;
		prefix = newPrefix;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.MAPPING__PREFIX, oldPrefix, prefix));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPackageName() {
		return packageName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPackageName(String newPackageName) {
		String oldPackageName = packageName;
		packageName = newPackageName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.MAPPING__PACKAGE_NAME, oldPackageName, packageName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isUseFullyQualifiedTypeName() {
		return useFullyQualifiedTypeName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUseFullyQualifiedTypeName(boolean newUseFullyQualifiedTypeName) {
		boolean oldUseFullyQualifiedTypeName = useFullyQualifiedTypeName;
		useFullyQualifiedTypeName = newUseFullyQualifiedTypeName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.MAPPING__USE_FULLY_QUALIFIED_TYPE_NAME, oldUseFullyQualifiedTypeName, useFullyQualifiedTypeName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isGenerateUserFactory() {
		return generateUserFactory;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGenerateUserFactory(boolean newGenerateUserFactory) {
		boolean oldGenerateUserFactory = generateUserFactory;
		generateUserFactory = newGenerateUserFactory;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.MAPPING__GENERATE_USER_FACTORY, oldGenerateUserFactory, generateUserFactory));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Module> getModules() {
		if (modules == null) {
			modules = new EObjectContainmentEList<Module>(Module.class, this, ModelPackage.MAPPING__MODULES);
		}
		return modules;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InstanceMapping> getInstanceMappings() {
		if (instanceMappings == null) {
			instanceMappings = new EObjectContainmentEList<InstanceMapping>(InstanceMapping.class, this, ModelPackage.MAPPING__INSTANCE_MAPPINGS);
		}
		return instanceMappings;
	}

	
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Mapping> getExternalMappings() {
		if (externalMappings == null) {
			externalMappings = new EObjectContainmentEList<Mapping>(Mapping.class, this, ModelPackage.MAPPING__EXTERNAL_MAPPINGS);
		}
		return externalMappings;
	}

	private boolean areSameClasses(EClass c1, EClass c2) {
		if(c1 == null || c2 == null)
			return false;
		
		if (c1.eIsProxy())
			c1 = (EClass) EcoreUtil.resolve(c1, this);
		if (c2.eIsProxy())
			c2 = (EClass) EcoreUtil.resolve(c2, this);
		EPackage ePackage1 = c1.getEPackage();
		EPackage ePackage2 = c2.getEPackage();
		if (ePackage1.getNsURI() == ePackage2.getNsURI()
				|| ePackage1.getNsURI().equals(ePackage2.getNsURI())) {
			return c1.getClassifierID() == c2.getClassifierID();
		}
		return false;

	}
	
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public Terminal getTerminal(EClass c, boolean isList) {
		if(c == null)
			return null;
		
		Terminal terminal = searchInTerminals(c, isList);
		if (terminal == null) {
			// Find a super class terminal
			List<EClass> superclasses = c.getESuperTypes();
			if (superclasses.size() > 0) {
				terminal = searchFirstTerminal(superclasses, isList);
			}
		}
		return terminal;
	}

	/**
	 * Search terminal of an {@link EClass} in the mapping terminals list.
	 * 
	 * @param c
	 * @return
	 */
	private Terminal searchInTerminals(EClass c, boolean isList) {
		if(c == null)
			return null;
	
		if (terminals != null)
			//System.out.println("Searching in local terminals for "+c.getName()+ " in "+terminals);
			for (Terminal term : terminals) {
				boolean many = term.isMany();
				if (many == isList && areSameClasses(term.getClass_(), c))
					return term;
			}
		
		if (externalTerminals!= null)
			//System.out.println("Searching in local terminals for "+c.getName()+ " in external "+terminals);
			for (Terminal term : externalTerminals) {
				boolean many = term.isMany();
				if (many == isList) {
					if (areSameClasses(term.getClass_(), c)) {
						return term;
					}
				}
			}
		
		if (isList && listTerminals != null) {
			for (Terminal term : listTerminals) {
				if (areSameClasses(term.getClass_(), c))
					return term;
			}
		}
		
		return null;
	}
	
	/**
	 * Search for a terminal in list of classes. If no terminals are found then
	 * search in super classes.
	 * 
	 * @param classes
	 * @return
	 */
	private Terminal searchFirstTerminal(List<EClass> classes, boolean isList) {
		if (classes.size() > 0) {
			for (EClass c : classes) {
				Terminal term = searchInTerminals(c, isList);
				if (term != null)
					return term;
				
			}
			return searchFirstTerminal(getSuperClasses(classes), isList);
		} else
			return null;
	}
	
	/**
	 * Get all super classes of a list of classes.
	 * 
	 * @param classes
	 * @return
	 */
	private List<EClass> getSuperClasses(List<EClass> classes) {
		List<EClass> superClasses = new ArrayList<EClass>();
		for (EClass eClass : classes) {
			superClasses.addAll(eClass.getESuperTypes());
		}
		return superClasses;
	}
	
	
	private EList<Terminal> listTerminals;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public EList<Terminal> getAllListTerminals() {
		if (listTerminals == null) {
			listTerminals = new UniqueEList<Terminal>();
			for (EPackage p : getMetamodelPackages()) {
				TreeIterator<EObject> eAllContents = p.eAllContents();
				while (eAllContents.hasNext()) {
					EObject o = eAllContents.next();
					if (o instanceof EReference) {
						EReference feature = (EReference) o;
						
						EClass eReferenceType = feature.getEReferenceType();
						if(eReferenceType == null)
							continue;
						
						if (feature.isMany()) {
							Terminal terminal = getTerminal(eReferenceType, false);
							if (terminal != null && getTerminal(eReferenceType, true) == null) {
								Terminal defaultListTerminal = ModelFactory.eINSTANCE.createTerminal();
								defaultListTerminal.setClass(terminal.getClass_());
								defaultListTerminal.setMany(true);
								defaultListTerminal.setName(terminal.getName()+ "List");
								listTerminals.add(defaultListTerminal);
							}
						}
					}
				}
			}
		}
		return listTerminals;
	}

	/**
	 * This is invalidated (set to null) on changes to imports!
	 * see {@link ModelFactoryImpl#createMapping()}.
	 */
	EList<EPackage> _importedEPackages; 
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public EList<EPackage> getMetamodelPackages() {
		if (_importedEPackages == null) {
			_importedEPackages = getImportedEPackages(this);
		}
		return _importedEPackages;
	}
	
	private GenericXMIResourceManager<EPackage> loader;
	private EList<EPackage> getImportedEPackages(Mapping mapping) {
		if(loader == null) {
			loader = new GenericXMIResourceManager<EPackage>(EcorePackage.eINSTANCE);
		}
		
		EList<EPackage> mm = new BasicEList<EPackage>();
		for (Import i : mapping.getImports()) {
			//import only ecore files
			String uri = i.getImportURI();
			if (uri.contains(".ecore")) {
				EPackage load = loader.load(uri);
				mm.add(load);
			}
			else if(uri.contains(".xcore")) {
				EPackage load = loader.loadEPackage(uri);
				mm.add(load);
			}
		}
		return mm;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ModelPackage.MAPPING__IMPORTS:
				return ((InternalEList<?>)getImports()).basicRemove(otherEnd, msgs);
			case ModelPackage.MAPPING__TERMINALS:
				return ((InternalEList<?>)getTerminals()).basicRemove(otherEnd, msgs);
			case ModelPackage.MAPPING__OPERATORS:
				return ((InternalEList<?>)getOperators()).basicRemove(otherEnd, msgs);
			case ModelPackage.MAPPING__MODULES:
				return ((InternalEList<?>)getModules()).basicRemove(otherEnd, msgs);
			case ModelPackage.MAPPING__INSTANCE_MAPPINGS:
				return ((InternalEList<?>)getInstanceMappings()).basicRemove(otherEnd, msgs);
			case ModelPackage.MAPPING__EXTERNAL_MAPPINGS:
				return ((InternalEList<?>)getExternalMappings()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ModelPackage.MAPPING__IMPORTS:
				return getImports();
			case ModelPackage.MAPPING__TERMINALS:
				return getTerminals();
			case ModelPackage.MAPPING__EXTERNAL_TERMINALS:
				return getExternalTerminals();
			case ModelPackage.MAPPING__OPERATORS:
				return getOperators();
			case ModelPackage.MAPPING__NAME:
				return getName();
			case ModelPackage.MAPPING__PREFIX:
				return getPrefix();
			case ModelPackage.MAPPING__PACKAGE_NAME:
				return getPackageName();
			case ModelPackage.MAPPING__USE_FULLY_QUALIFIED_TYPE_NAME:
				return isUseFullyQualifiedTypeName();
			case ModelPackage.MAPPING__GENERATE_USER_FACTORY:
				return isGenerateUserFactory();
			case ModelPackage.MAPPING__MODULES:
				return getModules();
			case ModelPackage.MAPPING__INSTANCE_MAPPINGS:
				return getInstanceMappings();
			case ModelPackage.MAPPING__EXTERNAL_MAPPINGS:
				return getExternalMappings();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ModelPackage.MAPPING__IMPORTS:
				getImports().clear();
				getImports().addAll((Collection<? extends Import>)newValue);
				return;
			case ModelPackage.MAPPING__TERMINALS:
				getTerminals().clear();
				getTerminals().addAll((Collection<? extends Terminal>)newValue);
				return;
			case ModelPackage.MAPPING__EXTERNAL_TERMINALS:
				getExternalTerminals().clear();
				getExternalTerminals().addAll((Collection<? extends Terminal>)newValue);
				return;
			case ModelPackage.MAPPING__OPERATORS:
				getOperators().clear();
				getOperators().addAll((Collection<? extends Operator>)newValue);
				return;
			case ModelPackage.MAPPING__NAME:
				setName((String)newValue);
				return;
			case ModelPackage.MAPPING__PREFIX:
				setPrefix((String)newValue);
				return;
			case ModelPackage.MAPPING__PACKAGE_NAME:
				setPackageName((String)newValue);
				return;
			case ModelPackage.MAPPING__USE_FULLY_QUALIFIED_TYPE_NAME:
				setUseFullyQualifiedTypeName((Boolean)newValue);
				return;
			case ModelPackage.MAPPING__GENERATE_USER_FACTORY:
				setGenerateUserFactory((Boolean)newValue);
				return;
			case ModelPackage.MAPPING__MODULES:
				getModules().clear();
				getModules().addAll((Collection<? extends Module>)newValue);
				return;
			case ModelPackage.MAPPING__INSTANCE_MAPPINGS:
				getInstanceMappings().clear();
				getInstanceMappings().addAll((Collection<? extends InstanceMapping>)newValue);
				return;
			case ModelPackage.MAPPING__EXTERNAL_MAPPINGS:
				getExternalMappings().clear();
				getExternalMappings().addAll((Collection<? extends Mapping>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ModelPackage.MAPPING__IMPORTS:
				getImports().clear();
				return;
			case ModelPackage.MAPPING__TERMINALS:
				getTerminals().clear();
				return;
			case ModelPackage.MAPPING__EXTERNAL_TERMINALS:
				getExternalTerminals().clear();
				return;
			case ModelPackage.MAPPING__OPERATORS:
				getOperators().clear();
				return;
			case ModelPackage.MAPPING__NAME:
				setName(NAME_EDEFAULT);
				return;
			case ModelPackage.MAPPING__PREFIX:
				setPrefix(PREFIX_EDEFAULT);
				return;
			case ModelPackage.MAPPING__PACKAGE_NAME:
				setPackageName(PACKAGE_NAME_EDEFAULT);
				return;
			case ModelPackage.MAPPING__USE_FULLY_QUALIFIED_TYPE_NAME:
				setUseFullyQualifiedTypeName(USE_FULLY_QUALIFIED_TYPE_NAME_EDEFAULT);
				return;
			case ModelPackage.MAPPING__GENERATE_USER_FACTORY:
				setGenerateUserFactory(GENERATE_USER_FACTORY_EDEFAULT);
				return;
			case ModelPackage.MAPPING__MODULES:
				getModules().clear();
				return;
			case ModelPackage.MAPPING__INSTANCE_MAPPINGS:
				getInstanceMappings().clear();
				return;
			case ModelPackage.MAPPING__EXTERNAL_MAPPINGS:
				getExternalMappings().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ModelPackage.MAPPING__IMPORTS:
				return imports != null && !imports.isEmpty();
			case ModelPackage.MAPPING__TERMINALS:
				return terminals != null && !terminals.isEmpty();
			case ModelPackage.MAPPING__EXTERNAL_TERMINALS:
				return externalTerminals != null && !externalTerminals.isEmpty();
			case ModelPackage.MAPPING__OPERATORS:
				return operators != null && !operators.isEmpty();
			case ModelPackage.MAPPING__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case ModelPackage.MAPPING__PREFIX:
				return PREFIX_EDEFAULT == null ? prefix != null : !PREFIX_EDEFAULT.equals(prefix);
			case ModelPackage.MAPPING__PACKAGE_NAME:
				return PACKAGE_NAME_EDEFAULT == null ? packageName != null : !PACKAGE_NAME_EDEFAULT.equals(packageName);
			case ModelPackage.MAPPING__USE_FULLY_QUALIFIED_TYPE_NAME:
				return useFullyQualifiedTypeName != USE_FULLY_QUALIFIED_TYPE_NAME_EDEFAULT;
			case ModelPackage.MAPPING__GENERATE_USER_FACTORY:
				return generateUserFactory != GENERATE_USER_FACTORY_EDEFAULT;
			case ModelPackage.MAPPING__MODULES:
				return modules != null && !modules.isEmpty();
			case ModelPackage.MAPPING__INSTANCE_MAPPINGS:
				return instanceMappings != null && !instanceMappings.isEmpty();
			case ModelPackage.MAPPING__EXTERNAL_MAPPINGS:
				return externalMappings != null && !externalMappings.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ModelPackage.MAPPING___GET_TERMINAL__ECLASS_BOOLEAN:
				return getTerminal((EClass)arguments.get(0), (Boolean)arguments.get(1));
			case ModelPackage.MAPPING___GET_ALL_LIST_TERMINALS:
				return getAllListTerminals();
			case ModelPackage.MAPPING___GET_METAMODEL_PACKAGES:
				return getMetamodelPackages();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", prefix: ");
		result.append(prefix);
		result.append(", packageName: ");
		result.append(packageName);
		result.append(", useFullyQualifiedTypeName: ");
		result.append(useFullyQualifiedTypeName);
		result.append(", generateUserFactory: ");
		result.append(generateUserFactory);
		result.append(')');
		return result.toString();
	}

} //MappingImpl
