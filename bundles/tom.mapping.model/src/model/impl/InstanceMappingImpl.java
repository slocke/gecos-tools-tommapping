/**
 */
package model.impl;

import model.InstanceMapping;
import model.ModelPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Instance Mapping</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link model.impl.InstanceMappingImpl#getEpackage <em>Epackage</em>}</li>
 *   <li>{@link model.impl.InstanceMappingImpl#getInstancePackageName <em>Instance Package Name</em>}</li>
 *   <li>{@link model.impl.InstanceMappingImpl#getPackageFactoryName <em>Package Factory Name</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class InstanceMappingImpl extends EObjectImpl implements InstanceMapping {
	/**
	 * The cached value of the '{@link #getEpackage() <em>Epackage</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEpackage()
	 * @generated
	 * @ordered
	 */
	protected EPackage epackage;
	/**
	 * The default value of the '{@link #getInstancePackageName() <em>Instance Package Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInstancePackageName()
	 * @generated
	 * @ordered
	 */
	protected static final String INSTANCE_PACKAGE_NAME_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getInstancePackageName() <em>Instance Package Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInstancePackageName()
	 * @generated
	 * @ordered
	 */
	protected String instancePackageName = INSTANCE_PACKAGE_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getPackageFactoryName() <em>Package Factory Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPackageFactoryName()
	 * @generated
	 * @ordered
	 */
	protected static final String PACKAGE_FACTORY_NAME_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getPackageFactoryName() <em>Package Factory Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPackageFactoryName()
	 * @generated
	 * @ordered
	 */
	protected String packageFactoryName = PACKAGE_FACTORY_NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InstanceMappingImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModelPackage.Literals.INSTANCE_MAPPING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EPackage getEpackage() {
		if (epackage != null && epackage.eIsProxy()) {
			InternalEObject oldEpackage = (InternalEObject)epackage;
			epackage = (EPackage)eResolveProxy(oldEpackage);
			if (epackage != oldEpackage) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ModelPackage.INSTANCE_MAPPING__EPACKAGE, oldEpackage, epackage));
			}
		}
		return epackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EPackage basicGetEpackage() {
		return epackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEpackage(EPackage newEpackage) {
		EPackage oldEpackage = epackage;
		epackage = newEpackage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.INSTANCE_MAPPING__EPACKAGE, oldEpackage, epackage));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getInstancePackageName() {
		return instancePackageName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInstancePackageName(String newInstancePackageName) {
		String oldInstancePackageName = instancePackageName;
		instancePackageName = newInstancePackageName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.INSTANCE_MAPPING__INSTANCE_PACKAGE_NAME, oldInstancePackageName, instancePackageName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPackageFactoryName() {
		return packageFactoryName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPackageFactoryName(String newPackageFactoryName) {
		String oldPackageFactoryName = packageFactoryName;
		packageFactoryName = newPackageFactoryName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.INSTANCE_MAPPING__PACKAGE_FACTORY_NAME, oldPackageFactoryName, packageFactoryName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ModelPackage.INSTANCE_MAPPING__EPACKAGE:
				if (resolve) return getEpackage();
				return basicGetEpackage();
			case ModelPackage.INSTANCE_MAPPING__INSTANCE_PACKAGE_NAME:
				return getInstancePackageName();
			case ModelPackage.INSTANCE_MAPPING__PACKAGE_FACTORY_NAME:
				return getPackageFactoryName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ModelPackage.INSTANCE_MAPPING__EPACKAGE:
				setEpackage((EPackage)newValue);
				return;
			case ModelPackage.INSTANCE_MAPPING__INSTANCE_PACKAGE_NAME:
				setInstancePackageName((String)newValue);
				return;
			case ModelPackage.INSTANCE_MAPPING__PACKAGE_FACTORY_NAME:
				setPackageFactoryName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ModelPackage.INSTANCE_MAPPING__EPACKAGE:
				setEpackage((EPackage)null);
				return;
			case ModelPackage.INSTANCE_MAPPING__INSTANCE_PACKAGE_NAME:
				setInstancePackageName(INSTANCE_PACKAGE_NAME_EDEFAULT);
				return;
			case ModelPackage.INSTANCE_MAPPING__PACKAGE_FACTORY_NAME:
				setPackageFactoryName(PACKAGE_FACTORY_NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ModelPackage.INSTANCE_MAPPING__EPACKAGE:
				return epackage != null;
			case ModelPackage.INSTANCE_MAPPING__INSTANCE_PACKAGE_NAME:
				return INSTANCE_PACKAGE_NAME_EDEFAULT == null ? instancePackageName != null : !INSTANCE_PACKAGE_NAME_EDEFAULT.equals(instancePackageName);
			case ModelPackage.INSTANCE_MAPPING__PACKAGE_FACTORY_NAME:
				return PACKAGE_FACTORY_NAME_EDEFAULT == null ? packageFactoryName != null : !PACKAGE_FACTORY_NAME_EDEFAULT.equals(packageFactoryName);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (instancePackageName: ");
		result.append(instancePackageName);
		result.append(", packageFactoryName: ");
		result.append(packageFactoryName);
		result.append(')');
		return result.toString();
	}

} //InstanceMappingImpl
