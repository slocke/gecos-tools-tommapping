/**
 */
package model;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see model.ModelPackage
 * @generated
 */
public interface ModelFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ModelFactory eINSTANCE = model.impl.ModelFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Mapping</em>'.
	 * @generated
	 */
	Mapping createMapping();

	/**
	 * Returns a new object of class '<em>Terminal</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Terminal</em>'.
	 * @generated
	 */
	Terminal createTerminal();

	/**
	 * Returns a new object of class '<em>Class Operator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Class Operator</em>'.
	 * @generated
	 */
	ClassOperator createClassOperator();

	/**
	 * Returns a new object of class '<em>User Operator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>User Operator</em>'.
	 * @generated
	 */
	UserOperator createUserOperator();

	/**
	 * Returns a new object of class '<em>Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Parameter</em>'.
	 * @generated
	 */
	Parameter createParameter();

	/**
	 * Returns a new object of class '<em>Import</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Import</em>'.
	 * @generated
	 */
	Import createImport();

	/**
	 * Returns a new object of class '<em>Accessor</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Accessor</em>'.
	 * @generated
	 */
	Accessor createAccessor();

	/**
	 * Returns a new object of class '<em>Feature Exception</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Feature Exception</em>'.
	 * @generated
	 */
	FeatureException createFeatureException();

	/**
	 * Returns a new object of class '<em>Feature Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Feature Parameter</em>'.
	 * @generated
	 */
	FeatureParameter createFeatureParameter();

	/**
	 * Returns a new object of class '<em>Setted Feature Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Setted Feature Parameter</em>'.
	 * @generated
	 */
	SettedFeatureParameter createSettedFeatureParameter();

	/**
	 * Returns a new object of class '<em>Setted Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Setted Value</em>'.
	 * @generated
	 */
	SettedValue createSettedValue();

	/**
	 * Returns a new object of class '<em>Java Code Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Java Code Value</em>'.
	 * @generated
	 */
	JavaCodeValue createJavaCodeValue();

	/**
	 * Returns a new object of class '<em>Enum Literal Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Enum Literal Value</em>'.
	 * @generated
	 */
	EnumLiteralValue createEnumLiteralValue();

	/**
	 * Returns a new object of class '<em>Int Setted Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Int Setted Value</em>'.
	 * @generated
	 */
	IntSettedValue createIntSettedValue();

	/**
	 * Returns a new object of class '<em>Boolean Setted Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Boolean Setted Value</em>'.
	 * @generated
	 */
	BooleanSettedValue createBooleanSettedValue();

	/**
	 * Returns a new object of class '<em>Alias Operator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Alias Operator</em>'.
	 * @generated
	 */
	AliasOperator createAliasOperator();

	/**
	 * Returns a new object of class '<em>Alias Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Alias Node</em>'.
	 * @generated
	 */
	AliasNode createAliasNode();

	/**
	 * Returns a new object of class '<em>Feature Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Feature Node</em>'.
	 * @generated
	 */
	FeatureNode createFeatureNode();

	/**
	 * Returns a new object of class '<em>Operator Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Operator Node</em>'.
	 * @generated
	 */
	OperatorNode createOperatorNode();

	/**
	 * Returns a new object of class '<em>Module</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Module</em>'.
	 * @generated
	 */
	Module createModule();

	/**
	 * Returns a new object of class '<em>Instance Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Instance Mapping</em>'.
	 * @generated
	 */
	InstanceMapping createInstanceMapping();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ModelPackage getModelPackage();

} //ModelFactory
