/**
 */
package model;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Feature Exception</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see model.ModelPackage#getFeatureException()
 * @model
 * @generated
 */
public interface FeatureException extends FeatureParameter {
} // FeatureException
