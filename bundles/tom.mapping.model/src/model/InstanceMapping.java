/**
 */
package model;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Instance Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link model.InstanceMapping#getEpackage <em>Epackage</em>}</li>
 *   <li>{@link model.InstanceMapping#getInstancePackageName <em>Instance Package Name</em>}</li>
 *   <li>{@link model.InstanceMapping#getPackageFactoryName <em>Package Factory Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see model.ModelPackage#getInstanceMapping()
 * @model
 * @generated
 */
public interface InstanceMapping extends EObject {

	/**
	 * Returns the value of the '<em><b>Epackage</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Epackage</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Epackage</em>' reference.
	 * @see #setEpackage(EPackage)
	 * @see model.ModelPackage#getInstanceMapping_Epackage()
	 * @model required="true"
	 * @generated
	 */
	EPackage getEpackage();

	/**
	 * Sets the value of the '{@link model.InstanceMapping#getEpackage <em>Epackage</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Epackage</em>' reference.
	 * @see #getEpackage()
	 * @generated
	 */
	void setEpackage(EPackage value);

	/**
	 * Returns the value of the '<em><b>Instance Package Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Instance Package Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Instance Package Name</em>' attribute.
	 * @see #setInstancePackageName(String)
	 * @see model.ModelPackage#getInstanceMapping_InstancePackageName()
	 * @model
	 * @generated
	 */
	String getInstancePackageName();

	/**
	 * Sets the value of the '{@link model.InstanceMapping#getInstancePackageName <em>Instance Package Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Instance Package Name</em>' attribute.
	 * @see #getInstancePackageName()
	 * @generated
	 */
	void setInstancePackageName(String value);

	/**
	 * Returns the value of the '<em><b>Package Factory Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Package Factory Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Package Factory Name</em>' attribute.
	 * @see #setPackageFactoryName(String)
	 * @see model.ModelPackage#getInstanceMapping_PackageFactoryName()
	 * @model
	 * @generated
	 */
	String getPackageFactoryName();

	/**
	 * Sets the value of the '{@link model.InstanceMapping#getPackageFactoryName <em>Package Factory Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Package Factory Name</em>' attribute.
	 * @see #getPackageFactoryName()
	 * @generated
	 */
	void setPackageFactoryName(String value);
} // InstanceMapping
