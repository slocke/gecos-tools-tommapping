/**
 */
package model;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link model.Mapping#getImports <em>Imports</em>}</li>
 *   <li>{@link model.Mapping#getTerminals <em>Terminals</em>}</li>
 *   <li>{@link model.Mapping#getExternalTerminals <em>External Terminals</em>}</li>
 *   <li>{@link model.Mapping#getOperators <em>Operators</em>}</li>
 *   <li>{@link model.Mapping#getName <em>Name</em>}</li>
 *   <li>{@link model.Mapping#getPrefix <em>Prefix</em>}</li>
 *   <li>{@link model.Mapping#getPackageName <em>Package Name</em>}</li>
 *   <li>{@link model.Mapping#isUseFullyQualifiedTypeName <em>Use Fully Qualified Type Name</em>}</li>
 *   <li>{@link model.Mapping#isGenerateUserFactory <em>Generate User Factory</em>}</li>
 *   <li>{@link model.Mapping#getModules <em>Modules</em>}</li>
 *   <li>{@link model.Mapping#getInstanceMappings <em>Instance Mappings</em>}</li>
 *   <li>{@link model.Mapping#getExternalMappings <em>External Mappings</em>}</li>
 * </ul>
 * </p>
 *
 * @see model.ModelPackage#getMapping()
 * @model
 * @generated
 */
public interface Mapping extends EObject {
	/**
	 * Returns the value of the '<em><b>Imports</b></em>' containment reference list.
	 * The list contents are of type {@link model.Import}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Imports</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Imports</em>' containment reference list.
	 * @see model.ModelPackage#getMapping_Imports()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Import> getImports();

	/**
	 * Returns the value of the '<em><b>Terminals</b></em>' containment reference list.
	 * The list contents are of type {@link model.Terminal}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Terminals</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Terminals</em>' containment reference list.
	 * @see model.ModelPackage#getMapping_Terminals()
	 * @model containment="true"
	 * @generated
	 */
	EList<Terminal> getTerminals();

	/**
	 * Returns the value of the '<em><b>External Terminals</b></em>' reference list.
	 * The list contents are of type {@link model.Terminal}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>External Terminals</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>External Terminals</em>' reference list.
	 * @see model.ModelPackage#getMapping_ExternalTerminals()
	 * @model
	 * @generated
	 */
	EList<Terminal> getExternalTerminals();

	/**
	 * Returns the value of the '<em><b>Operators</b></em>' containment reference list.
	 * The list contents are of type {@link model.Operator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operators</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operators</em>' containment reference list.
	 * @see model.ModelPackage#getMapping_Operators()
	 * @model containment="true"
	 * @generated
	 */
	EList<Operator> getOperators();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see model.ModelPackage#getMapping_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link model.Mapping#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Prefix</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Prefix</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Prefix</em>' attribute.
	 * @see #setPrefix(String)
	 * @see model.ModelPackage#getMapping_Prefix()
	 * @model
	 * @generated
	 */
	String getPrefix();

	/**
	 * Sets the value of the '{@link model.Mapping#getPrefix <em>Prefix</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Prefix</em>' attribute.
	 * @see #getPrefix()
	 * @generated
	 */
	void setPrefix(String value);

	/**
	 * Returns the value of the '<em><b>Package Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Package Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Package Name</em>' attribute.
	 * @see #setPackageName(String)
	 * @see model.ModelPackage#getMapping_PackageName()
	 * @model
	 * @generated
	 */
	String getPackageName();

	/**
	 * Sets the value of the '{@link model.Mapping#getPackageName <em>Package Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Package Name</em>' attribute.
	 * @see #getPackageName()
	 * @generated
	 */
	void setPackageName(String value);

	/**
	 * Returns the value of the '<em><b>Use Fully Qualified Type Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Use Fully Qualified Type Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Use Fully Qualified Type Name</em>' attribute.
	 * @see #setUseFullyQualifiedTypeName(boolean)
	 * @see model.ModelPackage#getMapping_UseFullyQualifiedTypeName()
	 * @model
	 * @generated
	 */
	boolean isUseFullyQualifiedTypeName();

	/**
	 * Sets the value of the '{@link model.Mapping#isUseFullyQualifiedTypeName <em>Use Fully Qualified Type Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Use Fully Qualified Type Name</em>' attribute.
	 * @see #isUseFullyQualifiedTypeName()
	 * @generated
	 */
	void setUseFullyQualifiedTypeName(boolean value);

	/**
	 * Returns the value of the '<em><b>Generate User Factory</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generate User Factory</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generate User Factory</em>' attribute.
	 * @see #setGenerateUserFactory(boolean)
	 * @see model.ModelPackage#getMapping_GenerateUserFactory()
	 * @model default="false"
	 * @generated
	 */
	boolean isGenerateUserFactory();

	/**
	 * Sets the value of the '{@link model.Mapping#isGenerateUserFactory <em>Generate User Factory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Generate User Factory</em>' attribute.
	 * @see #isGenerateUserFactory()
	 * @generated
	 */
	void setGenerateUserFactory(boolean value);

	/**
	 * Returns the value of the '<em><b>Modules</b></em>' containment reference list.
	 * The list contents are of type {@link model.Module}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Modules</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Modules</em>' containment reference list.
	 * @see model.ModelPackage#getMapping_Modules()
	 * @model containment="true"
	 * @generated
	 */
	EList<Module> getModules();

	/**
	 * Returns the value of the '<em><b>Instance Mappings</b></em>' containment reference list.
	 * The list contents are of type {@link model.InstanceMapping}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Instance Mappings</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Instance Mappings</em>' containment reference list.
	 * @see model.ModelPackage#getMapping_InstanceMappings()
	 * @model containment="true"
	 * @generated
	 */
	EList<InstanceMapping> getInstanceMappings();

	/**
	 * Returns the value of the '<em><b>External Mappings</b></em>' containment reference list.
	 * The list contents are of type {@link model.Mapping}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>External Mappings</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>External Mappings</em>' containment reference list.
	 * @see model.ModelPackage#getMapping_ExternalMappings()
	 * @model containment="true" transient="true" derived="true"
	 * @generated
	 */
	EList<Mapping> getExternalMappings();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	Terminal getTerminal(EClass c, boolean isList);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	EList<Terminal> getAllListTerminals();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	EList<EPackage> getMetamodelPackages();

} // Mapping
