/**
 */
package model;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Alias Node</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see model.ModelPackage#getAliasNode()
 * @model
 * @generated
 */
public interface AliasNode extends EObject {
} // AliasNode
