/**
 */
package model;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Alias Operator</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see model.ModelPackage#getAliasOperator()
 * @model
 * @generated
 */
public interface AliasOperator extends Operator, OperatorNode {
} // AliasOperator
