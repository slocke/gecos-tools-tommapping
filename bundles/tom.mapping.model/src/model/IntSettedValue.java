/**
 */
package model;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Int Setted Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link model.IntSettedValue#getJava <em>Java</em>}</li>
 * </ul>
 * </p>
 *
 * @see model.ModelPackage#getIntSettedValue()
 * @model
 * @generated
 */
public interface IntSettedValue extends SettedValue {
	/**
	 * Returns the value of the '<em><b>Java</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Java</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Java</em>' attribute.
	 * @see #setJava(int)
	 * @see model.ModelPackage#getIntSettedValue_Java()
	 * @model
	 * @generated
	 */
	int getJava();

	/**
	 * Sets the value of the '{@link model.IntSettedValue#getJava <em>Java</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Java</em>' attribute.
	 * @see #getJava()
	 * @generated
	 */
	void setJava(int value);

} // IntSettedValue
