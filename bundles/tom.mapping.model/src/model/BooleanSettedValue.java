/**
 */
package model;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Boolean Setted Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link model.BooleanSettedValue#isJava <em>Java</em>}</li>
 * </ul>
 * </p>
 *
 * @see model.ModelPackage#getBooleanSettedValue()
 * @model
 * @generated
 */
public interface BooleanSettedValue extends SettedValue {
	/**
	 * Returns the value of the '<em><b>Java</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Java</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Java</em>' attribute.
	 * @see #setJava(boolean)
	 * @see model.ModelPackage#getBooleanSettedValue_Java()
	 * @model
	 * @generated
	 */
	boolean isJava();

	/**
	 * Sets the value of the '{@link model.BooleanSettedValue#isJava <em>Java</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Java</em>' attribute.
	 * @see #isJava()
	 * @generated
	 */
	void setJava(boolean value);

} // BooleanSettedValue
