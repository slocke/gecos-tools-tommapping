/**
 * generated by Xtext 2.12.0-SNAPSHOT
 */
package tom.mapping.dsl.ui;

import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.xtend.lib.annotations.FinalFieldsConstructor;
import tom.mapping.dsl.ui.AbstractTomMappingUiModule;

/**
 * Use this class to register components to be used within the Eclipse IDE.
 */
@FinalFieldsConstructor
@SuppressWarnings("all")
public class TomMappingUiModule extends AbstractTomMappingUiModule {
  public TomMappingUiModule(final AbstractUIPlugin plugin) {
    super(plugin);
  }
}
