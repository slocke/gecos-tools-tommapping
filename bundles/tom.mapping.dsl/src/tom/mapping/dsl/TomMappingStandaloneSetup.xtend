/*
 * generated by Xtext 2.12.0-SNAPSHOT
 */
package tom.mapping.dsl

import org.eclipse.emf.ecore.EPackage
import com.google.inject.Injector

/**
 * Initialization support for running Xtext languages without Equinox extension registry.
 */
class TomMappingStandaloneSetup extends TomMappingStandaloneSetupGenerated {
	//XXX added
	override Injector createInjectorAndDoEMFRegistration() {
		if(!EPackage.Registry.INSTANCE.containsKey(model.ModelPackage.eNS_URI)) EPackage.Registry.INSTANCE.put(
			model.ModelPackage.eNS_URI, model.ModelPackage.eINSTANCE)
		return super.createInjectorAndDoEMFRegistration()
	}

	def static void doSetup() {
		new TomMappingStandaloneSetup().createInjectorAndDoEMFRegistration()
	}
}
