package tom.mapping.dsl.generator.tom

import com.google.inject.Inject
import model.BooleanSettedValue
import model.ClassOperator
import model.EnumLiteralValue
import model.FeatureParameter
import model.IntSettedValue
import model.JavaCodeValue
import model.Mapping
import model.Operator
import model.SettedFeatureParameter
import model.SettedValue
import org.eclipse.emf.ecore.EAttribute
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EStructuralFeature
import tom.mapping.dsl.generator.TomMappingExtensions

class OperatorsCompiler {
	
	extension TomMappingExtensions = new TomMappingExtensions()
	
	@Inject extension ParametersCompiler injpa
	
	def dispatch operator(Mapping mapping, Operator op) {
		''''''
	}
	
	def dispatch operator(Mapping mapping, ClassOperator clop) {
		if(clop.parameters.size > 0) {
			mapping.classOperatorWithParameters(clop);
		} else {
			mapping.classOperator(clop.name, clop.class_);
		}
	}
	
	
	def classOperator(Mapping m, String op, EClass ecl) {
		val parameters = ecl.getDefaultParameters(m)
		'''
		«IF m.getTerminal(ecl,false) != null»
			
			%op «m.getTerminal(ecl,false).name» «op»(«m.classAttributes(ecl)»«parameters.join(", ", [p|p.tomParameter(m)])») {
				/*PROTECTED REGION ID(tom_operator_«op») DISABLED START*/
				//make sure to change to ENABLED when using protected region
				is_fsym(t) {$t instanceof «ecl.javaType(m)»}
				«FOR attribute: ecl.EAllAttributes»
					get_slot(«attribute.paramName»,t) {((«ecl.javaType(m)»)$t).«attribute.getGetterPrefix()+attribute.paramName.toFirstUpper()»()}
				«ENDFOR»
				«FOR p: parameters»
					get_slot(«p.paramName»,t) {«m.getter(ecl, p)»}
				«ENDFOR»
				make(«(ecl.EAllAttributes+parameters).join(", ", [e|e.paramName])»){«m.tomFactoryQualifiedName()».«op.toFirstUpper()»(«(ecl.EAllAttributes+parameters).join(", ", [e|"$"+e.paramName])»)}
				/*PROTECTED REGION END*/
			}
		«ENDIF»
	   	'''
	}
	
	
	def getGetterPrefix(EAttribute a) {
		if (a == null || a.EAttributeType == null || a.EAttributeType.instanceClassName == null) '''get'''
		else {
			if (a.EAttributeType.instanceClassName.equals("boolean")) {
				'''is'''
			} else {
				'''get'''
			}
		}
	}
	
	def classOperatorWithParameters(Mapping m, ClassOperator clop) {
   		val parameters = clop.getCustomParameters()
		'''
		«IF m.getTerminal(clop.class_,false) != null»
			
			%op «m.getTerminal(clop.class_,false).tomType(m)» «clop.name»(«parameters.join(", ", [p|p.tomParameter(m)])») {
				/*PROTECTED REGION ID(tom_operator_with_param_«clop.name») DISABLED START*/
				//make sure to change to ENABLED when using protected region
				is_fsym(t) {$t instanceof «clop.class_.javaType(m)»«clop.parameters.join("", [p|m.settedParameterTest(clop.class_, p)])»}
				«FOR p: parameters»
					get_slot(«p.feature.paramName»,t) {«m.getter(clop.class_, p.feature)»}
				«ENDFOR»
				make(«parameters.join(",", [p|"_"+p.feature.paramName])») {«m.tomFactoryQualifiedName()».create«clop.name.toFirstUpper()»(«parameters.join(", ", [p|"$_"+p.feature.paramName])»)}
				/*PROTECTED REGION END*/
			}
		«ENDIF»
   	'''
	}
	
	
	def getter(Mapping m, EClass c, EStructuralFeature esf) {
		'''«IF esf.many»enforce(«ENDIF»((«c.javaType(m)»)$t).«getGetterPrefix(esf,m)+esf.paramName.toFirstUpper()»()«IF esf.many»)«ENDIF»'''
	}
	def getGetterPrefix(EStructuralFeature esf, Mapping m) {
		val str = esf.javaType(m);
		if (str.equals("boolean")) {
			'''is'''
		} else {
			'''get'''
		}	
	}
	
	
	
	def classAttributes(Mapping mapping, EClass ecl) {		
		'''«ecl.EAllAttributes.join(", ", [e|e.tomParameter(mapping)])»«IF (ecl.EAllAttributes.size() > 0) && (ecl.getDefaultParameters(mapping).size() >0)»,«ENDIF»'''
	}
	
	
	def javaClassAttributes(Mapping mapping, EClass ecl) {
		'''«ecl.EAllAttributes.join(", ", [e|e.javaParameter(mapping)])»«IF (ecl.EAllAttributes.size() > 0) && (ecl.getDefaultParameters(mapping).size() >0)»,«ENDIF»'''
	}
	
	
	def private dispatch settedParameterTest(Mapping m, EClass c, FeatureParameter feature) {''''''}
	
	
	def private dispatch settedParameterTest(Mapping m, EClass c, SettedFeatureParameter sfp) {
		val v = sfp.value
		var value = sfp.feature.settedValue(v, m)
		if ((value.toString.contentEquals("null"))
			|| (v instanceof IntSettedValue)
			|| (v instanceof BooleanSettedValue)
		) {
			''' && ((«c.javaType(m)»)$t).get«sfp.feature.name.toFirstUpper()»() == «value»'''
		} else {
			''' && ((«c.javaType(m)»)$t).get«sfp.feature.name.toFirstUpper()»().equals(«value»)'''
		}
	}
	
	
	def dispatch settedValue(EStructuralFeature feature, SettedValue sv, Mapping m) {''''''}
	
	def dispatch settedValue(EStructuralFeature feature, EnumLiteralValue elv, Mapping m) {
		'''«feature.EType.javaType(m)».«elv.literal.name»'''
	}
	def dispatch settedValue(EStructuralFeature feature, IntSettedValue elv, Mapping m) {
		'''«elv.java»'''
	}
	def dispatch settedValue(EStructuralFeature feature, BooleanSettedValue elv, Mapping m) {
		'''«elv.java»'''
	}
	
	
	def dispatch settedValue(EStructuralFeature feature, JavaCodeValue jcv, Mapping m) {
		val isString = ((feature.EType.instanceTypeName != null) && (feature.EType.instanceTypeName.contains("java.lang.String")))
		'''«IF isString»"«ENDIF»«jcv.java»«IF isString»"«ENDIF»'''
	}
	
	
}