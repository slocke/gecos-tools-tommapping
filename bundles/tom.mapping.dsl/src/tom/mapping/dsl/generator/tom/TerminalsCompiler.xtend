package tom.mapping.dsl.generator.tom

import org.eclipse.emf.ecore.EClassifier
import org.eclipse.emf.ecore.EDataType
import org.eclipse.emf.ecore.EEnum
import org.eclipse.emf.ecore.EPackage
import tom.mapping.dsl.generator.TomMappingExtensions
import model.Mapping
import model.Terminal
import com.google.inject.Inject

class TerminalsCompiler {

	extension TomMappingExtensions = new TomMappingExtensions()

	@Inject extension ParametersCompiler injpa;   

	def terminal(Mapping m, Terminal t) {
		if  (t.many) {
			m.listTerminal(t);
		} else {
			'''
			
			%typeterm «t.tomType(m)» {
				/*PROTECTED REGION ID(tom_terminal_«t.name») DISABLED START*/
				//make sure to change to ENABLED when using protected region
				implement 		{«t.javaType(m)»}
				is_sort(t) 		{$t instanceof «t.javaType(m)»}
				equals(l1,l2) 	{($l1!=null && $l1.equals($l2)) || $l1==$l2}
				/*PROTECTED REGION END*/
			}
			'''
		}
	}
	
	
	
	def listTerminal(Mapping m, Terminal t) {
	'''
	
	%typeterm «t.tomType(m)»{
		/*PROTECTED REGION ID(tom_terminal_«t.name») DISABLED START*/
		//make sure to change to ENABLED when using protected region
		implement { EList<«t.class_.javaType(m)»> }
	    is_sort(t) «m.listTest(t)»
		equals(l1,l2) 	{($l1!=null && $l1.equals($l2)) || $l1==$l2}
		/*PROTECTED REGION END*/
	}
	
	%oparray «t.tomType(m)» «t.tomType(m)»(«m.getTerminal(t.class_,false).tomType(m)»*) {
		/*PROTECTED REGION ID(tom_terminal_«t.name»_list) DISABLED START*/
		//make sure to change to ENABLED when using protected region
	 	is_fsym(t) «m.listTest(t)»
	    make_empty(n) { new BasicEList<«t.class_.javaType(m)»>($n) }
	    make_append(e,l) { append($e,$l) }
	    get_element(l,n) { $l.get($n) }
	    get_size(l)      { $l.size() }
		/*PROTECTED REGION END*/
	}
	'''
	}
	
	def listTest(Mapping m, Terminal t) {
		'''
		{ $t instanceof EList<?> &&
			(((EList<«t.class_.javaType(m)»>)$t).size() == 0 
			|| (((EList<«t.class_.javaType(m)»>)$t).size()>0 && ((EList<«t.class_.javaType(m)»>)$t).get(0) instanceof «t.class_.javaType(m)»))}
		'''
	}
	
	def dispatch CharSequence primitiveTerminal(Mapping mapping, EPackage c) '''
		«FOR classifier :c.eAllContents.filter(typeof(EClassifier)).toList»
			«mapping.primitiveTerminal(classifier)»
		«ENDFOR»
	'''
	
	def dispatch primitiveTerminal(Mapping m, EClassifier c)''''''
	
	
	def dispatch primitiveTerminal(Mapping m, EEnum c) '''
		
		%typeterm «c.tomType(m)» {
			/*PROTECTED REGION ID(tom_terminal_«c.name») DISABLED START*/
			//make sure to change to ENABLED when using protected region
			implement 		{«c.javaType(m)»}
			is_sort(t) 		{$t instanceof «c.javaType(m)»}
			equals(l1,l2) 	{$l1==$l2}
			/*PROTECTED REGION END*/
		}
	'''
	
	def dispatch primitiveTerminal(Mapping m, EDataType c) '''
		«val primitive = c.instanceTypeName.primitive»
		
		%typeterm «c.tomType(m)» {
			/*PROTECTED REGION ID(tom_terminal_«c.name») DISABLED START*/
			//make sure to change to ENABLED when using protected region
			implement 		{«c.javaType(m)»}
			is_sort(t) 		{«IF primitive»true«ELSE»$t instanceof «c.javaType(m)»«ENDIF»}
			equals(l1,l2) 	{«IF primitive»$l1==$l2«ELSE»$l1.equals($l2)«ENDIF»}
			/*PROTECTED REGION END*/
		}
	'''
}
