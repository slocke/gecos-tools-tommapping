// Licence
package tom.mapping.dsl.generator.tom

import model.FeatureParameter
import model.Mapping
import model.Parameter
import model.Terminal
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EClassifier
import org.eclipse.emf.ecore.EDataType
import org.eclipse.emf.ecore.EEnum
import org.eclipse.emf.ecore.EPackage
import org.eclipse.emf.ecore.EReference
import org.eclipse.emf.ecore.EStructuralFeature
import org.eclipse.emf.ecore.util.EcoreUtil
import tom.mapping.dsl.generator.TomMappingExtensions

class ParametersCompiler {
	
	extension TomMappingExtensions = new TomMappingExtensions()

	def dispatch tomParameter(Parameter p, Mapping m) {
		'''«p.name» : «p.type.getTomType(m)»'''
	}
	
	def dispatch tomParameter(FeatureParameter fp, Mapping m) {
		'''«fp.feature.paramName» : «fp.feature.tomType(m)»'''
	}
	
	def dispatch  tomParameter(EStructuralFeature efp, Mapping m) {
		'''«efp.paramName» : «efp.tomType(m)» '''
	}
	
	def dispatch javaParameter(Parameter p, Mapping m) {
		'''	«p.type.javaType(m)» «p.name»'''
	}
	
	def dispatch javaParameter(FeatureParameter fp, Mapping m) {
		'''«fp.feature.javaType(m)» _«fp.feature.name»'''
	}
	
	def dispatch  javaParameter(EStructuralFeature efp, Mapping m) {
		'''«efp.javaType(m)» _«efp.name»'''
	}
	
	
	
	//XXX hack to omit "___internal_cached" prefix
	// currently temporarily used in gecos xcore models 
	def paramName(EStructuralFeature p) {
//		return p.name
		p.name.replaceAll("___internal_cached___", "")
	}
	
	
	/**
	 * tomType is the main entrypoint for getting the tom type name
	 *  from many things (ERerefence, EStructuralFeature, EClass, Terminal)
	 * If it is an EClass, it tries to find the matching Terminal
	 * 
	 * getTomType is where it does the actual String generation
	 * 
	 */
	 
	def tomType(EStructuralFeature esf, Mapping m) {
		if (esf instanceof EReference) {
			return (esf as EReference).getTomType(m)
		} else {
			if (esf.many) {
				'''«esf.EType.getTomType(m)»List'''
			} else {
				'''«esf.EType.getTomType(m)»'''
			}
		}
	}
	def tomType(EClassifier eclass, Mapping m) {
		return eclass.getTomType(m)
	}
	
	def tomType(EEnum enumType, Mapping m) {
		return enumType.getTomType(m)
	}
	
	def tomType(EDataType edt, Mapping m) {
		return edt.getTomType(m)
	}
	
	def tomType(Terminal t, Mapping m) {
//		if (t.many) {
//			'''«t.getTomType(m)»List'''
//		} else {
			'''«t.getTomType(m)»'''
//		}
	}
	
	
	def private dispatch String getTomType(EReference eref, Mapping m) {
		val t = m.getTerminal(eref.EReferenceType, eref.many)
		if (t !== null) {
			return t.getTomType(m)
		} else {
			return eref.EReferenceType.getTomType(m)
		}
	}
	
	def private dispatch String getTomType(Terminal t, Mapping mapping) {
		t.name
	}
	
	def private dispatch String getTomType(EClassifier eclass, Mapping m) {
		if (eclass.instanceTypeName !== null || !m.useFullyQualifiedTypeName) {
			return eclass.renameEcoreClasses(m)
		} else {
			return m.getFullyQualifiedTomType(eclass);
		}
	}
	
	def private dispatch String getTomType(EDataType edt, Mapping m) {
		if (m.useFullyQualifiedTypeName && !edt.name.isPrimitive2) {
			return m.getFullyQualifiedTomType(edt);
		} else {
			return edt.name
		}
	}
	
	def private getFullyQualifiedTomType(Mapping mapping, EClassifier eclass) {
		val prefix = mapping.getPackagePrefix(eclass.EPackage).replaceAll("\\.", "_") + eclass.EPackage.name + "_"
		return prefix + eclass.name.replaceAll("\\$", "_");
	}
	
	/**
	 * javaType is the main entrypoint for getting the Java type name
	 *  from many things (ERerefence, EStructuralFeature, EClass, Terminal)
	 * javaTerminalType tries to find the corresponding Terminal
	 * 
	 * getJavaType is where it does the actual String generation
	 * 
	 */
	
	def javaTerminalType(EStructuralFeature fp, Mapping m) {
		if (fp instanceof EReference) {
			if (fp.many) {
			structured = true;
			val res = "List<"+((fp as EReference).EReferenceType).getJavaTerminalType(m)+">"
			structured = false;
			return res;
//				return '''List<«)»>'''
			} else {
				return ((fp as EReference).EReferenceType).getJavaTerminalType(m)
			}
		} else {
			return fp.javaType(m)
		}
	} 
	
	def javaTerminalType(EClass eclass, Mapping m) {
		return eclass.getJavaTerminalType(m)
	}
	
	def private getJavaTerminalType(EClass eclass, Mapping m) {
		val t = m.getTerminal(eclass, false)
		if (t !== null) {
			return t.javaType(m)
		} else {
			return eclass.javaType(m)
		}
	}
	
	def String javaType(EStructuralFeature esf, Mapping m) {
		if (esf.many) {
			structured = true;
			val res = "List<"+esf.EType.getJavaType(m)+">"
			structured = false;
			return res;
		} else {
			'''«esf.EType.getJavaType(m)»'''
		}
	}
	
//	def dispatch javaType(Mapping mapping, EClassifier eclass) {
//		'''«mapping.getJavaType(eclass)»'''
//	}
	
	def javaType(EClassifier eclass, Mapping m) {
		return eclass.getJavaType(m)
	}
	
	def javaType(EEnum enumType, Mapping m) {
		return enumType.getJavaType(m)
	}
	
	def javaType(EDataType edt, Mapping m) {
		return edt.getJavaType(m)
	}
	
	var structured = false;
	def javaType(Terminal t, Mapping m) {
		if (t.many) {
			structured = true;
			val res = "List<"+t.class_.getJavaType(m)+">"
			structured = false;
			return res;
		} else {
			'''«t.class_.getJavaType(m)»'''
		}
	}

	def private getJavaType(EClassifier eclass, Mapping mapping) {
		var res = ""
		if (eclass.instanceTypeName !== null || !mapping.useFullyQualifiedTypeName) {
			res = eclass.renameEcoreClasses(mapping)
		} else {
			res = eclass.getFullyQualifiedJavaType(mapping);
		}
		return res
	}
	
	def private getFullyQualifiedJavaType(EClassifier eclass, Mapping mapping) {
		val prefix = mapping.getPackagePrefix(eclass.EPackage) + eclass.EPackage.name + "."
		return prefix + eclass.name.replaceAll("\\$", ".");
	}
	
	def String renameEcoreClasses(EClassifier eat, Mapping m) {
		var eEat = eat;
		if (eEat.eIsProxy)
		 {
			eEat = EcoreUtil.resolve(eEat,m) as EClassifier
		}
//			throw new RuntimeException("Proxy to external ecore files are not supported: " + eat);
		if (!structured)
			switch (eEat.name) {
				case "EInt": return '''int'''
				case "ELong": return '''long'''
				case "EFloat": return '''float'''
				case "EDouble": return '''double'''
				case "EBoolean": return '''boolean'''
				case "EString": return '''String'''
				//XCore
				case "String": return '''String'''
			}
		else
			switch (eEat.name) {
				case "EInt": return '''Integer'''
				case "ELong": return '''Long'''
				case "EFloat": return '''Float'''
				case "EDouble": return '''Double'''
				case "EBoolean": return '''Boolean'''
				case "EString": return '''String'''
				//XCore
				case "String": return '''String'''
			}
		if (eEat.instanceTypeName !== null) {
			var res = eEat.instanceTypeName.replaceAll("\\$", ".");
			if (eEat instanceof EClass && (eEat as EClass).EStructuralFeatures.size > 0) {
				val eclass = eEat as EClass
				var first = true;
				res += "<"
				for (feat : eclass.EStructuralFeatures) {
					if (first) first = false
					else res += ","
					res += feat.EType.getJavaType(m)
				}
				res += ">"
			}
			return res
		} else if (eEat.name !== null) {
			return eEat.name.replaceAll("\\$", ".");
		} else {
			return "null";
		}
	}
	
	def dispatch primitiveType(EDataType edt) {
		'''«IF edt.instanceTypeName == "java.lang.String"»String«ELSE»«edt.instanceTypeName»«ENDIF»'''
	}
	
	def dispatch primitiveType(EEnum ee) {
		'''«ee.name»'''
	}
	
	def private packageName(Mapping m, EPackage p) {
		val instance = m.findMappedInstance(p)
		
		if (instance !== null && instance.packageFactoryName !== null) {
			return instance.packageFactoryName;
		}
		
		return p.name.toFirstUpper
	}
	
	def getFactoryType(Mapping m, EPackage p) {
		if (m.useFullyQualifiedTypeName) {
			'''«m.getPackagePrefix(p)»«p.name».«m.packageName(p)»Factory'''
		} else {
			'''«m.packageName(p)»Factory'''
		}
	}
	
	def getSwitchType(Mapping m, EPackage p) {
		if (m.useFullyQualifiedTypeName) {
			'''«m.getPackagePrefix(p)»«p.name».util.«m.packageName(p)»Switch'''
		} else {
			'''«m.packageName(p)»Switch'''
		}
	}
	
	def getPackageType(Mapping m, EPackage p) {
		if (m.useFullyQualifiedTypeName) {
			'''«m.getPackagePrefix(p)»«p.name».«m.packageName(p)»Package'''
		} else {
			'''«m.packageName(p)»Package'''
		}
	}
	
	
}