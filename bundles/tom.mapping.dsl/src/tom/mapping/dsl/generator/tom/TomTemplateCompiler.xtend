package tom.mapping.dsl.generator.tom

import com.google.inject.Inject
import java.util.LinkedHashMap
import java.util.List
import model.Mapping
import model.Module
import org.eclipse.emf.ecore.EClassifier
import org.eclipse.emf.ecore.EPackage
import org.eclipse.xtext.generator.IFileSystemAccess
import tom.mapping.dsl.generator.TomMappingExtensions

class TomTemplateCompiler {
	
	extension TomMappingExtensions = new TomMappingExtensions()
	
	@Inject extension TerminalsCompiler terminals
	@Inject extension OperatorsCompiler injop
//	@Inject extension ParametersCompiler injp
	
	String prefix="tom"
	
	
	def compile(Mapping m, IFileSystemAccess fsa){
	
		fsa.generateFile(prefix+"/"+m.name+"_common.tom",m.common())
		fsa.generateFile(prefix+"/"+m.name+"_terminals.tom",m.terminals())
		fsa.generateFile(prefix+"/"+m.name+"_operators.tom",m.operators())
		fsa.generateFile(prefix+"/"+m.name+"_defaultOperators.tom",m.defaultOperators())
		for(module: m.modules){
			fsa.generateFile(prefix+"/"+m.name+"_"+module.name+".tom",m.module(module))
		}
	}
	
	def protected common(Mapping m){
		'''
		%include { string.tom }
		%include { boolean.tom }
		%include { int.tom }
		%include { long.tom }
		%include { float.tom }
		%include { double.tom }
		
		private static <O> EList<O> enforce(EList l) {
				return l;
		}
		
		private static <O> EList<O> append(O e,EList<O> l) {
		       l.add(e);
		       return l;
		}
		'''
	}
	

	def protected terminals(Mapping m) {
		//XXX hack to avoid duplicates in case a terminal with the same name is defined in multiple epackages..
		// also omit String, Long, int, long, double data types (which are currently added in xcore models see gecos .xcore models for example)
		val set = new LinkedHashMap<String, EClassifier>
		for(p : m.newRootPackages.toList) {
			for(c : p.eAllContents.filter(EClassifier).toList) {
				if(!c.name.isPrimitive2)
					if(!set.containsKey(c.name))
						set.put(c.name, c)
			}
		}
		
		'''
		// Primitive terminals (enum and data types)
«««		«FOR p: m.getNewRootPackages()»
«««			«m.primitiveTerminal(p)»
«««		«ENDFOR»
		«FOR c: set.values»
			«m.primitiveTerminal(c)»
		«ENDFOR»
		
		// Terminals
		«FOR t: m.terminals»
			«m.terminal(t)»
		«ENDFOR»
		
		// List Terminals
		«FOR lt:m.allListTerminals»
			«m.listTerminal(lt)»
		«ENDFOR»
		'''
	}
	
	def protected operators(Mapping m) {
		'''
		// User operators
		«FOR op: m.operators»
			«m.operator(op)»
		«ENDFOR»
		'''
	}
	
	def protected defaultOperators(Mapping m) {
		'''
		// Default operators
		«FOR op: m.allDefaultOperators»
			«m.classOperator(op.name, op)»
		«ENDFOR»
		'''
	}

	
	def protected module(Mapping m, Module mod) {
		'''
		/* PROTECTED REGION ID(module_«mod.name»_user) ENABLED START */
		// Protected user region
		/* PROTECTED REGION END */
		
		«FOR op: mod.operators»
			«injop.operator(m,op)»
		«ENDFOR»
		'''
	}
	
	
	def protected void primitiveTerminals(Mapping mapping, EPackage epa) {
		
		for(c: epa.EClassifiers) {
			mapping.primitiveTerminal(c);
		}
		
		for(subp: epa.ESubpackages) {
			mapping.primitiveTerminals(subp);
		}
	}
		
		
//	def protected dispatch primitiveTerminal(Mapping mapping, EClassifier ecl) {
//	}	
//		
//	def protected dispatch primitiveTerminal(Mapping mapping, EEnum enu) {
//		'''
//		
//		%typeterm «enu.tomType(mapping)» {
//			implement {«enu.name»}
//			is_sort(t) {$t instanceof «enu.name»}
//			equals(l1,l2) {$l1==$l2}
//		}	
//		'''
//	}	
//		
//	def protected dispatch primitiveTerminal(Mapping mapping, EDataType edaty){
//		val primitive = edaty.instanceTypeName.isPrimitive();
//		'''
//		
//		%typeterm «edaty.tomType(mapping)» {
//			implement {«edaty.instanceTypeName»}
//			is_sort(t) {«IF primitive»true«ELSE»{$t instanceof «edaty.instanceTypeName»«ENDIF»}
//			equals(l1,l2) {«IF primitive»{$l1=$l2}«ELSE»{l1.equals($l2)«ENDIF»}
//		}
//		'''	
//	}
			
}