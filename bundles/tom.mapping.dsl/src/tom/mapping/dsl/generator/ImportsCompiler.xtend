// Licence
package tom.mapping.dsl.generator

import org.eclipse.emf.ecore.EPackage
import model.Mapping

class ImportsCompiler {

	extension TomMappingExtensions = new TomMappingExtensions()
	
	def imports(Mapping map) {
		'''
		«FOR EPackage p: map.getAllRootPackages()»
			«map.imports(p)»
		«ENDFOR»
		'''
	}
	
	
	def importsWithUtils(Mapping map) {
		'''
		«FOR EPackage p: map.getAllRootPackages()»
			«map.importsWithUtils(p)»
		«ENDFOR»
		'''
	}
	
	
	def CharSequence imports(Mapping mapping, EPackage ep) { // Check this function
		if (mapping.useFullyQualifiedTypeName) {
			''''''
		} else {
			'''
			«IF ep.EClassifiers.size() > 0»	
				import «mapping.getPackagePrefix(ep)»«ep.name».*;
			«ENDIF» 
			«FOR p : ep.ESubpackages»
				 «mapping.imports(p)»
			«ENDFOR»
			'''
		}
	}


	def CharSequence importsWithUtils(Mapping mapping, EPackage ep) { // Check this function
		if (mapping.useFullyQualifiedTypeName) {
			''''''
		} else {
			'''
			«IF ep.EClassifiers.size() > 0»
				import «mapping.getPackagePrefix(ep)»«ep.name».*; 
				import «mapping.getPackagePrefix(ep)»«ep.name».util.*;
			«ENDIF»
			«FOR p: ep.ESubpackages»
				«mapping.importsWithUtils(p)»
			«ENDFOR»
			'''
		}
	}

}