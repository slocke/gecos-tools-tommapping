package tom.mapping.dsl.generator

import java.util.ArrayList
import java.util.Collection
import java.util.List
import model.Accessor
import model.ClassOperator
import model.FeatureException
import model.FeatureParameter
import model.InstanceMapping
import model.Mapping
import model.Operator
import model.SettedFeatureParameter
import model.Terminal
import model.UserOperator
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EPackage
import org.eclipse.emf.ecore.EReference
import org.eclipse.emf.ecore.EStructuralFeature
import org.eclipse.emf.ecore.impl.EStringToStringMapEntryImpl

class TomMappingExtensions {
	
	def  String getCustomOperatorSlotAccessorName(Accessor a){
		return "get"+(a as UserOperator).name.toFirstUpper()+"Slot"+a.slot.name.toUpperCase(); 
	}
	
	def  String getCustomOperatorsClass(Mapping mapping) {
		return mapping.name.toFirstUpper()+"CustomAccessors";
	}
	
	def  String getChildrenGetterName(EPackage p){
		p.name.toFirstUpper()+"ChildrenGetter";
	}
	
	def String getChildrenSetterName(EPackage p){
		p.name.toFirstUpper()+"ChildrenSetter";
	}
	
	def String name(Terminal t, Mapping m){
		if(t.many&&t.isInferedList(m)){
			return t.name+"List";
		} else {
			return t.name;
		}
	}
	

	def boolean isInferedList(Terminal t, Mapping m){
		t.many&&!(m.terminals.contains(t)||m.externalTerminals.contains(t));
	}
	
	def String javaFactoryName(Mapping m){
		m.packageName()+"UserFactory";
	}
	def String tomFactoryName(Mapping m){
		m.packageName()+"TomFactory";
	}
//	def String introspectorName(Mapping m){
//		m.packageName()+"Introspector";
//	}
	
	def private packageName(Mapping m) {
		if (m.packageName === null) {
			return m.name.toFirstUpper();
		} else {
			return m.packageName;
		}
	}
	
	def String tomFactoryQualifiedName(Mapping m){
		m.prefix+"."+m.name+"."+"internal"+"."+tomFactoryName(m);
	}

	def String getMainPackagePrefix(Mapping mapping) {
		if (mapping.prefix !== null && mapping.prefix.length > 0) {
			return mapping.prefix + ".";
		} else {
			return "";
		}
	}

	def String getPackagePrefix(Mapping mapping, EPackage epackage){
		if (mapping.name == epackage.name) {
			if (mapping.prefix !== null && mapping.prefix.length > 0) {
				return mapping.prefix + ".";
			} else {
				return "";
			}
		} else {
			return mapping.searchMappedInstance(epackage)
		}
	}
	
	def InstanceMapping findMappedInstance(Mapping mapping, EPackage epackage) {
		var instance = mapping.instanceMappings.findFirst([map|map.epackage.name == epackage.name])
		
		if (instance !== null)
			return instance
		
		for (submap : mapping.externalMappings) {
			instance = submap.findMappedInstance(epackage)
			
			if (instance !== null)
				return instance
		}
		
		return instance
	}
	
	def private searchMappedInstance(Mapping mapping, EPackage epackage) {
		val instance = mapping.findMappedInstance(epackage)
		if (instance !== null && instance.instancePackageName !== null) {
			return instance.instancePackageName +"."
		} else {
			if (epackage.ESuperPackage !== null) {
				var res = mapping.getPackagePrefix(epackage.ESuperPackage)
				if (res !== null && res.length > 0) {
					return res + epackage.ESuperPackage.name + "."
				} else {
					return epackage.ESuperPackage.name + "."
				}
				
			} else {
				val basePackage = epackage.getBasePackage
				return if(basePackage !== null) basePackage + "." else ""
			}
		}
	}
	
	/*
	 * Try to get the value of "basePackage" annotation of the EPackage
	 */ 
	def getBasePackage(EPackage p) {
		//! tested with xcore packages, but not with ecore 
		val basePackage = p.EAnnotations
			.map[details].flatten
			.filter(EStringToStringMapEntryImpl)
			.findFirst[key == "basePackage"]
			?.value
		return basePackage
	}

//	def String getPackagePrefix(String prefix){
//		if(prefix==null || prefix.compareTo("")==0){
//			return "";
//		}else{
//			return prefix+".";
//		}
//	}
	
		
	def getDefaultParameters(EClass c, Mapping mapping){
		c.EAllReferences.filter([e|e.isParameterFeature(mapping)]).toList;
	}
	

	def getCustomParameters(ClassOperator c){
		c.parameters.filter([e|isExplicitParameter(e)]);
		
	}
		
	def getSettedCustomParameters(ClassOperator c){
		c.parameters.filter(typeof(SettedFeatureParameter)).toList;
	}
	
	def private dispatch boolean isParameterFeature(EStructuralFeature f,Mapping mapping){
		true;
	}
		
	def private dispatch boolean isParameterFeature(EReference f,Mapping mapping){
		 f.derived==false 
		&& f.volatile==false 
		&& (f.containment || f.EOpposite === null)
		&& (mapping.getTerminal(f.EReferenceType,false) !== null || mapping.getTerminal(f.EReferenceType,true) !== null);
	}
	
	def private dispatch boolean isExplicitParameter(FeatureParameter pf){
		true; 
	
	}
	
	def private dispatch boolean  isExplicitParameter(SettedFeatureParameter pf){
		false; 
	}
		
	def private dispatch boolean  isExplicitParameter(FeatureException pf){
		false; 
	}	
		
	def getAllSubPackages(EPackage p){
		p.eAllContents.filter(typeof(EPackage)).toList
	}
	
	def getAllPackages(Mapping m){
		val selected = new ArrayList<EPackage>()
		for(rp: m.allRootPackages){
			if (rp.isSelected(m))
				selected.add(rp)
				
			selected.addAll(rp.allSubPackages.filter([e | e.isSelected(m)]));
		}
		return selected.intersectName
	}
		
	def boolean isSelected(EPackage p, Mapping m){
		p.EClassifiers.filter(typeof(EClass)).filter([e|m.getTerminal(e as EClass,false) !== null]).size >0; 
	}	
		
	def  packageToPath(String s){
		if(s !== null){
			return s.replaceAll("\\.", "/")
		} else{
			return ""
		}
	}	
	
	def Iterable<EPackage> getAllRootPackages(Mapping mapping){
		var packages = mapping.metamodelPackages + mapping.allRootPackagesInExternalMappings
		for (exMap : mapping.externalMappings) {
			packages = packages + exMap.allRootPackages
		}
		packages = packages.toList.intersectName
		return packages
	}
	
	/**
	 * Returns root packages introduced by this mapping.
	 * i.e., getAllRootPackages - getNewRootPacakges
	 * 
	 */
	def Iterable<EPackage> getNewRootPackages(Mapping mapping) {
		var root = mapping.metamodelPackages
		var exRoot = mapping.allRootPackagesInExternalMappings
		
//		System::err.println("root: " + root)
//		System::err.println("exRoot: " + exRoot)
//		System::err.println("exMaps: " + mapping.externalMappings)
		
		var rmList = new ArrayList<EPackage>
		for (rp : root) {
			if (exRoot.exists([p|p.name == rp.name])) {
				rmList.add(rp)
			}
		}
		
		root.removeAll(rmList)
		
		return root
	}
	
	def Collection<? extends EPackage> getAllRootPackagesInExternalMappings(Mapping mapping) {
		var packages = new ArrayList<EPackage>
		
		for (exMap : mapping.externalMappings) {
			packages.addAll(exMap.metamodelPackages)
			packages.addAll(exMap.allRootPackagesInExternalMappings)
		}
		
		return packages.intersectName
	}
	
	def allDefaultOperators(Mapping map) {
		var ops = new ArrayList<EClass>();
		for (p : map.newRootPackages) {
			ops.addAll(p.allDefaultOperators(map))
		}
		return ops;
	}
	
	def private Collection<? extends EClass> allDefaultOperators(EPackage p, Mapping m) {
		var ops = new ArrayList<EClass>();
		for (c : p.EClassifiers) {
			if (c instanceof EClass) {
				if ((c as EClass).isDefaultOperator(m)) {
					ops.add(c as EClass);
				}
			}
		}
		for (subp : p.ESubpackages) {
			ops.addAll(subp.allDefaultOperators(m));
		}
		return ops;
	}
	
	/**
	 * A class is a default operator if it has one Terminal in its inheritance
	 * top hierachy or if no terminals have been defined. A class operator with
	 * a setted feature parameter isn't a default operator.
	 * 
	 */
	def private boolean isDefaultOperator(EClass c, Mapping m) {
		for (Operator op : m.operators) {
			if (op instanceof ClassOperator) {
				val cop = op as ClassOperator;
				if (areSameClasses(cop.getClass_(), c)
						&& !isSettedOperator(cop))
					return false;
			}
		}
		return !c.isAbstract()
				&& (m.getTerminal(c, false) !== null || m.terminals.size == 0);
	}
	
	
	
	def private boolean areSameClasses(EClass c1, EClass c2) {
		val ePackage1 = c1.getEPackage();
		val ePackage2 = c2.getEPackage();
		if (ePackage1.getNsURI() == ePackage2.getNsURI()
				|| ePackage1.getNsURI().equals(ePackage2.getNsURI())) {
			return c1.getClassifierID() == c2.getClassifierID();
		}
		return false;

	}
	
	def private boolean isSettedOperator(ClassOperator op) {
		for (FeatureParameter fp : op.getParameters()) {
			if (fp instanceof SettedFeatureParameter) {
				return true;
			}
		}
		return false;
	}
		
	def boolean isPrimitive(String type){
		type.compareTo("int")==0 || type.compareTo("long")==0 || type.compareTo("float")==0|| type.compareTo("double")==0 || type.compareTo("boolean")==0 || type.compareTo("char")==0;
	}
	
	private static final List<String> _primitiveTypes = #["long", "int", "float", "double", "boolean",
												"String", "Long", "Int", "Float", "Double", "Boolean"];
	def isPrimitive2(String type) {
		tom.mapping.dsl.generator.TomMappingExtensions._primitiveTypes.contains(type)
	}
	
	def packageList(Mapping map) {
		var packageList = new ArrayList<EPackage>()
		for(elt : map.operators.filter(typeof(ClassOperator))) {
	    	packageList.add(elt.class_.EPackage)
		}
		for (module : map.modules) {
			for(elt : module.operators.filter(typeof(ClassOperator))) {
		    	packageList.add(elt.class_.EPackage)
			}
		}
		for(elt : map.allDefaultOperators) {
			packageList.add(elt.EPackage)
		}
		
		return packageList.intersectName()
	}
	
	def private List<EPackage> intersectName(List<EPackage> listBase) {
		var listDestination = new ArrayList<EPackage>()
		
		for(eltB : listBase) {
			var sameName = false;
			for(eltD : listDestination) {
				sameName = sameName || eltB.name.equals(eltD.name)
			} 
			if(!sameName) {
				listDestination.add(eltB)
			}
		}
		return listDestination;
	}
	
		

}