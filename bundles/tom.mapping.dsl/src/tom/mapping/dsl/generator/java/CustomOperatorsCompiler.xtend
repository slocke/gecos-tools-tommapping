package tom.mapping.dsl.generator.java

import org.eclipse.xtext.generator.IFileSystemAccess
import tom.mapping.dsl.generator.TomMappingExtensions
import model.Accessor
import model.Mapping
import model.Operator
import model.UserOperator
import com.google.inject.Inject
import tom.mapping.dsl.generator.tom.ParametersCompiler

class CustomOperatorsCompiler {
	
	extension TomMappingExtensions = new TomMappingExtensions()
	
	@Inject extension ParametersCompiler injpa
	
	String prefix = ""
	
	
	def compile(Mapping m, IFileSystemAccess fsa) {
		if(m.operators.filter(e | e instanceof UserOperator).size() > 0) {
			fsa.generateFile(prefix+"/"+m.getCustomOperatorsClass()+".java", m.main())
		}
	}
	
	def main(Mapping map) {
		'''	
		public class «map.getCustomOperatorsClass()» {
			«FOR op: map.operators»
				«map.operator(op)»
			«ENDFOR»
		}
		'''
	}
	

	def dispatch operator(Mapping map, Operator op){}
	
	def dispatch operator(Mapping map, UserOperator usop){
		for(a: usop.accessors) {
			map.accessor(usop, a) 
		}
		map.test(usop)
		map.make(usop)
	}
	
	
	def accessor(Mapping m, UserOperator op, Accessor acc) {
		'''
		public static «acc.slot.type.javaType(m)»«acc.getCustomOperatorSlotAccessorName()»(«op.type.javaType(m)» t) {
			return «acc.java»;
		}
		'''
	}


	def test(Mapping m, UserOperator usop) {
		'''
		public static boolean is«usop.name.toFirstUpper()»(«usop.type.javaType(m)» t) {
			return «usop.test»;
		}
		'''
	}


	def make(Mapping m, UserOperator usop) {
		'''
		public static «usop.type.javaType(m)» make «usop.name.toFirstUpper()»(«usop.accessors.join(", ", [acc|acc.slot.javaParameter(m)])») {
			return «usop.make»
		}
		'''
	}

}