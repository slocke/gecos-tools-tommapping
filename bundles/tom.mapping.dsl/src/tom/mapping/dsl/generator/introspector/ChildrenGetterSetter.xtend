// Licence
package tom.mapping.dsl.generator.introspector

import com.google.inject.Inject
import model.Mapping
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EClassifier
import org.eclipse.emf.ecore.EPackage
import tom.mapping.dsl.generator.TomMappingExtensions
import tom.mapping.dsl.generator.tom.ParametersCompiler

class ChildrenGetterSetter {
	
	extension TomMappingExtensions = new TomMappingExtensions()
	
	@Inject extension ParametersCompiler injpa
	
	// Getter
	
	def dispatch CharSequence getter(Mapping mapping, EPackage ep) {
		'''
		private static class «ep.getChildrenGetterName()» extends «mapping.getSwitchType(ep)»<Object[]> {
			public final static «ep.getChildrenGetterName()» INSTANCE = new «ep.getChildrenGetterName()»();
			
			private «ep.getChildrenGetterName()»(){}
			
			public Object[] children(Object i) {
				Object[] children = doSwitch((EObject) i);
				return children != null ? children : new Object[0];
			}
			«FOR c: ep.EClassifiers»
				«mapping.getter(c)»
			«ENDFOR»
		}
		'''
	}
	
	
	def dispatch getter(Mapping mapping, EClassifier ecf) {}
	
	
	def dispatch getter(Mapping mapping, EClass ec) {
		val parameters = ec.getDefaultParameters(mapping);
		'''
		«IF parameters.size() > 0»
			
			public Object[] case«ec.name.toFirstUpper()»(«ec.javaType(mapping)» o) {
				List<Object> l = new ArrayList<Object>();
				«FOR param: parameters»
					if (o.get«param.paramName.toFirstUpper()»() != null)
						l.add(o.get«param.paramName.toFirstUpper()»());
				«ENDFOR»
				
				/*PROTECTED REGION ID(getter_«ec.EPackage.name»_«ec.name») ENABLED START*/
				/*PROTECTED REGION END*/
				
				return l.toArray();
			}
		«ENDIF»
		'''	
	}
	
	// Setter
	
	def dispatch CharSequence setter(Mapping mapping, EPackage ep) {
		var genChildren = false
		for (c: ep.EClassifiers) {
			if (c instanceof EClass) {
				val parameters = (c as EClass).getDefaultParameters(mapping).filter([e|!e.many]).toList;
				if (parameters.size > 0)
					genChildren = true
			}
		}
		
		'''
		private static class «ep.getChildrenSetterName()» extends «mapping.getSwitchType(ep)»<Object> {
			public final static «ep.getChildrenSetterName()» INSTANCE = new «ep.getChildrenSetterName()»();
			
			«IF genChildren»
			private Object[] children;
			«ENDIF»
			private «ep.getChildrenSetterName()»(){}
			
			public Object set(Object i, Object[] children) {
				«IF genChildren»
				this.children = children;
				«ENDIF»
				return doSwitch((EObject) i);
			}
			«FOR c: ep.EClassifiers»
				«mapping.setter(c)»
			«ENDFOR»
		}
		'''
	}
	
	def dispatch setter(Mapping mapping, EClassifier ecf) {}
	
	
//
//		public Object caseReduceExpression(ReduceExpression o){
//			
//			o.setProjection((AffineFunction)children[0]);
//			
//			o.setExpr((Expression)children[1]);
//			
//			if (REMOVE_EMPTY_BRANCH_AT_SETTER) {
//				o.setExpr(Normalize.removeEmptyBranch(o.getExpr()));
//			}
//			
//			return o;
//		}
	
	def dispatch setter(Mapping mapping, EClass ec) {
		val parameters = ec.getDefaultParameters(mapping).filter([e|!e.many]).toList;
		
		'''
		«IF parameters.size() > 0»

			public Object case«ec.name.toFirstUpper()»(«ec.javaType(mapping)» o) {
				«FOR p : parameters»
					o.set«p.paramName.toFirstUpper()»((«p.EReferenceType.javaType(mapping)»)children[«parameters.indexOf(p)»]);
				«ENDFOR»

				/*PROTECTED REGION ID(setter_«ec.EPackage.name»_«ec.name») ENABLED START*/
				/*PROTECTED REGION END*/

				return o;
			}
		«ENDIF»
		'''
	}
	

}