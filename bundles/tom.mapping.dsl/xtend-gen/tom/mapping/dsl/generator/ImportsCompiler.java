package tom.mapping.dsl.generator;

import model.Mapping;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Extension;
import tom.mapping.dsl.generator.TomMappingExtensions;

@SuppressWarnings("all")
public class ImportsCompiler {
  @Extension
  private TomMappingExtensions _tomMappingExtensions = new TomMappingExtensions();
  
  public CharSequence imports(final Mapping map) {
    StringConcatenation _builder = new StringConcatenation();
    {
      Iterable<EPackage> _allRootPackages = this._tomMappingExtensions.getAllRootPackages(map);
      for(final EPackage p : _allRootPackages) {
        CharSequence _imports = this.imports(map, p);
        _builder.append(_imports);
        _builder.newLineIfNotEmpty();
      }
    }
    return _builder;
  }
  
  public CharSequence importsWithUtils(final Mapping map) {
    StringConcatenation _builder = new StringConcatenation();
    {
      Iterable<EPackage> _allRootPackages = this._tomMappingExtensions.getAllRootPackages(map);
      for(final EPackage p : _allRootPackages) {
        CharSequence _importsWithUtils = this.importsWithUtils(map, p);
        _builder.append(_importsWithUtils);
        _builder.newLineIfNotEmpty();
      }
    }
    return _builder;
  }
  
  public CharSequence imports(final Mapping mapping, final EPackage ep) {
    CharSequence _xifexpression = null;
    boolean _isUseFullyQualifiedTypeName = mapping.isUseFullyQualifiedTypeName();
    if (_isUseFullyQualifiedTypeName) {
      StringConcatenation _builder = new StringConcatenation();
      _xifexpression = _builder;
    } else {
      StringConcatenation _builder_1 = new StringConcatenation();
      {
        int _size = ep.getEClassifiers().size();
        boolean _greaterThan = (_size > 0);
        if (_greaterThan) {
          _builder_1.append("import ");
          String _packagePrefix = this._tomMappingExtensions.getPackagePrefix(mapping, ep);
          _builder_1.append(_packagePrefix);
          String _name = ep.getName();
          _builder_1.append(_name);
          _builder_1.append(".*;");
          _builder_1.newLineIfNotEmpty();
        }
      }
      {
        EList<EPackage> _eSubpackages = ep.getESubpackages();
        for(final EPackage p : _eSubpackages) {
          CharSequence _imports = this.imports(mapping, p);
          _builder_1.append(_imports);
          _builder_1.newLineIfNotEmpty();
        }
      }
      _xifexpression = _builder_1;
    }
    return _xifexpression;
  }
  
  public CharSequence importsWithUtils(final Mapping mapping, final EPackage ep) {
    CharSequence _xifexpression = null;
    boolean _isUseFullyQualifiedTypeName = mapping.isUseFullyQualifiedTypeName();
    if (_isUseFullyQualifiedTypeName) {
      StringConcatenation _builder = new StringConcatenation();
      _xifexpression = _builder;
    } else {
      StringConcatenation _builder_1 = new StringConcatenation();
      {
        int _size = ep.getEClassifiers().size();
        boolean _greaterThan = (_size > 0);
        if (_greaterThan) {
          _builder_1.append("import ");
          String _packagePrefix = this._tomMappingExtensions.getPackagePrefix(mapping, ep);
          _builder_1.append(_packagePrefix);
          String _name = ep.getName();
          _builder_1.append(_name);
          _builder_1.append(".*; ");
          _builder_1.newLineIfNotEmpty();
          _builder_1.append("import ");
          String _packagePrefix_1 = this._tomMappingExtensions.getPackagePrefix(mapping, ep);
          _builder_1.append(_packagePrefix_1);
          String _name_1 = ep.getName();
          _builder_1.append(_name_1);
          _builder_1.append(".util.*;");
          _builder_1.newLineIfNotEmpty();
        }
      }
      {
        EList<EPackage> _eSubpackages = ep.getESubpackages();
        for(final EPackage p : _eSubpackages) {
          CharSequence _importsWithUtils = this.importsWithUtils(mapping, p);
          _builder_1.append(_importsWithUtils);
          _builder_1.newLineIfNotEmpty();
        }
      }
      _xifexpression = _builder_1;
    }
    return _xifexpression;
  }
}
