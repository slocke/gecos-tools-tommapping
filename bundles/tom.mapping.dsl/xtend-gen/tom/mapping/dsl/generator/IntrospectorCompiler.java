package tom.mapping.dsl.generator;

import com.google.common.base.Objects;
import com.google.inject.Inject;
import java.util.List;
import model.InstanceMapping;
import model.Mapping;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.generator.IFileSystemAccess;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.StringExtensions;
import tom.mapping.dsl.generator.ImportsCompiler;
import tom.mapping.dsl.generator.TomMappingExtensions;
import tom.mapping.dsl.generator.introspector.ChildrenGetterSetter;

@SuppressWarnings("all")
public class IntrospectorCompiler {
  @Extension
  private TomMappingExtensions _tomMappingExtensions = new TomMappingExtensions();
  
  @Inject
  @Extension
  private ImportsCompiler injim;
  
  @Inject
  @Extension
  private ChildrenGetterSetter injchi;
  
  public void compile(final Mapping map, final IFileSystemAccess fsa) {
    Iterable<EPackage> _allRootPackages = this._tomMappingExtensions.getAllRootPackages(map);
    for (final EPackage rootp : _allRootPackages) {
      {
        int _size = rootp.getEClassifiers().size();
        boolean _greaterThan = (_size > 0);
        if (_greaterThan) {
          fsa.generateFile(this.introspectorPath(rootp, map), this.main(rootp, map));
        }
        List<EPackage> _allSubPackages = this._tomMappingExtensions.getAllSubPackages(rootp);
        for (final EPackage subp : _allSubPackages) {
          int _size_1 = subp.getEClassifiers().size();
          boolean _greaterThan_1 = (_size_1 > 0);
          if (_greaterThan_1) {
            fsa.generateFile(this.introspectorPath(subp, map), this.main(subp, map));
          }
        }
      }
    }
  }
  
  private CharSequence main(final EPackage p, final Mapping map) {
    return this.introspector(p, map);
  }
  
  protected CharSequence introspectorName(final EPackage p, final Mapping map) {
    StringConcatenation _builder = new StringConcatenation();
    String _firstUpper = StringExtensions.toFirstUpper(p.getName());
    _builder.append(_firstUpper);
    _builder.append("Introspector");
    return _builder;
  }
  
  protected String introspectorPath(final EPackage p, final Mapping map) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("tom/mapping/introspectors/");
    String _replaceAll = this.ePackagePath(p).replaceAll("\\.", "/");
    _builder.append(_replaceAll);
    _builder.append("/");
    CharSequence _introspectorName = this.introspectorName(p, map);
    _builder.append(_introspectorName);
    _builder.append(".java");
    return _builder.toString();
  }
  
  protected String introspectorPackage(final EPackage p, final Mapping map) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("tom.mapping.introspectors.");
    String _ePackagePath = this.ePackagePath(p);
    _builder.append(_ePackagePath);
    return _builder.toString();
  }
  
  protected String ePackagePath(final EPackage epackage) {
    if (((!Objects.equal(epackage.getESuperPackage(), null)) && (epackage.getESuperPackage().getEClassifiers().size() > 0))) {
      String _ePackagePath = this.ePackagePath(epackage.getESuperPackage());
      String _plus = (_ePackagePath + ".");
      String _name = epackage.getName();
      return (_plus + _name);
    }
    return epackage.getName();
  }
  
  protected CharSequence introspector(final EPackage p, final Mapping map) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("package ");
    String _introspectorPackage = this.introspectorPackage(p, map);
    _builder.append(_introspectorPackage);
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("import java.util.ArrayList;");
    _builder.newLine();
    _builder.append("import java.util.HashSet;");
    _builder.newLine();
    _builder.append("import java.util.List;");
    _builder.newLine();
    _builder.append("import java.util.Set;");
    _builder.newLine();
    _builder.newLine();
    _builder.append("import org.eclipse.emf.common.util.EList;");
    _builder.newLine();
    _builder.append("import org.eclipse.emf.ecore.EObject;");
    _builder.newLine();
    _builder.append("import org.eclipse.emf.ecore.EStructuralFeature;");
    _builder.newLine();
    _builder.append("import org.eclipse.emf.ecore.EPackage;");
    _builder.newLine();
    _builder.append("import org.eclipse.emf.ecore.EClass;");
    _builder.newLine();
    _builder.newLine();
    _builder.append("import tom.library.sl.Introspector;");
    _builder.newLine();
    _builder.append("import tom.mapping.IntrospectorManager;");
    _builder.newLine();
    _builder.newLine();
    _builder.append("import ");
    String _packagePrefix = this._tomMappingExtensions.getPackagePrefix(map, p);
    _builder.append(_packagePrefix);
    String _name = p.getName();
    _builder.append(_name);
    _builder.append(".");
    String _computeName = this.computeName(map, p);
    _builder.append(_computeName);
    _builder.append("Package;");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    CharSequence _importsWithUtils = this.injim.importsWithUtils(map);
    _builder.append(_importsWithUtils);
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("/* PROTECTED REGION ID(introspector_imports) ENABLED START */");
    _builder.newLine();
    _builder.append("// protected imports");
    _builder.newLine();
    _builder.append("/* PROTECTED REGION END */");
    _builder.newLine();
    _builder.newLine();
    _builder.append("/**");
    _builder.newLine();
    _builder.append("* TOM introspector for ");
    String _name_1 = map.getName();
    _builder.append(_name_1);
    _builder.append(".");
    _builder.newLineIfNotEmpty();
    _builder.append("* -- Autogenerated by TOM mapping EMF genrator --");
    _builder.newLine();
    _builder.append("*/");
    _builder.newLine();
    _builder.newLine();
    _builder.append("public class ");
    CharSequence _introspectorName = this.introspectorName(p, map);
    _builder.append(_introspectorName);
    _builder.append(" implements Introspector {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("public static final ");
    CharSequence _introspectorName_1 = this.introspectorName(p, map);
    _builder.append(_introspectorName_1, "\t");
    _builder.append(" INSTANCE = new ");
    CharSequence _introspectorName_2 = this.introspectorName(p, map);
    _builder.append(_introspectorName_2, "\t");
    _builder.append("();");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("static {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("IntrospectorManager.INSTANCE.register(");
    String _computeName_1 = this.computeName(map, p);
    _builder.append(_computeName_1, "\t\t");
    _builder.append("Package.eINSTANCE, INSTANCE);");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("/* PROTECTED REGION ID(introspector_members) ENABLED START */");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("/* PROTECTED REGION END */");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("protected ");
    CharSequence _introspectorName_3 = this.introspectorName(p, map);
    _builder.append(_introspectorName_3, "\t");
    _builder.append("() {}");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    CharSequence _childAt = this.getChildAt(map);
    _builder.append(_childAt, "\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    CharSequence _childCount = this.getChildCount(map);
    _builder.append(_childCount, "\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    CharSequence _children = this.getChildren(p, map);
    _builder.append(_children, "\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    CharSequence _setChildren = this.setChildren(p, map);
    _builder.append(_setChildren, "\t");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("\t");
    CharSequence _setChildAt = this.setChildAt(map);
    _builder.append(_setChildAt, "\t");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  protected CharSequence getChildAt(final Mapping map) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("public Object getChildAt(Object o, int i) {");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("return getChildren(o)[i];");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  protected CharSequence getChildCount(final Mapping map) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("public int getChildCount(Object o) {");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("return getChildren(o).length;");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  protected CharSequence setChildAt(final Mapping map) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("public <T> T setChildAt(T o, int i, Object obj) {");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("throw new RuntimeException(\"Not implemented\");");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  protected CharSequence getChildren(final EPackage p, final Mapping map) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("@SuppressWarnings(\"unchecked\")");
    _builder.newLine();
    _builder.append("public Object[] getChildren(Object arg0) {");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("List<Object> l = new ArrayList<Object>();");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("if (arg0 instanceof List) {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("// Children of a list are its content");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("for(Object object : (List<Object>) arg0) {");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("l.add(object);");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("return l.toArray();");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("return ");
    String _childrenGetterName = this._tomMappingExtensions.getChildrenGetterName(p);
    _builder.append(_childrenGetterName, "\t");
    _builder.append(".INSTANCE.children(arg0);");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    _builder.newLine();
    CharSequence _ter = this.injchi.getter(map, p);
    _builder.append(_ter);
    _builder.newLineIfNotEmpty();
    return _builder;
  }
  
  protected CharSequence setChildren(final EPackage p, final Mapping map) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("@SuppressWarnings(\"unchecked\")");
    _builder.newLine();
    _builder.append("public <T> T setChildren(T arg0, Object[] arg1) {");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("if (arg0 instanceof List) {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("// If object is a list then content of the original list has to be replaced");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("List<Object> list = (List<Object>) arg0;");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("list.clear();");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("for (int i = 0; i < arg1.length; i++) {");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("list.add(arg1[i]);");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("return arg0;");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("} else {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("return (T) ");
    String _childrenSetterName = this._tomMappingExtensions.getChildrenSetterName(p);
    _builder.append(_childrenSetterName, "\t\t");
    _builder.append(".INSTANCE.set(arg0, arg1);");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    _builder.newLine();
    CharSequence _setter = this.injchi.setter(map, p);
    _builder.append(_setter);
    _builder.newLineIfNotEmpty();
    return _builder;
  }
  
  private String computeName(final Mapping m, final EPackage p) {
    final InstanceMapping instance = this._tomMappingExtensions.findMappedInstance(m, p);
    String res = StringExtensions.toFirstUpper(p.getName());
    if (((!Objects.equal(instance, null)) && (!Objects.equal(instance.getPackageFactoryName(), null)))) {
      res = instance.getPackageFactoryName();
    }
    return res;
  }
}
