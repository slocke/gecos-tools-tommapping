package tom.mapping.dsl.generator.introspector;

import com.google.inject.Inject;
import java.util.Arrays;
import java.util.List;
import model.Mapping;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.StringExtensions;
import tom.mapping.dsl.generator.TomMappingExtensions;
import tom.mapping.dsl.generator.tom.ParametersCompiler;

@SuppressWarnings("all")
public class ChildrenGetterSetter {
  @Extension
  private TomMappingExtensions _tomMappingExtensions = new TomMappingExtensions();
  
  @Inject
  @Extension
  private ParametersCompiler injpa;
  
  protected CharSequence _getter(final Mapping mapping, final EPackage ep) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("private static class ");
    String _childrenGetterName = this._tomMappingExtensions.getChildrenGetterName(ep);
    _builder.append(_childrenGetterName);
    _builder.append(" extends ");
    CharSequence _switchType = this.injpa.getSwitchType(mapping, ep);
    _builder.append(_switchType);
    _builder.append("<Object[]> {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("public final static ");
    String _childrenGetterName_1 = this._tomMappingExtensions.getChildrenGetterName(ep);
    _builder.append(_childrenGetterName_1, "\t");
    _builder.append(" INSTANCE = new ");
    String _childrenGetterName_2 = this._tomMappingExtensions.getChildrenGetterName(ep);
    _builder.append(_childrenGetterName_2, "\t");
    _builder.append("();");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("private ");
    String _childrenGetterName_3 = this._tomMappingExtensions.getChildrenGetterName(ep);
    _builder.append(_childrenGetterName_3, "\t");
    _builder.append("(){}");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("public Object[] children(Object i) {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("Object[] children = doSwitch((EObject) i);");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("return children != null ? children : new Object[0];");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    {
      EList<EClassifier> _eClassifiers = ep.getEClassifiers();
      for(final EClassifier c : _eClassifiers) {
        _builder.append("\t");
        CharSequence _ter = this.getter(mapping, c);
        _builder.append(_ter, "\t");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  protected CharSequence _getter(final Mapping mapping, final EClassifier ecf) {
    return null;
  }
  
  protected CharSequence _getter(final Mapping mapping, final EClass ec) {
    CharSequence _xblockexpression = null;
    {
      final List<EReference> parameters = this._tomMappingExtensions.getDefaultParameters(ec, mapping);
      StringConcatenation _builder = new StringConcatenation();
      {
        int _size = parameters.size();
        boolean _greaterThan = (_size > 0);
        if (_greaterThan) {
          _builder.newLine();
          _builder.append("public Object[] case");
          String _firstUpper = StringExtensions.toFirstUpper(ec.getName());
          _builder.append(_firstUpper);
          _builder.append("(");
          String _javaType = this.injpa.javaType(ec, mapping);
          _builder.append(_javaType);
          _builder.append(" o) {");
          _builder.newLineIfNotEmpty();
          _builder.append("\t");
          _builder.append("List<Object> l = new ArrayList<Object>();");
          _builder.newLine();
          {
            for(final EReference param : parameters) {
              _builder.append("\t");
              _builder.append("if (o.get");
              String _firstUpper_1 = StringExtensions.toFirstUpper(this.injpa.paramName(param));
              _builder.append(_firstUpper_1, "\t");
              _builder.append("() != null)");
              _builder.newLineIfNotEmpty();
              _builder.append("\t");
              _builder.append("\t");
              _builder.append("l.add(o.get");
              String _firstUpper_2 = StringExtensions.toFirstUpper(this.injpa.paramName(param));
              _builder.append(_firstUpper_2, "\t\t");
              _builder.append("());");
              _builder.newLineIfNotEmpty();
            }
          }
          _builder.append("\t");
          _builder.newLine();
          _builder.append("\t");
          _builder.append("/*PROTECTED REGION ID(getter_");
          String _name = ec.getEPackage().getName();
          _builder.append(_name, "\t");
          _builder.append("_");
          String _name_1 = ec.getName();
          _builder.append(_name_1, "\t");
          _builder.append(") ENABLED START*/");
          _builder.newLineIfNotEmpty();
          _builder.append("\t");
          _builder.append("/*PROTECTED REGION END*/");
          _builder.newLine();
          _builder.append("\t");
          _builder.newLine();
          _builder.append("\t");
          _builder.append("return l.toArray();");
          _builder.newLine();
          _builder.append("}");
          _builder.newLine();
        }
      }
      _xblockexpression = _builder;
    }
    return _xblockexpression;
  }
  
  protected CharSequence _setter(final Mapping mapping, final EPackage ep) {
    CharSequence _xblockexpression = null;
    {
      boolean genChildren = false;
      EList<EClassifier> _eClassifiers = ep.getEClassifiers();
      for (final EClassifier c : _eClassifiers) {
        if ((c instanceof EClass)) {
          final Function1<EReference, Boolean> _function = new Function1<EReference, Boolean>() {
            @Override
            public Boolean apply(final EReference e) {
              boolean _isMany = e.isMany();
              return Boolean.valueOf((!_isMany));
            }
          };
          final List<EReference> parameters = IterableExtensions.<EReference>toList(IterableExtensions.<EReference>filter(this._tomMappingExtensions.getDefaultParameters(((EClass) c), mapping), _function));
          int _size = parameters.size();
          boolean _greaterThan = (_size > 0);
          if (_greaterThan) {
            genChildren = true;
          }
        }
      }
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("private static class ");
      String _childrenSetterName = this._tomMappingExtensions.getChildrenSetterName(ep);
      _builder.append(_childrenSetterName);
      _builder.append(" extends ");
      CharSequence _switchType = this.injpa.getSwitchType(mapping, ep);
      _builder.append(_switchType);
      _builder.append("<Object> {");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.append("public final static ");
      String _childrenSetterName_1 = this._tomMappingExtensions.getChildrenSetterName(ep);
      _builder.append(_childrenSetterName_1, "\t");
      _builder.append(" INSTANCE = new ");
      String _childrenSetterName_2 = this._tomMappingExtensions.getChildrenSetterName(ep);
      _builder.append(_childrenSetterName_2, "\t");
      _builder.append("();");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.newLine();
      {
        if (genChildren) {
          _builder.append("\t");
          _builder.append("private Object[] children;");
          _builder.newLine();
        }
      }
      _builder.append("\t");
      _builder.append("private ");
      String _childrenSetterName_3 = this._tomMappingExtensions.getChildrenSetterName(ep);
      _builder.append(_childrenSetterName_3, "\t");
      _builder.append("(){}");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("public Object set(Object i, Object[] children) {");
      _builder.newLine();
      {
        if (genChildren) {
          _builder.append("\t\t");
          _builder.append("this.children = children;");
          _builder.newLine();
        }
      }
      _builder.append("\t\t");
      _builder.append("return doSwitch((EObject) i);");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      {
        EList<EClassifier> _eClassifiers_1 = ep.getEClassifiers();
        for(final EClassifier c_1 : _eClassifiers_1) {
          _builder.append("\t");
          CharSequence _setter = this.setter(mapping, c_1);
          _builder.append(_setter, "\t");
          _builder.newLineIfNotEmpty();
        }
      }
      _builder.append("}");
      _builder.newLine();
      _xblockexpression = _builder;
    }
    return _xblockexpression;
  }
  
  protected CharSequence _setter(final Mapping mapping, final EClassifier ecf) {
    return null;
  }
  
  protected CharSequence _setter(final Mapping mapping, final EClass ec) {
    CharSequence _xblockexpression = null;
    {
      final Function1<EReference, Boolean> _function = new Function1<EReference, Boolean>() {
        @Override
        public Boolean apply(final EReference e) {
          boolean _isMany = e.isMany();
          return Boolean.valueOf((!_isMany));
        }
      };
      final List<EReference> parameters = IterableExtensions.<EReference>toList(IterableExtensions.<EReference>filter(this._tomMappingExtensions.getDefaultParameters(ec, mapping), _function));
      StringConcatenation _builder = new StringConcatenation();
      {
        int _size = parameters.size();
        boolean _greaterThan = (_size > 0);
        if (_greaterThan) {
          _builder.newLine();
          _builder.append("public Object case");
          String _firstUpper = StringExtensions.toFirstUpper(ec.getName());
          _builder.append(_firstUpper);
          _builder.append("(");
          String _javaType = this.injpa.javaType(ec, mapping);
          _builder.append(_javaType);
          _builder.append(" o) {");
          _builder.newLineIfNotEmpty();
          {
            for(final EReference p : parameters) {
              _builder.append("\t");
              _builder.append("o.set");
              String _firstUpper_1 = StringExtensions.toFirstUpper(this.injpa.paramName(p));
              _builder.append(_firstUpper_1, "\t");
              _builder.append("((");
              String _javaType_1 = this.injpa.javaType(p.getEReferenceType(), mapping);
              _builder.append(_javaType_1, "\t");
              _builder.append(")children[");
              int _indexOf = parameters.indexOf(p);
              _builder.append(_indexOf, "\t");
              _builder.append("]);");
              _builder.newLineIfNotEmpty();
            }
          }
          _builder.newLine();
          _builder.append("\t");
          _builder.append("/*PROTECTED REGION ID(setter_");
          String _name = ec.getEPackage().getName();
          _builder.append(_name, "\t");
          _builder.append("_");
          String _name_1 = ec.getName();
          _builder.append(_name_1, "\t");
          _builder.append(") ENABLED START*/");
          _builder.newLineIfNotEmpty();
          _builder.append("\t");
          _builder.append("/*PROTECTED REGION END*/");
          _builder.newLine();
          _builder.newLine();
          _builder.append("\t");
          _builder.append("return o;");
          _builder.newLine();
          _builder.append("}");
          _builder.newLine();
        }
      }
      _xblockexpression = _builder;
    }
    return _xblockexpression;
  }
  
  public CharSequence getter(final Mapping mapping, final ENamedElement ec) {
    if (ec instanceof EClass) {
      return _getter(mapping, (EClass)ec);
    } else if (ec instanceof EClassifier) {
      return _getter(mapping, (EClassifier)ec);
    } else if (ec instanceof EPackage) {
      return _getter(mapping, (EPackage)ec);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(mapping, ec).toString());
    }
  }
  
  public CharSequence setter(final Mapping mapping, final ENamedElement ec) {
    if (ec instanceof EClass) {
      return _setter(mapping, (EClass)ec);
    } else if (ec instanceof EClassifier) {
      return _setter(mapping, (EClassifier)ec);
    } else if (ec instanceof EPackage) {
      return _setter(mapping, (EPackage)ec);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(mapping, ec).toString());
    }
  }
}
