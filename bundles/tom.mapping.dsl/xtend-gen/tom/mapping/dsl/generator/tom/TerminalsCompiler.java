package tom.mapping.dsl.generator.tom;

import com.google.common.collect.Iterators;
import com.google.inject.Inject;
import java.util.Arrays;
import java.util.List;
import model.Mapping;
import model.Terminal;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.IteratorExtensions;
import tom.mapping.dsl.generator.TomMappingExtensions;
import tom.mapping.dsl.generator.tom.ParametersCompiler;

@SuppressWarnings("all")
public class TerminalsCompiler {
  @Extension
  private TomMappingExtensions _tomMappingExtensions = new TomMappingExtensions();
  
  @Inject
  @Extension
  private ParametersCompiler injpa;
  
  public CharSequence terminal(final Mapping m, final Terminal t) {
    CharSequence _xifexpression = null;
    boolean _isMany = t.isMany();
    if (_isMany) {
      _xifexpression = this.listTerminal(m, t);
    } else {
      StringConcatenation _builder = new StringConcatenation();
      _builder.newLine();
      _builder.append("%typeterm ");
      CharSequence _mType = this.injpa.tomType(t, m);
      _builder.append(_mType);
      _builder.append(" {");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.append("/*PROTECTED REGION ID(tom_terminal_");
      String _name = t.getName();
      _builder.append(_name, "\t");
      _builder.append(") DISABLED START*/");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.append("//make sure to change to ENABLED when using protected region");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("implement \t\t{");
      CharSequence _javaType = this.injpa.javaType(t, m);
      _builder.append(_javaType, "\t");
      _builder.append("}");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.append("is_sort(t) \t\t{$t instanceof ");
      CharSequence _javaType_1 = this.injpa.javaType(t, m);
      _builder.append(_javaType_1, "\t");
      _builder.append("}");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.append("equals(l1,l2) \t{($l1!=null && $l1.equals($l2)) || $l1==$l2}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("/*PROTECTED REGION END*/");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      _xifexpression = _builder;
    }
    return _xifexpression;
  }
  
  public CharSequence listTerminal(final Mapping m, final Terminal t) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.newLine();
    _builder.append("%typeterm ");
    CharSequence _mType = this.injpa.tomType(t, m);
    _builder.append(_mType);
    _builder.append("{");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("/*PROTECTED REGION ID(tom_terminal_");
    String _name = t.getName();
    _builder.append(_name, "\t");
    _builder.append(") DISABLED START*/");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("//make sure to change to ENABLED when using protected region");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("implement { EList<");
    String _javaType = this.injpa.javaType(t.getClass_(), m);
    _builder.append(_javaType, "\t");
    _builder.append("> }");
    _builder.newLineIfNotEmpty();
    _builder.append("    ");
    _builder.append("is_sort(t) ");
    CharSequence _listTest = this.listTest(m, t);
    _builder.append(_listTest, "    ");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("equals(l1,l2) \t{($l1!=null && $l1.equals($l2)) || $l1==$l2}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("/*PROTECTED REGION END*/");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    _builder.newLine();
    _builder.append("%oparray ");
    CharSequence _mType_1 = this.injpa.tomType(t, m);
    _builder.append(_mType_1);
    _builder.append(" ");
    CharSequence _mType_2 = this.injpa.tomType(t, m);
    _builder.append(_mType_2);
    _builder.append("(");
    CharSequence _mType_3 = this.injpa.tomType(m.getTerminal(t.getClass_(), false), m);
    _builder.append(_mType_3);
    _builder.append("*) {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("/*PROTECTED REGION ID(tom_terminal_");
    String _name_1 = t.getName();
    _builder.append(_name_1, "\t");
    _builder.append("_list) DISABLED START*/");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("//make sure to change to ENABLED when using protected region");
    _builder.newLine();
    _builder.append(" \t");
    _builder.append("is_fsym(t) ");
    CharSequence _listTest_1 = this.listTest(m, t);
    _builder.append(_listTest_1, " \t");
    _builder.newLineIfNotEmpty();
    _builder.append("    ");
    _builder.append("make_empty(n) { new BasicEList<");
    String _javaType_1 = this.injpa.javaType(t.getClass_(), m);
    _builder.append(_javaType_1, "    ");
    _builder.append(">($n) }");
    _builder.newLineIfNotEmpty();
    _builder.append("    ");
    _builder.append("make_append(e,l) { append($e,$l) }");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("get_element(l,n) { $l.get($n) }");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("get_size(l)      { $l.size() }");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("/*PROTECTED REGION END*/");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  public CharSequence listTest(final Mapping m, final Terminal t) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("{ $t instanceof EList<?> &&");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("(((EList<");
    String _javaType = this.injpa.javaType(t.getClass_(), m);
    _builder.append(_javaType, "\t");
    _builder.append(">)$t).size() == 0 ");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("|| (((EList<");
    String _javaType_1 = this.injpa.javaType(t.getClass_(), m);
    _builder.append(_javaType_1, "\t");
    _builder.append(">)$t).size()>0 && ((EList<");
    String _javaType_2 = this.injpa.javaType(t.getClass_(), m);
    _builder.append(_javaType_2, "\t");
    _builder.append(">)$t).get(0) instanceof ");
    String _javaType_3 = this.injpa.javaType(t.getClass_(), m);
    _builder.append(_javaType_3, "\t");
    _builder.append("))}");
    _builder.newLineIfNotEmpty();
    return _builder;
  }
  
  protected CharSequence _primitiveTerminal(final Mapping mapping, final EPackage c) {
    StringConcatenation _builder = new StringConcatenation();
    {
      List<EClassifier> _list = IteratorExtensions.<EClassifier>toList(Iterators.<EClassifier>filter(c.eAllContents(), EClassifier.class));
      for(final EClassifier classifier : _list) {
        CharSequence _primitiveTerminal = this.primitiveTerminal(mapping, classifier);
        _builder.append(_primitiveTerminal);
        _builder.newLineIfNotEmpty();
      }
    }
    return _builder;
  }
  
  protected CharSequence _primitiveTerminal(final Mapping m, final EClassifier c) {
    StringConcatenation _builder = new StringConcatenation();
    return _builder;
  }
  
  protected CharSequence _primitiveTerminal(final Mapping m, final EEnum c) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.newLine();
    _builder.append("%typeterm ");
    String _mType = this.injpa.tomType(c, m);
    _builder.append(_mType);
    _builder.append(" {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("/*PROTECTED REGION ID(tom_terminal_");
    String _name = c.getName();
    _builder.append(_name, "\t");
    _builder.append(") DISABLED START*/");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("//make sure to change to ENABLED when using protected region");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("implement \t\t{");
    String _javaType = this.injpa.javaType(c, m);
    _builder.append(_javaType, "\t");
    _builder.append("}");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("is_sort(t) \t\t{$t instanceof ");
    String _javaType_1 = this.injpa.javaType(c, m);
    _builder.append(_javaType_1, "\t");
    _builder.append("}");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("equals(l1,l2) \t{$l1==$l2}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("/*PROTECTED REGION END*/");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  protected CharSequence _primitiveTerminal(final Mapping m, final EDataType c) {
    StringConcatenation _builder = new StringConcatenation();
    final boolean primitive = this._tomMappingExtensions.isPrimitive(c.getInstanceTypeName());
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("%typeterm ");
    String _mType = this.injpa.tomType(c, m);
    _builder.append(_mType);
    _builder.append(" {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("/*PROTECTED REGION ID(tom_terminal_");
    String _name = c.getName();
    _builder.append(_name, "\t");
    _builder.append(") DISABLED START*/");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("//make sure to change to ENABLED when using protected region");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("implement \t\t{");
    String _javaType = this.injpa.javaType(c, m);
    _builder.append(_javaType, "\t");
    _builder.append("}");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("is_sort(t) \t\t{");
    {
      if (primitive) {
        _builder.append("true");
      } else {
        _builder.append("$t instanceof ");
        String _javaType_1 = this.injpa.javaType(c, m);
        _builder.append(_javaType_1, "\t");
      }
    }
    _builder.append("}");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("equals(l1,l2) \t{");
    {
      if (primitive) {
        _builder.append("$l1==$l2");
      } else {
        _builder.append("$l1.equals($l2)");
      }
    }
    _builder.append("}");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("/*PROTECTED REGION END*/");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  public CharSequence primitiveTerminal(final Mapping m, final ENamedElement c) {
    if (c instanceof EEnum) {
      return _primitiveTerminal(m, (EEnum)c);
    } else if (c instanceof EDataType) {
      return _primitiveTerminal(m, (EDataType)c);
    } else if (c instanceof EClassifier) {
      return _primitiveTerminal(m, (EClassifier)c);
    } else if (c instanceof EPackage) {
      return _primitiveTerminal(m, (EPackage)c);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(m, c).toString());
    }
  }
}
