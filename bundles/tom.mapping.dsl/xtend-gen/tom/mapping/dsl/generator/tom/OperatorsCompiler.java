package tom.mapping.dsl.generator.tom;

import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import com.google.inject.Inject;
import java.util.Arrays;
import java.util.List;
import model.BooleanSettedValue;
import model.ClassOperator;
import model.EnumLiteralValue;
import model.FeatureParameter;
import model.IntSettedValue;
import model.JavaCodeValue;
import model.Mapping;
import model.Operator;
import model.SettedFeatureParameter;
import model.SettedValue;
import model.Terminal;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.StringExtensions;
import tom.mapping.dsl.generator.TomMappingExtensions;
import tom.mapping.dsl.generator.tom.ParametersCompiler;

@SuppressWarnings("all")
public class OperatorsCompiler {
  @Extension
  private TomMappingExtensions _tomMappingExtensions = new TomMappingExtensions();
  
  @Inject
  @Extension
  private ParametersCompiler injpa;
  
  protected CharSequence _operator(final Mapping mapping, final Operator op) {
    StringConcatenation _builder = new StringConcatenation();
    return _builder;
  }
  
  protected CharSequence _operator(final Mapping mapping, final ClassOperator clop) {
    CharSequence _xifexpression = null;
    int _size = clop.getParameters().size();
    boolean _greaterThan = (_size > 0);
    if (_greaterThan) {
      _xifexpression = this.classOperatorWithParameters(mapping, clop);
    } else {
      _xifexpression = this.classOperator(mapping, clop.getName(), clop.getClass_());
    }
    return _xifexpression;
  }
  
  public CharSequence classOperator(final Mapping m, final String op, final EClass ecl) {
    CharSequence _xblockexpression = null;
    {
      final List<EReference> parameters = this._tomMappingExtensions.getDefaultParameters(ecl, m);
      StringConcatenation _builder = new StringConcatenation();
      {
        Terminal _terminal = m.getTerminal(ecl, false);
        boolean _notEquals = (!Objects.equal(_terminal, null));
        if (_notEquals) {
          _builder.newLine();
          _builder.append("%op ");
          String _name = m.getTerminal(ecl, false).getName();
          _builder.append(_name);
          _builder.append(" ");
          _builder.append(op);
          _builder.append("(");
          CharSequence _classAttributes = this.classAttributes(m, ecl);
          _builder.append(_classAttributes);
          final Function1<EReference, CharSequence> _function = new Function1<EReference, CharSequence>() {
            @Override
            public CharSequence apply(final EReference p) {
              return OperatorsCompiler.this.injpa.tomParameter(p, m);
            }
          };
          String _join = IterableExtensions.<EReference>join(parameters, ", ", _function);
          _builder.append(_join);
          _builder.append(") {");
          _builder.newLineIfNotEmpty();
          _builder.append("\t");
          _builder.append("/*PROTECTED REGION ID(tom_operator_");
          _builder.append(op, "\t");
          _builder.append(") DISABLED START*/");
          _builder.newLineIfNotEmpty();
          _builder.append("\t");
          _builder.append("//make sure to change to ENABLED when using protected region");
          _builder.newLine();
          _builder.append("\t");
          _builder.append("is_fsym(t) {$t instanceof ");
          String _javaType = this.injpa.javaType(ecl, m);
          _builder.append(_javaType, "\t");
          _builder.append("}");
          _builder.newLineIfNotEmpty();
          {
            EList<EAttribute> _eAllAttributes = ecl.getEAllAttributes();
            for(final EAttribute attribute : _eAllAttributes) {
              _builder.append("\t");
              _builder.append("get_slot(");
              String _paramName = this.injpa.paramName(attribute);
              _builder.append(_paramName, "\t");
              _builder.append(",t) {((");
              String _javaType_1 = this.injpa.javaType(ecl, m);
              _builder.append(_javaType_1, "\t");
              _builder.append(")$t).");
              CharSequence _getterPrefix = this.getGetterPrefix(attribute);
              String _firstUpper = StringExtensions.toFirstUpper(this.injpa.paramName(attribute));
              String _plus = (_getterPrefix + _firstUpper);
              _builder.append(_plus, "\t");
              _builder.append("()}");
              _builder.newLineIfNotEmpty();
            }
          }
          {
            for(final EReference p : parameters) {
              _builder.append("\t");
              _builder.append("get_slot(");
              String _paramName_1 = this.injpa.paramName(p);
              _builder.append(_paramName_1, "\t");
              _builder.append(",t) {");
              CharSequence _ter = this.getter(m, ecl, p);
              _builder.append(_ter, "\t");
              _builder.append("}");
              _builder.newLineIfNotEmpty();
            }
          }
          _builder.append("\t");
          _builder.append("make(");
          EList<EAttribute> _eAllAttributes_1 = ecl.getEAllAttributes();
          final Function1<EStructuralFeature, CharSequence> _function_1 = new Function1<EStructuralFeature, CharSequence>() {
            @Override
            public CharSequence apply(final EStructuralFeature e) {
              return OperatorsCompiler.this.injpa.paramName(e);
            }
          };
          String _join_1 = IterableExtensions.<EStructuralFeature>join(Iterables.<EStructuralFeature>concat(_eAllAttributes_1, parameters), ", ", _function_1);
          _builder.append(_join_1, "\t");
          _builder.append("){");
          String _mFactoryQualifiedName = this._tomMappingExtensions.tomFactoryQualifiedName(m);
          _builder.append(_mFactoryQualifiedName, "\t");
          _builder.append(".");
          String _firstUpper_1 = StringExtensions.toFirstUpper(op);
          _builder.append(_firstUpper_1, "\t");
          _builder.append("(");
          EList<EAttribute> _eAllAttributes_2 = ecl.getEAllAttributes();
          final Function1<EStructuralFeature, CharSequence> _function_2 = new Function1<EStructuralFeature, CharSequence>() {
            @Override
            public CharSequence apply(final EStructuralFeature e) {
              String _paramName = OperatorsCompiler.this.injpa.paramName(e);
              return ("$" + _paramName);
            }
          };
          String _join_2 = IterableExtensions.<EStructuralFeature>join(Iterables.<EStructuralFeature>concat(_eAllAttributes_2, parameters), ", ", _function_2);
          _builder.append(_join_2, "\t");
          _builder.append(")}");
          _builder.newLineIfNotEmpty();
          _builder.append("\t");
          _builder.append("/*PROTECTED REGION END*/");
          _builder.newLine();
          _builder.append("}");
          _builder.newLine();
        }
      }
      _xblockexpression = _builder;
    }
    return _xblockexpression;
  }
  
  public CharSequence getGetterPrefix(final EAttribute a) {
    CharSequence _xifexpression = null;
    if (((Objects.equal(a, null) || Objects.equal(a.getEAttributeType(), null)) || Objects.equal(a.getEAttributeType().getInstanceClassName(), null))) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("get");
      _xifexpression = _builder;
    } else {
      CharSequence _xifexpression_1 = null;
      boolean _equals = a.getEAttributeType().getInstanceClassName().equals("boolean");
      if (_equals) {
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("is");
        _xifexpression_1 = _builder_1;
      } else {
        StringConcatenation _builder_2 = new StringConcatenation();
        _builder_2.append("get");
        _xifexpression_1 = _builder_2;
      }
      _xifexpression = _xifexpression_1;
    }
    return _xifexpression;
  }
  
  public CharSequence classOperatorWithParameters(final Mapping m, final ClassOperator clop) {
    CharSequence _xblockexpression = null;
    {
      final Iterable<FeatureParameter> parameters = this._tomMappingExtensions.getCustomParameters(clop);
      StringConcatenation _builder = new StringConcatenation();
      {
        Terminal _terminal = m.getTerminal(clop.getClass_(), false);
        boolean _notEquals = (!Objects.equal(_terminal, null));
        if (_notEquals) {
          _builder.newLine();
          _builder.append("%op ");
          CharSequence _mType = this.injpa.tomType(m.getTerminal(clop.getClass_(), false), m);
          _builder.append(_mType);
          _builder.append(" ");
          String _name = clop.getName();
          _builder.append(_name);
          _builder.append("(");
          final Function1<FeatureParameter, CharSequence> _function = new Function1<FeatureParameter, CharSequence>() {
            @Override
            public CharSequence apply(final FeatureParameter p) {
              return OperatorsCompiler.this.injpa.tomParameter(p, m);
            }
          };
          String _join = IterableExtensions.<FeatureParameter>join(parameters, ", ", _function);
          _builder.append(_join);
          _builder.append(") {");
          _builder.newLineIfNotEmpty();
          _builder.append("\t");
          _builder.append("/*PROTECTED REGION ID(tom_operator_with_param_");
          String _name_1 = clop.getName();
          _builder.append(_name_1, "\t");
          _builder.append(") DISABLED START*/");
          _builder.newLineIfNotEmpty();
          _builder.append("\t");
          _builder.append("//make sure to change to ENABLED when using protected region");
          _builder.newLine();
          _builder.append("\t");
          _builder.append("is_fsym(t) {$t instanceof ");
          String _javaType = this.injpa.javaType(clop.getClass_(), m);
          _builder.append(_javaType, "\t");
          final Function1<FeatureParameter, CharSequence> _function_1 = new Function1<FeatureParameter, CharSequence>() {
            @Override
            public CharSequence apply(final FeatureParameter p) {
              return OperatorsCompiler.this.settedParameterTest(m, clop.getClass_(), p);
            }
          };
          String _join_1 = IterableExtensions.<FeatureParameter>join(clop.getParameters(), "", _function_1);
          _builder.append(_join_1, "\t");
          _builder.append("}");
          _builder.newLineIfNotEmpty();
          {
            for(final FeatureParameter p : parameters) {
              _builder.append("\t");
              _builder.append("get_slot(");
              String _paramName = this.injpa.paramName(p.getFeature());
              _builder.append(_paramName, "\t");
              _builder.append(",t) {");
              CharSequence _ter = this.getter(m, clop.getClass_(), p.getFeature());
              _builder.append(_ter, "\t");
              _builder.append("}");
              _builder.newLineIfNotEmpty();
            }
          }
          _builder.append("\t");
          _builder.append("make(");
          final Function1<FeatureParameter, CharSequence> _function_2 = new Function1<FeatureParameter, CharSequence>() {
            @Override
            public CharSequence apply(final FeatureParameter p) {
              String _paramName = OperatorsCompiler.this.injpa.paramName(p.getFeature());
              return ("_" + _paramName);
            }
          };
          String _join_2 = IterableExtensions.<FeatureParameter>join(parameters, ",", _function_2);
          _builder.append(_join_2, "\t");
          _builder.append(") {");
          String _mFactoryQualifiedName = this._tomMappingExtensions.tomFactoryQualifiedName(m);
          _builder.append(_mFactoryQualifiedName, "\t");
          _builder.append(".create");
          String _firstUpper = StringExtensions.toFirstUpper(clop.getName());
          _builder.append(_firstUpper, "\t");
          _builder.append("(");
          final Function1<FeatureParameter, CharSequence> _function_3 = new Function1<FeatureParameter, CharSequence>() {
            @Override
            public CharSequence apply(final FeatureParameter p) {
              String _paramName = OperatorsCompiler.this.injpa.paramName(p.getFeature());
              return ("$_" + _paramName);
            }
          };
          String _join_3 = IterableExtensions.<FeatureParameter>join(parameters, ", ", _function_3);
          _builder.append(_join_3, "\t");
          _builder.append(")}");
          _builder.newLineIfNotEmpty();
          _builder.append("\t");
          _builder.append("/*PROTECTED REGION END*/");
          _builder.newLine();
          _builder.append("}");
          _builder.newLine();
        }
      }
      _xblockexpression = _builder;
    }
    return _xblockexpression;
  }
  
  public CharSequence getter(final Mapping m, final EClass c, final EStructuralFeature esf) {
    StringConcatenation _builder = new StringConcatenation();
    {
      boolean _isMany = esf.isMany();
      if (_isMany) {
        _builder.append("enforce(");
      }
    }
    _builder.append("((");
    String _javaType = this.injpa.javaType(c, m);
    _builder.append(_javaType);
    _builder.append(")$t).");
    CharSequence _getterPrefix = this.getGetterPrefix(esf, m);
    String _firstUpper = StringExtensions.toFirstUpper(this.injpa.paramName(esf));
    String _plus = (_getterPrefix + _firstUpper);
    _builder.append(_plus);
    _builder.append("()");
    {
      boolean _isMany_1 = esf.isMany();
      if (_isMany_1) {
        _builder.append(")");
      }
    }
    return _builder;
  }
  
  public CharSequence getGetterPrefix(final EStructuralFeature esf, final Mapping m) {
    CharSequence _xblockexpression = null;
    {
      final String str = this.injpa.javaType(esf, m);
      CharSequence _xifexpression = null;
      boolean _equals = str.equals("boolean");
      if (_equals) {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("is");
        _xifexpression = _builder;
      } else {
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("get");
        _xifexpression = _builder_1;
      }
      _xblockexpression = _xifexpression;
    }
    return _xblockexpression;
  }
  
  public CharSequence classAttributes(final Mapping mapping, final EClass ecl) {
    StringConcatenation _builder = new StringConcatenation();
    final Function1<EAttribute, CharSequence> _function = new Function1<EAttribute, CharSequence>() {
      @Override
      public CharSequence apply(final EAttribute e) {
        return OperatorsCompiler.this.injpa.tomParameter(e, mapping);
      }
    };
    String _join = IterableExtensions.<EAttribute>join(ecl.getEAllAttributes(), ", ", _function);
    _builder.append(_join);
    {
      if (((ecl.getEAllAttributes().size() > 0) && (this._tomMappingExtensions.getDefaultParameters(ecl, mapping).size() > 0))) {
        _builder.append(",");
      }
    }
    return _builder;
  }
  
  public CharSequence javaClassAttributes(final Mapping mapping, final EClass ecl) {
    StringConcatenation _builder = new StringConcatenation();
    final Function1<EAttribute, CharSequence> _function = new Function1<EAttribute, CharSequence>() {
      @Override
      public CharSequence apply(final EAttribute e) {
        return OperatorsCompiler.this.injpa.javaParameter(e, mapping);
      }
    };
    String _join = IterableExtensions.<EAttribute>join(ecl.getEAllAttributes(), ", ", _function);
    _builder.append(_join);
    {
      if (((ecl.getEAllAttributes().size() > 0) && (this._tomMappingExtensions.getDefaultParameters(ecl, mapping).size() > 0))) {
        _builder.append(",");
      }
    }
    return _builder;
  }
  
  private CharSequence _settedParameterTest(final Mapping m, final EClass c, final FeatureParameter feature) {
    StringConcatenation _builder = new StringConcatenation();
    return _builder;
  }
  
  private CharSequence _settedParameterTest(final Mapping m, final EClass c, final SettedFeatureParameter sfp) {
    CharSequence _xblockexpression = null;
    {
      final SettedValue v = sfp.getValue();
      CharSequence value = this.settedValue(sfp.getFeature(), v, m);
      CharSequence _xifexpression = null;
      if (((value.toString().contentEquals("null") || (v instanceof IntSettedValue)) || (v instanceof BooleanSettedValue))) {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append(" ");
        _builder.append("&& ((");
        String _javaType = this.injpa.javaType(c, m);
        _builder.append(_javaType, " ");
        _builder.append(")$t).get");
        String _firstUpper = StringExtensions.toFirstUpper(sfp.getFeature().getName());
        _builder.append(_firstUpper, " ");
        _builder.append("() == ");
        _builder.append(value, " ");
        _xifexpression = _builder;
      } else {
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append(" ");
        _builder_1.append("&& ((");
        String _javaType_1 = this.injpa.javaType(c, m);
        _builder_1.append(_javaType_1, " ");
        _builder_1.append(")$t).get");
        String _firstUpper_1 = StringExtensions.toFirstUpper(sfp.getFeature().getName());
        _builder_1.append(_firstUpper_1, " ");
        _builder_1.append("().equals(");
        _builder_1.append(value, " ");
        _builder_1.append(")");
        _xifexpression = _builder_1;
      }
      _xblockexpression = _xifexpression;
    }
    return _xblockexpression;
  }
  
  protected CharSequence _settedValue(final EStructuralFeature feature, final SettedValue sv, final Mapping m) {
    StringConcatenation _builder = new StringConcatenation();
    return _builder;
  }
  
  protected CharSequence _settedValue(final EStructuralFeature feature, final EnumLiteralValue elv, final Mapping m) {
    StringConcatenation _builder = new StringConcatenation();
    String _javaType = this.injpa.javaType(feature.getEType(), m);
    _builder.append(_javaType);
    _builder.append(".");
    String _name = elv.getLiteral().getName();
    _builder.append(_name);
    return _builder;
  }
  
  protected CharSequence _settedValue(final EStructuralFeature feature, final IntSettedValue elv, final Mapping m) {
    StringConcatenation _builder = new StringConcatenation();
    int _java = elv.getJava();
    _builder.append(_java);
    return _builder;
  }
  
  protected CharSequence _settedValue(final EStructuralFeature feature, final BooleanSettedValue elv, final Mapping m) {
    StringConcatenation _builder = new StringConcatenation();
    boolean _isJava = elv.isJava();
    _builder.append(_isJava);
    return _builder;
  }
  
  protected CharSequence _settedValue(final EStructuralFeature feature, final JavaCodeValue jcv, final Mapping m) {
    CharSequence _xblockexpression = null;
    {
      final boolean isString = ((!Objects.equal(feature.getEType().getInstanceTypeName(), null)) && feature.getEType().getInstanceTypeName().contains("java.lang.String"));
      StringConcatenation _builder = new StringConcatenation();
      {
        if (isString) {
          _builder.append("\"");
        }
      }
      String _java = jcv.getJava();
      _builder.append(_java);
      {
        if (isString) {
          _builder.append("\"");
        }
      }
      _xblockexpression = _builder;
    }
    return _xblockexpression;
  }
  
  public CharSequence operator(final Mapping mapping, final Operator clop) {
    if (clop instanceof ClassOperator) {
      return _operator(mapping, (ClassOperator)clop);
    } else if (clop != null) {
      return _operator(mapping, clop);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(mapping, clop).toString());
    }
  }
  
  private CharSequence settedParameterTest(final Mapping m, final EClass c, final FeatureParameter sfp) {
    if (sfp instanceof SettedFeatureParameter) {
      return _settedParameterTest(m, c, (SettedFeatureParameter)sfp);
    } else if (sfp != null) {
      return _settedParameterTest(m, c, sfp);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(m, c, sfp).toString());
    }
  }
  
  public CharSequence settedValue(final EStructuralFeature feature, final SettedValue elv, final Mapping m) {
    if (elv instanceof BooleanSettedValue) {
      return _settedValue(feature, (BooleanSettedValue)elv, m);
    } else if (elv instanceof EnumLiteralValue) {
      return _settedValue(feature, (EnumLiteralValue)elv, m);
    } else if (elv instanceof IntSettedValue) {
      return _settedValue(feature, (IntSettedValue)elv, m);
    } else if (elv instanceof JavaCodeValue) {
      return _settedValue(feature, (JavaCodeValue)elv, m);
    } else if (elv != null) {
      return _settedValue(feature, elv, m);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(feature, elv, m).toString());
    }
  }
}
