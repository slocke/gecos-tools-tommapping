package tom.mapping.dsl.generator.tom;

import com.google.common.collect.Iterators;
import com.google.inject.Inject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import model.Mapping;
import model.Module;
import model.Operator;
import model.Terminal;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.generator.IFileSystemAccess;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.IteratorExtensions;
import tom.mapping.dsl.generator.TomMappingExtensions;
import tom.mapping.dsl.generator.tom.OperatorsCompiler;
import tom.mapping.dsl.generator.tom.TerminalsCompiler;

@SuppressWarnings("all")
public class TomTemplateCompiler {
  @Extension
  private TomMappingExtensions _tomMappingExtensions = new TomMappingExtensions();
  
  @Inject
  @Extension
  private TerminalsCompiler terminals;
  
  @Inject
  @Extension
  private OperatorsCompiler injop;
  
  private String prefix = "tom";
  
  public void compile(final Mapping m, final IFileSystemAccess fsa) {
    String _name = m.getName();
    String _plus = ((this.prefix + "/") + _name);
    String _plus_1 = (_plus + "_common.tom");
    fsa.generateFile(_plus_1, this.common(m));
    String _name_1 = m.getName();
    String _plus_2 = ((this.prefix + "/") + _name_1);
    String _plus_3 = (_plus_2 + "_terminals.tom");
    fsa.generateFile(_plus_3, this.terminals(m));
    String _name_2 = m.getName();
    String _plus_4 = ((this.prefix + "/") + _name_2);
    String _plus_5 = (_plus_4 + "_operators.tom");
    fsa.generateFile(_plus_5, this.operators(m));
    String _name_3 = m.getName();
    String _plus_6 = ((this.prefix + "/") + _name_3);
    String _plus_7 = (_plus_6 + "_defaultOperators.tom");
    fsa.generateFile(_plus_7, this.defaultOperators(m));
    EList<Module> _modules = m.getModules();
    for (final Module module : _modules) {
      String _name_4 = m.getName();
      String _plus_8 = ((this.prefix + "/") + _name_4);
      String _plus_9 = (_plus_8 + "_");
      String _name_5 = module.getName();
      String _plus_10 = (_plus_9 + _name_5);
      String _plus_11 = (_plus_10 + ".tom");
      fsa.generateFile(_plus_11, this.module(m, module));
    }
  }
  
  protected CharSequence common(final Mapping m) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("%include { string.tom }");
    _builder.newLine();
    _builder.append("%include { boolean.tom }");
    _builder.newLine();
    _builder.append("%include { int.tom }");
    _builder.newLine();
    _builder.append("%include { long.tom }");
    _builder.newLine();
    _builder.append("%include { float.tom }");
    _builder.newLine();
    _builder.append("%include { double.tom }");
    _builder.newLine();
    _builder.newLine();
    _builder.append("private static <O> EList<O> enforce(EList l) {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("return l;");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    _builder.newLine();
    _builder.append("private static <O> EList<O> append(O e,EList<O> l) {");
    _builder.newLine();
    _builder.append("       ");
    _builder.append("l.add(e);");
    _builder.newLine();
    _builder.append("       ");
    _builder.append("return l;");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  protected CharSequence terminals(final Mapping m) {
    CharSequence _xblockexpression = null;
    {
      final LinkedHashMap<String, EClassifier> set = new LinkedHashMap<String, EClassifier>();
      List<EPackage> _list = IterableExtensions.<EPackage>toList(this._tomMappingExtensions.getNewRootPackages(m));
      for (final EPackage p : _list) {
        List<EClassifier> _list_1 = IteratorExtensions.<EClassifier>toList(Iterators.<EClassifier>filter(p.eAllContents(), EClassifier.class));
        for (final EClassifier c : _list_1) {
          boolean _isPrimitive2 = this._tomMappingExtensions.isPrimitive2(c.getName());
          boolean _not = (!_isPrimitive2);
          if (_not) {
            boolean _containsKey = set.containsKey(c.getName());
            boolean _not_1 = (!_containsKey);
            if (_not_1) {
              set.put(c.getName(), c);
            }
          }
        }
      }
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("// Primitive terminals (enum and data types)");
      _builder.newLine();
      {
        Collection<EClassifier> _values = set.values();
        for(final EClassifier c_1 : _values) {
          CharSequence _primitiveTerminal = this.terminals.primitiveTerminal(m, c_1);
          _builder.append(_primitiveTerminal);
          _builder.newLineIfNotEmpty();
        }
      }
      _builder.newLine();
      _builder.append("// Terminals");
      _builder.newLine();
      {
        EList<Terminal> _terminals = m.getTerminals();
        for(final Terminal t : _terminals) {
          CharSequence _terminal = this.terminals.terminal(m, t);
          _builder.append(_terminal);
          _builder.newLineIfNotEmpty();
        }
      }
      _builder.newLine();
      _builder.append("// List Terminals");
      _builder.newLine();
      {
        EList<Terminal> _allListTerminals = m.getAllListTerminals();
        for(final Terminal lt : _allListTerminals) {
          CharSequence _listTerminal = this.terminals.listTerminal(m, lt);
          _builder.append(_listTerminal);
          _builder.newLineIfNotEmpty();
        }
      }
      _xblockexpression = _builder;
    }
    return _xblockexpression;
  }
  
  protected CharSequence operators(final Mapping m) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("// User operators");
    _builder.newLine();
    {
      EList<Operator> _operators = m.getOperators();
      for(final Operator op : _operators) {
        CharSequence _operator = this.injop.operator(m, op);
        _builder.append(_operator);
        _builder.newLineIfNotEmpty();
      }
    }
    return _builder;
  }
  
  protected CharSequence defaultOperators(final Mapping m) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("// Default operators");
    _builder.newLine();
    {
      ArrayList<EClass> _allDefaultOperators = this._tomMappingExtensions.allDefaultOperators(m);
      for(final EClass op : _allDefaultOperators) {
        CharSequence _classOperator = this.injop.classOperator(m, op.getName(), op);
        _builder.append(_classOperator);
        _builder.newLineIfNotEmpty();
      }
    }
    return _builder;
  }
  
  protected CharSequence module(final Mapping m, final Module mod) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("/* PROTECTED REGION ID(module_");
    String _name = mod.getName();
    _builder.append(_name);
    _builder.append("_user) ENABLED START */");
    _builder.newLineIfNotEmpty();
    _builder.append("// Protected user region");
    _builder.newLine();
    _builder.append("/* PROTECTED REGION END */");
    _builder.newLine();
    _builder.newLine();
    {
      EList<Operator> _operators = mod.getOperators();
      for(final Operator op : _operators) {
        CharSequence _operator = this.injop.operator(m, op);
        _builder.append(_operator);
        _builder.newLineIfNotEmpty();
      }
    }
    return _builder;
  }
  
  protected void primitiveTerminals(final Mapping mapping, final EPackage epa) {
    EList<EClassifier> _eClassifiers = epa.getEClassifiers();
    for (final EClassifier c : _eClassifiers) {
      this.terminals.primitiveTerminal(mapping, c);
    }
    EList<EPackage> _eSubpackages = epa.getESubpackages();
    for (final EPackage subp : _eSubpackages) {
      this.primitiveTerminals(mapping, subp);
    }
  }
}
