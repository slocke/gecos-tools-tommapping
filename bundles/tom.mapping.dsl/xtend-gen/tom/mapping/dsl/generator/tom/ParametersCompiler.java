package tom.mapping.dsl.generator.tom;

import com.google.common.base.Objects;
import java.util.Arrays;
import model.FeatureParameter;
import model.InstanceMapping;
import model.Mapping;
import model.Parameter;
import model.Terminal;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.StringExtensions;
import tom.mapping.dsl.generator.TomMappingExtensions;

@SuppressWarnings("all")
public class ParametersCompiler {
  @Extension
  private TomMappingExtensions _tomMappingExtensions = new TomMappingExtensions();
  
  protected CharSequence _tomParameter(final Parameter p, final Mapping m) {
    StringConcatenation _builder = new StringConcatenation();
    String _name = p.getName();
    _builder.append(_name);
    _builder.append(" : ");
    String _tomType = this.getTomType(p.getType(), m);
    _builder.append(_tomType);
    return _builder;
  }
  
  protected CharSequence _tomParameter(final FeatureParameter fp, final Mapping m) {
    StringConcatenation _builder = new StringConcatenation();
    String _paramName = this.paramName(fp.getFeature());
    _builder.append(_paramName);
    _builder.append(" : ");
    CharSequence _mType = this.tomType(fp.getFeature(), m);
    _builder.append(_mType);
    return _builder;
  }
  
  protected CharSequence _tomParameter(final EStructuralFeature efp, final Mapping m) {
    StringConcatenation _builder = new StringConcatenation();
    String _paramName = this.paramName(efp);
    _builder.append(_paramName);
    _builder.append(" : ");
    CharSequence _mType = this.tomType(efp, m);
    _builder.append(_mType);
    _builder.append(" ");
    return _builder;
  }
  
  protected CharSequence _javaParameter(final Parameter p, final Mapping m) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("\t");
    CharSequence _javaType = this.javaType(p.getType(), m);
    _builder.append(_javaType, "\t");
    _builder.append(" ");
    String _name = p.getName();
    _builder.append(_name, "\t");
    return _builder;
  }
  
  protected CharSequence _javaParameter(final FeatureParameter fp, final Mapping m) {
    StringConcatenation _builder = new StringConcatenation();
    String _javaType = this.javaType(fp.getFeature(), m);
    _builder.append(_javaType);
    _builder.append(" _");
    String _name = fp.getFeature().getName();
    _builder.append(_name);
    return _builder;
  }
  
  protected CharSequence _javaParameter(final EStructuralFeature efp, final Mapping m) {
    StringConcatenation _builder = new StringConcatenation();
    String _javaType = this.javaType(efp, m);
    _builder.append(_javaType);
    _builder.append(" _");
    String _name = efp.getName();
    _builder.append(_name);
    return _builder;
  }
  
  public String paramName(final EStructuralFeature p) {
    return p.getName().replaceAll("___internal_cached___", "");
  }
  
  /**
   * tomType is the main entrypoint for getting the tom type name
   *  from many things (ERerefence, EStructuralFeature, EClass, Terminal)
   * If it is an EClass, it tries to find the matching Terminal
   * 
   * getTomType is where it does the actual String generation
   */
  public CharSequence tomType(final EStructuralFeature esf, final Mapping m) {
    CharSequence _xifexpression = null;
    if ((esf instanceof EReference)) {
      return this.getTomType(((EReference) esf), m);
    } else {
      CharSequence _xifexpression_1 = null;
      boolean _isMany = esf.isMany();
      if (_isMany) {
        StringConcatenation _builder = new StringConcatenation();
        String _tomType = this.getTomType(esf.getEType(), m);
        _builder.append(_tomType);
        _builder.append("List");
        _xifexpression_1 = _builder;
      } else {
        StringConcatenation _builder_1 = new StringConcatenation();
        String _tomType_1 = this.getTomType(esf.getEType(), m);
        _builder_1.append(_tomType_1);
        _xifexpression_1 = _builder_1;
      }
      _xifexpression = _xifexpression_1;
    }
    return _xifexpression;
  }
  
  public String tomType(final EClassifier eclass, final Mapping m) {
    return this.getTomType(eclass, m);
  }
  
  public String tomType(final EEnum enumType, final Mapping m) {
    return this.getTomType(enumType, m);
  }
  
  public String tomType(final EDataType edt, final Mapping m) {
    return this.getTomType(edt, m);
  }
  
  public CharSequence tomType(final Terminal t, final Mapping m) {
    StringConcatenation _builder = new StringConcatenation();
    String _tomType = this.getTomType(t, m);
    _builder.append(_tomType);
    return _builder;
  }
  
  private String _getTomType(final EReference eref, final Mapping m) {
    final Terminal t = m.getTerminal(eref.getEReferenceType(), eref.isMany());
    if ((t != null)) {
      return this.getTomType(t, m);
    } else {
      return this.getTomType(eref.getEReferenceType(), m);
    }
  }
  
  private String _getTomType(final Terminal t, final Mapping mapping) {
    return t.getName();
  }
  
  private String _getTomType(final EClassifier eclass, final Mapping m) {
    if (((eclass.getInstanceTypeName() != null) || (!m.isUseFullyQualifiedTypeName()))) {
      return this.renameEcoreClasses(eclass, m);
    } else {
      return this.getFullyQualifiedTomType(m, eclass);
    }
  }
  
  private String _getTomType(final EDataType edt, final Mapping m) {
    if ((m.isUseFullyQualifiedTypeName() && (!this._tomMappingExtensions.isPrimitive2(edt.getName())))) {
      return this.getFullyQualifiedTomType(m, edt);
    } else {
      return edt.getName();
    }
  }
  
  private String getFullyQualifiedTomType(final Mapping mapping, final EClassifier eclass) {
    String _replaceAll = this._tomMappingExtensions.getPackagePrefix(mapping, eclass.getEPackage()).replaceAll("\\.", "_");
    String _name = eclass.getEPackage().getName();
    String _plus = (_replaceAll + _name);
    final String prefix = (_plus + "_");
    String _replaceAll_1 = eclass.getName().replaceAll("\\$", "_");
    return (prefix + _replaceAll_1);
  }
  
  /**
   * javaType is the main entrypoint for getting the Java type name
   *  from many things (ERerefence, EStructuralFeature, EClass, Terminal)
   * javaTerminalType tries to find the corresponding Terminal
   * 
   * getJavaType is where it does the actual String generation
   */
  public CharSequence javaTerminalType(final EStructuralFeature fp, final Mapping m) {
    if ((fp instanceof EReference)) {
      boolean _isMany = ((EReference)fp).isMany();
      if (_isMany) {
        this.structured = true;
        CharSequence _javaTerminalType = this.getJavaTerminalType(((EReference) fp).getEReferenceType(), m);
        String _plus = ("List<" + _javaTerminalType);
        final String res = (_plus + ">");
        this.structured = false;
        return res;
      } else {
        return this.getJavaTerminalType(((EReference) fp).getEReferenceType(), m);
      }
    } else {
      return this.javaType(fp, m);
    }
  }
  
  public CharSequence javaTerminalType(final EClass eclass, final Mapping m) {
    return this.getJavaTerminalType(eclass, m);
  }
  
  private CharSequence getJavaTerminalType(final EClass eclass, final Mapping m) {
    final Terminal t = m.getTerminal(eclass, false);
    if ((t != null)) {
      return this.javaType(t, m);
    } else {
      return this.javaType(eclass, m);
    }
  }
  
  public String javaType(final EStructuralFeature esf, final Mapping m) {
    String _xifexpression = null;
    boolean _isMany = esf.isMany();
    if (_isMany) {
      this.structured = true;
      String _javaType = this.getJavaType(esf.getEType(), m);
      String _plus = ("List<" + _javaType);
      final String res = (_plus + ">");
      this.structured = false;
      return res;
    } else {
      StringConcatenation _builder = new StringConcatenation();
      String _javaType_1 = this.getJavaType(esf.getEType(), m);
      _builder.append(_javaType_1);
      _xifexpression = _builder.toString();
    }
    return _xifexpression;
  }
  
  public String javaType(final EClassifier eclass, final Mapping m) {
    return this.getJavaType(eclass, m);
  }
  
  public String javaType(final EEnum enumType, final Mapping m) {
    return this.getJavaType(enumType, m);
  }
  
  public String javaType(final EDataType edt, final Mapping m) {
    return this.getJavaType(edt, m);
  }
  
  private boolean structured = false;
  
  public CharSequence javaType(final Terminal t, final Mapping m) {
    CharSequence _xifexpression = null;
    boolean _isMany = t.isMany();
    if (_isMany) {
      this.structured = true;
      String _javaType = this.getJavaType(t.getClass_(), m);
      String _plus = ("List<" + _javaType);
      final String res = (_plus + ">");
      this.structured = false;
      return res;
    } else {
      StringConcatenation _builder = new StringConcatenation();
      String _javaType_1 = this.getJavaType(t.getClass_(), m);
      _builder.append(_javaType_1);
      _xifexpression = _builder;
    }
    return _xifexpression;
  }
  
  private String getJavaType(final EClassifier eclass, final Mapping mapping) {
    String res = "";
    if (((eclass.getInstanceTypeName() != null) || (!mapping.isUseFullyQualifiedTypeName()))) {
      res = this.renameEcoreClasses(eclass, mapping);
    } else {
      res = this.getFullyQualifiedJavaType(eclass, mapping);
    }
    return res;
  }
  
  private String getFullyQualifiedJavaType(final EClassifier eclass, final Mapping mapping) {
    String _packagePrefix = this._tomMappingExtensions.getPackagePrefix(mapping, eclass.getEPackage());
    String _name = eclass.getEPackage().getName();
    String _plus = (_packagePrefix + _name);
    final String prefix = (_plus + ".");
    String _replaceAll = eclass.getName().replaceAll("\\$", ".");
    return (prefix + _replaceAll);
  }
  
  public String renameEcoreClasses(final EClassifier eat, final Mapping m) {
    EClassifier eEat = eat;
    boolean _eIsProxy = eEat.eIsProxy();
    if (_eIsProxy) {
      EObject _resolve = EcoreUtil.resolve(eEat, m);
      eEat = ((EClassifier) _resolve);
    }
    if ((!this.structured)) {
      String _name = eEat.getName();
      if (_name != null) {
        switch (_name) {
          case "EInt":
            StringConcatenation _builder = new StringConcatenation();
            _builder.append("int");
            return _builder.toString();
          case "ELong":
            StringConcatenation _builder_1 = new StringConcatenation();
            _builder_1.append("long");
            return _builder_1.toString();
          case "EFloat":
            StringConcatenation _builder_2 = new StringConcatenation();
            _builder_2.append("float");
            return _builder_2.toString();
          case "EDouble":
            StringConcatenation _builder_3 = new StringConcatenation();
            _builder_3.append("double");
            return _builder_3.toString();
          case "EBoolean":
            StringConcatenation _builder_4 = new StringConcatenation();
            _builder_4.append("boolean");
            return _builder_4.toString();
          case "EString":
            StringConcatenation _builder_5 = new StringConcatenation();
            _builder_5.append("String");
            return _builder_5.toString();
          case "String":
            StringConcatenation _builder_6 = new StringConcatenation();
            _builder_6.append("String");
            return _builder_6.toString();
        }
      }
    } else {
      String _name_1 = eEat.getName();
      if (_name_1 != null) {
        switch (_name_1) {
          case "EInt":
            StringConcatenation _builder_7 = new StringConcatenation();
            _builder_7.append("Integer");
            return _builder_7.toString();
          case "ELong":
            StringConcatenation _builder_8 = new StringConcatenation();
            _builder_8.append("Long");
            return _builder_8.toString();
          case "EFloat":
            StringConcatenation _builder_9 = new StringConcatenation();
            _builder_9.append("Float");
            return _builder_9.toString();
          case "EDouble":
            StringConcatenation _builder_10 = new StringConcatenation();
            _builder_10.append("Double");
            return _builder_10.toString();
          case "EBoolean":
            StringConcatenation _builder_11 = new StringConcatenation();
            _builder_11.append("Boolean");
            return _builder_11.toString();
          case "EString":
            StringConcatenation _builder_12 = new StringConcatenation();
            _builder_12.append("String");
            return _builder_12.toString();
          case "String":
            StringConcatenation _builder_13 = new StringConcatenation();
            _builder_13.append("String");
            return _builder_13.toString();
        }
      }
    }
    String _instanceTypeName = eEat.getInstanceTypeName();
    boolean _tripleNotEquals = (_instanceTypeName != null);
    if (_tripleNotEquals) {
      String res = eEat.getInstanceTypeName().replaceAll("\\$", ".");
      if (((eEat instanceof EClass) && (((EClass) eEat).getEStructuralFeatures().size() > 0))) {
        final EClass eclass = ((EClass) eEat);
        boolean first = true;
        String _res = res;
        res = (_res + "<");
        EList<EStructuralFeature> _eStructuralFeatures = eclass.getEStructuralFeatures();
        for (final EStructuralFeature feat : _eStructuralFeatures) {
          {
            if (first) {
              first = false;
            } else {
              String _res_1 = res;
              res = (_res_1 + ",");
            }
            String _res_2 = res;
            String _javaType = this.getJavaType(feat.getEType(), m);
            res = (_res_2 + _javaType);
          }
        }
        String _res_1 = res;
        res = (_res_1 + ">");
      }
      return res;
    } else {
      String _name_2 = eEat.getName();
      boolean _tripleNotEquals_1 = (_name_2 != null);
      if (_tripleNotEquals_1) {
        return eEat.getName().replaceAll("\\$", ".");
      } else {
        return "null";
      }
    }
  }
  
  protected CharSequence _primitiveType(final EDataType edt) {
    StringConcatenation _builder = new StringConcatenation();
    {
      String _instanceTypeName = edt.getInstanceTypeName();
      boolean _equals = Objects.equal(_instanceTypeName, "java.lang.String");
      if (_equals) {
        _builder.append("String");
      } else {
        String _instanceTypeName_1 = edt.getInstanceTypeName();
        _builder.append(_instanceTypeName_1);
      }
    }
    return _builder;
  }
  
  protected CharSequence _primitiveType(final EEnum ee) {
    StringConcatenation _builder = new StringConcatenation();
    String _name = ee.getName();
    _builder.append(_name);
    return _builder;
  }
  
  private String packageName(final Mapping m, final EPackage p) {
    final InstanceMapping instance = this._tomMappingExtensions.findMappedInstance(m, p);
    if (((instance != null) && (instance.getPackageFactoryName() != null))) {
      return instance.getPackageFactoryName();
    }
    return StringExtensions.toFirstUpper(p.getName());
  }
  
  public CharSequence getFactoryType(final Mapping m, final EPackage p) {
    CharSequence _xifexpression = null;
    boolean _isUseFullyQualifiedTypeName = m.isUseFullyQualifiedTypeName();
    if (_isUseFullyQualifiedTypeName) {
      StringConcatenation _builder = new StringConcatenation();
      String _packagePrefix = this._tomMappingExtensions.getPackagePrefix(m, p);
      _builder.append(_packagePrefix);
      String _name = p.getName();
      _builder.append(_name);
      _builder.append(".");
      String _packageName = this.packageName(m, p);
      _builder.append(_packageName);
      _builder.append("Factory");
      _xifexpression = _builder;
    } else {
      StringConcatenation _builder_1 = new StringConcatenation();
      String _packageName_1 = this.packageName(m, p);
      _builder_1.append(_packageName_1);
      _builder_1.append("Factory");
      _xifexpression = _builder_1;
    }
    return _xifexpression;
  }
  
  public CharSequence getSwitchType(final Mapping m, final EPackage p) {
    CharSequence _xifexpression = null;
    boolean _isUseFullyQualifiedTypeName = m.isUseFullyQualifiedTypeName();
    if (_isUseFullyQualifiedTypeName) {
      StringConcatenation _builder = new StringConcatenation();
      String _packagePrefix = this._tomMappingExtensions.getPackagePrefix(m, p);
      _builder.append(_packagePrefix);
      String _name = p.getName();
      _builder.append(_name);
      _builder.append(".util.");
      String _packageName = this.packageName(m, p);
      _builder.append(_packageName);
      _builder.append("Switch");
      _xifexpression = _builder;
    } else {
      StringConcatenation _builder_1 = new StringConcatenation();
      String _packageName_1 = this.packageName(m, p);
      _builder_1.append(_packageName_1);
      _builder_1.append("Switch");
      _xifexpression = _builder_1;
    }
    return _xifexpression;
  }
  
  public CharSequence getPackageType(final Mapping m, final EPackage p) {
    CharSequence _xifexpression = null;
    boolean _isUseFullyQualifiedTypeName = m.isUseFullyQualifiedTypeName();
    if (_isUseFullyQualifiedTypeName) {
      StringConcatenation _builder = new StringConcatenation();
      String _packagePrefix = this._tomMappingExtensions.getPackagePrefix(m, p);
      _builder.append(_packagePrefix);
      String _name = p.getName();
      _builder.append(_name);
      _builder.append(".");
      String _packageName = this.packageName(m, p);
      _builder.append(_packageName);
      _builder.append("Package");
      _xifexpression = _builder;
    } else {
      StringConcatenation _builder_1 = new StringConcatenation();
      String _packageName_1 = this.packageName(m, p);
      _builder_1.append(_packageName_1);
      _builder_1.append("Package");
      _xifexpression = _builder_1;
    }
    return _xifexpression;
  }
  
  public CharSequence tomParameter(final EObject efp, final Mapping m) {
    if (efp instanceof EStructuralFeature) {
      return _tomParameter((EStructuralFeature)efp, m);
    } else if (efp instanceof FeatureParameter) {
      return _tomParameter((FeatureParameter)efp, m);
    } else if (efp instanceof Parameter) {
      return _tomParameter((Parameter)efp, m);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(efp, m).toString());
    }
  }
  
  public CharSequence javaParameter(final EObject efp, final Mapping m) {
    if (efp instanceof EStructuralFeature) {
      return _javaParameter((EStructuralFeature)efp, m);
    } else if (efp instanceof FeatureParameter) {
      return _javaParameter((FeatureParameter)efp, m);
    } else if (efp instanceof Parameter) {
      return _javaParameter((Parameter)efp, m);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(efp, m).toString());
    }
  }
  
  private String getTomType(final EObject eref, final Mapping m) {
    if (eref instanceof EReference) {
      return _getTomType((EReference)eref, m);
    } else if (eref instanceof EDataType) {
      return _getTomType((EDataType)eref, m);
    } else if (eref instanceof EClassifier) {
      return _getTomType((EClassifier)eref, m);
    } else if (eref instanceof Terminal) {
      return _getTomType((Terminal)eref, m);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(eref, m).toString());
    }
  }
  
  public CharSequence primitiveType(final EDataType ee) {
    if (ee instanceof EEnum) {
      return _primitiveType((EEnum)ee);
    } else if (ee != null) {
      return _primitiveType(ee);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(ee).toString());
    }
  }
}
