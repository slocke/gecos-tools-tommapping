package tom.mapping.dsl.generator;

import com.google.common.base.Objects;
import model.Accessor;
import model.Mapping;
import model.Terminal;
import model.UserOperator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.xtext.xbase.lib.StringExtensions;

@SuppressWarnings("all")
public class TomMappingNaming {
  public String getCustomOperatorSlotAccessorName(final Accessor accessor) {
    EObject _eContainer = accessor.eContainer();
    String _firstUpper = StringExtensions.toFirstUpper(((UserOperator) _eContainer).getName());
    String _plus = ("get" + _firstUpper);
    String _plus_1 = (_plus + "Slot");
    String _upperCase = accessor.getSlot().getName().toUpperCase();
    return (_plus_1 + _upperCase);
  }
  
  public String getCustomOperatorsClass(final Mapping mapping) {
    String _firstUpper = StringExtensions.toFirstUpper(mapping.getName());
    return (_firstUpper + "CustomAccessors");
  }
  
  public String getChildrenGetterName(final EPackage p) {
    String _firstUpper = StringExtensions.toFirstUpper(p.getName());
    return (_firstUpper + "ChildrenGetter");
  }
  
  public String getChildrenSetterName(final EPackage p) {
    String _firstUpper = StringExtensions.toFirstUpper(p.getName());
    return (_firstUpper + "ChildrenSetter");
  }
  
  public String name(final Terminal t, final Mapping m) {
    String _xifexpression = null;
    if ((t.isMany() && (this.isInferedList(t, m)).booleanValue())) {
      String _name = t.getName();
      _xifexpression = (_name + "List");
    } else {
      _xifexpression = t.getName();
    }
    return _xifexpression;
  }
  
  public String factoryName(final Mapping m) {
    String _firstUpper = StringExtensions.toFirstUpper(m.getName());
    return (_firstUpper + "UserFactory");
  }
  
  public String tomFactoryName(final Mapping m) {
    String _firstUpper = StringExtensions.toFirstUpper(m.getName());
    return (_firstUpper + "TomFactory");
  }
  
  public String tomFactoryQualifiedName(final Mapping m) {
    String _prefix = m.getPrefix();
    String _plus = (_prefix + ".");
    String _name = m.getName();
    String _plus_1 = (_plus + _name);
    String _plus_2 = (_plus_1 + ".");
    String _plus_3 = (_plus_2 + "internal");
    String _plus_4 = (_plus_3 + ".");
    String _mFactoryName = this.tomFactoryName(m);
    return (_plus_4 + _mFactoryName);
  }
  
  public String getPackagePrefix(final String prefix) {
    String _xifexpression = null;
    if ((Objects.equal(prefix, null) || (prefix.compareTo("") == 0))) {
      _xifexpression = "";
    } else {
      _xifexpression = (prefix + ".");
    }
    return _xifexpression;
  }
  
  private Boolean isInferedList(final Terminal t, final Mapping m) {
    return Boolean.valueOf((t.isMany() && (!(m.getTerminals().contains(t) || m.getExternalTerminals().contains(t)))));
  }
}
