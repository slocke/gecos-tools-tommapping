package tom.mapping.dsl.generator;

import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import com.google.common.collect.Iterators;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import model.Accessor;
import model.ClassOperator;
import model.FeatureException;
import model.FeatureParameter;
import model.InstanceMapping;
import model.Mapping;
import model.Module;
import model.Operator;
import model.SettedFeatureParameter;
import model.Terminal;
import model.UserOperator;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.impl.EStringToStringMapEntryImpl;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.IteratorExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;
import org.eclipse.xtext.xbase.lib.StringExtensions;

@SuppressWarnings("all")
public class TomMappingExtensions {
  public String getCustomOperatorSlotAccessorName(final Accessor a) {
    String _firstUpper = StringExtensions.toFirstUpper(((UserOperator) a).getName());
    String _plus = ("get" + _firstUpper);
    String _plus_1 = (_plus + "Slot");
    String _upperCase = a.getSlot().getName().toUpperCase();
    return (_plus_1 + _upperCase);
  }
  
  public String getCustomOperatorsClass(final Mapping mapping) {
    String _firstUpper = StringExtensions.toFirstUpper(mapping.getName());
    return (_firstUpper + "CustomAccessors");
  }
  
  public String getChildrenGetterName(final EPackage p) {
    String _firstUpper = StringExtensions.toFirstUpper(p.getName());
    return (_firstUpper + "ChildrenGetter");
  }
  
  public String getChildrenSetterName(final EPackage p) {
    String _firstUpper = StringExtensions.toFirstUpper(p.getName());
    return (_firstUpper + "ChildrenSetter");
  }
  
  public String name(final Terminal t, final Mapping m) {
    if ((t.isMany() && this.isInferedList(t, m))) {
      String _name = t.getName();
      return (_name + "List");
    } else {
      return t.getName();
    }
  }
  
  public boolean isInferedList(final Terminal t, final Mapping m) {
    return (t.isMany() && (!(m.getTerminals().contains(t) || m.getExternalTerminals().contains(t))));
  }
  
  public String javaFactoryName(final Mapping m) {
    String _packageName = this.packageName(m);
    return (_packageName + "UserFactory");
  }
  
  public String tomFactoryName(final Mapping m) {
    String _packageName = this.packageName(m);
    return (_packageName + "TomFactory");
  }
  
  private String packageName(final Mapping m) {
    String _packageName = m.getPackageName();
    boolean _tripleEquals = (_packageName == null);
    if (_tripleEquals) {
      return StringExtensions.toFirstUpper(m.getName());
    } else {
      return m.getPackageName();
    }
  }
  
  public String tomFactoryQualifiedName(final Mapping m) {
    String _prefix = m.getPrefix();
    String _plus = (_prefix + ".");
    String _name = m.getName();
    String _plus_1 = (_plus + _name);
    String _plus_2 = (_plus_1 + ".");
    String _plus_3 = (_plus_2 + "internal");
    String _plus_4 = (_plus_3 + ".");
    String _mFactoryName = this.tomFactoryName(m);
    return (_plus_4 + _mFactoryName);
  }
  
  public String getMainPackagePrefix(final Mapping mapping) {
    if (((mapping.getPrefix() != null) && (mapping.getPrefix().length() > 0))) {
      String _prefix = mapping.getPrefix();
      return (_prefix + ".");
    } else {
      return "";
    }
  }
  
  public String getPackagePrefix(final Mapping mapping, final EPackage epackage) {
    String _name = mapping.getName();
    String _name_1 = epackage.getName();
    boolean _equals = Objects.equal(_name, _name_1);
    if (_equals) {
      if (((mapping.getPrefix() != null) && (mapping.getPrefix().length() > 0))) {
        String _prefix = mapping.getPrefix();
        return (_prefix + ".");
      } else {
        return "";
      }
    } else {
      return this.searchMappedInstance(mapping, epackage);
    }
  }
  
  public InstanceMapping findMappedInstance(final Mapping mapping, final EPackage epackage) {
    final Function1<InstanceMapping, Boolean> _function = new Function1<InstanceMapping, Boolean>() {
      @Override
      public Boolean apply(final InstanceMapping map) {
        String _name = map.getEpackage().getName();
        String _name_1 = epackage.getName();
        return Boolean.valueOf(Objects.equal(_name, _name_1));
      }
    };
    InstanceMapping instance = IterableExtensions.<InstanceMapping>findFirst(mapping.getInstanceMappings(), _function);
    if ((instance != null)) {
      return instance;
    }
    EList<Mapping> _externalMappings = mapping.getExternalMappings();
    for (final Mapping submap : _externalMappings) {
      {
        instance = this.findMappedInstance(submap, epackage);
        if ((instance != null)) {
          return instance;
        }
      }
    }
    return instance;
  }
  
  private String searchMappedInstance(final Mapping mapping, final EPackage epackage) {
    final InstanceMapping instance = this.findMappedInstance(mapping, epackage);
    if (((instance != null) && (instance.getInstancePackageName() != null))) {
      String _instancePackageName = instance.getInstancePackageName();
      return (_instancePackageName + ".");
    } else {
      EPackage _eSuperPackage = epackage.getESuperPackage();
      boolean _tripleNotEquals = (_eSuperPackage != null);
      if (_tripleNotEquals) {
        String res = this.getPackagePrefix(mapping, epackage.getESuperPackage());
        if (((res != null) && (res.length() > 0))) {
          String _name = epackage.getESuperPackage().getName();
          String _plus = (res + _name);
          return (_plus + ".");
        } else {
          String _name_1 = epackage.getESuperPackage().getName();
          return (_name_1 + ".");
        }
      } else {
        final String basePackage = this.getBasePackage(epackage);
        String _xifexpression = null;
        if ((basePackage != null)) {
          _xifexpression = (basePackage + ".");
        } else {
          _xifexpression = "";
        }
        return _xifexpression;
      }
    }
  }
  
  /**
   * Try to get the value of "basePackage" annotation of the EPackage
   */
  public String getBasePackage(final EPackage p) {
    final Function1<EAnnotation, EMap<String, String>> _function = new Function1<EAnnotation, EMap<String, String>>() {
      @Override
      public EMap<String, String> apply(final EAnnotation it) {
        return it.getDetails();
      }
    };
    final Function1<EStringToStringMapEntryImpl, Boolean> _function_1 = new Function1<EStringToStringMapEntryImpl, Boolean>() {
      @Override
      public Boolean apply(final EStringToStringMapEntryImpl it) {
        String _key = it.getKey();
        return Boolean.valueOf(Objects.equal(_key, "basePackage"));
      }
    };
    EStringToStringMapEntryImpl _findFirst = IterableExtensions.<EStringToStringMapEntryImpl>findFirst(Iterables.<EStringToStringMapEntryImpl>filter(Iterables.<Map.Entry<String, String>>concat(ListExtensions.<EAnnotation, EMap<String, String>>map(p.getEAnnotations(), _function)), EStringToStringMapEntryImpl.class), _function_1);
    String _value = null;
    if (_findFirst!=null) {
      _value=_findFirst.getValue();
    }
    final String basePackage = _value;
    return basePackage;
  }
  
  public List<EReference> getDefaultParameters(final EClass c, final Mapping mapping) {
    final Function1<EReference, Boolean> _function = new Function1<EReference, Boolean>() {
      @Override
      public Boolean apply(final EReference e) {
        return Boolean.valueOf(TomMappingExtensions.this.isParameterFeature(e, mapping));
      }
    };
    return IterableExtensions.<EReference>toList(IterableExtensions.<EReference>filter(c.getEAllReferences(), _function));
  }
  
  public Iterable<FeatureParameter> getCustomParameters(final ClassOperator c) {
    final Function1<FeatureParameter, Boolean> _function = new Function1<FeatureParameter, Boolean>() {
      @Override
      public Boolean apply(final FeatureParameter e) {
        return Boolean.valueOf(TomMappingExtensions.this.isExplicitParameter(e));
      }
    };
    return IterableExtensions.<FeatureParameter>filter(c.getParameters(), _function);
  }
  
  public List<SettedFeatureParameter> getSettedCustomParameters(final ClassOperator c) {
    return IterableExtensions.<SettedFeatureParameter>toList(Iterables.<SettedFeatureParameter>filter(c.getParameters(), SettedFeatureParameter.class));
  }
  
  private boolean _isParameterFeature(final EStructuralFeature f, final Mapping mapping) {
    return true;
  }
  
  private boolean _isParameterFeature(final EReference f, final Mapping mapping) {
    return ((((f.isDerived() == false) && (f.isVolatile() == false)) && (f.isContainment() || (f.getEOpposite() == null))) && ((mapping.getTerminal(f.getEReferenceType(), false) != null) || (mapping.getTerminal(f.getEReferenceType(), true) != null)));
  }
  
  private boolean _isExplicitParameter(final FeatureParameter pf) {
    return true;
  }
  
  private boolean _isExplicitParameter(final SettedFeatureParameter pf) {
    return false;
  }
  
  private boolean _isExplicitParameter(final FeatureException pf) {
    return false;
  }
  
  public List<EPackage> getAllSubPackages(final EPackage p) {
    return IteratorExtensions.<EPackage>toList(Iterators.<EPackage>filter(p.eAllContents(), EPackage.class));
  }
  
  public List<EPackage> getAllPackages(final Mapping m) {
    final ArrayList<EPackage> selected = new ArrayList<EPackage>();
    Iterable<EPackage> _allRootPackages = this.getAllRootPackages(m);
    for (final EPackage rp : _allRootPackages) {
      {
        boolean _isSelected = this.isSelected(rp, m);
        if (_isSelected) {
          selected.add(rp);
        }
        final Function1<EPackage, Boolean> _function = new Function1<EPackage, Boolean>() {
          @Override
          public Boolean apply(final EPackage e) {
            return Boolean.valueOf(TomMappingExtensions.this.isSelected(e, m));
          }
        };
        Iterables.<EPackage>addAll(selected, IterableExtensions.<EPackage>filter(this.getAllSubPackages(rp), _function));
      }
    }
    return this.intersectName(selected);
  }
  
  public boolean isSelected(final EPackage p, final Mapping m) {
    final Function1<EClass, Boolean> _function = new Function1<EClass, Boolean>() {
      @Override
      public Boolean apply(final EClass e) {
        Terminal _terminal = m.getTerminal(((EClass) e), false);
        return Boolean.valueOf((_terminal != null));
      }
    };
    int _size = IterableExtensions.size(IterableExtensions.<EClass>filter(Iterables.<EClass>filter(p.getEClassifiers(), EClass.class), _function));
    return (_size > 0);
  }
  
  public String packageToPath(final String s) {
    if ((s != null)) {
      return s.replaceAll("\\.", "/");
    } else {
      return "";
    }
  }
  
  public Iterable<EPackage> getAllRootPackages(final Mapping mapping) {
    EList<EPackage> _metamodelPackages = mapping.getMetamodelPackages();
    Collection<? extends EPackage> _allRootPackagesInExternalMappings = this.getAllRootPackagesInExternalMappings(mapping);
    Iterable<EPackage> packages = Iterables.<EPackage>concat(_metamodelPackages, _allRootPackagesInExternalMappings);
    EList<Mapping> _externalMappings = mapping.getExternalMappings();
    for (final Mapping exMap : _externalMappings) {
      Iterable<EPackage> _allRootPackages = this.getAllRootPackages(exMap);
      Iterable<EPackage> _plus = Iterables.<EPackage>concat(packages, _allRootPackages);
      packages = _plus;
    }
    packages = this.intersectName(IterableExtensions.<EPackage>toList(packages));
    return packages;
  }
  
  /**
   * Returns root packages introduced by this mapping.
   * i.e., getAllRootPackages - getNewRootPacakges
   */
  public Iterable<EPackage> getNewRootPackages(final Mapping mapping) {
    EList<EPackage> root = mapping.getMetamodelPackages();
    Collection<? extends EPackage> exRoot = this.getAllRootPackagesInExternalMappings(mapping);
    ArrayList<EPackage> rmList = new ArrayList<EPackage>();
    for (final EPackage rp : root) {
      final Function1<EPackage, Boolean> _function = new Function1<EPackage, Boolean>() {
        @Override
        public Boolean apply(final EPackage p) {
          String _name = p.getName();
          String _name_1 = rp.getName();
          return Boolean.valueOf(Objects.equal(_name, _name_1));
        }
      };
      boolean _exists = IterableExtensions.exists(exRoot, _function);
      if (_exists) {
        rmList.add(rp);
      }
    }
    root.removeAll(rmList);
    return root;
  }
  
  public Collection<? extends EPackage> getAllRootPackagesInExternalMappings(final Mapping mapping) {
    ArrayList<EPackage> packages = new ArrayList<EPackage>();
    EList<Mapping> _externalMappings = mapping.getExternalMappings();
    for (final Mapping exMap : _externalMappings) {
      {
        packages.addAll(exMap.getMetamodelPackages());
        packages.addAll(this.getAllRootPackagesInExternalMappings(exMap));
      }
    }
    return this.intersectName(packages);
  }
  
  public ArrayList<EClass> allDefaultOperators(final Mapping map) {
    ArrayList<EClass> ops = new ArrayList<EClass>();
    Iterable<EPackage> _newRootPackages = this.getNewRootPackages(map);
    for (final EPackage p : _newRootPackages) {
      ops.addAll(this.allDefaultOperators(p, map));
    }
    return ops;
  }
  
  private Collection<? extends EClass> allDefaultOperators(final EPackage p, final Mapping m) {
    ArrayList<EClass> ops = new ArrayList<EClass>();
    EList<EClassifier> _eClassifiers = p.getEClassifiers();
    for (final EClassifier c : _eClassifiers) {
      if ((c instanceof EClass)) {
        boolean _isDefaultOperator = this.isDefaultOperator(((EClass) c), m);
        if (_isDefaultOperator) {
          ops.add(((EClass) c));
        }
      }
    }
    EList<EPackage> _eSubpackages = p.getESubpackages();
    for (final EPackage subp : _eSubpackages) {
      ops.addAll(this.allDefaultOperators(subp, m));
    }
    return ops;
  }
  
  /**
   * A class is a default operator if it has one Terminal in its inheritance
   * top hierachy or if no terminals have been defined. A class operator with
   * a setted feature parameter isn't a default operator.
   */
  private boolean isDefaultOperator(final EClass c, final Mapping m) {
    EList<Operator> _operators = m.getOperators();
    for (final Operator op : _operators) {
      if ((op instanceof ClassOperator)) {
        final ClassOperator cop = ((ClassOperator) op);
        if ((this.areSameClasses(cop.getClass_(), c) && (!this.isSettedOperator(cop)))) {
          return false;
        }
      }
    }
    return ((!c.isAbstract()) && ((m.getTerminal(c, false) != null) || (m.getTerminals().size() == 0)));
  }
  
  private boolean areSameClasses(final EClass c1, final EClass c2) {
    final EPackage ePackage1 = c1.getEPackage();
    final EPackage ePackage2 = c2.getEPackage();
    if ((Objects.equal(ePackage1.getNsURI(), ePackage2.getNsURI()) || ePackage1.getNsURI().equals(ePackage2.getNsURI()))) {
      int _classifierID = c1.getClassifierID();
      int _classifierID_1 = c2.getClassifierID();
      return (_classifierID == _classifierID_1);
    }
    return false;
  }
  
  private boolean isSettedOperator(final ClassOperator op) {
    EList<FeatureParameter> _parameters = op.getParameters();
    for (final FeatureParameter fp : _parameters) {
      if ((fp instanceof SettedFeatureParameter)) {
        return true;
      }
    }
    return false;
  }
  
  public boolean isPrimitive(final String type) {
    return ((((((type.compareTo("int") == 0) || (type.compareTo("long") == 0)) || (type.compareTo("float") == 0)) || (type.compareTo("double") == 0)) || (type.compareTo("boolean") == 0)) || (type.compareTo("char") == 0));
  }
  
  private final static List<String> _primitiveTypes = Collections.<String>unmodifiableList(CollectionLiterals.<String>newArrayList("long", "int", "float", "double", "boolean", "String", "Long", "Int", "Float", "Double", "Boolean"));
  
  public boolean isPrimitive2(final String type) {
    return TomMappingExtensions._primitiveTypes.contains(type);
  }
  
  public List<EPackage> packageList(final Mapping map) {
    ArrayList<EPackage> packageList = new ArrayList<EPackage>();
    Iterable<ClassOperator> _filter = Iterables.<ClassOperator>filter(map.getOperators(), ClassOperator.class);
    for (final ClassOperator elt : _filter) {
      packageList.add(elt.getClass_().getEPackage());
    }
    EList<Module> _modules = map.getModules();
    for (final Module module : _modules) {
      Iterable<ClassOperator> _filter_1 = Iterables.<ClassOperator>filter(module.getOperators(), ClassOperator.class);
      for (final ClassOperator elt_1 : _filter_1) {
        packageList.add(elt_1.getClass_().getEPackage());
      }
    }
    ArrayList<EClass> _allDefaultOperators = this.allDefaultOperators(map);
    for (final EClass elt_2 : _allDefaultOperators) {
      packageList.add(elt_2.getEPackage());
    }
    return this.intersectName(packageList);
  }
  
  private List<EPackage> intersectName(final List<EPackage> listBase) {
    ArrayList<EPackage> listDestination = new ArrayList<EPackage>();
    for (final EPackage eltB : listBase) {
      {
        boolean sameName = false;
        for (final EPackage eltD : listDestination) {
          sameName = (sameName || eltB.getName().equals(eltD.getName()));
        }
        if ((!sameName)) {
          listDestination.add(eltB);
        }
      }
    }
    return listDestination;
  }
  
  private boolean isParameterFeature(final EStructuralFeature f, final Mapping mapping) {
    if (f instanceof EReference) {
      return _isParameterFeature((EReference)f, mapping);
    } else if (f != null) {
      return _isParameterFeature(f, mapping);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(f, mapping).toString());
    }
  }
  
  private boolean isExplicitParameter(final FeatureParameter pf) {
    if (pf instanceof FeatureException) {
      return _isExplicitParameter((FeatureException)pf);
    } else if (pf instanceof SettedFeatureParameter) {
      return _isExplicitParameter((SettedFeatureParameter)pf);
    } else if (pf != null) {
      return _isExplicitParameter(pf);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(pf).toString());
    }
  }
}
