package tom.mapping.dsl.generator.java;

import com.google.inject.Inject;
import java.util.Arrays;
import model.Accessor;
import model.Mapping;
import model.Operator;
import model.UserOperator;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.generator.IFileSystemAccess;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.StringExtensions;
import tom.mapping.dsl.generator.TomMappingExtensions;
import tom.mapping.dsl.generator.tom.ParametersCompiler;

@SuppressWarnings("all")
public class CustomOperatorsCompiler {
  @Extension
  private TomMappingExtensions _tomMappingExtensions = new TomMappingExtensions();
  
  @Inject
  @Extension
  private ParametersCompiler injpa;
  
  private String prefix = "";
  
  public void compile(final Mapping m, final IFileSystemAccess fsa) {
    final Function1<Operator, Boolean> _function = new Function1<Operator, Boolean>() {
      @Override
      public Boolean apply(final Operator e) {
        return Boolean.valueOf((e instanceof UserOperator));
      }
    };
    int _size = IterableExtensions.size(IterableExtensions.<Operator>filter(m.getOperators(), _function));
    boolean _greaterThan = (_size > 0);
    if (_greaterThan) {
      String _customOperatorsClass = this._tomMappingExtensions.getCustomOperatorsClass(m);
      String _plus = ((this.prefix + "/") + _customOperatorsClass);
      String _plus_1 = (_plus + ".java");
      fsa.generateFile(_plus_1, this.main(m));
    }
  }
  
  public CharSequence main(final Mapping map) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("public class ");
    String _customOperatorsClass = this._tomMappingExtensions.getCustomOperatorsClass(map);
    _builder.append(_customOperatorsClass);
    _builder.append(" {");
    _builder.newLineIfNotEmpty();
    {
      EList<Operator> _operators = map.getOperators();
      for(final Operator op : _operators) {
        _builder.append("\t");
        CharSequence _operator = this.operator(map, op);
        _builder.append(_operator, "\t");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  protected CharSequence _operator(final Mapping map, final Operator op) {
    return null;
  }
  
  protected CharSequence _operator(final Mapping map, final UserOperator usop) {
    CharSequence _xblockexpression = null;
    {
      EList<Accessor> _accessors = usop.getAccessors();
      for (final Accessor a : _accessors) {
        this.accessor(map, usop, a);
      }
      this.test(map, usop);
      _xblockexpression = this.make(map, usop);
    }
    return _xblockexpression;
  }
  
  public CharSequence accessor(final Mapping m, final UserOperator op, final Accessor acc) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("public static ");
    CharSequence _javaType = this.injpa.javaType(acc.getSlot().getType(), m);
    _builder.append(_javaType);
    String _customOperatorSlotAccessorName = this._tomMappingExtensions.getCustomOperatorSlotAccessorName(acc);
    _builder.append(_customOperatorSlotAccessorName);
    _builder.append("(");
    CharSequence _javaType_1 = this.injpa.javaType(op.getType(), m);
    _builder.append(_javaType_1);
    _builder.append(" t) {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("return ");
    String _java = acc.getJava();
    _builder.append(_java, "\t");
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  public CharSequence test(final Mapping m, final UserOperator usop) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("public static boolean is");
    String _firstUpper = StringExtensions.toFirstUpper(usop.getName());
    _builder.append(_firstUpper);
    _builder.append("(");
    CharSequence _javaType = this.injpa.javaType(usop.getType(), m);
    _builder.append(_javaType);
    _builder.append(" t) {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("return ");
    String _test = usop.getTest();
    _builder.append(_test, "\t");
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  public CharSequence make(final Mapping m, final UserOperator usop) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("public static ");
    CharSequence _javaType = this.injpa.javaType(usop.getType(), m);
    _builder.append(_javaType);
    _builder.append(" make ");
    String _firstUpper = StringExtensions.toFirstUpper(usop.getName());
    _builder.append(_firstUpper);
    _builder.append("(");
    final Function1<Accessor, CharSequence> _function = new Function1<Accessor, CharSequence>() {
      @Override
      public CharSequence apply(final Accessor acc) {
        return CustomOperatorsCompiler.this.injpa.javaParameter(acc.getSlot(), m);
      }
    };
    String _join = IterableExtensions.<Accessor>join(usop.getAccessors(), ", ", _function);
    _builder.append(_join);
    _builder.append(") {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("return ");
    String _make = usop.getMake();
    _builder.append(_make, "\t");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  public CharSequence operator(final Mapping map, final Operator usop) {
    if (usop instanceof UserOperator) {
      return _operator(map, (UserOperator)usop);
    } else if (usop != null) {
      return _operator(map, usop);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(map, usop).toString());
    }
  }
}
