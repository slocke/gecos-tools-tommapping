package tom.mapping.dsl.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import tom.mapping.dsl.services.TomMappingGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalTomMappingParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'TomMapping'", "';'", "'prefix'", "'package'", "'fullyQualified'", "'userFactory'", "'terminals'", "'{'", "'define'", "','", "'}'", "'use'", "'operators'", "'custom'", "'factory'", "'module'", "'import'", "':'", "'[]'", "'alias'", "'::'", "'('", "')'", "'op'", "'make'", "'='", "'is_fsym'", "'slot'", "'ignore'", "'true'", "'false'", "'.'"
    };
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__37=37;
    public static final int T__16=16;
    public static final int T__38=38;
    public static final int T__17=17;
    public static final int T__39=39;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__33=33;
    public static final int T__12=12;
    public static final int T__34=34;
    public static final int T__13=13;
    public static final int T__35=35;
    public static final int T__14=14;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_ID=5;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__20=20;
    public static final int T__42=42;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalTomMappingParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalTomMappingParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalTomMappingParser.tokenNames; }
    public String getGrammarFileName() { return "InternalTomMapping.g"; }



     	private TomMappingGrammarAccess grammarAccess;

        public InternalTomMappingParser(TokenStream input, TomMappingGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Mapping";
       	}

       	@Override
       	protected TomMappingGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleMapping"
    // InternalTomMapping.g:64:1: entryRuleMapping returns [EObject current=null] : iv_ruleMapping= ruleMapping EOF ;
    public final EObject entryRuleMapping() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMapping = null;


        try {
            // InternalTomMapping.g:64:48: (iv_ruleMapping= ruleMapping EOF )
            // InternalTomMapping.g:65:2: iv_ruleMapping= ruleMapping EOF
            {
             newCompositeNode(grammarAccess.getMappingRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleMapping=ruleMapping();

            state._fsp--;

             current =iv_ruleMapping; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMapping"


    // $ANTLR start "ruleMapping"
    // InternalTomMapping.g:71:1: ruleMapping returns [EObject current=null] : (otherlv_0= 'TomMapping' ( (lv_name_1_0= ruleSN ) ) otherlv_2= ';' (otherlv_3= 'prefix' ( (lv_prefix_4_0= RULE_STRING ) ) otherlv_5= ';' )? (otherlv_6= 'package' ( (lv_packageName_7_0= RULE_STRING ) ) otherlv_8= ';' )? ( ( (lv_useFullyQualifiedTypeName_9_0= 'fullyQualified' ) ) otherlv_10= ';' )? ( ( (lv_generateUserFactory_11_0= 'userFactory' ) ) otherlv_12= ';' )? ( (lv_imports_13_0= ruleImport ) )* ( (lv_instanceMappings_14_0= ruleInstanceMapping ) )* (otherlv_15= 'terminals' otherlv_16= '{' ( ( (otherlv_17= 'define' otherlv_18= '{' ( (lv_terminals_19_0= ruleTerminal ) ) (otherlv_20= ',' ( (lv_terminals_21_0= ruleTerminal ) ) )* otherlv_22= '}' )? (otherlv_23= 'use' otherlv_24= '{' ( ( ruleFQN ) ) (otherlv_26= ',' ( ( ruleFQN ) ) )* otherlv_28= '}' )? ) | ( ( (lv_terminals_29_0= ruleTerminal ) ) (otherlv_30= ',' ( (lv_terminals_31_0= ruleTerminal ) ) )* ) ) otherlv_32= '}' )? ( (otherlv_33= 'operators' otherlv_34= '{' ( (lv_operators_35_0= ruleOperator ) ) (otherlv_36= ';' ( (lv_operators_37_0= ruleOperator ) ) )* otherlv_38= ';' otherlv_39= '}' ) | ( (lv_modules_40_0= ruleModule ) ) )* ) ;
    public final EObject ruleMapping() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token lv_prefix_4_0=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token lv_packageName_7_0=null;
        Token otherlv_8=null;
        Token lv_useFullyQualifiedTypeName_9_0=null;
        Token otherlv_10=null;
        Token lv_generateUserFactory_11_0=null;
        Token otherlv_12=null;
        Token otherlv_15=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        Token otherlv_18=null;
        Token otherlv_20=null;
        Token otherlv_22=null;
        Token otherlv_23=null;
        Token otherlv_24=null;
        Token otherlv_26=null;
        Token otherlv_28=null;
        Token otherlv_30=null;
        Token otherlv_32=null;
        Token otherlv_33=null;
        Token otherlv_34=null;
        Token otherlv_36=null;
        Token otherlv_38=null;
        Token otherlv_39=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        EObject lv_imports_13_0 = null;

        EObject lv_instanceMappings_14_0 = null;

        EObject lv_terminals_19_0 = null;

        EObject lv_terminals_21_0 = null;

        EObject lv_terminals_29_0 = null;

        EObject lv_terminals_31_0 = null;

        EObject lv_operators_35_0 = null;

        EObject lv_operators_37_0 = null;

        EObject lv_modules_40_0 = null;



        	enterRule();

        try {
            // InternalTomMapping.g:77:2: ( (otherlv_0= 'TomMapping' ( (lv_name_1_0= ruleSN ) ) otherlv_2= ';' (otherlv_3= 'prefix' ( (lv_prefix_4_0= RULE_STRING ) ) otherlv_5= ';' )? (otherlv_6= 'package' ( (lv_packageName_7_0= RULE_STRING ) ) otherlv_8= ';' )? ( ( (lv_useFullyQualifiedTypeName_9_0= 'fullyQualified' ) ) otherlv_10= ';' )? ( ( (lv_generateUserFactory_11_0= 'userFactory' ) ) otherlv_12= ';' )? ( (lv_imports_13_0= ruleImport ) )* ( (lv_instanceMappings_14_0= ruleInstanceMapping ) )* (otherlv_15= 'terminals' otherlv_16= '{' ( ( (otherlv_17= 'define' otherlv_18= '{' ( (lv_terminals_19_0= ruleTerminal ) ) (otherlv_20= ',' ( (lv_terminals_21_0= ruleTerminal ) ) )* otherlv_22= '}' )? (otherlv_23= 'use' otherlv_24= '{' ( ( ruleFQN ) ) (otherlv_26= ',' ( ( ruleFQN ) ) )* otherlv_28= '}' )? ) | ( ( (lv_terminals_29_0= ruleTerminal ) ) (otherlv_30= ',' ( (lv_terminals_31_0= ruleTerminal ) ) )* ) ) otherlv_32= '}' )? ( (otherlv_33= 'operators' otherlv_34= '{' ( (lv_operators_35_0= ruleOperator ) ) (otherlv_36= ';' ( (lv_operators_37_0= ruleOperator ) ) )* otherlv_38= ';' otherlv_39= '}' ) | ( (lv_modules_40_0= ruleModule ) ) )* ) )
            // InternalTomMapping.g:78:2: (otherlv_0= 'TomMapping' ( (lv_name_1_0= ruleSN ) ) otherlv_2= ';' (otherlv_3= 'prefix' ( (lv_prefix_4_0= RULE_STRING ) ) otherlv_5= ';' )? (otherlv_6= 'package' ( (lv_packageName_7_0= RULE_STRING ) ) otherlv_8= ';' )? ( ( (lv_useFullyQualifiedTypeName_9_0= 'fullyQualified' ) ) otherlv_10= ';' )? ( ( (lv_generateUserFactory_11_0= 'userFactory' ) ) otherlv_12= ';' )? ( (lv_imports_13_0= ruleImport ) )* ( (lv_instanceMappings_14_0= ruleInstanceMapping ) )* (otherlv_15= 'terminals' otherlv_16= '{' ( ( (otherlv_17= 'define' otherlv_18= '{' ( (lv_terminals_19_0= ruleTerminal ) ) (otherlv_20= ',' ( (lv_terminals_21_0= ruleTerminal ) ) )* otherlv_22= '}' )? (otherlv_23= 'use' otherlv_24= '{' ( ( ruleFQN ) ) (otherlv_26= ',' ( ( ruleFQN ) ) )* otherlv_28= '}' )? ) | ( ( (lv_terminals_29_0= ruleTerminal ) ) (otherlv_30= ',' ( (lv_terminals_31_0= ruleTerminal ) ) )* ) ) otherlv_32= '}' )? ( (otherlv_33= 'operators' otherlv_34= '{' ( (lv_operators_35_0= ruleOperator ) ) (otherlv_36= ';' ( (lv_operators_37_0= ruleOperator ) ) )* otherlv_38= ';' otherlv_39= '}' ) | ( (lv_modules_40_0= ruleModule ) ) )* )
            {
            // InternalTomMapping.g:78:2: (otherlv_0= 'TomMapping' ( (lv_name_1_0= ruleSN ) ) otherlv_2= ';' (otherlv_3= 'prefix' ( (lv_prefix_4_0= RULE_STRING ) ) otherlv_5= ';' )? (otherlv_6= 'package' ( (lv_packageName_7_0= RULE_STRING ) ) otherlv_8= ';' )? ( ( (lv_useFullyQualifiedTypeName_9_0= 'fullyQualified' ) ) otherlv_10= ';' )? ( ( (lv_generateUserFactory_11_0= 'userFactory' ) ) otherlv_12= ';' )? ( (lv_imports_13_0= ruleImport ) )* ( (lv_instanceMappings_14_0= ruleInstanceMapping ) )* (otherlv_15= 'terminals' otherlv_16= '{' ( ( (otherlv_17= 'define' otherlv_18= '{' ( (lv_terminals_19_0= ruleTerminal ) ) (otherlv_20= ',' ( (lv_terminals_21_0= ruleTerminal ) ) )* otherlv_22= '}' )? (otherlv_23= 'use' otherlv_24= '{' ( ( ruleFQN ) ) (otherlv_26= ',' ( ( ruleFQN ) ) )* otherlv_28= '}' )? ) | ( ( (lv_terminals_29_0= ruleTerminal ) ) (otherlv_30= ',' ( (lv_terminals_31_0= ruleTerminal ) ) )* ) ) otherlv_32= '}' )? ( (otherlv_33= 'operators' otherlv_34= '{' ( (lv_operators_35_0= ruleOperator ) ) (otherlv_36= ';' ( (lv_operators_37_0= ruleOperator ) ) )* otherlv_38= ';' otherlv_39= '}' ) | ( (lv_modules_40_0= ruleModule ) ) )* )
            // InternalTomMapping.g:79:3: otherlv_0= 'TomMapping' ( (lv_name_1_0= ruleSN ) ) otherlv_2= ';' (otherlv_3= 'prefix' ( (lv_prefix_4_0= RULE_STRING ) ) otherlv_5= ';' )? (otherlv_6= 'package' ( (lv_packageName_7_0= RULE_STRING ) ) otherlv_8= ';' )? ( ( (lv_useFullyQualifiedTypeName_9_0= 'fullyQualified' ) ) otherlv_10= ';' )? ( ( (lv_generateUserFactory_11_0= 'userFactory' ) ) otherlv_12= ';' )? ( (lv_imports_13_0= ruleImport ) )* ( (lv_instanceMappings_14_0= ruleInstanceMapping ) )* (otherlv_15= 'terminals' otherlv_16= '{' ( ( (otherlv_17= 'define' otherlv_18= '{' ( (lv_terminals_19_0= ruleTerminal ) ) (otherlv_20= ',' ( (lv_terminals_21_0= ruleTerminal ) ) )* otherlv_22= '}' )? (otherlv_23= 'use' otherlv_24= '{' ( ( ruleFQN ) ) (otherlv_26= ',' ( ( ruleFQN ) ) )* otherlv_28= '}' )? ) | ( ( (lv_terminals_29_0= ruleTerminal ) ) (otherlv_30= ',' ( (lv_terminals_31_0= ruleTerminal ) ) )* ) ) otherlv_32= '}' )? ( (otherlv_33= 'operators' otherlv_34= '{' ( (lv_operators_35_0= ruleOperator ) ) (otherlv_36= ';' ( (lv_operators_37_0= ruleOperator ) ) )* otherlv_38= ';' otherlv_39= '}' ) | ( (lv_modules_40_0= ruleModule ) ) )*
            {
            otherlv_0=(Token)match(input,11,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getMappingAccess().getTomMappingKeyword_0());
            		
            // InternalTomMapping.g:83:3: ( (lv_name_1_0= ruleSN ) )
            // InternalTomMapping.g:84:4: (lv_name_1_0= ruleSN )
            {
            // InternalTomMapping.g:84:4: (lv_name_1_0= ruleSN )
            // InternalTomMapping.g:85:5: lv_name_1_0= ruleSN
            {

            					newCompositeNode(grammarAccess.getMappingAccess().getNameSNParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_1_0=ruleSN();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getMappingRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"tom.mapping.dsl.TomMapping.SN");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_5); 

            			newLeafNode(otherlv_2, grammarAccess.getMappingAccess().getSemicolonKeyword_2());
            		
            // InternalTomMapping.g:106:3: (otherlv_3= 'prefix' ( (lv_prefix_4_0= RULE_STRING ) ) otherlv_5= ';' )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==13) ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // InternalTomMapping.g:107:4: otherlv_3= 'prefix' ( (lv_prefix_4_0= RULE_STRING ) ) otherlv_5= ';'
                    {
                    otherlv_3=(Token)match(input,13,FOLLOW_6); 

                    				newLeafNode(otherlv_3, grammarAccess.getMappingAccess().getPrefixKeyword_3_0());
                    			
                    // InternalTomMapping.g:111:4: ( (lv_prefix_4_0= RULE_STRING ) )
                    // InternalTomMapping.g:112:5: (lv_prefix_4_0= RULE_STRING )
                    {
                    // InternalTomMapping.g:112:5: (lv_prefix_4_0= RULE_STRING )
                    // InternalTomMapping.g:113:6: lv_prefix_4_0= RULE_STRING
                    {
                    lv_prefix_4_0=(Token)match(input,RULE_STRING,FOLLOW_4); 

                    						newLeafNode(lv_prefix_4_0, grammarAccess.getMappingAccess().getPrefixSTRINGTerminalRuleCall_3_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getMappingRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"prefix",
                    							lv_prefix_4_0,
                    							"tom.mapping.dsl.TomMapping.STRING");
                    					

                    }


                    }

                    otherlv_5=(Token)match(input,12,FOLLOW_7); 

                    				newLeafNode(otherlv_5, grammarAccess.getMappingAccess().getSemicolonKeyword_3_2());
                    			

                    }
                    break;

            }

            // InternalTomMapping.g:134:3: (otherlv_6= 'package' ( (lv_packageName_7_0= RULE_STRING ) ) otherlv_8= ';' )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==14) ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // InternalTomMapping.g:135:4: otherlv_6= 'package' ( (lv_packageName_7_0= RULE_STRING ) ) otherlv_8= ';'
                    {
                    otherlv_6=(Token)match(input,14,FOLLOW_6); 

                    				newLeafNode(otherlv_6, grammarAccess.getMappingAccess().getPackageKeyword_4_0());
                    			
                    // InternalTomMapping.g:139:4: ( (lv_packageName_7_0= RULE_STRING ) )
                    // InternalTomMapping.g:140:5: (lv_packageName_7_0= RULE_STRING )
                    {
                    // InternalTomMapping.g:140:5: (lv_packageName_7_0= RULE_STRING )
                    // InternalTomMapping.g:141:6: lv_packageName_7_0= RULE_STRING
                    {
                    lv_packageName_7_0=(Token)match(input,RULE_STRING,FOLLOW_4); 

                    						newLeafNode(lv_packageName_7_0, grammarAccess.getMappingAccess().getPackageNameSTRINGTerminalRuleCall_4_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getMappingRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"packageName",
                    							lv_packageName_7_0,
                    							"tom.mapping.dsl.TomMapping.STRING");
                    					

                    }


                    }

                    otherlv_8=(Token)match(input,12,FOLLOW_8); 

                    				newLeafNode(otherlv_8, grammarAccess.getMappingAccess().getSemicolonKeyword_4_2());
                    			

                    }
                    break;

            }

            // InternalTomMapping.g:162:3: ( ( (lv_useFullyQualifiedTypeName_9_0= 'fullyQualified' ) ) otherlv_10= ';' )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==15) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // InternalTomMapping.g:163:4: ( (lv_useFullyQualifiedTypeName_9_0= 'fullyQualified' ) ) otherlv_10= ';'
                    {
                    // InternalTomMapping.g:163:4: ( (lv_useFullyQualifiedTypeName_9_0= 'fullyQualified' ) )
                    // InternalTomMapping.g:164:5: (lv_useFullyQualifiedTypeName_9_0= 'fullyQualified' )
                    {
                    // InternalTomMapping.g:164:5: (lv_useFullyQualifiedTypeName_9_0= 'fullyQualified' )
                    // InternalTomMapping.g:165:6: lv_useFullyQualifiedTypeName_9_0= 'fullyQualified'
                    {
                    lv_useFullyQualifiedTypeName_9_0=(Token)match(input,15,FOLLOW_4); 

                    						newLeafNode(lv_useFullyQualifiedTypeName_9_0, grammarAccess.getMappingAccess().getUseFullyQualifiedTypeNameFullyQualifiedKeyword_5_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getMappingRule());
                    						}
                    						setWithLastConsumed(current, "useFullyQualifiedTypeName", true, "fullyQualified");
                    					

                    }


                    }

                    otherlv_10=(Token)match(input,12,FOLLOW_9); 

                    				newLeafNode(otherlv_10, grammarAccess.getMappingAccess().getSemicolonKeyword_5_1());
                    			

                    }
                    break;

            }

            // InternalTomMapping.g:182:3: ( ( (lv_generateUserFactory_11_0= 'userFactory' ) ) otherlv_12= ';' )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==16) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // InternalTomMapping.g:183:4: ( (lv_generateUserFactory_11_0= 'userFactory' ) ) otherlv_12= ';'
                    {
                    // InternalTomMapping.g:183:4: ( (lv_generateUserFactory_11_0= 'userFactory' ) )
                    // InternalTomMapping.g:184:5: (lv_generateUserFactory_11_0= 'userFactory' )
                    {
                    // InternalTomMapping.g:184:5: (lv_generateUserFactory_11_0= 'userFactory' )
                    // InternalTomMapping.g:185:6: lv_generateUserFactory_11_0= 'userFactory'
                    {
                    lv_generateUserFactory_11_0=(Token)match(input,16,FOLLOW_4); 

                    						newLeafNode(lv_generateUserFactory_11_0, grammarAccess.getMappingAccess().getGenerateUserFactoryUserFactoryKeyword_6_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getMappingRule());
                    						}
                    						setWithLastConsumed(current, "generateUserFactory", true, "userFactory");
                    					

                    }


                    }

                    otherlv_12=(Token)match(input,12,FOLLOW_10); 

                    				newLeafNode(otherlv_12, grammarAccess.getMappingAccess().getSemicolonKeyword_6_1());
                    			

                    }
                    break;

            }

            // InternalTomMapping.g:202:3: ( (lv_imports_13_0= ruleImport ) )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==27) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalTomMapping.g:203:4: (lv_imports_13_0= ruleImport )
            	    {
            	    // InternalTomMapping.g:203:4: (lv_imports_13_0= ruleImport )
            	    // InternalTomMapping.g:204:5: lv_imports_13_0= ruleImport
            	    {

            	    					newCompositeNode(grammarAccess.getMappingAccess().getImportsImportParserRuleCall_7_0());
            	    				
            	    pushFollow(FOLLOW_10);
            	    lv_imports_13_0=ruleImport();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getMappingRule());
            	    					}
            	    					add(
            	    						current,
            	    						"imports",
            	    						lv_imports_13_0,
            	    						"tom.mapping.dsl.TomMapping.Import");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

            // InternalTomMapping.g:221:3: ( (lv_instanceMappings_14_0= ruleInstanceMapping ) )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==24) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalTomMapping.g:222:4: (lv_instanceMappings_14_0= ruleInstanceMapping )
            	    {
            	    // InternalTomMapping.g:222:4: (lv_instanceMappings_14_0= ruleInstanceMapping )
            	    // InternalTomMapping.g:223:5: lv_instanceMappings_14_0= ruleInstanceMapping
            	    {

            	    					newCompositeNode(grammarAccess.getMappingAccess().getInstanceMappingsInstanceMappingParserRuleCall_8_0());
            	    				
            	    pushFollow(FOLLOW_11);
            	    lv_instanceMappings_14_0=ruleInstanceMapping();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getMappingRule());
            	    					}
            	    					add(
            	    						current,
            	    						"instanceMappings",
            	    						lv_instanceMappings_14_0,
            	    						"tom.mapping.dsl.TomMapping.InstanceMapping");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

            // InternalTomMapping.g:240:3: (otherlv_15= 'terminals' otherlv_16= '{' ( ( (otherlv_17= 'define' otherlv_18= '{' ( (lv_terminals_19_0= ruleTerminal ) ) (otherlv_20= ',' ( (lv_terminals_21_0= ruleTerminal ) ) )* otherlv_22= '}' )? (otherlv_23= 'use' otherlv_24= '{' ( ( ruleFQN ) ) (otherlv_26= ',' ( ( ruleFQN ) ) )* otherlv_28= '}' )? ) | ( ( (lv_terminals_29_0= ruleTerminal ) ) (otherlv_30= ',' ( (lv_terminals_31_0= ruleTerminal ) ) )* ) ) otherlv_32= '}' )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==17) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // InternalTomMapping.g:241:4: otherlv_15= 'terminals' otherlv_16= '{' ( ( (otherlv_17= 'define' otherlv_18= '{' ( (lv_terminals_19_0= ruleTerminal ) ) (otherlv_20= ',' ( (lv_terminals_21_0= ruleTerminal ) ) )* otherlv_22= '}' )? (otherlv_23= 'use' otherlv_24= '{' ( ( ruleFQN ) ) (otherlv_26= ',' ( ( ruleFQN ) ) )* otherlv_28= '}' )? ) | ( ( (lv_terminals_29_0= ruleTerminal ) ) (otherlv_30= ',' ( (lv_terminals_31_0= ruleTerminal ) ) )* ) ) otherlv_32= '}'
                    {
                    otherlv_15=(Token)match(input,17,FOLLOW_12); 

                    				newLeafNode(otherlv_15, grammarAccess.getMappingAccess().getTerminalsKeyword_9_0());
                    			
                    otherlv_16=(Token)match(input,18,FOLLOW_13); 

                    				newLeafNode(otherlv_16, grammarAccess.getMappingAccess().getLeftCurlyBracketKeyword_9_1());
                    			
                    // InternalTomMapping.g:249:4: ( ( (otherlv_17= 'define' otherlv_18= '{' ( (lv_terminals_19_0= ruleTerminal ) ) (otherlv_20= ',' ( (lv_terminals_21_0= ruleTerminal ) ) )* otherlv_22= '}' )? (otherlv_23= 'use' otherlv_24= '{' ( ( ruleFQN ) ) (otherlv_26= ',' ( ( ruleFQN ) ) )* otherlv_28= '}' )? ) | ( ( (lv_terminals_29_0= ruleTerminal ) ) (otherlv_30= ',' ( (lv_terminals_31_0= ruleTerminal ) ) )* ) )
                    int alt12=2;
                    int LA12_0 = input.LA(1);

                    if ( (LA12_0==19||(LA12_0>=21 && LA12_0<=22)) ) {
                        alt12=1;
                    }
                    else if ( (LA12_0==RULE_ID) ) {
                        alt12=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 12, 0, input);

                        throw nvae;
                    }
                    switch (alt12) {
                        case 1 :
                            // InternalTomMapping.g:250:5: ( (otherlv_17= 'define' otherlv_18= '{' ( (lv_terminals_19_0= ruleTerminal ) ) (otherlv_20= ',' ( (lv_terminals_21_0= ruleTerminal ) ) )* otherlv_22= '}' )? (otherlv_23= 'use' otherlv_24= '{' ( ( ruleFQN ) ) (otherlv_26= ',' ( ( ruleFQN ) ) )* otherlv_28= '}' )? )
                            {
                            // InternalTomMapping.g:250:5: ( (otherlv_17= 'define' otherlv_18= '{' ( (lv_terminals_19_0= ruleTerminal ) ) (otherlv_20= ',' ( (lv_terminals_21_0= ruleTerminal ) ) )* otherlv_22= '}' )? (otherlv_23= 'use' otherlv_24= '{' ( ( ruleFQN ) ) (otherlv_26= ',' ( ( ruleFQN ) ) )* otherlv_28= '}' )? )
                            // InternalTomMapping.g:251:6: (otherlv_17= 'define' otherlv_18= '{' ( (lv_terminals_19_0= ruleTerminal ) ) (otherlv_20= ',' ( (lv_terminals_21_0= ruleTerminal ) ) )* otherlv_22= '}' )? (otherlv_23= 'use' otherlv_24= '{' ( ( ruleFQN ) ) (otherlv_26= ',' ( ( ruleFQN ) ) )* otherlv_28= '}' )?
                            {
                            // InternalTomMapping.g:251:6: (otherlv_17= 'define' otherlv_18= '{' ( (lv_terminals_19_0= ruleTerminal ) ) (otherlv_20= ',' ( (lv_terminals_21_0= ruleTerminal ) ) )* otherlv_22= '}' )?
                            int alt8=2;
                            int LA8_0 = input.LA(1);

                            if ( (LA8_0==19) ) {
                                alt8=1;
                            }
                            switch (alt8) {
                                case 1 :
                                    // InternalTomMapping.g:252:7: otherlv_17= 'define' otherlv_18= '{' ( (lv_terminals_19_0= ruleTerminal ) ) (otherlv_20= ',' ( (lv_terminals_21_0= ruleTerminal ) ) )* otherlv_22= '}'
                                    {
                                    otherlv_17=(Token)match(input,19,FOLLOW_12); 

                                    							newLeafNode(otherlv_17, grammarAccess.getMappingAccess().getDefineKeyword_9_2_0_0_0());
                                    						
                                    otherlv_18=(Token)match(input,18,FOLLOW_13); 

                                    							newLeafNode(otherlv_18, grammarAccess.getMappingAccess().getLeftCurlyBracketKeyword_9_2_0_0_1());
                                    						
                                    // InternalTomMapping.g:260:7: ( (lv_terminals_19_0= ruleTerminal ) )
                                    // InternalTomMapping.g:261:8: (lv_terminals_19_0= ruleTerminal )
                                    {
                                    // InternalTomMapping.g:261:8: (lv_terminals_19_0= ruleTerminal )
                                    // InternalTomMapping.g:262:9: lv_terminals_19_0= ruleTerminal
                                    {

                                    									newCompositeNode(grammarAccess.getMappingAccess().getTerminalsTerminalParserRuleCall_9_2_0_0_2_0());
                                    								
                                    pushFollow(FOLLOW_14);
                                    lv_terminals_19_0=ruleTerminal();

                                    state._fsp--;


                                    									if (current==null) {
                                    										current = createModelElementForParent(grammarAccess.getMappingRule());
                                    									}
                                    									add(
                                    										current,
                                    										"terminals",
                                    										lv_terminals_19_0,
                                    										"tom.mapping.dsl.TomMapping.Terminal");
                                    									afterParserOrEnumRuleCall();
                                    								

                                    }


                                    }

                                    // InternalTomMapping.g:279:7: (otherlv_20= ',' ( (lv_terminals_21_0= ruleTerminal ) ) )*
                                    loop7:
                                    do {
                                        int alt7=2;
                                        int LA7_0 = input.LA(1);

                                        if ( (LA7_0==20) ) {
                                            alt7=1;
                                        }


                                        switch (alt7) {
                                    	case 1 :
                                    	    // InternalTomMapping.g:280:8: otherlv_20= ',' ( (lv_terminals_21_0= ruleTerminal ) )
                                    	    {
                                    	    otherlv_20=(Token)match(input,20,FOLLOW_13); 

                                    	    								newLeafNode(otherlv_20, grammarAccess.getMappingAccess().getCommaKeyword_9_2_0_0_3_0());
                                    	    							
                                    	    // InternalTomMapping.g:284:8: ( (lv_terminals_21_0= ruleTerminal ) )
                                    	    // InternalTomMapping.g:285:9: (lv_terminals_21_0= ruleTerminal )
                                    	    {
                                    	    // InternalTomMapping.g:285:9: (lv_terminals_21_0= ruleTerminal )
                                    	    // InternalTomMapping.g:286:10: lv_terminals_21_0= ruleTerminal
                                    	    {

                                    	    										newCompositeNode(grammarAccess.getMappingAccess().getTerminalsTerminalParserRuleCall_9_2_0_0_3_1_0());
                                    	    									
                                    	    pushFollow(FOLLOW_14);
                                    	    lv_terminals_21_0=ruleTerminal();

                                    	    state._fsp--;


                                    	    										if (current==null) {
                                    	    											current = createModelElementForParent(grammarAccess.getMappingRule());
                                    	    										}
                                    	    										add(
                                    	    											current,
                                    	    											"terminals",
                                    	    											lv_terminals_21_0,
                                    	    											"tom.mapping.dsl.TomMapping.Terminal");
                                    	    										afterParserOrEnumRuleCall();
                                    	    									

                                    	    }


                                    	    }


                                    	    }
                                    	    break;

                                    	default :
                                    	    break loop7;
                                        }
                                    } while (true);

                                    otherlv_22=(Token)match(input,21,FOLLOW_15); 

                                    							newLeafNode(otherlv_22, grammarAccess.getMappingAccess().getRightCurlyBracketKeyword_9_2_0_0_4());
                                    						

                                    }
                                    break;

                            }

                            // InternalTomMapping.g:309:6: (otherlv_23= 'use' otherlv_24= '{' ( ( ruleFQN ) ) (otherlv_26= ',' ( ( ruleFQN ) ) )* otherlv_28= '}' )?
                            int alt10=2;
                            int LA10_0 = input.LA(1);

                            if ( (LA10_0==22) ) {
                                alt10=1;
                            }
                            switch (alt10) {
                                case 1 :
                                    // InternalTomMapping.g:310:7: otherlv_23= 'use' otherlv_24= '{' ( ( ruleFQN ) ) (otherlv_26= ',' ( ( ruleFQN ) ) )* otherlv_28= '}'
                                    {
                                    otherlv_23=(Token)match(input,22,FOLLOW_12); 

                                    							newLeafNode(otherlv_23, grammarAccess.getMappingAccess().getUseKeyword_9_2_0_1_0());
                                    						
                                    otherlv_24=(Token)match(input,18,FOLLOW_3); 

                                    							newLeafNode(otherlv_24, grammarAccess.getMappingAccess().getLeftCurlyBracketKeyword_9_2_0_1_1());
                                    						
                                    // InternalTomMapping.g:318:7: ( ( ruleFQN ) )
                                    // InternalTomMapping.g:319:8: ( ruleFQN )
                                    {
                                    // InternalTomMapping.g:319:8: ( ruleFQN )
                                    // InternalTomMapping.g:320:9: ruleFQN
                                    {

                                    									if (current==null) {
                                    										current = createModelElement(grammarAccess.getMappingRule());
                                    									}
                                    								

                                    									newCompositeNode(grammarAccess.getMappingAccess().getExternalTerminalsTerminalCrossReference_9_2_0_1_2_0());
                                    								
                                    pushFollow(FOLLOW_14);
                                    ruleFQN();

                                    state._fsp--;


                                    									afterParserOrEnumRuleCall();
                                    								

                                    }


                                    }

                                    // InternalTomMapping.g:334:7: (otherlv_26= ',' ( ( ruleFQN ) ) )*
                                    loop9:
                                    do {
                                        int alt9=2;
                                        int LA9_0 = input.LA(1);

                                        if ( (LA9_0==20) ) {
                                            alt9=1;
                                        }


                                        switch (alt9) {
                                    	case 1 :
                                    	    // InternalTomMapping.g:335:8: otherlv_26= ',' ( ( ruleFQN ) )
                                    	    {
                                    	    otherlv_26=(Token)match(input,20,FOLLOW_3); 

                                    	    								newLeafNode(otherlv_26, grammarAccess.getMappingAccess().getCommaKeyword_9_2_0_1_3_0());
                                    	    							
                                    	    // InternalTomMapping.g:339:8: ( ( ruleFQN ) )
                                    	    // InternalTomMapping.g:340:9: ( ruleFQN )
                                    	    {
                                    	    // InternalTomMapping.g:340:9: ( ruleFQN )
                                    	    // InternalTomMapping.g:341:10: ruleFQN
                                    	    {

                                    	    										if (current==null) {
                                    	    											current = createModelElement(grammarAccess.getMappingRule());
                                    	    										}
                                    	    									

                                    	    										newCompositeNode(grammarAccess.getMappingAccess().getExternalTerminalsTerminalCrossReference_9_2_0_1_3_1_0());
                                    	    									
                                    	    pushFollow(FOLLOW_14);
                                    	    ruleFQN();

                                    	    state._fsp--;


                                    	    										afterParserOrEnumRuleCall();
                                    	    									

                                    	    }


                                    	    }


                                    	    }
                                    	    break;

                                    	default :
                                    	    break loop9;
                                        }
                                    } while (true);

                                    otherlv_28=(Token)match(input,21,FOLLOW_16); 

                                    							newLeafNode(otherlv_28, grammarAccess.getMappingAccess().getRightCurlyBracketKeyword_9_2_0_1_4());
                                    						

                                    }
                                    break;

                            }


                            }


                            }
                            break;
                        case 2 :
                            // InternalTomMapping.g:363:5: ( ( (lv_terminals_29_0= ruleTerminal ) ) (otherlv_30= ',' ( (lv_terminals_31_0= ruleTerminal ) ) )* )
                            {
                            // InternalTomMapping.g:363:5: ( ( (lv_terminals_29_0= ruleTerminal ) ) (otherlv_30= ',' ( (lv_terminals_31_0= ruleTerminal ) ) )* )
                            // InternalTomMapping.g:364:6: ( (lv_terminals_29_0= ruleTerminal ) ) (otherlv_30= ',' ( (lv_terminals_31_0= ruleTerminal ) ) )*
                            {
                            // InternalTomMapping.g:364:6: ( (lv_terminals_29_0= ruleTerminal ) )
                            // InternalTomMapping.g:365:7: (lv_terminals_29_0= ruleTerminal )
                            {
                            // InternalTomMapping.g:365:7: (lv_terminals_29_0= ruleTerminal )
                            // InternalTomMapping.g:366:8: lv_terminals_29_0= ruleTerminal
                            {

                            								newCompositeNode(grammarAccess.getMappingAccess().getTerminalsTerminalParserRuleCall_9_2_1_0_0());
                            							
                            pushFollow(FOLLOW_14);
                            lv_terminals_29_0=ruleTerminal();

                            state._fsp--;


                            								if (current==null) {
                            									current = createModelElementForParent(grammarAccess.getMappingRule());
                            								}
                            								add(
                            									current,
                            									"terminals",
                            									lv_terminals_29_0,
                            									"tom.mapping.dsl.TomMapping.Terminal");
                            								afterParserOrEnumRuleCall();
                            							

                            }


                            }

                            // InternalTomMapping.g:383:6: (otherlv_30= ',' ( (lv_terminals_31_0= ruleTerminal ) ) )*
                            loop11:
                            do {
                                int alt11=2;
                                int LA11_0 = input.LA(1);

                                if ( (LA11_0==20) ) {
                                    alt11=1;
                                }


                                switch (alt11) {
                            	case 1 :
                            	    // InternalTomMapping.g:384:7: otherlv_30= ',' ( (lv_terminals_31_0= ruleTerminal ) )
                            	    {
                            	    otherlv_30=(Token)match(input,20,FOLLOW_13); 

                            	    							newLeafNode(otherlv_30, grammarAccess.getMappingAccess().getCommaKeyword_9_2_1_1_0());
                            	    						
                            	    // InternalTomMapping.g:388:7: ( (lv_terminals_31_0= ruleTerminal ) )
                            	    // InternalTomMapping.g:389:8: (lv_terminals_31_0= ruleTerminal )
                            	    {
                            	    // InternalTomMapping.g:389:8: (lv_terminals_31_0= ruleTerminal )
                            	    // InternalTomMapping.g:390:9: lv_terminals_31_0= ruleTerminal
                            	    {

                            	    									newCompositeNode(grammarAccess.getMappingAccess().getTerminalsTerminalParserRuleCall_9_2_1_1_1_0());
                            	    								
                            	    pushFollow(FOLLOW_14);
                            	    lv_terminals_31_0=ruleTerminal();

                            	    state._fsp--;


                            	    									if (current==null) {
                            	    										current = createModelElementForParent(grammarAccess.getMappingRule());
                            	    									}
                            	    									add(
                            	    										current,
                            	    										"terminals",
                            	    										lv_terminals_31_0,
                            	    										"tom.mapping.dsl.TomMapping.Terminal");
                            	    									afterParserOrEnumRuleCall();
                            	    								

                            	    }


                            	    }


                            	    }
                            	    break;

                            	default :
                            	    break loop11;
                                }
                            } while (true);


                            }


                            }
                            break;

                    }

                    otherlv_32=(Token)match(input,21,FOLLOW_17); 

                    				newLeafNode(otherlv_32, grammarAccess.getMappingAccess().getRightCurlyBracketKeyword_9_3());
                    			

                    }
                    break;

            }

            // InternalTomMapping.g:415:3: ( (otherlv_33= 'operators' otherlv_34= '{' ( (lv_operators_35_0= ruleOperator ) ) (otherlv_36= ';' ( (lv_operators_37_0= ruleOperator ) ) )* otherlv_38= ';' otherlv_39= '}' ) | ( (lv_modules_40_0= ruleModule ) ) )*
            loop15:
            do {
                int alt15=3;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==23) ) {
                    alt15=1;
                }
                else if ( (LA15_0==26) ) {
                    alt15=2;
                }


                switch (alt15) {
            	case 1 :
            	    // InternalTomMapping.g:416:4: (otherlv_33= 'operators' otherlv_34= '{' ( (lv_operators_35_0= ruleOperator ) ) (otherlv_36= ';' ( (lv_operators_37_0= ruleOperator ) ) )* otherlv_38= ';' otherlv_39= '}' )
            	    {
            	    // InternalTomMapping.g:416:4: (otherlv_33= 'operators' otherlv_34= '{' ( (lv_operators_35_0= ruleOperator ) ) (otherlv_36= ';' ( (lv_operators_37_0= ruleOperator ) ) )* otherlv_38= ';' otherlv_39= '}' )
            	    // InternalTomMapping.g:417:5: otherlv_33= 'operators' otherlv_34= '{' ( (lv_operators_35_0= ruleOperator ) ) (otherlv_36= ';' ( (lv_operators_37_0= ruleOperator ) ) )* otherlv_38= ';' otherlv_39= '}'
            	    {
            	    otherlv_33=(Token)match(input,23,FOLLOW_12); 

            	    					newLeafNode(otherlv_33, grammarAccess.getMappingAccess().getOperatorsKeyword_10_0_0());
            	    				
            	    otherlv_34=(Token)match(input,18,FOLLOW_18); 

            	    					newLeafNode(otherlv_34, grammarAccess.getMappingAccess().getLeftCurlyBracketKeyword_10_0_1());
            	    				
            	    // InternalTomMapping.g:425:5: ( (lv_operators_35_0= ruleOperator ) )
            	    // InternalTomMapping.g:426:6: (lv_operators_35_0= ruleOperator )
            	    {
            	    // InternalTomMapping.g:426:6: (lv_operators_35_0= ruleOperator )
            	    // InternalTomMapping.g:427:7: lv_operators_35_0= ruleOperator
            	    {

            	    							newCompositeNode(grammarAccess.getMappingAccess().getOperatorsOperatorParserRuleCall_10_0_2_0());
            	    						
            	    pushFollow(FOLLOW_4);
            	    lv_operators_35_0=ruleOperator();

            	    state._fsp--;


            	    							if (current==null) {
            	    								current = createModelElementForParent(grammarAccess.getMappingRule());
            	    							}
            	    							add(
            	    								current,
            	    								"operators",
            	    								lv_operators_35_0,
            	    								"tom.mapping.dsl.TomMapping.Operator");
            	    							afterParserOrEnumRuleCall();
            	    						

            	    }


            	    }

            	    // InternalTomMapping.g:444:5: (otherlv_36= ';' ( (lv_operators_37_0= ruleOperator ) ) )*
            	    loop14:
            	    do {
            	        int alt14=2;
            	        int LA14_0 = input.LA(1);

            	        if ( (LA14_0==12) ) {
            	            int LA14_1 = input.LA(2);

            	            if ( (LA14_1==30||LA14_1==34) ) {
            	                alt14=1;
            	            }


            	        }


            	        switch (alt14) {
            	    	case 1 :
            	    	    // InternalTomMapping.g:445:6: otherlv_36= ';' ( (lv_operators_37_0= ruleOperator ) )
            	    	    {
            	    	    otherlv_36=(Token)match(input,12,FOLLOW_18); 

            	    	    						newLeafNode(otherlv_36, grammarAccess.getMappingAccess().getSemicolonKeyword_10_0_3_0());
            	    	    					
            	    	    // InternalTomMapping.g:449:6: ( (lv_operators_37_0= ruleOperator ) )
            	    	    // InternalTomMapping.g:450:7: (lv_operators_37_0= ruleOperator )
            	    	    {
            	    	    // InternalTomMapping.g:450:7: (lv_operators_37_0= ruleOperator )
            	    	    // InternalTomMapping.g:451:8: lv_operators_37_0= ruleOperator
            	    	    {

            	    	    								newCompositeNode(grammarAccess.getMappingAccess().getOperatorsOperatorParserRuleCall_10_0_3_1_0());
            	    	    							
            	    	    pushFollow(FOLLOW_4);
            	    	    lv_operators_37_0=ruleOperator();

            	    	    state._fsp--;


            	    	    								if (current==null) {
            	    	    									current = createModelElementForParent(grammarAccess.getMappingRule());
            	    	    								}
            	    	    								add(
            	    	    									current,
            	    	    									"operators",
            	    	    									lv_operators_37_0,
            	    	    									"tom.mapping.dsl.TomMapping.Operator");
            	    	    								afterParserOrEnumRuleCall();
            	    	    							

            	    	    }


            	    	    }


            	    	    }
            	    	    break;

            	    	default :
            	    	    break loop14;
            	        }
            	    } while (true);

            	    otherlv_38=(Token)match(input,12,FOLLOW_16); 

            	    					newLeafNode(otherlv_38, grammarAccess.getMappingAccess().getSemicolonKeyword_10_0_4());
            	    				
            	    otherlv_39=(Token)match(input,21,FOLLOW_17); 

            	    					newLeafNode(otherlv_39, grammarAccess.getMappingAccess().getRightCurlyBracketKeyword_10_0_5());
            	    				

            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalTomMapping.g:479:4: ( (lv_modules_40_0= ruleModule ) )
            	    {
            	    // InternalTomMapping.g:479:4: ( (lv_modules_40_0= ruleModule ) )
            	    // InternalTomMapping.g:480:5: (lv_modules_40_0= ruleModule )
            	    {
            	    // InternalTomMapping.g:480:5: (lv_modules_40_0= ruleModule )
            	    // InternalTomMapping.g:481:6: lv_modules_40_0= ruleModule
            	    {

            	    						newCompositeNode(grammarAccess.getMappingAccess().getModulesModuleParserRuleCall_10_1_0());
            	    					
            	    pushFollow(FOLLOW_17);
            	    lv_modules_40_0=ruleModule();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getMappingRule());
            	    						}
            	    						add(
            	    							current,
            	    							"modules",
            	    							lv_modules_40_0,
            	    							"tom.mapping.dsl.TomMapping.Module");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMapping"


    // $ANTLR start "entryRuleInstanceMapping"
    // InternalTomMapping.g:503:1: entryRuleInstanceMapping returns [EObject current=null] : iv_ruleInstanceMapping= ruleInstanceMapping EOF ;
    public final EObject entryRuleInstanceMapping() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInstanceMapping = null;


        try {
            // InternalTomMapping.g:503:56: (iv_ruleInstanceMapping= ruleInstanceMapping EOF )
            // InternalTomMapping.g:504:2: iv_ruleInstanceMapping= ruleInstanceMapping EOF
            {
             newCompositeNode(grammarAccess.getInstanceMappingRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleInstanceMapping=ruleInstanceMapping();

            state._fsp--;

             current =iv_ruleInstanceMapping; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInstanceMapping"


    // $ANTLR start "ruleInstanceMapping"
    // InternalTomMapping.g:510:1: ruleInstanceMapping returns [EObject current=null] : (otherlv_0= 'custom' ( ( ruleFQN ) ) otherlv_2= '{' (otherlv_3= 'package' ( (lv_instancePackageName_4_0= RULE_STRING ) ) otherlv_5= ';' )? (otherlv_6= 'factory' ( (lv_packageFactoryName_7_0= RULE_STRING ) ) otherlv_8= ';' )? otherlv_9= '}' ) ;
    public final EObject ruleInstanceMapping() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token lv_instancePackageName_4_0=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token lv_packageFactoryName_7_0=null;
        Token otherlv_8=null;
        Token otherlv_9=null;


        	enterRule();

        try {
            // InternalTomMapping.g:516:2: ( (otherlv_0= 'custom' ( ( ruleFQN ) ) otherlv_2= '{' (otherlv_3= 'package' ( (lv_instancePackageName_4_0= RULE_STRING ) ) otherlv_5= ';' )? (otherlv_6= 'factory' ( (lv_packageFactoryName_7_0= RULE_STRING ) ) otherlv_8= ';' )? otherlv_9= '}' ) )
            // InternalTomMapping.g:517:2: (otherlv_0= 'custom' ( ( ruleFQN ) ) otherlv_2= '{' (otherlv_3= 'package' ( (lv_instancePackageName_4_0= RULE_STRING ) ) otherlv_5= ';' )? (otherlv_6= 'factory' ( (lv_packageFactoryName_7_0= RULE_STRING ) ) otherlv_8= ';' )? otherlv_9= '}' )
            {
            // InternalTomMapping.g:517:2: (otherlv_0= 'custom' ( ( ruleFQN ) ) otherlv_2= '{' (otherlv_3= 'package' ( (lv_instancePackageName_4_0= RULE_STRING ) ) otherlv_5= ';' )? (otherlv_6= 'factory' ( (lv_packageFactoryName_7_0= RULE_STRING ) ) otherlv_8= ';' )? otherlv_9= '}' )
            // InternalTomMapping.g:518:3: otherlv_0= 'custom' ( ( ruleFQN ) ) otherlv_2= '{' (otherlv_3= 'package' ( (lv_instancePackageName_4_0= RULE_STRING ) ) otherlv_5= ';' )? (otherlv_6= 'factory' ( (lv_packageFactoryName_7_0= RULE_STRING ) ) otherlv_8= ';' )? otherlv_9= '}'
            {
            otherlv_0=(Token)match(input,24,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getInstanceMappingAccess().getCustomKeyword_0());
            		
            // InternalTomMapping.g:522:3: ( ( ruleFQN ) )
            // InternalTomMapping.g:523:4: ( ruleFQN )
            {
            // InternalTomMapping.g:523:4: ( ruleFQN )
            // InternalTomMapping.g:524:5: ruleFQN
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getInstanceMappingRule());
            					}
            				

            					newCompositeNode(grammarAccess.getInstanceMappingAccess().getEpackageEPackageCrossReference_1_0());
            				
            pushFollow(FOLLOW_12);
            ruleFQN();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,18,FOLLOW_19); 

            			newLeafNode(otherlv_2, grammarAccess.getInstanceMappingAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalTomMapping.g:542:3: (otherlv_3= 'package' ( (lv_instancePackageName_4_0= RULE_STRING ) ) otherlv_5= ';' )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==14) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // InternalTomMapping.g:543:4: otherlv_3= 'package' ( (lv_instancePackageName_4_0= RULE_STRING ) ) otherlv_5= ';'
                    {
                    otherlv_3=(Token)match(input,14,FOLLOW_6); 

                    				newLeafNode(otherlv_3, grammarAccess.getInstanceMappingAccess().getPackageKeyword_3_0());
                    			
                    // InternalTomMapping.g:547:4: ( (lv_instancePackageName_4_0= RULE_STRING ) )
                    // InternalTomMapping.g:548:5: (lv_instancePackageName_4_0= RULE_STRING )
                    {
                    // InternalTomMapping.g:548:5: (lv_instancePackageName_4_0= RULE_STRING )
                    // InternalTomMapping.g:549:6: lv_instancePackageName_4_0= RULE_STRING
                    {
                    lv_instancePackageName_4_0=(Token)match(input,RULE_STRING,FOLLOW_4); 

                    						newLeafNode(lv_instancePackageName_4_0, grammarAccess.getInstanceMappingAccess().getInstancePackageNameSTRINGTerminalRuleCall_3_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getInstanceMappingRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"instancePackageName",
                    							lv_instancePackageName_4_0,
                    							"tom.mapping.dsl.TomMapping.STRING");
                    					

                    }


                    }

                    otherlv_5=(Token)match(input,12,FOLLOW_20); 

                    				newLeafNode(otherlv_5, grammarAccess.getInstanceMappingAccess().getSemicolonKeyword_3_2());
                    			

                    }
                    break;

            }

            // InternalTomMapping.g:570:3: (otherlv_6= 'factory' ( (lv_packageFactoryName_7_0= RULE_STRING ) ) otherlv_8= ';' )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==25) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // InternalTomMapping.g:571:4: otherlv_6= 'factory' ( (lv_packageFactoryName_7_0= RULE_STRING ) ) otherlv_8= ';'
                    {
                    otherlv_6=(Token)match(input,25,FOLLOW_6); 

                    				newLeafNode(otherlv_6, grammarAccess.getInstanceMappingAccess().getFactoryKeyword_4_0());
                    			
                    // InternalTomMapping.g:575:4: ( (lv_packageFactoryName_7_0= RULE_STRING ) )
                    // InternalTomMapping.g:576:5: (lv_packageFactoryName_7_0= RULE_STRING )
                    {
                    // InternalTomMapping.g:576:5: (lv_packageFactoryName_7_0= RULE_STRING )
                    // InternalTomMapping.g:577:6: lv_packageFactoryName_7_0= RULE_STRING
                    {
                    lv_packageFactoryName_7_0=(Token)match(input,RULE_STRING,FOLLOW_4); 

                    						newLeafNode(lv_packageFactoryName_7_0, grammarAccess.getInstanceMappingAccess().getPackageFactoryNameSTRINGTerminalRuleCall_4_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getInstanceMappingRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"packageFactoryName",
                    							lv_packageFactoryName_7_0,
                    							"tom.mapping.dsl.TomMapping.STRING");
                    					

                    }


                    }

                    otherlv_8=(Token)match(input,12,FOLLOW_16); 

                    				newLeafNode(otherlv_8, grammarAccess.getInstanceMappingAccess().getSemicolonKeyword_4_2());
                    			

                    }
                    break;

            }

            otherlv_9=(Token)match(input,21,FOLLOW_2); 

            			newLeafNode(otherlv_9, grammarAccess.getInstanceMappingAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInstanceMapping"


    // $ANTLR start "entryRuleModule"
    // InternalTomMapping.g:606:1: entryRuleModule returns [EObject current=null] : iv_ruleModule= ruleModule EOF ;
    public final EObject entryRuleModule() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleModule = null;


        try {
            // InternalTomMapping.g:606:47: (iv_ruleModule= ruleModule EOF )
            // InternalTomMapping.g:607:2: iv_ruleModule= ruleModule EOF
            {
             newCompositeNode(grammarAccess.getModuleRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleModule=ruleModule();

            state._fsp--;

             current =iv_ruleModule; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleModule"


    // $ANTLR start "ruleModule"
    // InternalTomMapping.g:613:1: ruleModule returns [EObject current=null] : (otherlv_0= 'module' ( (lv_name_1_0= ruleSN ) ) otherlv_2= '{' (otherlv_3= 'operators' otherlv_4= '{' ( (lv_operators_5_0= ruleOperator ) ) (otherlv_6= ';' ( (lv_operators_7_0= ruleOperator ) ) )* otherlv_8= ';' otherlv_9= '}' )? otherlv_10= '}' ) ;
    public final EObject ruleModule() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        EObject lv_operators_5_0 = null;

        EObject lv_operators_7_0 = null;



        	enterRule();

        try {
            // InternalTomMapping.g:619:2: ( (otherlv_0= 'module' ( (lv_name_1_0= ruleSN ) ) otherlv_2= '{' (otherlv_3= 'operators' otherlv_4= '{' ( (lv_operators_5_0= ruleOperator ) ) (otherlv_6= ';' ( (lv_operators_7_0= ruleOperator ) ) )* otherlv_8= ';' otherlv_9= '}' )? otherlv_10= '}' ) )
            // InternalTomMapping.g:620:2: (otherlv_0= 'module' ( (lv_name_1_0= ruleSN ) ) otherlv_2= '{' (otherlv_3= 'operators' otherlv_4= '{' ( (lv_operators_5_0= ruleOperator ) ) (otherlv_6= ';' ( (lv_operators_7_0= ruleOperator ) ) )* otherlv_8= ';' otherlv_9= '}' )? otherlv_10= '}' )
            {
            // InternalTomMapping.g:620:2: (otherlv_0= 'module' ( (lv_name_1_0= ruleSN ) ) otherlv_2= '{' (otherlv_3= 'operators' otherlv_4= '{' ( (lv_operators_5_0= ruleOperator ) ) (otherlv_6= ';' ( (lv_operators_7_0= ruleOperator ) ) )* otherlv_8= ';' otherlv_9= '}' )? otherlv_10= '}' )
            // InternalTomMapping.g:621:3: otherlv_0= 'module' ( (lv_name_1_0= ruleSN ) ) otherlv_2= '{' (otherlv_3= 'operators' otherlv_4= '{' ( (lv_operators_5_0= ruleOperator ) ) (otherlv_6= ';' ( (lv_operators_7_0= ruleOperator ) ) )* otherlv_8= ';' otherlv_9= '}' )? otherlv_10= '}'
            {
            otherlv_0=(Token)match(input,26,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getModuleAccess().getModuleKeyword_0());
            		
            // InternalTomMapping.g:625:3: ( (lv_name_1_0= ruleSN ) )
            // InternalTomMapping.g:626:4: (lv_name_1_0= ruleSN )
            {
            // InternalTomMapping.g:626:4: (lv_name_1_0= ruleSN )
            // InternalTomMapping.g:627:5: lv_name_1_0= ruleSN
            {

            					newCompositeNode(grammarAccess.getModuleAccess().getNameSNParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_12);
            lv_name_1_0=ruleSN();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getModuleRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"tom.mapping.dsl.TomMapping.SN");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,18,FOLLOW_21); 

            			newLeafNode(otherlv_2, grammarAccess.getModuleAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalTomMapping.g:648:3: (otherlv_3= 'operators' otherlv_4= '{' ( (lv_operators_5_0= ruleOperator ) ) (otherlv_6= ';' ( (lv_operators_7_0= ruleOperator ) ) )* otherlv_8= ';' otherlv_9= '}' )?
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==23) ) {
                alt19=1;
            }
            switch (alt19) {
                case 1 :
                    // InternalTomMapping.g:649:4: otherlv_3= 'operators' otherlv_4= '{' ( (lv_operators_5_0= ruleOperator ) ) (otherlv_6= ';' ( (lv_operators_7_0= ruleOperator ) ) )* otherlv_8= ';' otherlv_9= '}'
                    {
                    otherlv_3=(Token)match(input,23,FOLLOW_12); 

                    				newLeafNode(otherlv_3, grammarAccess.getModuleAccess().getOperatorsKeyword_3_0());
                    			
                    otherlv_4=(Token)match(input,18,FOLLOW_18); 

                    				newLeafNode(otherlv_4, grammarAccess.getModuleAccess().getLeftCurlyBracketKeyword_3_1());
                    			
                    // InternalTomMapping.g:657:4: ( (lv_operators_5_0= ruleOperator ) )
                    // InternalTomMapping.g:658:5: (lv_operators_5_0= ruleOperator )
                    {
                    // InternalTomMapping.g:658:5: (lv_operators_5_0= ruleOperator )
                    // InternalTomMapping.g:659:6: lv_operators_5_0= ruleOperator
                    {

                    						newCompositeNode(grammarAccess.getModuleAccess().getOperatorsOperatorParserRuleCall_3_2_0());
                    					
                    pushFollow(FOLLOW_4);
                    lv_operators_5_0=ruleOperator();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getModuleRule());
                    						}
                    						add(
                    							current,
                    							"operators",
                    							lv_operators_5_0,
                    							"tom.mapping.dsl.TomMapping.Operator");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalTomMapping.g:676:4: (otherlv_6= ';' ( (lv_operators_7_0= ruleOperator ) ) )*
                    loop18:
                    do {
                        int alt18=2;
                        int LA18_0 = input.LA(1);

                        if ( (LA18_0==12) ) {
                            int LA18_1 = input.LA(2);

                            if ( (LA18_1==30||LA18_1==34) ) {
                                alt18=1;
                            }


                        }


                        switch (alt18) {
                    	case 1 :
                    	    // InternalTomMapping.g:677:5: otherlv_6= ';' ( (lv_operators_7_0= ruleOperator ) )
                    	    {
                    	    otherlv_6=(Token)match(input,12,FOLLOW_18); 

                    	    					newLeafNode(otherlv_6, grammarAccess.getModuleAccess().getSemicolonKeyword_3_3_0());
                    	    				
                    	    // InternalTomMapping.g:681:5: ( (lv_operators_7_0= ruleOperator ) )
                    	    // InternalTomMapping.g:682:6: (lv_operators_7_0= ruleOperator )
                    	    {
                    	    // InternalTomMapping.g:682:6: (lv_operators_7_0= ruleOperator )
                    	    // InternalTomMapping.g:683:7: lv_operators_7_0= ruleOperator
                    	    {

                    	    							newCompositeNode(grammarAccess.getModuleAccess().getOperatorsOperatorParserRuleCall_3_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_4);
                    	    lv_operators_7_0=ruleOperator();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getModuleRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"operators",
                    	    								lv_operators_7_0,
                    	    								"tom.mapping.dsl.TomMapping.Operator");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop18;
                        }
                    } while (true);

                    otherlv_8=(Token)match(input,12,FOLLOW_16); 

                    				newLeafNode(otherlv_8, grammarAccess.getModuleAccess().getSemicolonKeyword_3_4());
                    			
                    otherlv_9=(Token)match(input,21,FOLLOW_16); 

                    				newLeafNode(otherlv_9, grammarAccess.getModuleAccess().getRightCurlyBracketKeyword_3_5());
                    			

                    }
                    break;

            }

            otherlv_10=(Token)match(input,21,FOLLOW_2); 

            			newLeafNode(otherlv_10, grammarAccess.getModuleAccess().getRightCurlyBracketKeyword_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleModule"


    // $ANTLR start "entryRuleOperator"
    // InternalTomMapping.g:718:1: entryRuleOperator returns [EObject current=null] : iv_ruleOperator= ruleOperator EOF ;
    public final EObject entryRuleOperator() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOperator = null;


        try {
            // InternalTomMapping.g:718:49: (iv_ruleOperator= ruleOperator EOF )
            // InternalTomMapping.g:719:2: iv_ruleOperator= ruleOperator EOF
            {
             newCompositeNode(grammarAccess.getOperatorRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOperator=ruleOperator();

            state._fsp--;

             current =iv_ruleOperator; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOperator"


    // $ANTLR start "ruleOperator"
    // InternalTomMapping.g:725:1: ruleOperator returns [EObject current=null] : (this_ClassOperator_0= ruleClassOperator | this_ClassOperatorWithExceptions_1= ruleClassOperatorWithExceptions | this_UserOperator_2= ruleUserOperator | this_AliasOperator_3= ruleAliasOperator ) ;
    public final EObject ruleOperator() throws RecognitionException {
        EObject current = null;

        EObject this_ClassOperator_0 = null;

        EObject this_ClassOperatorWithExceptions_1 = null;

        EObject this_UserOperator_2 = null;

        EObject this_AliasOperator_3 = null;



        	enterRule();

        try {
            // InternalTomMapping.g:731:2: ( (this_ClassOperator_0= ruleClassOperator | this_ClassOperatorWithExceptions_1= ruleClassOperatorWithExceptions | this_UserOperator_2= ruleUserOperator | this_AliasOperator_3= ruleAliasOperator ) )
            // InternalTomMapping.g:732:2: (this_ClassOperator_0= ruleClassOperator | this_ClassOperatorWithExceptions_1= ruleClassOperatorWithExceptions | this_UserOperator_2= ruleUserOperator | this_AliasOperator_3= ruleAliasOperator )
            {
            // InternalTomMapping.g:732:2: (this_ClassOperator_0= ruleClassOperator | this_ClassOperatorWithExceptions_1= ruleClassOperatorWithExceptions | this_UserOperator_2= ruleUserOperator | this_AliasOperator_3= ruleAliasOperator )
            int alt20=4;
            alt20 = dfa20.predict(input);
            switch (alt20) {
                case 1 :
                    // InternalTomMapping.g:733:3: this_ClassOperator_0= ruleClassOperator
                    {

                    			newCompositeNode(grammarAccess.getOperatorAccess().getClassOperatorParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_ClassOperator_0=ruleClassOperator();

                    state._fsp--;


                    			current = this_ClassOperator_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalTomMapping.g:742:3: this_ClassOperatorWithExceptions_1= ruleClassOperatorWithExceptions
                    {

                    			newCompositeNode(grammarAccess.getOperatorAccess().getClassOperatorWithExceptionsParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_ClassOperatorWithExceptions_1=ruleClassOperatorWithExceptions();

                    state._fsp--;


                    			current = this_ClassOperatorWithExceptions_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalTomMapping.g:751:3: this_UserOperator_2= ruleUserOperator
                    {

                    			newCompositeNode(grammarAccess.getOperatorAccess().getUserOperatorParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_UserOperator_2=ruleUserOperator();

                    state._fsp--;


                    			current = this_UserOperator_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 4 :
                    // InternalTomMapping.g:760:3: this_AliasOperator_3= ruleAliasOperator
                    {

                    			newCompositeNode(grammarAccess.getOperatorAccess().getAliasOperatorParserRuleCall_3());
                    		
                    pushFollow(FOLLOW_2);
                    this_AliasOperator_3=ruleAliasOperator();

                    state._fsp--;


                    			current = this_AliasOperator_3;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOperator"


    // $ANTLR start "entryRuleImport"
    // InternalTomMapping.g:772:1: entryRuleImport returns [EObject current=null] : iv_ruleImport= ruleImport EOF ;
    public final EObject entryRuleImport() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImport = null;


        try {
            // InternalTomMapping.g:772:47: (iv_ruleImport= ruleImport EOF )
            // InternalTomMapping.g:773:2: iv_ruleImport= ruleImport EOF
            {
             newCompositeNode(grammarAccess.getImportRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleImport=ruleImport();

            state._fsp--;

             current =iv_ruleImport; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImport"


    // $ANTLR start "ruleImport"
    // InternalTomMapping.g:779:1: ruleImport returns [EObject current=null] : (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) otherlv_2= ';' ) ;
    public final EObject ruleImport() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_importURI_1_0=null;
        Token otherlv_2=null;


        	enterRule();

        try {
            // InternalTomMapping.g:785:2: ( (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) otherlv_2= ';' ) )
            // InternalTomMapping.g:786:2: (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) otherlv_2= ';' )
            {
            // InternalTomMapping.g:786:2: (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) otherlv_2= ';' )
            // InternalTomMapping.g:787:3: otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) otherlv_2= ';'
            {
            otherlv_0=(Token)match(input,27,FOLLOW_6); 

            			newLeafNode(otherlv_0, grammarAccess.getImportAccess().getImportKeyword_0());
            		
            // InternalTomMapping.g:791:3: ( (lv_importURI_1_0= RULE_STRING ) )
            // InternalTomMapping.g:792:4: (lv_importURI_1_0= RULE_STRING )
            {
            // InternalTomMapping.g:792:4: (lv_importURI_1_0= RULE_STRING )
            // InternalTomMapping.g:793:5: lv_importURI_1_0= RULE_STRING
            {
            lv_importURI_1_0=(Token)match(input,RULE_STRING,FOLLOW_4); 

            					newLeafNode(lv_importURI_1_0, grammarAccess.getImportAccess().getImportURISTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getImportRule());
            					}
            					setWithLastConsumed(
            						current,
            						"importURI",
            						lv_importURI_1_0,
            						"tom.mapping.dsl.TomMapping.STRING");
            				

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_2); 

            			newLeafNode(otherlv_2, grammarAccess.getImportAccess().getSemicolonKeyword_2());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImport"


    // $ANTLR start "entryRuleTerminal"
    // InternalTomMapping.g:817:1: entryRuleTerminal returns [EObject current=null] : iv_ruleTerminal= ruleTerminal EOF ;
    public final EObject entryRuleTerminal() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTerminal = null;


        try {
            // InternalTomMapping.g:817:49: (iv_ruleTerminal= ruleTerminal EOF )
            // InternalTomMapping.g:818:2: iv_ruleTerminal= ruleTerminal EOF
            {
             newCompositeNode(grammarAccess.getTerminalRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTerminal=ruleTerminal();

            state._fsp--;

             current =iv_ruleTerminal; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTerminal"


    // $ANTLR start "ruleTerminal"
    // InternalTomMapping.g:824:1: ruleTerminal returns [EObject current=null] : ( ( (lv_name_0_0= ruleSN ) ) otherlv_1= ':' ( ( ruleFQN ) ) ( (lv_many_3_0= '[]' ) )? ) ;
    public final EObject ruleTerminal() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_many_3_0=null;
        AntlrDatatypeRuleToken lv_name_0_0 = null;



        	enterRule();

        try {
            // InternalTomMapping.g:830:2: ( ( ( (lv_name_0_0= ruleSN ) ) otherlv_1= ':' ( ( ruleFQN ) ) ( (lv_many_3_0= '[]' ) )? ) )
            // InternalTomMapping.g:831:2: ( ( (lv_name_0_0= ruleSN ) ) otherlv_1= ':' ( ( ruleFQN ) ) ( (lv_many_3_0= '[]' ) )? )
            {
            // InternalTomMapping.g:831:2: ( ( (lv_name_0_0= ruleSN ) ) otherlv_1= ':' ( ( ruleFQN ) ) ( (lv_many_3_0= '[]' ) )? )
            // InternalTomMapping.g:832:3: ( (lv_name_0_0= ruleSN ) ) otherlv_1= ':' ( ( ruleFQN ) ) ( (lv_many_3_0= '[]' ) )?
            {
            // InternalTomMapping.g:832:3: ( (lv_name_0_0= ruleSN ) )
            // InternalTomMapping.g:833:4: (lv_name_0_0= ruleSN )
            {
            // InternalTomMapping.g:833:4: (lv_name_0_0= ruleSN )
            // InternalTomMapping.g:834:5: lv_name_0_0= ruleSN
            {

            					newCompositeNode(grammarAccess.getTerminalAccess().getNameSNParserRuleCall_0_0());
            				
            pushFollow(FOLLOW_22);
            lv_name_0_0=ruleSN();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getTerminalRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_0_0,
            						"tom.mapping.dsl.TomMapping.SN");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_1=(Token)match(input,28,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getTerminalAccess().getColonKeyword_1());
            		
            // InternalTomMapping.g:855:3: ( ( ruleFQN ) )
            // InternalTomMapping.g:856:4: ( ruleFQN )
            {
            // InternalTomMapping.g:856:4: ( ruleFQN )
            // InternalTomMapping.g:857:5: ruleFQN
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getTerminalRule());
            					}
            				

            					newCompositeNode(grammarAccess.getTerminalAccess().getClassEClassCrossReference_2_0());
            				
            pushFollow(FOLLOW_23);
            ruleFQN();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTomMapping.g:871:3: ( (lv_many_3_0= '[]' ) )?
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==29) ) {
                alt21=1;
            }
            switch (alt21) {
                case 1 :
                    // InternalTomMapping.g:872:4: (lv_many_3_0= '[]' )
                    {
                    // InternalTomMapping.g:872:4: (lv_many_3_0= '[]' )
                    // InternalTomMapping.g:873:5: lv_many_3_0= '[]'
                    {
                    lv_many_3_0=(Token)match(input,29,FOLLOW_2); 

                    					newLeafNode(lv_many_3_0, grammarAccess.getTerminalAccess().getManyLeftSquareBracketRightSquareBracketKeyword_3_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getTerminalRule());
                    					}
                    					setWithLastConsumed(current, "many", true, "[]");
                    				

                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTerminal"


    // $ANTLR start "entryRuleAliasOperator"
    // InternalTomMapping.g:889:1: entryRuleAliasOperator returns [EObject current=null] : iv_ruleAliasOperator= ruleAliasOperator EOF ;
    public final EObject entryRuleAliasOperator() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAliasOperator = null;


        try {
            // InternalTomMapping.g:889:54: (iv_ruleAliasOperator= ruleAliasOperator EOF )
            // InternalTomMapping.g:890:2: iv_ruleAliasOperator= ruleAliasOperator EOF
            {
             newCompositeNode(grammarAccess.getAliasOperatorRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAliasOperator=ruleAliasOperator();

            state._fsp--;

             current =iv_ruleAliasOperator; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAliasOperator"


    // $ANTLR start "ruleAliasOperator"
    // InternalTomMapping.g:896:1: ruleAliasOperator returns [EObject current=null] : (otherlv_0= 'alias' ( (lv_name_1_0= ruleSN ) ) otherlv_2= '::' ( (otherlv_3= RULE_ID ) ) otherlv_4= '(' ( (lv_nodes_5_0= ruleAliasNode ) ) (otherlv_6= ',' ( (lv_nodes_7_0= ruleAliasNode ) ) )* otherlv_8= ')' ) ;
    public final EObject ruleAliasOperator() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        EObject lv_nodes_5_0 = null;

        EObject lv_nodes_7_0 = null;



        	enterRule();

        try {
            // InternalTomMapping.g:902:2: ( (otherlv_0= 'alias' ( (lv_name_1_0= ruleSN ) ) otherlv_2= '::' ( (otherlv_3= RULE_ID ) ) otherlv_4= '(' ( (lv_nodes_5_0= ruleAliasNode ) ) (otherlv_6= ',' ( (lv_nodes_7_0= ruleAliasNode ) ) )* otherlv_8= ')' ) )
            // InternalTomMapping.g:903:2: (otherlv_0= 'alias' ( (lv_name_1_0= ruleSN ) ) otherlv_2= '::' ( (otherlv_3= RULE_ID ) ) otherlv_4= '(' ( (lv_nodes_5_0= ruleAliasNode ) ) (otherlv_6= ',' ( (lv_nodes_7_0= ruleAliasNode ) ) )* otherlv_8= ')' )
            {
            // InternalTomMapping.g:903:2: (otherlv_0= 'alias' ( (lv_name_1_0= ruleSN ) ) otherlv_2= '::' ( (otherlv_3= RULE_ID ) ) otherlv_4= '(' ( (lv_nodes_5_0= ruleAliasNode ) ) (otherlv_6= ',' ( (lv_nodes_7_0= ruleAliasNode ) ) )* otherlv_8= ')' )
            // InternalTomMapping.g:904:3: otherlv_0= 'alias' ( (lv_name_1_0= ruleSN ) ) otherlv_2= '::' ( (otherlv_3= RULE_ID ) ) otherlv_4= '(' ( (lv_nodes_5_0= ruleAliasNode ) ) (otherlv_6= ',' ( (lv_nodes_7_0= ruleAliasNode ) ) )* otherlv_8= ')'
            {
            otherlv_0=(Token)match(input,30,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getAliasOperatorAccess().getAliasKeyword_0());
            		
            // InternalTomMapping.g:908:3: ( (lv_name_1_0= ruleSN ) )
            // InternalTomMapping.g:909:4: (lv_name_1_0= ruleSN )
            {
            // InternalTomMapping.g:909:4: (lv_name_1_0= ruleSN )
            // InternalTomMapping.g:910:5: lv_name_1_0= ruleSN
            {

            					newCompositeNode(grammarAccess.getAliasOperatorAccess().getNameSNParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_24);
            lv_name_1_0=ruleSN();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAliasOperatorRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"tom.mapping.dsl.TomMapping.SN");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,31,FOLLOW_3); 

            			newLeafNode(otherlv_2, grammarAccess.getAliasOperatorAccess().getColonColonKeyword_2());
            		
            // InternalTomMapping.g:931:3: ( (otherlv_3= RULE_ID ) )
            // InternalTomMapping.g:932:4: (otherlv_3= RULE_ID )
            {
            // InternalTomMapping.g:932:4: (otherlv_3= RULE_ID )
            // InternalTomMapping.g:933:5: otherlv_3= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAliasOperatorRule());
            					}
            				
            otherlv_3=(Token)match(input,RULE_ID,FOLLOW_25); 

            					newLeafNode(otherlv_3, grammarAccess.getAliasOperatorAccess().getOpOperatorCrossReference_3_0());
            				

            }


            }

            otherlv_4=(Token)match(input,32,FOLLOW_3); 

            			newLeafNode(otherlv_4, grammarAccess.getAliasOperatorAccess().getLeftParenthesisKeyword_4());
            		
            // InternalTomMapping.g:948:3: ( (lv_nodes_5_0= ruleAliasNode ) )
            // InternalTomMapping.g:949:4: (lv_nodes_5_0= ruleAliasNode )
            {
            // InternalTomMapping.g:949:4: (lv_nodes_5_0= ruleAliasNode )
            // InternalTomMapping.g:950:5: lv_nodes_5_0= ruleAliasNode
            {

            					newCompositeNode(grammarAccess.getAliasOperatorAccess().getNodesAliasNodeParserRuleCall_5_0());
            				
            pushFollow(FOLLOW_26);
            lv_nodes_5_0=ruleAliasNode();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAliasOperatorRule());
            					}
            					add(
            						current,
            						"nodes",
            						lv_nodes_5_0,
            						"tom.mapping.dsl.TomMapping.AliasNode");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTomMapping.g:967:3: (otherlv_6= ',' ( (lv_nodes_7_0= ruleAliasNode ) ) )*
            loop22:
            do {
                int alt22=2;
                int LA22_0 = input.LA(1);

                if ( (LA22_0==20) ) {
                    alt22=1;
                }


                switch (alt22) {
            	case 1 :
            	    // InternalTomMapping.g:968:4: otherlv_6= ',' ( (lv_nodes_7_0= ruleAliasNode ) )
            	    {
            	    otherlv_6=(Token)match(input,20,FOLLOW_3); 

            	    				newLeafNode(otherlv_6, grammarAccess.getAliasOperatorAccess().getCommaKeyword_6_0());
            	    			
            	    // InternalTomMapping.g:972:4: ( (lv_nodes_7_0= ruleAliasNode ) )
            	    // InternalTomMapping.g:973:5: (lv_nodes_7_0= ruleAliasNode )
            	    {
            	    // InternalTomMapping.g:973:5: (lv_nodes_7_0= ruleAliasNode )
            	    // InternalTomMapping.g:974:6: lv_nodes_7_0= ruleAliasNode
            	    {

            	    						newCompositeNode(grammarAccess.getAliasOperatorAccess().getNodesAliasNodeParserRuleCall_6_1_0());
            	    					
            	    pushFollow(FOLLOW_26);
            	    lv_nodes_7_0=ruleAliasNode();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getAliasOperatorRule());
            	    						}
            	    						add(
            	    							current,
            	    							"nodes",
            	    							lv_nodes_7_0,
            	    							"tom.mapping.dsl.TomMapping.AliasNode");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop22;
                }
            } while (true);

            otherlv_8=(Token)match(input,33,FOLLOW_2); 

            			newLeafNode(otherlv_8, grammarAccess.getAliasOperatorAccess().getRightParenthesisKeyword_7());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAliasOperator"


    // $ANTLR start "entryRuleAliasNode"
    // InternalTomMapping.g:1000:1: entryRuleAliasNode returns [EObject current=null] : iv_ruleAliasNode= ruleAliasNode EOF ;
    public final EObject entryRuleAliasNode() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAliasNode = null;


        try {
            // InternalTomMapping.g:1000:50: (iv_ruleAliasNode= ruleAliasNode EOF )
            // InternalTomMapping.g:1001:2: iv_ruleAliasNode= ruleAliasNode EOF
            {
             newCompositeNode(grammarAccess.getAliasNodeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAliasNode=ruleAliasNode();

            state._fsp--;

             current =iv_ruleAliasNode; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAliasNode"


    // $ANTLR start "ruleAliasNode"
    // InternalTomMapping.g:1007:1: ruleAliasNode returns [EObject current=null] : (this_FeatureNode_0= ruleFeatureNode | this_OperatorNode_1= ruleOperatorNode ) ;
    public final EObject ruleAliasNode() throws RecognitionException {
        EObject current = null;

        EObject this_FeatureNode_0 = null;

        EObject this_OperatorNode_1 = null;



        	enterRule();

        try {
            // InternalTomMapping.g:1013:2: ( (this_FeatureNode_0= ruleFeatureNode | this_OperatorNode_1= ruleOperatorNode ) )
            // InternalTomMapping.g:1014:2: (this_FeatureNode_0= ruleFeatureNode | this_OperatorNode_1= ruleOperatorNode )
            {
            // InternalTomMapping.g:1014:2: (this_FeatureNode_0= ruleFeatureNode | this_OperatorNode_1= ruleOperatorNode )
            int alt23=2;
            int LA23_0 = input.LA(1);

            if ( (LA23_0==RULE_ID) ) {
                int LA23_1 = input.LA(2);

                if ( (LA23_1==32) ) {
                    alt23=2;
                }
                else if ( (LA23_1==EOF||LA23_1==20||LA23_1==33) ) {
                    alt23=1;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 23, 1, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 23, 0, input);

                throw nvae;
            }
            switch (alt23) {
                case 1 :
                    // InternalTomMapping.g:1015:3: this_FeatureNode_0= ruleFeatureNode
                    {

                    			newCompositeNode(grammarAccess.getAliasNodeAccess().getFeatureNodeParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_FeatureNode_0=ruleFeatureNode();

                    state._fsp--;


                    			current = this_FeatureNode_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalTomMapping.g:1024:3: this_OperatorNode_1= ruleOperatorNode
                    {

                    			newCompositeNode(grammarAccess.getAliasNodeAccess().getOperatorNodeParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_OperatorNode_1=ruleOperatorNode();

                    state._fsp--;


                    			current = this_OperatorNode_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAliasNode"


    // $ANTLR start "entryRuleFeatureNode"
    // InternalTomMapping.g:1036:1: entryRuleFeatureNode returns [EObject current=null] : iv_ruleFeatureNode= ruleFeatureNode EOF ;
    public final EObject entryRuleFeatureNode() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFeatureNode = null;


        try {
            // InternalTomMapping.g:1036:52: (iv_ruleFeatureNode= ruleFeatureNode EOF )
            // InternalTomMapping.g:1037:2: iv_ruleFeatureNode= ruleFeatureNode EOF
            {
             newCompositeNode(grammarAccess.getFeatureNodeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFeatureNode=ruleFeatureNode();

            state._fsp--;

             current =iv_ruleFeatureNode; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFeatureNode"


    // $ANTLR start "ruleFeatureNode"
    // InternalTomMapping.g:1043:1: ruleFeatureNode returns [EObject current=null] : ( (lv_feature_0_0= ruleSN ) ) ;
    public final EObject ruleFeatureNode() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_feature_0_0 = null;



        	enterRule();

        try {
            // InternalTomMapping.g:1049:2: ( ( (lv_feature_0_0= ruleSN ) ) )
            // InternalTomMapping.g:1050:2: ( (lv_feature_0_0= ruleSN ) )
            {
            // InternalTomMapping.g:1050:2: ( (lv_feature_0_0= ruleSN ) )
            // InternalTomMapping.g:1051:3: (lv_feature_0_0= ruleSN )
            {
            // InternalTomMapping.g:1051:3: (lv_feature_0_0= ruleSN )
            // InternalTomMapping.g:1052:4: lv_feature_0_0= ruleSN
            {

            				newCompositeNode(grammarAccess.getFeatureNodeAccess().getFeatureSNParserRuleCall_0());
            			
            pushFollow(FOLLOW_2);
            lv_feature_0_0=ruleSN();

            state._fsp--;


            				if (current==null) {
            					current = createModelElementForParent(grammarAccess.getFeatureNodeRule());
            				}
            				set(
            					current,
            					"feature",
            					lv_feature_0_0,
            					"tom.mapping.dsl.TomMapping.SN");
            				afterParserOrEnumRuleCall();
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFeatureNode"


    // $ANTLR start "entryRuleOperatorNode"
    // InternalTomMapping.g:1072:1: entryRuleOperatorNode returns [EObject current=null] : iv_ruleOperatorNode= ruleOperatorNode EOF ;
    public final EObject entryRuleOperatorNode() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOperatorNode = null;


        try {
            // InternalTomMapping.g:1072:53: (iv_ruleOperatorNode= ruleOperatorNode EOF )
            // InternalTomMapping.g:1073:2: iv_ruleOperatorNode= ruleOperatorNode EOF
            {
             newCompositeNode(grammarAccess.getOperatorNodeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOperatorNode=ruleOperatorNode();

            state._fsp--;

             current =iv_ruleOperatorNode; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOperatorNode"


    // $ANTLR start "ruleOperatorNode"
    // InternalTomMapping.g:1079:1: ruleOperatorNode returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '(' ( (lv_nodes_2_0= ruleAliasNode ) ) (otherlv_3= ',' ( (lv_nodes_4_0= ruleAliasNode ) ) )* otherlv_5= ')' ) ;
    public final EObject ruleOperatorNode() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_nodes_2_0 = null;

        EObject lv_nodes_4_0 = null;



        	enterRule();

        try {
            // InternalTomMapping.g:1085:2: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '(' ( (lv_nodes_2_0= ruleAliasNode ) ) (otherlv_3= ',' ( (lv_nodes_4_0= ruleAliasNode ) ) )* otherlv_5= ')' ) )
            // InternalTomMapping.g:1086:2: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '(' ( (lv_nodes_2_0= ruleAliasNode ) ) (otherlv_3= ',' ( (lv_nodes_4_0= ruleAliasNode ) ) )* otherlv_5= ')' )
            {
            // InternalTomMapping.g:1086:2: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '(' ( (lv_nodes_2_0= ruleAliasNode ) ) (otherlv_3= ',' ( (lv_nodes_4_0= ruleAliasNode ) ) )* otherlv_5= ')' )
            // InternalTomMapping.g:1087:3: ( (otherlv_0= RULE_ID ) ) otherlv_1= '(' ( (lv_nodes_2_0= ruleAliasNode ) ) (otherlv_3= ',' ( (lv_nodes_4_0= ruleAliasNode ) ) )* otherlv_5= ')'
            {
            // InternalTomMapping.g:1087:3: ( (otherlv_0= RULE_ID ) )
            // InternalTomMapping.g:1088:4: (otherlv_0= RULE_ID )
            {
            // InternalTomMapping.g:1088:4: (otherlv_0= RULE_ID )
            // InternalTomMapping.g:1089:5: otherlv_0= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getOperatorNodeRule());
            					}
            				
            otherlv_0=(Token)match(input,RULE_ID,FOLLOW_25); 

            					newLeafNode(otherlv_0, grammarAccess.getOperatorNodeAccess().getOpOperatorCrossReference_0_0());
            				

            }


            }

            otherlv_1=(Token)match(input,32,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getOperatorNodeAccess().getLeftParenthesisKeyword_1());
            		
            // InternalTomMapping.g:1104:3: ( (lv_nodes_2_0= ruleAliasNode ) )
            // InternalTomMapping.g:1105:4: (lv_nodes_2_0= ruleAliasNode )
            {
            // InternalTomMapping.g:1105:4: (lv_nodes_2_0= ruleAliasNode )
            // InternalTomMapping.g:1106:5: lv_nodes_2_0= ruleAliasNode
            {

            					newCompositeNode(grammarAccess.getOperatorNodeAccess().getNodesAliasNodeParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_26);
            lv_nodes_2_0=ruleAliasNode();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getOperatorNodeRule());
            					}
            					add(
            						current,
            						"nodes",
            						lv_nodes_2_0,
            						"tom.mapping.dsl.TomMapping.AliasNode");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTomMapping.g:1123:3: (otherlv_3= ',' ( (lv_nodes_4_0= ruleAliasNode ) ) )*
            loop24:
            do {
                int alt24=2;
                int LA24_0 = input.LA(1);

                if ( (LA24_0==20) ) {
                    alt24=1;
                }


                switch (alt24) {
            	case 1 :
            	    // InternalTomMapping.g:1124:4: otherlv_3= ',' ( (lv_nodes_4_0= ruleAliasNode ) )
            	    {
            	    otherlv_3=(Token)match(input,20,FOLLOW_3); 

            	    				newLeafNode(otherlv_3, grammarAccess.getOperatorNodeAccess().getCommaKeyword_3_0());
            	    			
            	    // InternalTomMapping.g:1128:4: ( (lv_nodes_4_0= ruleAliasNode ) )
            	    // InternalTomMapping.g:1129:5: (lv_nodes_4_0= ruleAliasNode )
            	    {
            	    // InternalTomMapping.g:1129:5: (lv_nodes_4_0= ruleAliasNode )
            	    // InternalTomMapping.g:1130:6: lv_nodes_4_0= ruleAliasNode
            	    {

            	    						newCompositeNode(grammarAccess.getOperatorNodeAccess().getNodesAliasNodeParserRuleCall_3_1_0());
            	    					
            	    pushFollow(FOLLOW_26);
            	    lv_nodes_4_0=ruleAliasNode();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getOperatorNodeRule());
            	    						}
            	    						add(
            	    							current,
            	    							"nodes",
            	    							lv_nodes_4_0,
            	    							"tom.mapping.dsl.TomMapping.AliasNode");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop24;
                }
            } while (true);

            otherlv_5=(Token)match(input,33,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getOperatorNodeAccess().getRightParenthesisKeyword_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOperatorNode"


    // $ANTLR start "entryRuleClassOperator"
    // InternalTomMapping.g:1156:1: entryRuleClassOperator returns [EObject current=null] : iv_ruleClassOperator= ruleClassOperator EOF ;
    public final EObject entryRuleClassOperator() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleClassOperator = null;


        try {
            // InternalTomMapping.g:1156:54: (iv_ruleClassOperator= ruleClassOperator EOF )
            // InternalTomMapping.g:1157:2: iv_ruleClassOperator= ruleClassOperator EOF
            {
             newCompositeNode(grammarAccess.getClassOperatorRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleClassOperator=ruleClassOperator();

            state._fsp--;

             current =iv_ruleClassOperator; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleClassOperator"


    // $ANTLR start "ruleClassOperator"
    // InternalTomMapping.g:1163:1: ruleClassOperator returns [EObject current=null] : (otherlv_0= 'op' ( (lv_name_1_0= ruleSN ) ) otherlv_2= '::' ( ( ruleFQN ) ) ) ;
    public final EObject ruleClassOperator() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;



        	enterRule();

        try {
            // InternalTomMapping.g:1169:2: ( (otherlv_0= 'op' ( (lv_name_1_0= ruleSN ) ) otherlv_2= '::' ( ( ruleFQN ) ) ) )
            // InternalTomMapping.g:1170:2: (otherlv_0= 'op' ( (lv_name_1_0= ruleSN ) ) otherlv_2= '::' ( ( ruleFQN ) ) )
            {
            // InternalTomMapping.g:1170:2: (otherlv_0= 'op' ( (lv_name_1_0= ruleSN ) ) otherlv_2= '::' ( ( ruleFQN ) ) )
            // InternalTomMapping.g:1171:3: otherlv_0= 'op' ( (lv_name_1_0= ruleSN ) ) otherlv_2= '::' ( ( ruleFQN ) )
            {
            otherlv_0=(Token)match(input,34,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getClassOperatorAccess().getOpKeyword_0());
            		
            // InternalTomMapping.g:1175:3: ( (lv_name_1_0= ruleSN ) )
            // InternalTomMapping.g:1176:4: (lv_name_1_0= ruleSN )
            {
            // InternalTomMapping.g:1176:4: (lv_name_1_0= ruleSN )
            // InternalTomMapping.g:1177:5: lv_name_1_0= ruleSN
            {

            					newCompositeNode(grammarAccess.getClassOperatorAccess().getNameSNParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_24);
            lv_name_1_0=ruleSN();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getClassOperatorRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"tom.mapping.dsl.TomMapping.SN");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,31,FOLLOW_3); 

            			newLeafNode(otherlv_2, grammarAccess.getClassOperatorAccess().getColonColonKeyword_2());
            		
            // InternalTomMapping.g:1198:3: ( ( ruleFQN ) )
            // InternalTomMapping.g:1199:4: ( ruleFQN )
            {
            // InternalTomMapping.g:1199:4: ( ruleFQN )
            // InternalTomMapping.g:1200:5: ruleFQN
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getClassOperatorRule());
            					}
            				

            					newCompositeNode(grammarAccess.getClassOperatorAccess().getClassEClassCrossReference_3_0());
            				
            pushFollow(FOLLOW_2);
            ruleFQN();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleClassOperator"


    // $ANTLR start "entryRuleClassOperatorWithExceptions"
    // InternalTomMapping.g:1218:1: entryRuleClassOperatorWithExceptions returns [EObject current=null] : iv_ruleClassOperatorWithExceptions= ruleClassOperatorWithExceptions EOF ;
    public final EObject entryRuleClassOperatorWithExceptions() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleClassOperatorWithExceptions = null;


        try {
            // InternalTomMapping.g:1218:68: (iv_ruleClassOperatorWithExceptions= ruleClassOperatorWithExceptions EOF )
            // InternalTomMapping.g:1219:2: iv_ruleClassOperatorWithExceptions= ruleClassOperatorWithExceptions EOF
            {
             newCompositeNode(grammarAccess.getClassOperatorWithExceptionsRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleClassOperatorWithExceptions=ruleClassOperatorWithExceptions();

            state._fsp--;

             current =iv_ruleClassOperatorWithExceptions; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleClassOperatorWithExceptions"


    // $ANTLR start "ruleClassOperatorWithExceptions"
    // InternalTomMapping.g:1225:1: ruleClassOperatorWithExceptions returns [EObject current=null] : (otherlv_0= 'op' ( (lv_name_1_0= ruleSN ) ) otherlv_2= '::' ( ( ruleFQN ) ) otherlv_4= '(' ( (lv_parameters_5_0= ruleFeatureParameter ) ) (otherlv_6= ',' ( (lv_parameters_7_0= ruleFeatureParameter ) ) )* otherlv_8= ')' ) ;
    public final EObject ruleClassOperatorWithExceptions() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        EObject lv_parameters_5_0 = null;

        EObject lv_parameters_7_0 = null;



        	enterRule();

        try {
            // InternalTomMapping.g:1231:2: ( (otherlv_0= 'op' ( (lv_name_1_0= ruleSN ) ) otherlv_2= '::' ( ( ruleFQN ) ) otherlv_4= '(' ( (lv_parameters_5_0= ruleFeatureParameter ) ) (otherlv_6= ',' ( (lv_parameters_7_0= ruleFeatureParameter ) ) )* otherlv_8= ')' ) )
            // InternalTomMapping.g:1232:2: (otherlv_0= 'op' ( (lv_name_1_0= ruleSN ) ) otherlv_2= '::' ( ( ruleFQN ) ) otherlv_4= '(' ( (lv_parameters_5_0= ruleFeatureParameter ) ) (otherlv_6= ',' ( (lv_parameters_7_0= ruleFeatureParameter ) ) )* otherlv_8= ')' )
            {
            // InternalTomMapping.g:1232:2: (otherlv_0= 'op' ( (lv_name_1_0= ruleSN ) ) otherlv_2= '::' ( ( ruleFQN ) ) otherlv_4= '(' ( (lv_parameters_5_0= ruleFeatureParameter ) ) (otherlv_6= ',' ( (lv_parameters_7_0= ruleFeatureParameter ) ) )* otherlv_8= ')' )
            // InternalTomMapping.g:1233:3: otherlv_0= 'op' ( (lv_name_1_0= ruleSN ) ) otherlv_2= '::' ( ( ruleFQN ) ) otherlv_4= '(' ( (lv_parameters_5_0= ruleFeatureParameter ) ) (otherlv_6= ',' ( (lv_parameters_7_0= ruleFeatureParameter ) ) )* otherlv_8= ')'
            {
            otherlv_0=(Token)match(input,34,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getClassOperatorWithExceptionsAccess().getOpKeyword_0());
            		
            // InternalTomMapping.g:1237:3: ( (lv_name_1_0= ruleSN ) )
            // InternalTomMapping.g:1238:4: (lv_name_1_0= ruleSN )
            {
            // InternalTomMapping.g:1238:4: (lv_name_1_0= ruleSN )
            // InternalTomMapping.g:1239:5: lv_name_1_0= ruleSN
            {

            					newCompositeNode(grammarAccess.getClassOperatorWithExceptionsAccess().getNameSNParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_24);
            lv_name_1_0=ruleSN();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getClassOperatorWithExceptionsRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"tom.mapping.dsl.TomMapping.SN");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,31,FOLLOW_3); 

            			newLeafNode(otherlv_2, grammarAccess.getClassOperatorWithExceptionsAccess().getColonColonKeyword_2());
            		
            // InternalTomMapping.g:1260:3: ( ( ruleFQN ) )
            // InternalTomMapping.g:1261:4: ( ruleFQN )
            {
            // InternalTomMapping.g:1261:4: ( ruleFQN )
            // InternalTomMapping.g:1262:5: ruleFQN
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getClassOperatorWithExceptionsRule());
            					}
            				

            					newCompositeNode(grammarAccess.getClassOperatorWithExceptionsAccess().getClassEClassCrossReference_3_0());
            				
            pushFollow(FOLLOW_25);
            ruleFQN();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_4=(Token)match(input,32,FOLLOW_27); 

            			newLeafNode(otherlv_4, grammarAccess.getClassOperatorWithExceptionsAccess().getLeftParenthesisKeyword_4());
            		
            // InternalTomMapping.g:1280:3: ( (lv_parameters_5_0= ruleFeatureParameter ) )
            // InternalTomMapping.g:1281:4: (lv_parameters_5_0= ruleFeatureParameter )
            {
            // InternalTomMapping.g:1281:4: (lv_parameters_5_0= ruleFeatureParameter )
            // InternalTomMapping.g:1282:5: lv_parameters_5_0= ruleFeatureParameter
            {

            					newCompositeNode(grammarAccess.getClassOperatorWithExceptionsAccess().getParametersFeatureParameterParserRuleCall_5_0());
            				
            pushFollow(FOLLOW_26);
            lv_parameters_5_0=ruleFeatureParameter();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getClassOperatorWithExceptionsRule());
            					}
            					add(
            						current,
            						"parameters",
            						lv_parameters_5_0,
            						"tom.mapping.dsl.TomMapping.FeatureParameter");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTomMapping.g:1299:3: (otherlv_6= ',' ( (lv_parameters_7_0= ruleFeatureParameter ) ) )*
            loop25:
            do {
                int alt25=2;
                int LA25_0 = input.LA(1);

                if ( (LA25_0==20) ) {
                    alt25=1;
                }


                switch (alt25) {
            	case 1 :
            	    // InternalTomMapping.g:1300:4: otherlv_6= ',' ( (lv_parameters_7_0= ruleFeatureParameter ) )
            	    {
            	    otherlv_6=(Token)match(input,20,FOLLOW_27); 

            	    				newLeafNode(otherlv_6, grammarAccess.getClassOperatorWithExceptionsAccess().getCommaKeyword_6_0());
            	    			
            	    // InternalTomMapping.g:1304:4: ( (lv_parameters_7_0= ruleFeatureParameter ) )
            	    // InternalTomMapping.g:1305:5: (lv_parameters_7_0= ruleFeatureParameter )
            	    {
            	    // InternalTomMapping.g:1305:5: (lv_parameters_7_0= ruleFeatureParameter )
            	    // InternalTomMapping.g:1306:6: lv_parameters_7_0= ruleFeatureParameter
            	    {

            	    						newCompositeNode(grammarAccess.getClassOperatorWithExceptionsAccess().getParametersFeatureParameterParserRuleCall_6_1_0());
            	    					
            	    pushFollow(FOLLOW_26);
            	    lv_parameters_7_0=ruleFeatureParameter();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getClassOperatorWithExceptionsRule());
            	    						}
            	    						add(
            	    							current,
            	    							"parameters",
            	    							lv_parameters_7_0,
            	    							"tom.mapping.dsl.TomMapping.FeatureParameter");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop25;
                }
            } while (true);

            otherlv_8=(Token)match(input,33,FOLLOW_2); 

            			newLeafNode(otherlv_8, grammarAccess.getClassOperatorWithExceptionsAccess().getRightParenthesisKeyword_7());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleClassOperatorWithExceptions"


    // $ANTLR start "entryRuleUserOperator"
    // InternalTomMapping.g:1332:1: entryRuleUserOperator returns [EObject current=null] : iv_ruleUserOperator= ruleUserOperator EOF ;
    public final EObject entryRuleUserOperator() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUserOperator = null;


        try {
            // InternalTomMapping.g:1332:53: (iv_ruleUserOperator= ruleUserOperator EOF )
            // InternalTomMapping.g:1333:2: iv_ruleUserOperator= ruleUserOperator EOF
            {
             newCompositeNode(grammarAccess.getUserOperatorRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleUserOperator=ruleUserOperator();

            state._fsp--;

             current =iv_ruleUserOperator; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUserOperator"


    // $ANTLR start "ruleUserOperator"
    // InternalTomMapping.g:1339:1: ruleUserOperator returns [EObject current=null] : (otherlv_0= 'op' ( (lv_name_1_0= ruleSN ) ) otherlv_2= '(' ( (lv_parameters_3_0= ruleParameter ) ) (otherlv_4= ',' ( (lv_parameters_5_0= ruleParameter ) ) )* otherlv_6= ')' otherlv_7= '::' ( (otherlv_8= RULE_ID ) ) otherlv_9= '{' ( (lv_accessors_10_0= ruleAccessor ) ) (otherlv_11= ';' ( (lv_accessors_12_0= ruleAccessor ) ) )* otherlv_13= ';' otherlv_14= 'make' otherlv_15= '=' ( (lv_make_16_0= RULE_STRING ) ) otherlv_17= ';' otherlv_18= 'is_fsym' otherlv_19= '=' ( (lv_test_20_0= RULE_STRING ) ) otherlv_21= ';' otherlv_22= '}' ) ;
    public final EObject ruleUserOperator() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        Token lv_make_16_0=null;
        Token otherlv_17=null;
        Token otherlv_18=null;
        Token otherlv_19=null;
        Token lv_test_20_0=null;
        Token otherlv_21=null;
        Token otherlv_22=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        EObject lv_parameters_3_0 = null;

        EObject lv_parameters_5_0 = null;

        EObject lv_accessors_10_0 = null;

        EObject lv_accessors_12_0 = null;



        	enterRule();

        try {
            // InternalTomMapping.g:1345:2: ( (otherlv_0= 'op' ( (lv_name_1_0= ruleSN ) ) otherlv_2= '(' ( (lv_parameters_3_0= ruleParameter ) ) (otherlv_4= ',' ( (lv_parameters_5_0= ruleParameter ) ) )* otherlv_6= ')' otherlv_7= '::' ( (otherlv_8= RULE_ID ) ) otherlv_9= '{' ( (lv_accessors_10_0= ruleAccessor ) ) (otherlv_11= ';' ( (lv_accessors_12_0= ruleAccessor ) ) )* otherlv_13= ';' otherlv_14= 'make' otherlv_15= '=' ( (lv_make_16_0= RULE_STRING ) ) otherlv_17= ';' otherlv_18= 'is_fsym' otherlv_19= '=' ( (lv_test_20_0= RULE_STRING ) ) otherlv_21= ';' otherlv_22= '}' ) )
            // InternalTomMapping.g:1346:2: (otherlv_0= 'op' ( (lv_name_1_0= ruleSN ) ) otherlv_2= '(' ( (lv_parameters_3_0= ruleParameter ) ) (otherlv_4= ',' ( (lv_parameters_5_0= ruleParameter ) ) )* otherlv_6= ')' otherlv_7= '::' ( (otherlv_8= RULE_ID ) ) otherlv_9= '{' ( (lv_accessors_10_0= ruleAccessor ) ) (otherlv_11= ';' ( (lv_accessors_12_0= ruleAccessor ) ) )* otherlv_13= ';' otherlv_14= 'make' otherlv_15= '=' ( (lv_make_16_0= RULE_STRING ) ) otherlv_17= ';' otherlv_18= 'is_fsym' otherlv_19= '=' ( (lv_test_20_0= RULE_STRING ) ) otherlv_21= ';' otherlv_22= '}' )
            {
            // InternalTomMapping.g:1346:2: (otherlv_0= 'op' ( (lv_name_1_0= ruleSN ) ) otherlv_2= '(' ( (lv_parameters_3_0= ruleParameter ) ) (otherlv_4= ',' ( (lv_parameters_5_0= ruleParameter ) ) )* otherlv_6= ')' otherlv_7= '::' ( (otherlv_8= RULE_ID ) ) otherlv_9= '{' ( (lv_accessors_10_0= ruleAccessor ) ) (otherlv_11= ';' ( (lv_accessors_12_0= ruleAccessor ) ) )* otherlv_13= ';' otherlv_14= 'make' otherlv_15= '=' ( (lv_make_16_0= RULE_STRING ) ) otherlv_17= ';' otherlv_18= 'is_fsym' otherlv_19= '=' ( (lv_test_20_0= RULE_STRING ) ) otherlv_21= ';' otherlv_22= '}' )
            // InternalTomMapping.g:1347:3: otherlv_0= 'op' ( (lv_name_1_0= ruleSN ) ) otherlv_2= '(' ( (lv_parameters_3_0= ruleParameter ) ) (otherlv_4= ',' ( (lv_parameters_5_0= ruleParameter ) ) )* otherlv_6= ')' otherlv_7= '::' ( (otherlv_8= RULE_ID ) ) otherlv_9= '{' ( (lv_accessors_10_0= ruleAccessor ) ) (otherlv_11= ';' ( (lv_accessors_12_0= ruleAccessor ) ) )* otherlv_13= ';' otherlv_14= 'make' otherlv_15= '=' ( (lv_make_16_0= RULE_STRING ) ) otherlv_17= ';' otherlv_18= 'is_fsym' otherlv_19= '=' ( (lv_test_20_0= RULE_STRING ) ) otherlv_21= ';' otherlv_22= '}'
            {
            otherlv_0=(Token)match(input,34,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getUserOperatorAccess().getOpKeyword_0());
            		
            // InternalTomMapping.g:1351:3: ( (lv_name_1_0= ruleSN ) )
            // InternalTomMapping.g:1352:4: (lv_name_1_0= ruleSN )
            {
            // InternalTomMapping.g:1352:4: (lv_name_1_0= ruleSN )
            // InternalTomMapping.g:1353:5: lv_name_1_0= ruleSN
            {

            					newCompositeNode(grammarAccess.getUserOperatorAccess().getNameSNParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_25);
            lv_name_1_0=ruleSN();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getUserOperatorRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"tom.mapping.dsl.TomMapping.SN");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,32,FOLLOW_3); 

            			newLeafNode(otherlv_2, grammarAccess.getUserOperatorAccess().getLeftParenthesisKeyword_2());
            		
            // InternalTomMapping.g:1374:3: ( (lv_parameters_3_0= ruleParameter ) )
            // InternalTomMapping.g:1375:4: (lv_parameters_3_0= ruleParameter )
            {
            // InternalTomMapping.g:1375:4: (lv_parameters_3_0= ruleParameter )
            // InternalTomMapping.g:1376:5: lv_parameters_3_0= ruleParameter
            {

            					newCompositeNode(grammarAccess.getUserOperatorAccess().getParametersParameterParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_26);
            lv_parameters_3_0=ruleParameter();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getUserOperatorRule());
            					}
            					add(
            						current,
            						"parameters",
            						lv_parameters_3_0,
            						"tom.mapping.dsl.TomMapping.Parameter");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTomMapping.g:1393:3: (otherlv_4= ',' ( (lv_parameters_5_0= ruleParameter ) ) )*
            loop26:
            do {
                int alt26=2;
                int LA26_0 = input.LA(1);

                if ( (LA26_0==20) ) {
                    alt26=1;
                }


                switch (alt26) {
            	case 1 :
            	    // InternalTomMapping.g:1394:4: otherlv_4= ',' ( (lv_parameters_5_0= ruleParameter ) )
            	    {
            	    otherlv_4=(Token)match(input,20,FOLLOW_3); 

            	    				newLeafNode(otherlv_4, grammarAccess.getUserOperatorAccess().getCommaKeyword_4_0());
            	    			
            	    // InternalTomMapping.g:1398:4: ( (lv_parameters_5_0= ruleParameter ) )
            	    // InternalTomMapping.g:1399:5: (lv_parameters_5_0= ruleParameter )
            	    {
            	    // InternalTomMapping.g:1399:5: (lv_parameters_5_0= ruleParameter )
            	    // InternalTomMapping.g:1400:6: lv_parameters_5_0= ruleParameter
            	    {

            	    						newCompositeNode(grammarAccess.getUserOperatorAccess().getParametersParameterParserRuleCall_4_1_0());
            	    					
            	    pushFollow(FOLLOW_26);
            	    lv_parameters_5_0=ruleParameter();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getUserOperatorRule());
            	    						}
            	    						add(
            	    							current,
            	    							"parameters",
            	    							lv_parameters_5_0,
            	    							"tom.mapping.dsl.TomMapping.Parameter");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop26;
                }
            } while (true);

            otherlv_6=(Token)match(input,33,FOLLOW_24); 

            			newLeafNode(otherlv_6, grammarAccess.getUserOperatorAccess().getRightParenthesisKeyword_5());
            		
            otherlv_7=(Token)match(input,31,FOLLOW_3); 

            			newLeafNode(otherlv_7, grammarAccess.getUserOperatorAccess().getColonColonKeyword_6());
            		
            // InternalTomMapping.g:1426:3: ( (otherlv_8= RULE_ID ) )
            // InternalTomMapping.g:1427:4: (otherlv_8= RULE_ID )
            {
            // InternalTomMapping.g:1427:4: (otherlv_8= RULE_ID )
            // InternalTomMapping.g:1428:5: otherlv_8= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getUserOperatorRule());
            					}
            				
            otherlv_8=(Token)match(input,RULE_ID,FOLLOW_12); 

            					newLeafNode(otherlv_8, grammarAccess.getUserOperatorAccess().getTypeTerminalCrossReference_7_0());
            				

            }


            }

            otherlv_9=(Token)match(input,18,FOLLOW_28); 

            			newLeafNode(otherlv_9, grammarAccess.getUserOperatorAccess().getLeftCurlyBracketKeyword_8());
            		
            // InternalTomMapping.g:1443:3: ( (lv_accessors_10_0= ruleAccessor ) )
            // InternalTomMapping.g:1444:4: (lv_accessors_10_0= ruleAccessor )
            {
            // InternalTomMapping.g:1444:4: (lv_accessors_10_0= ruleAccessor )
            // InternalTomMapping.g:1445:5: lv_accessors_10_0= ruleAccessor
            {

            					newCompositeNode(grammarAccess.getUserOperatorAccess().getAccessorsAccessorParserRuleCall_9_0());
            				
            pushFollow(FOLLOW_4);
            lv_accessors_10_0=ruleAccessor();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getUserOperatorRule());
            					}
            					add(
            						current,
            						"accessors",
            						lv_accessors_10_0,
            						"tom.mapping.dsl.TomMapping.Accessor");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTomMapping.g:1462:3: (otherlv_11= ';' ( (lv_accessors_12_0= ruleAccessor ) ) )*
            loop27:
            do {
                int alt27=2;
                int LA27_0 = input.LA(1);

                if ( (LA27_0==12) ) {
                    int LA27_1 = input.LA(2);

                    if ( (LA27_1==38) ) {
                        alt27=1;
                    }


                }


                switch (alt27) {
            	case 1 :
            	    // InternalTomMapping.g:1463:4: otherlv_11= ';' ( (lv_accessors_12_0= ruleAccessor ) )
            	    {
            	    otherlv_11=(Token)match(input,12,FOLLOW_28); 

            	    				newLeafNode(otherlv_11, grammarAccess.getUserOperatorAccess().getSemicolonKeyword_10_0());
            	    			
            	    // InternalTomMapping.g:1467:4: ( (lv_accessors_12_0= ruleAccessor ) )
            	    // InternalTomMapping.g:1468:5: (lv_accessors_12_0= ruleAccessor )
            	    {
            	    // InternalTomMapping.g:1468:5: (lv_accessors_12_0= ruleAccessor )
            	    // InternalTomMapping.g:1469:6: lv_accessors_12_0= ruleAccessor
            	    {

            	    						newCompositeNode(grammarAccess.getUserOperatorAccess().getAccessorsAccessorParserRuleCall_10_1_0());
            	    					
            	    pushFollow(FOLLOW_4);
            	    lv_accessors_12_0=ruleAccessor();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getUserOperatorRule());
            	    						}
            	    						add(
            	    							current,
            	    							"accessors",
            	    							lv_accessors_12_0,
            	    							"tom.mapping.dsl.TomMapping.Accessor");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop27;
                }
            } while (true);

            otherlv_13=(Token)match(input,12,FOLLOW_29); 

            			newLeafNode(otherlv_13, grammarAccess.getUserOperatorAccess().getSemicolonKeyword_11());
            		
            otherlv_14=(Token)match(input,35,FOLLOW_30); 

            			newLeafNode(otherlv_14, grammarAccess.getUserOperatorAccess().getMakeKeyword_12());
            		
            otherlv_15=(Token)match(input,36,FOLLOW_6); 

            			newLeafNode(otherlv_15, grammarAccess.getUserOperatorAccess().getEqualsSignKeyword_13());
            		
            // InternalTomMapping.g:1499:3: ( (lv_make_16_0= RULE_STRING ) )
            // InternalTomMapping.g:1500:4: (lv_make_16_0= RULE_STRING )
            {
            // InternalTomMapping.g:1500:4: (lv_make_16_0= RULE_STRING )
            // InternalTomMapping.g:1501:5: lv_make_16_0= RULE_STRING
            {
            lv_make_16_0=(Token)match(input,RULE_STRING,FOLLOW_4); 

            					newLeafNode(lv_make_16_0, grammarAccess.getUserOperatorAccess().getMakeSTRINGTerminalRuleCall_14_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getUserOperatorRule());
            					}
            					setWithLastConsumed(
            						current,
            						"make",
            						lv_make_16_0,
            						"tom.mapping.dsl.TomMapping.STRING");
            				

            }


            }

            otherlv_17=(Token)match(input,12,FOLLOW_31); 

            			newLeafNode(otherlv_17, grammarAccess.getUserOperatorAccess().getSemicolonKeyword_15());
            		
            otherlv_18=(Token)match(input,37,FOLLOW_30); 

            			newLeafNode(otherlv_18, grammarAccess.getUserOperatorAccess().getIs_fsymKeyword_16());
            		
            otherlv_19=(Token)match(input,36,FOLLOW_6); 

            			newLeafNode(otherlv_19, grammarAccess.getUserOperatorAccess().getEqualsSignKeyword_17());
            		
            // InternalTomMapping.g:1529:3: ( (lv_test_20_0= RULE_STRING ) )
            // InternalTomMapping.g:1530:4: (lv_test_20_0= RULE_STRING )
            {
            // InternalTomMapping.g:1530:4: (lv_test_20_0= RULE_STRING )
            // InternalTomMapping.g:1531:5: lv_test_20_0= RULE_STRING
            {
            lv_test_20_0=(Token)match(input,RULE_STRING,FOLLOW_4); 

            					newLeafNode(lv_test_20_0, grammarAccess.getUserOperatorAccess().getTestSTRINGTerminalRuleCall_18_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getUserOperatorRule());
            					}
            					setWithLastConsumed(
            						current,
            						"test",
            						lv_test_20_0,
            						"tom.mapping.dsl.TomMapping.STRING");
            				

            }


            }

            otherlv_21=(Token)match(input,12,FOLLOW_16); 

            			newLeafNode(otherlv_21, grammarAccess.getUserOperatorAccess().getSemicolonKeyword_19());
            		
            otherlv_22=(Token)match(input,21,FOLLOW_2); 

            			newLeafNode(otherlv_22, grammarAccess.getUserOperatorAccess().getRightCurlyBracketKeyword_20());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUserOperator"


    // $ANTLR start "entryRuleParameter"
    // InternalTomMapping.g:1559:1: entryRuleParameter returns [EObject current=null] : iv_ruleParameter= ruleParameter EOF ;
    public final EObject entryRuleParameter() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleParameter = null;


        try {
            // InternalTomMapping.g:1559:50: (iv_ruleParameter= ruleParameter EOF )
            // InternalTomMapping.g:1560:2: iv_ruleParameter= ruleParameter EOF
            {
             newCompositeNode(grammarAccess.getParameterRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleParameter=ruleParameter();

            state._fsp--;

             current =iv_ruleParameter; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleParameter"


    // $ANTLR start "ruleParameter"
    // InternalTomMapping.g:1566:1: ruleParameter returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) ( (lv_name_1_0= ruleSN ) ) ) ;
    public final EObject ruleParameter() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;



        	enterRule();

        try {
            // InternalTomMapping.g:1572:2: ( ( ( (otherlv_0= RULE_ID ) ) ( (lv_name_1_0= ruleSN ) ) ) )
            // InternalTomMapping.g:1573:2: ( ( (otherlv_0= RULE_ID ) ) ( (lv_name_1_0= ruleSN ) ) )
            {
            // InternalTomMapping.g:1573:2: ( ( (otherlv_0= RULE_ID ) ) ( (lv_name_1_0= ruleSN ) ) )
            // InternalTomMapping.g:1574:3: ( (otherlv_0= RULE_ID ) ) ( (lv_name_1_0= ruleSN ) )
            {
            // InternalTomMapping.g:1574:3: ( (otherlv_0= RULE_ID ) )
            // InternalTomMapping.g:1575:4: (otherlv_0= RULE_ID )
            {
            // InternalTomMapping.g:1575:4: (otherlv_0= RULE_ID )
            // InternalTomMapping.g:1576:5: otherlv_0= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getParameterRule());
            					}
            				
            otherlv_0=(Token)match(input,RULE_ID,FOLLOW_3); 

            					newLeafNode(otherlv_0, grammarAccess.getParameterAccess().getTypeTerminalCrossReference_0_0());
            				

            }


            }

            // InternalTomMapping.g:1587:3: ( (lv_name_1_0= ruleSN ) )
            // InternalTomMapping.g:1588:4: (lv_name_1_0= ruleSN )
            {
            // InternalTomMapping.g:1588:4: (lv_name_1_0= ruleSN )
            // InternalTomMapping.g:1589:5: lv_name_1_0= ruleSN
            {

            					newCompositeNode(grammarAccess.getParameterAccess().getNameSNParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_2);
            lv_name_1_0=ruleSN();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getParameterRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"tom.mapping.dsl.TomMapping.SN");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleParameter"


    // $ANTLR start "entryRuleAccessor"
    // InternalTomMapping.g:1610:1: entryRuleAccessor returns [EObject current=null] : iv_ruleAccessor= ruleAccessor EOF ;
    public final EObject entryRuleAccessor() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAccessor = null;


        try {
            // InternalTomMapping.g:1610:49: (iv_ruleAccessor= ruleAccessor EOF )
            // InternalTomMapping.g:1611:2: iv_ruleAccessor= ruleAccessor EOF
            {
             newCompositeNode(grammarAccess.getAccessorRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAccessor=ruleAccessor();

            state._fsp--;

             current =iv_ruleAccessor; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAccessor"


    // $ANTLR start "ruleAccessor"
    // InternalTomMapping.g:1617:1: ruleAccessor returns [EObject current=null] : (otherlv_0= 'slot' ( (otherlv_1= RULE_ID ) ) otherlv_2= '=' ( (lv_java_3_0= RULE_STRING ) ) ) ;
    public final EObject ruleAccessor() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token lv_java_3_0=null;


        	enterRule();

        try {
            // InternalTomMapping.g:1623:2: ( (otherlv_0= 'slot' ( (otherlv_1= RULE_ID ) ) otherlv_2= '=' ( (lv_java_3_0= RULE_STRING ) ) ) )
            // InternalTomMapping.g:1624:2: (otherlv_0= 'slot' ( (otherlv_1= RULE_ID ) ) otherlv_2= '=' ( (lv_java_3_0= RULE_STRING ) ) )
            {
            // InternalTomMapping.g:1624:2: (otherlv_0= 'slot' ( (otherlv_1= RULE_ID ) ) otherlv_2= '=' ( (lv_java_3_0= RULE_STRING ) ) )
            // InternalTomMapping.g:1625:3: otherlv_0= 'slot' ( (otherlv_1= RULE_ID ) ) otherlv_2= '=' ( (lv_java_3_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,38,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getAccessorAccess().getSlotKeyword_0());
            		
            // InternalTomMapping.g:1629:3: ( (otherlv_1= RULE_ID ) )
            // InternalTomMapping.g:1630:4: (otherlv_1= RULE_ID )
            {
            // InternalTomMapping.g:1630:4: (otherlv_1= RULE_ID )
            // InternalTomMapping.g:1631:5: otherlv_1= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAccessorRule());
            					}
            				
            otherlv_1=(Token)match(input,RULE_ID,FOLLOW_30); 

            					newLeafNode(otherlv_1, grammarAccess.getAccessorAccess().getSlotParameterCrossReference_1_0());
            				

            }


            }

            otherlv_2=(Token)match(input,36,FOLLOW_6); 

            			newLeafNode(otherlv_2, grammarAccess.getAccessorAccess().getEqualsSignKeyword_2());
            		
            // InternalTomMapping.g:1646:3: ( (lv_java_3_0= RULE_STRING ) )
            // InternalTomMapping.g:1647:4: (lv_java_3_0= RULE_STRING )
            {
            // InternalTomMapping.g:1647:4: (lv_java_3_0= RULE_STRING )
            // InternalTomMapping.g:1648:5: lv_java_3_0= RULE_STRING
            {
            lv_java_3_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_java_3_0, grammarAccess.getAccessorAccess().getJavaSTRINGTerminalRuleCall_3_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAccessorRule());
            					}
            					setWithLastConsumed(
            						current,
            						"java",
            						lv_java_3_0,
            						"tom.mapping.dsl.TomMapping.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAccessor"


    // $ANTLR start "entryRuleFeatureException"
    // InternalTomMapping.g:1668:1: entryRuleFeatureException returns [EObject current=null] : iv_ruleFeatureException= ruleFeatureException EOF ;
    public final EObject entryRuleFeatureException() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFeatureException = null;


        try {
            // InternalTomMapping.g:1668:57: (iv_ruleFeatureException= ruleFeatureException EOF )
            // InternalTomMapping.g:1669:2: iv_ruleFeatureException= ruleFeatureException EOF
            {
             newCompositeNode(grammarAccess.getFeatureExceptionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFeatureException=ruleFeatureException();

            state._fsp--;

             current =iv_ruleFeatureException; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFeatureException"


    // $ANTLR start "ruleFeatureException"
    // InternalTomMapping.g:1675:1: ruleFeatureException returns [EObject current=null] : (otherlv_0= 'ignore' ( (otherlv_1= RULE_ID ) ) ) ;
    public final EObject ruleFeatureException() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalTomMapping.g:1681:2: ( (otherlv_0= 'ignore' ( (otherlv_1= RULE_ID ) ) ) )
            // InternalTomMapping.g:1682:2: (otherlv_0= 'ignore' ( (otherlv_1= RULE_ID ) ) )
            {
            // InternalTomMapping.g:1682:2: (otherlv_0= 'ignore' ( (otherlv_1= RULE_ID ) ) )
            // InternalTomMapping.g:1683:3: otherlv_0= 'ignore' ( (otherlv_1= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,39,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getFeatureExceptionAccess().getIgnoreKeyword_0());
            		
            // InternalTomMapping.g:1687:3: ( (otherlv_1= RULE_ID ) )
            // InternalTomMapping.g:1688:4: (otherlv_1= RULE_ID )
            {
            // InternalTomMapping.g:1688:4: (otherlv_1= RULE_ID )
            // InternalTomMapping.g:1689:5: otherlv_1= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getFeatureExceptionRule());
            					}
            				
            otherlv_1=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(otherlv_1, grammarAccess.getFeatureExceptionAccess().getFeatureEStructuralFeatureCrossReference_1_0());
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFeatureException"


    // $ANTLR start "entryRuleFeatureParameter"
    // InternalTomMapping.g:1704:1: entryRuleFeatureParameter returns [EObject current=null] : iv_ruleFeatureParameter= ruleFeatureParameter EOF ;
    public final EObject entryRuleFeatureParameter() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFeatureParameter = null;


        try {
            // InternalTomMapping.g:1704:57: (iv_ruleFeatureParameter= ruleFeatureParameter EOF )
            // InternalTomMapping.g:1705:2: iv_ruleFeatureParameter= ruleFeatureParameter EOF
            {
             newCompositeNode(grammarAccess.getFeatureParameterRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFeatureParameter=ruleFeatureParameter();

            state._fsp--;

             current =iv_ruleFeatureParameter; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFeatureParameter"


    // $ANTLR start "ruleFeatureParameter"
    // InternalTomMapping.g:1711:1: ruleFeatureParameter returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) | this_FeatureException_1= ruleFeatureException | this_SettedFeatureParameter_2= ruleSettedFeatureParameter ) ;
    public final EObject ruleFeatureParameter() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject this_FeatureException_1 = null;

        EObject this_SettedFeatureParameter_2 = null;



        	enterRule();

        try {
            // InternalTomMapping.g:1717:2: ( ( ( (otherlv_0= RULE_ID ) ) | this_FeatureException_1= ruleFeatureException | this_SettedFeatureParameter_2= ruleSettedFeatureParameter ) )
            // InternalTomMapping.g:1718:2: ( ( (otherlv_0= RULE_ID ) ) | this_FeatureException_1= ruleFeatureException | this_SettedFeatureParameter_2= ruleSettedFeatureParameter )
            {
            // InternalTomMapping.g:1718:2: ( ( (otherlv_0= RULE_ID ) ) | this_FeatureException_1= ruleFeatureException | this_SettedFeatureParameter_2= ruleSettedFeatureParameter )
            int alt28=3;
            int LA28_0 = input.LA(1);

            if ( (LA28_0==RULE_ID) ) {
                int LA28_1 = input.LA(2);

                if ( (LA28_1==EOF||LA28_1==20||LA28_1==33) ) {
                    alt28=1;
                }
                else if ( (LA28_1==36) ) {
                    alt28=3;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 28, 1, input);

                    throw nvae;
                }
            }
            else if ( (LA28_0==39) ) {
                alt28=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 28, 0, input);

                throw nvae;
            }
            switch (alt28) {
                case 1 :
                    // InternalTomMapping.g:1719:3: ( (otherlv_0= RULE_ID ) )
                    {
                    // InternalTomMapping.g:1719:3: ( (otherlv_0= RULE_ID ) )
                    // InternalTomMapping.g:1720:4: (otherlv_0= RULE_ID )
                    {
                    // InternalTomMapping.g:1720:4: (otherlv_0= RULE_ID )
                    // InternalTomMapping.g:1721:5: otherlv_0= RULE_ID
                    {

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getFeatureParameterRule());
                    					}
                    				
                    otherlv_0=(Token)match(input,RULE_ID,FOLLOW_2); 

                    					newLeafNode(otherlv_0, grammarAccess.getFeatureParameterAccess().getFeatureEStructuralFeatureCrossReference_0_0());
                    				

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalTomMapping.g:1733:3: this_FeatureException_1= ruleFeatureException
                    {

                    			newCompositeNode(grammarAccess.getFeatureParameterAccess().getFeatureExceptionParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_FeatureException_1=ruleFeatureException();

                    state._fsp--;


                    			current = this_FeatureException_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalTomMapping.g:1742:3: this_SettedFeatureParameter_2= ruleSettedFeatureParameter
                    {

                    			newCompositeNode(grammarAccess.getFeatureParameterAccess().getSettedFeatureParameterParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_SettedFeatureParameter_2=ruleSettedFeatureParameter();

                    state._fsp--;


                    			current = this_SettedFeatureParameter_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFeatureParameter"


    // $ANTLR start "entryRuleSettedFeatureParameter"
    // InternalTomMapping.g:1754:1: entryRuleSettedFeatureParameter returns [EObject current=null] : iv_ruleSettedFeatureParameter= ruleSettedFeatureParameter EOF ;
    public final EObject entryRuleSettedFeatureParameter() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSettedFeatureParameter = null;


        try {
            // InternalTomMapping.g:1754:63: (iv_ruleSettedFeatureParameter= ruleSettedFeatureParameter EOF )
            // InternalTomMapping.g:1755:2: iv_ruleSettedFeatureParameter= ruleSettedFeatureParameter EOF
            {
             newCompositeNode(grammarAccess.getSettedFeatureParameterRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSettedFeatureParameter=ruleSettedFeatureParameter();

            state._fsp--;

             current =iv_ruleSettedFeatureParameter; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSettedFeatureParameter"


    // $ANTLR start "ruleSettedFeatureParameter"
    // InternalTomMapping.g:1761:1: ruleSettedFeatureParameter returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_value_2_0= ruleSettedValue ) ) ) ;
    public final EObject ruleSettedFeatureParameter() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        EObject lv_value_2_0 = null;



        	enterRule();

        try {
            // InternalTomMapping.g:1767:2: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_value_2_0= ruleSettedValue ) ) ) )
            // InternalTomMapping.g:1768:2: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_value_2_0= ruleSettedValue ) ) )
            {
            // InternalTomMapping.g:1768:2: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_value_2_0= ruleSettedValue ) ) )
            // InternalTomMapping.g:1769:3: ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_value_2_0= ruleSettedValue ) )
            {
            // InternalTomMapping.g:1769:3: ( (otherlv_0= RULE_ID ) )
            // InternalTomMapping.g:1770:4: (otherlv_0= RULE_ID )
            {
            // InternalTomMapping.g:1770:4: (otherlv_0= RULE_ID )
            // InternalTomMapping.g:1771:5: otherlv_0= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getSettedFeatureParameterRule());
            					}
            				
            otherlv_0=(Token)match(input,RULE_ID,FOLLOW_30); 

            					newLeafNode(otherlv_0, grammarAccess.getSettedFeatureParameterAccess().getFeatureEStructuralFeatureCrossReference_0_0());
            				

            }


            }

            otherlv_1=(Token)match(input,36,FOLLOW_32); 

            			newLeafNode(otherlv_1, grammarAccess.getSettedFeatureParameterAccess().getEqualsSignKeyword_1());
            		
            // InternalTomMapping.g:1786:3: ( (lv_value_2_0= ruleSettedValue ) )
            // InternalTomMapping.g:1787:4: (lv_value_2_0= ruleSettedValue )
            {
            // InternalTomMapping.g:1787:4: (lv_value_2_0= ruleSettedValue )
            // InternalTomMapping.g:1788:5: lv_value_2_0= ruleSettedValue
            {

            					newCompositeNode(grammarAccess.getSettedFeatureParameterAccess().getValueSettedValueParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_value_2_0=ruleSettedValue();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getSettedFeatureParameterRule());
            					}
            					set(
            						current,
            						"value",
            						lv_value_2_0,
            						"tom.mapping.dsl.TomMapping.SettedValue");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSettedFeatureParameter"


    // $ANTLR start "entryRuleSettedValue"
    // InternalTomMapping.g:1809:1: entryRuleSettedValue returns [EObject current=null] : iv_ruleSettedValue= ruleSettedValue EOF ;
    public final EObject entryRuleSettedValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSettedValue = null;


        try {
            // InternalTomMapping.g:1809:52: (iv_ruleSettedValue= ruleSettedValue EOF )
            // InternalTomMapping.g:1810:2: iv_ruleSettedValue= ruleSettedValue EOF
            {
             newCompositeNode(grammarAccess.getSettedValueRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSettedValue=ruleSettedValue();

            state._fsp--;

             current =iv_ruleSettedValue; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSettedValue"


    // $ANTLR start "ruleSettedValue"
    // InternalTomMapping.g:1816:1: ruleSettedValue returns [EObject current=null] : (this_JavaCodeValue_0= ruleJavaCodeValue | this_LiteralValue_1= ruleLiteralValue | this_IntSettedValue_2= ruleIntSettedValue | this_BooleanSettedValue_3= ruleBooleanSettedValue ) ;
    public final EObject ruleSettedValue() throws RecognitionException {
        EObject current = null;

        EObject this_JavaCodeValue_0 = null;

        EObject this_LiteralValue_1 = null;

        EObject this_IntSettedValue_2 = null;

        EObject this_BooleanSettedValue_3 = null;



        	enterRule();

        try {
            // InternalTomMapping.g:1822:2: ( (this_JavaCodeValue_0= ruleJavaCodeValue | this_LiteralValue_1= ruleLiteralValue | this_IntSettedValue_2= ruleIntSettedValue | this_BooleanSettedValue_3= ruleBooleanSettedValue ) )
            // InternalTomMapping.g:1823:2: (this_JavaCodeValue_0= ruleJavaCodeValue | this_LiteralValue_1= ruleLiteralValue | this_IntSettedValue_2= ruleIntSettedValue | this_BooleanSettedValue_3= ruleBooleanSettedValue )
            {
            // InternalTomMapping.g:1823:2: (this_JavaCodeValue_0= ruleJavaCodeValue | this_LiteralValue_1= ruleLiteralValue | this_IntSettedValue_2= ruleIntSettedValue | this_BooleanSettedValue_3= ruleBooleanSettedValue )
            int alt29=4;
            switch ( input.LA(1) ) {
            case RULE_STRING:
                {
                alt29=1;
                }
                break;
            case RULE_ID:
                {
                alt29=2;
                }
                break;
            case RULE_INT:
                {
                alt29=3;
                }
                break;
            case 40:
            case 41:
                {
                alt29=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 29, 0, input);

                throw nvae;
            }

            switch (alt29) {
                case 1 :
                    // InternalTomMapping.g:1824:3: this_JavaCodeValue_0= ruleJavaCodeValue
                    {

                    			newCompositeNode(grammarAccess.getSettedValueAccess().getJavaCodeValueParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_JavaCodeValue_0=ruleJavaCodeValue();

                    state._fsp--;


                    			current = this_JavaCodeValue_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalTomMapping.g:1833:3: this_LiteralValue_1= ruleLiteralValue
                    {

                    			newCompositeNode(grammarAccess.getSettedValueAccess().getLiteralValueParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_LiteralValue_1=ruleLiteralValue();

                    state._fsp--;


                    			current = this_LiteralValue_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalTomMapping.g:1842:3: this_IntSettedValue_2= ruleIntSettedValue
                    {

                    			newCompositeNode(grammarAccess.getSettedValueAccess().getIntSettedValueParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_IntSettedValue_2=ruleIntSettedValue();

                    state._fsp--;


                    			current = this_IntSettedValue_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 4 :
                    // InternalTomMapping.g:1851:3: this_BooleanSettedValue_3= ruleBooleanSettedValue
                    {

                    			newCompositeNode(grammarAccess.getSettedValueAccess().getBooleanSettedValueParserRuleCall_3());
                    		
                    pushFollow(FOLLOW_2);
                    this_BooleanSettedValue_3=ruleBooleanSettedValue();

                    state._fsp--;


                    			current = this_BooleanSettedValue_3;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSettedValue"


    // $ANTLR start "entryRuleIntSettedValue"
    // InternalTomMapping.g:1863:1: entryRuleIntSettedValue returns [EObject current=null] : iv_ruleIntSettedValue= ruleIntSettedValue EOF ;
    public final EObject entryRuleIntSettedValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntSettedValue = null;


        try {
            // InternalTomMapping.g:1863:55: (iv_ruleIntSettedValue= ruleIntSettedValue EOF )
            // InternalTomMapping.g:1864:2: iv_ruleIntSettedValue= ruleIntSettedValue EOF
            {
             newCompositeNode(grammarAccess.getIntSettedValueRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleIntSettedValue=ruleIntSettedValue();

            state._fsp--;

             current =iv_ruleIntSettedValue; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntSettedValue"


    // $ANTLR start "ruleIntSettedValue"
    // InternalTomMapping.g:1870:1: ruleIntSettedValue returns [EObject current=null] : ( (lv_java_0_0= RULE_INT ) ) ;
    public final EObject ruleIntSettedValue() throws RecognitionException {
        EObject current = null;

        Token lv_java_0_0=null;


        	enterRule();

        try {
            // InternalTomMapping.g:1876:2: ( ( (lv_java_0_0= RULE_INT ) ) )
            // InternalTomMapping.g:1877:2: ( (lv_java_0_0= RULE_INT ) )
            {
            // InternalTomMapping.g:1877:2: ( (lv_java_0_0= RULE_INT ) )
            // InternalTomMapping.g:1878:3: (lv_java_0_0= RULE_INT )
            {
            // InternalTomMapping.g:1878:3: (lv_java_0_0= RULE_INT )
            // InternalTomMapping.g:1879:4: lv_java_0_0= RULE_INT
            {
            lv_java_0_0=(Token)match(input,RULE_INT,FOLLOW_2); 

            				newLeafNode(lv_java_0_0, grammarAccess.getIntSettedValueAccess().getJavaINTTerminalRuleCall_0());
            			

            				if (current==null) {
            					current = createModelElement(grammarAccess.getIntSettedValueRule());
            				}
            				setWithLastConsumed(
            					current,
            					"java",
            					lv_java_0_0,
            					"tom.mapping.dsl.TomMapping.INT");
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntSettedValue"


    // $ANTLR start "entryRuleBooleanSettedValue"
    // InternalTomMapping.g:1898:1: entryRuleBooleanSettedValue returns [EObject current=null] : iv_ruleBooleanSettedValue= ruleBooleanSettedValue EOF ;
    public final EObject entryRuleBooleanSettedValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBooleanSettedValue = null;


        try {
            // InternalTomMapping.g:1898:59: (iv_ruleBooleanSettedValue= ruleBooleanSettedValue EOF )
            // InternalTomMapping.g:1899:2: iv_ruleBooleanSettedValue= ruleBooleanSettedValue EOF
            {
             newCompositeNode(grammarAccess.getBooleanSettedValueRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBooleanSettedValue=ruleBooleanSettedValue();

            state._fsp--;

             current =iv_ruleBooleanSettedValue; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBooleanSettedValue"


    // $ANTLR start "ruleBooleanSettedValue"
    // InternalTomMapping.g:1905:1: ruleBooleanSettedValue returns [EObject current=null] : ( (lv_java_0_0= ruleEBoolean ) ) ;
    public final EObject ruleBooleanSettedValue() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_java_0_0 = null;



        	enterRule();

        try {
            // InternalTomMapping.g:1911:2: ( ( (lv_java_0_0= ruleEBoolean ) ) )
            // InternalTomMapping.g:1912:2: ( (lv_java_0_0= ruleEBoolean ) )
            {
            // InternalTomMapping.g:1912:2: ( (lv_java_0_0= ruleEBoolean ) )
            // InternalTomMapping.g:1913:3: (lv_java_0_0= ruleEBoolean )
            {
            // InternalTomMapping.g:1913:3: (lv_java_0_0= ruleEBoolean )
            // InternalTomMapping.g:1914:4: lv_java_0_0= ruleEBoolean
            {

            				newCompositeNode(grammarAccess.getBooleanSettedValueAccess().getJavaEBooleanParserRuleCall_0());
            			
            pushFollow(FOLLOW_2);
            lv_java_0_0=ruleEBoolean();

            state._fsp--;


            				if (current==null) {
            					current = createModelElementForParent(grammarAccess.getBooleanSettedValueRule());
            				}
            				set(
            					current,
            					"java",
            					lv_java_0_0,
            					"tom.mapping.dsl.TomMapping.EBoolean");
            				afterParserOrEnumRuleCall();
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooleanSettedValue"


    // $ANTLR start "entryRuleJavaCodeValue"
    // InternalTomMapping.g:1934:1: entryRuleJavaCodeValue returns [EObject current=null] : iv_ruleJavaCodeValue= ruleJavaCodeValue EOF ;
    public final EObject entryRuleJavaCodeValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleJavaCodeValue = null;


        try {
            // InternalTomMapping.g:1934:54: (iv_ruleJavaCodeValue= ruleJavaCodeValue EOF )
            // InternalTomMapping.g:1935:2: iv_ruleJavaCodeValue= ruleJavaCodeValue EOF
            {
             newCompositeNode(grammarAccess.getJavaCodeValueRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleJavaCodeValue=ruleJavaCodeValue();

            state._fsp--;

             current =iv_ruleJavaCodeValue; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleJavaCodeValue"


    // $ANTLR start "ruleJavaCodeValue"
    // InternalTomMapping.g:1941:1: ruleJavaCodeValue returns [EObject current=null] : ( (lv_java_0_0= RULE_STRING ) ) ;
    public final EObject ruleJavaCodeValue() throws RecognitionException {
        EObject current = null;

        Token lv_java_0_0=null;


        	enterRule();

        try {
            // InternalTomMapping.g:1947:2: ( ( (lv_java_0_0= RULE_STRING ) ) )
            // InternalTomMapping.g:1948:2: ( (lv_java_0_0= RULE_STRING ) )
            {
            // InternalTomMapping.g:1948:2: ( (lv_java_0_0= RULE_STRING ) )
            // InternalTomMapping.g:1949:3: (lv_java_0_0= RULE_STRING )
            {
            // InternalTomMapping.g:1949:3: (lv_java_0_0= RULE_STRING )
            // InternalTomMapping.g:1950:4: lv_java_0_0= RULE_STRING
            {
            lv_java_0_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            				newLeafNode(lv_java_0_0, grammarAccess.getJavaCodeValueAccess().getJavaSTRINGTerminalRuleCall_0());
            			

            				if (current==null) {
            					current = createModelElement(grammarAccess.getJavaCodeValueRule());
            				}
            				setWithLastConsumed(
            					current,
            					"java",
            					lv_java_0_0,
            					"tom.mapping.dsl.TomMapping.STRING");
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleJavaCodeValue"


    // $ANTLR start "entryRuleLiteralValue"
    // InternalTomMapping.g:1969:1: entryRuleLiteralValue returns [EObject current=null] : iv_ruleLiteralValue= ruleLiteralValue EOF ;
    public final EObject entryRuleLiteralValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLiteralValue = null;


        try {
            // InternalTomMapping.g:1969:53: (iv_ruleLiteralValue= ruleLiteralValue EOF )
            // InternalTomMapping.g:1970:2: iv_ruleLiteralValue= ruleLiteralValue EOF
            {
             newCompositeNode(grammarAccess.getLiteralValueRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLiteralValue=ruleLiteralValue();

            state._fsp--;

             current =iv_ruleLiteralValue; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLiteralValue"


    // $ANTLR start "ruleLiteralValue"
    // InternalTomMapping.g:1976:1: ruleLiteralValue returns [EObject current=null] : ( ( ruleFQN ) ) ;
    public final EObject ruleLiteralValue() throws RecognitionException {
        EObject current = null;


        	enterRule();

        try {
            // InternalTomMapping.g:1982:2: ( ( ( ruleFQN ) ) )
            // InternalTomMapping.g:1983:2: ( ( ruleFQN ) )
            {
            // InternalTomMapping.g:1983:2: ( ( ruleFQN ) )
            // InternalTomMapping.g:1984:3: ( ruleFQN )
            {
            // InternalTomMapping.g:1984:3: ( ruleFQN )
            // InternalTomMapping.g:1985:4: ruleFQN
            {

            				if (current==null) {
            					current = createModelElement(grammarAccess.getLiteralValueRule());
            				}
            			

            				newCompositeNode(grammarAccess.getLiteralValueAccess().getLiteralEEnumLiteralCrossReference_0());
            			
            pushFollow(FOLLOW_2);
            ruleFQN();

            state._fsp--;


            				afterParserOrEnumRuleCall();
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLiteralValue"


    // $ANTLR start "entryRuleEBoolean"
    // InternalTomMapping.g:2002:1: entryRuleEBoolean returns [String current=null] : iv_ruleEBoolean= ruleEBoolean EOF ;
    public final String entryRuleEBoolean() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEBoolean = null;


        try {
            // InternalTomMapping.g:2002:48: (iv_ruleEBoolean= ruleEBoolean EOF )
            // InternalTomMapping.g:2003:2: iv_ruleEBoolean= ruleEBoolean EOF
            {
             newCompositeNode(grammarAccess.getEBooleanRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEBoolean=ruleEBoolean();

            state._fsp--;

             current =iv_ruleEBoolean.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEBoolean"


    // $ANTLR start "ruleEBoolean"
    // InternalTomMapping.g:2009:1: ruleEBoolean returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'true' | kw= 'false' ) ;
    public final AntlrDatatypeRuleToken ruleEBoolean() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalTomMapping.g:2015:2: ( (kw= 'true' | kw= 'false' ) )
            // InternalTomMapping.g:2016:2: (kw= 'true' | kw= 'false' )
            {
            // InternalTomMapping.g:2016:2: (kw= 'true' | kw= 'false' )
            int alt30=2;
            int LA30_0 = input.LA(1);

            if ( (LA30_0==40) ) {
                alt30=1;
            }
            else if ( (LA30_0==41) ) {
                alt30=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 30, 0, input);

                throw nvae;
            }
            switch (alt30) {
                case 1 :
                    // InternalTomMapping.g:2017:3: kw= 'true'
                    {
                    kw=(Token)match(input,40,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getEBooleanAccess().getTrueKeyword_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalTomMapping.g:2023:3: kw= 'false'
                    {
                    kw=(Token)match(input,41,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getEBooleanAccess().getFalseKeyword_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEBoolean"


    // $ANTLR start "entryRuleSN"
    // InternalTomMapping.g:2032:1: entryRuleSN returns [String current=null] : iv_ruleSN= ruleSN EOF ;
    public final String entryRuleSN() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleSN = null;


        try {
            // InternalTomMapping.g:2032:42: (iv_ruleSN= ruleSN EOF )
            // InternalTomMapping.g:2033:2: iv_ruleSN= ruleSN EOF
            {
             newCompositeNode(grammarAccess.getSNRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSN=ruleSN();

            state._fsp--;

             current =iv_ruleSN.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSN"


    // $ANTLR start "ruleSN"
    // InternalTomMapping.g:2039:1: ruleSN returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : this_ID_0= RULE_ID ;
    public final AntlrDatatypeRuleToken ruleSN() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;


        	enterRule();

        try {
            // InternalTomMapping.g:2045:2: (this_ID_0= RULE_ID )
            // InternalTomMapping.g:2046:2: this_ID_0= RULE_ID
            {
            this_ID_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            		current.merge(this_ID_0);
            	

            		newLeafNode(this_ID_0, grammarAccess.getSNAccess().getIDTerminalRuleCall());
            	

            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSN"


    // $ANTLR start "entryRuleFQN"
    // InternalTomMapping.g:2056:1: entryRuleFQN returns [String current=null] : iv_ruleFQN= ruleFQN EOF ;
    public final String entryRuleFQN() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleFQN = null;


        try {
            // InternalTomMapping.g:2056:43: (iv_ruleFQN= ruleFQN EOF )
            // InternalTomMapping.g:2057:2: iv_ruleFQN= ruleFQN EOF
            {
             newCompositeNode(grammarAccess.getFQNRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFQN=ruleFQN();

            state._fsp--;

             current =iv_ruleFQN.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFQN"


    // $ANTLR start "ruleFQN"
    // InternalTomMapping.g:2063:1: ruleFQN returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) ;
    public final AntlrDatatypeRuleToken ruleFQN() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        Token kw=null;
        Token this_ID_2=null;


        	enterRule();

        try {
            // InternalTomMapping.g:2069:2: ( (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) )
            // InternalTomMapping.g:2070:2: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            {
            // InternalTomMapping.g:2070:2: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            // InternalTomMapping.g:2071:3: this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )*
            {
            this_ID_0=(Token)match(input,RULE_ID,FOLLOW_33); 

            			current.merge(this_ID_0);
            		

            			newLeafNode(this_ID_0, grammarAccess.getFQNAccess().getIDTerminalRuleCall_0());
            		
            // InternalTomMapping.g:2078:3: (kw= '.' this_ID_2= RULE_ID )*
            loop31:
            do {
                int alt31=2;
                int LA31_0 = input.LA(1);

                if ( (LA31_0==42) ) {
                    alt31=1;
                }


                switch (alt31) {
            	case 1 :
            	    // InternalTomMapping.g:2079:4: kw= '.' this_ID_2= RULE_ID
            	    {
            	    kw=(Token)match(input,42,FOLLOW_3); 

            	    				current.merge(kw);
            	    				newLeafNode(kw, grammarAccess.getFQNAccess().getFullStopKeyword_1_0());
            	    			
            	    this_ID_2=(Token)match(input,RULE_ID,FOLLOW_33); 

            	    				current.merge(this_ID_2);
            	    			

            	    				newLeafNode(this_ID_2, grammarAccess.getFQNAccess().getIDTerminalRuleCall_1_1());
            	    			

            	    }
            	    break;

            	default :
            	    break loop31;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFQN"

    // Delegated rules


    protected DFA20 dfa20 = new DFA20(this);
    static final String dfa_1s = "\13\uffff";
    static final String dfa_2s = "\6\uffff\1\11\3\uffff\1\11";
    static final String dfa_3s = "\1\36\1\5\1\uffff\1\37\1\5\1\uffff\1\14\1\5\2\uffff\1\14";
    static final String dfa_4s = "\1\42\1\5\1\uffff\1\40\1\5\1\uffff\1\52\1\5\2\uffff\1\52";
    static final String dfa_5s = "\2\uffff\1\4\2\uffff\1\3\2\uffff\1\2\1\1\1\uffff";
    static final String dfa_6s = "\13\uffff}>";
    static final String[] dfa_7s = {
            "\1\2\3\uffff\1\1",
            "\1\3",
            "",
            "\1\4\1\5",
            "\1\6",
            "",
            "\1\11\23\uffff\1\10\11\uffff\1\7",
            "\1\12",
            "",
            "",
            "\1\11\23\uffff\1\10\11\uffff\1\7"
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final short[] dfa_2 = DFA.unpackEncodedString(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final char[] dfa_4 = DFA.unpackEncodedStringToUnsignedChars(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[] dfa_6 = DFA.unpackEncodedString(dfa_6s);
    static final short[][] dfa_7 = unpackEncodedStringArray(dfa_7s);

    class DFA20 extends DFA {

        public DFA20(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 20;
            this.eot = dfa_1;
            this.eof = dfa_2;
            this.min = dfa_3;
            this.max = dfa_4;
            this.accept = dfa_5;
            this.special = dfa_6;
            this.transition = dfa_7;
        }
        public String getDescription() {
            return "732:2: (this_ClassOperator_0= ruleClassOperator | this_ClassOperatorWithExceptions_1= ruleClassOperatorWithExceptions | this_UserOperator_2= ruleUserOperator | this_AliasOperator_3= ruleAliasOperator )";
        }
    }
 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x000000000D83E002L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x000000000D83C002L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x000000000D838002L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x000000000D830002L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x000000000D820002L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000005820002L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000680020L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000300000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000600000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000004800002L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000440000000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000002204000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000002200000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000000A00000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000020000002L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000200100000L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000008000000020L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000000800000000L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000001000000000L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000002000000000L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000030000000070L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000040000000002L});

}